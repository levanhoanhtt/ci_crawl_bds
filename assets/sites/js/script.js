$('.slide-bds').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
$('.slide-bds2').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        },

    }
})
$(document).ready(function(){
    
    /*
    $('.number-plus').click(function(event) {
       var val = $(this).parents('.number-1').find('.input-number').val();
       $(this).parents('.number-1').find('.input-number').val(parseInt(val)+1);
       $(this).parents('.number-1').find('.input-number').trigger('change');

    });

    $('.number-minus').click(function(event) {
        var val = $(this).parents('.number-1').find('.input-number').val();
        if ( val > 1) {
            $(this).parents('.number-1').find('.input-number').val(parseInt(val)-1);
        }
        else {
            $(this).parents('.number-1').find('.input-number').val(1);
        }
      
    });
    */

$("#mytable #checkall").click(function () {
        if ($("#mytable #checkall").is(':checked')) {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    
    $("[data-toggle=tooltip]").tooltip();
});

//Menu
jQuery(document).ready(function () {

    jQuery('.menu-box .main-menu>li.menu-item-has-children>a').append('<i class="fa fa-angle-down"></i>');
    jQuery('.menu-box .main-menu .sub-menu>li.menu-item-has-children>a').append('<i class="fa fa-angle-right"></i>');

    jQuery('.menu-site .btn-show-menu').click(function () {

        jQuery(this).parents('.menu-site').find('.menu-box').css('width','100%');
    });
    jQuery('.menu-box .btn-hide-menu, .menu-box .bg-menu').click(function () {
        jQuery(this).parents('.menu-box ').css('width','0');
    });

    if($(window).width()<992){
        jQuery('.main-menu li.menu-item-has-children>a>i').click(function (e) {
            e.preventDefault();
            jQuery(this).parent().parent().children('.sub-menu').slideToggle('fast');
        });
        // jQuery('.jumbotron form').hide();
        
        // jQuery('.buido-fix .fa-chevron-down').click(function () {
        //     jQuery('.buido-fix .fa-chevron-down').toggleClass('gui');
        //    jQuery('.jumbotron form').slideToggle();
        // });
        
        
    }
    

    
    jQuery('.treeview-item>a').on("click",function(e){
            e.preventDefault();
            jQuery(this).parent().children('.sub-treeview').slideToggle('fast');
        });

    var current_page = jQuery('.number-1 .currentpage').text();

    if (current_page!='')
    {
        jQuery('.currentpage').html('<input type="text" class="input-number" value="'+current_page+'" min="1" readonly>');
    }
    else
    {
        jQuery('.number-1').html('<input type="text" class="input-number" value="1" min="1" readonly>');
    }

    var prev_object = jQuery('.number-1').find('a[rel="prev"]');
    if (prev_object.length==0)
    {
        jQuery('.number-1').prepend('<span class="number-minus"><i class="fa fa-caret-left" aria-hidden="true"></i></span>');
    }

    var next_object = jQuery('.number-1').find('a[rel="next"]');
    if (next_object.length==0)
    {
        jQuery('.number-1').append('<span class="number-minus"><i class="fa fa-caret-right" aria-hidden="true"></i></span>');
    }

});

$(document).ready(function () {
    // var url = window.location.href;
    $('.click-change').change(function(){
        var val = $(this).val();      
        window.location.href = val;
    });

    var img = document.getElementsByTagName('img');
    var width = img.clientWidth;
    var height = img.clientHeight;
    if (width<height)
    {
        img.css('transform', 'rotate(90deg)');
    }

    jQuery('img').each(function(){
        var width = this.width;
        var height = this.height;
        console.log(width);
        if (width<height)
        {
            //jQuery(this).css('transform', 'rotate(90deg)');
        }
    });

});

$(document).ready(function () {
    $('.owl-nav span').html('');
});

