/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

 CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
	{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
	{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
	{ name: 'links' },
	{ name: 'insert' },
	{ name: 'forms' },
	{ name: 'tools' },
	{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
	{ name: 'others' },
	'/',
	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
	{ name: 'styles' },
	{ name: 'colors' },
	{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

   	config.extraPlugins = ['justify','font','entities'];

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.filebrowserBrowseUrl = '../../assets/ckfinder/ckfinder.html';
	
	config.filebrowserImageBrowseUrl = '../../assets/ckfinder/ckfinder.html?type=Images';
	
	config.filebrowserFlashBrowseUrl = '../../assets/ckfinder/ckfinder.html?type=Flash';
	
	config.filebrowserUploadUrl = '../../assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	
	config.filebrowserImageUploadUrl = '../../assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	
	config.filebrowserFlashUploadUrl = '../../assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

	config.extraPlugins = 'justify';

     config.basicEntities = false;

     config.fillEmptyBlocks = false;
     config.entities = false;
     config.entities_greek = false;
     config.entities_latin = false;
     config.htmlEncodeOutput = false;

};
