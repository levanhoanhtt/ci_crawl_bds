
jQuery(document).ready(function($) {
    jQuery(".num-only").keydown(function (e) {
        // Allow: backspace, delete, tab
        if ($.inArray(e.keyCode, [46, 8, 9]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+V
            (e.keyCode == 86 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    // Get latlng by input 
    jQuery("#address").on("change",function(){
        var address = jQuery(this).val();
        address = address.replace(" ", "+");
        var geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='+address+'&&key=AIzaSyAT_j6hTG_ca5pWwFViCMv1I-HfjOwCNCc');
        var output = JSON.parse(geocode);
        jQuery("#lat").val(output.results[0].geometry.location.lat);
        jQuery("#lng").val(output.results[0].geometry.location.lng);
    });

    // Datetime-picker
    // $(function () {
    //     $('.datepicker').datepicker({format: "dd-mm-yyyy"});
    // });
});

// Ckfinder 
jQuery(document).ready(function(){
    jQuery('body').on("click",".choose_image_product",function(){
        var formfinder = jQuery(this).parents('.form-group');
        CKFinder.popup( {
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    var file = evt.data.files.first();
                    formfinder.find('.finder-img').val(file.getUrl());
                    formfinder.find(".imgProduct").show();
                    formfinder.find('.imgProduct').attr('src', file.getUrl());
                } );
                finder.on( 'file:choose:resizedImage', function( evt ) {
                    var output = document.getElementById( elementId );
                    formfinder.find('.finder-img').val(evt.data.resizedUrl);
                    formfinder.find(".imgProduct").show();
                    formfinder.find('.imgProduct').attr('src', evt.data.resizedUrl);
                } );
            }
        } );  
    });
});

// Tạo slug
jQuery(document).ready(function(){
    function ChangeToSlug(str)
    {
        var slug;
        //Đổi chữ hoa thành chữ thường
        slug = str.toLowerCase();
        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        slug = slug.replace(/\&/gi, 'va');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "-");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');

        return slug;
    }

    jQuery(".to-slug").on("change",function(){
        var old_slug = jQuery(".main-slug").val();
        if(typeof old_slug === "undefined" || old_slug == ''){
            var slug = ChangeToSlug(jQuery(this).val());
            jQuery(".main-slug").val(slug);
        }
    });
});


jQuery(document).ready(function() {
  $('.title-cover').click(function(event) {
        /* Act on the event */
        $('.modal-content').addClass('themthongtin');
        $('.nen').addClass('them-nen');
    });
    
});