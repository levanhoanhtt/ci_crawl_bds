jQuery(document).ready(function($) {
	var html_partner = `
	        <div class="form-group item-partner">
                <div class="col-xs-12 col-sm-3"></div>
                <div class="col-xs-12 col-sm-8">
                    <div class="row">
                        <div class="col-xs-12 col-sm-7 col-md-8">
                            <div class="input-box">
                                <input id="thumbnail" type="text" name="thumbnail[]" class="form-control finder-img" value="" readonly placeholder="Đường dẫn ảnh">
                                <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                            </div><br />
                            <input type="text" class="form-control" placeholder="Liên kết" name="link[]" />
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-4">
                            <div class="box-img text-right">
                                <img src="" class="imgProduct img-thumbnail"/>
                                <button type="button" class="remove-partner btn btn-danger"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    `;
	jQuery(".more-partner").click(function () {
        jQuery(this).parents(".panel-body").find(".list-partner-box").append(html_partner);
    });
	jQuery("body").on("click",".remove-partner",function () {
        if(confirm("Xóa slide này?")){
            jQuery(this).parents(".item-partner").remove();
        }
    });

    var html_post_image = `
	        <div class="form-group item-image">
                
                <div class="col-xs-12 col-sm-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-7 col-md-9">
                            <div class="input-box">
                                <input id="thumbnail" type="text" name="images[]" class="form-control finder-img" value="" readonly placeholder="Đường dẫn ảnh">
                                <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                            </div>
                          
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-3">
                            <div class="box-img text-right">
                                <img src="" class="imgProduct img-thumbnail"/>
                                <button type="button" class="remove-image btn btn-danger"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    `;
    jQuery(".more-image").click(function () {
        jQuery(this).parents(".panel-body").find(".list-image-box").append(html_post_image);
    });
    jQuery("body").on("click",".remove-image",function () {
        if(confirm("Xóa hình ảnh này?")){
            jQuery(this).parents(".item-image").remove();
        }
    });

    var html_feedback = `
	        <div class="form-group item-partner">
                <div class="col-xs-12 col-sm-3"></div>
                <div class="col-xs-12 col-sm-8">
                    <div class="row">
                        <div class="col-xs-12 col-sm-7 col-md-8">
                            <div class="input-box">
                                <input id="thumbnail" type="text" name="thumbnail[]" class="form-control finder-img" value="" readonly placeholder="Đường dẫn ảnh">
                                <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                            </div><br />
                            <input type="text" class="form-control" placeholder="Tên khách hàng" name="name[]" /><br/>
                            <textarea class="form-control" placeholder="Nội dung..." name="content[]"></textarea>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-4">
                            <div class="box-img text-right">
                                <img src="" class="imgProduct img-thumbnail"/>
                                <button type="button" class="remove-feedback btn btn-danger"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    `;
    jQuery(".more-feedback").click(function () {
        jQuery(this).parents(".panel-body").find(".list-feedback-box").append(html_feedback);
    });
    jQuery("body").on("click",".remove-feedback",function () {
        if(confirm("Xóa cảm nhận này?")){
            jQuery(this).parents(".item-partner").remove();
        }
    });

    // jQuery(".select-icon").on("change",function() {
    //     var src_img = jQuery(this).val();
    //     if(src>0)
    // });

    jQuery('.change_ftp i').click(function(){
        if(confirm("Bạn muốn thay đổi trạng thái bài viết ?"))
        {
            var cor = jQuery(this);
            var stt = jQuery(this).attr('value');
            var id = jQuery(this).data('id');
            

            jQuery.ajax({
                url: home+'admin/post/feature/'+id+'/'+stt,
                type: 'post',
                dataType: 'html',
                data: {
                },
                beforeSend: function(){

                },
                success: function(res){
                    if(res == 1)
                    {
                        cor.toggleClass('checked');
                        if(cor.hasClass('checked'))
                        {
                            cor.attr('value','YES');
                        }
                        else {
                            cor.attr('value','NO');
                        }
                        
                    }

                    if(res == -1) alert("Bài viết không tồn tại");
                    if(res == 0) alert("Có lỗi xảy ra. Vui lòng thử lại.");
                },
                error: function(){
                }
        });
        }
        
    });


    jQuery('.feature_dn i').click(function(){
        if(confirm("Bạn muốn thay đổi trạng thái doanh nghiệp ?"))
        {
            var cor = jQuery(this);
            var stt = jQuery(this).attr('value');
            var id = jQuery(this).data('id');
            

            jQuery.ajax({
                url: home+'admin/business/feature_dn/'+id+'/'+stt,
                type: 'post',
                dataType: 'html',
                data: {
                    
                },
                beforeSend: function(){

                },
                success: function(res){
                    if(res == 1)
                    {
                        cor.toggleClass('checked');
                        if(cor.hasClass('checked'))
                        {
                            cor.attr('value','YES');
                        }
                        else {
                            cor.attr('value','NO');
                        }
                        
                    }

                    if(res == -1) alert("Doanh nghiệp không tồn tại");
                    if(res == 0) alert("Có lỗi xảy ra. Vui lòng thử lại.");
                },
                error: function(){
                }
        });
        }
        
    });




    (function( $ ) {
        'use strict';
        var datatableInit = function() {
            $('#my-datatable').dataTable({
                "order": [[ 0, "desc" ]]
            });
        };

        $(function() {
            datatableInit();
        });
    }).apply( this, [ jQuery ]);
});

jQuery(document).ready(function(){
    jQuery('img').each(function(){
        var width = this.width;
        var height = this.height;
        console.log(width);
        if (width<height)
        {
            //jQuery(this).css('transform', 'rotate(90deg)');
        }
    });
});