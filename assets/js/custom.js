//Menu
jQuery(document).ready(function () {
   
    jQuery('.menu-box .main-menu>li.menu-item-has-children>a').append('<i class="fa fa-angle-down"></i>');
    jQuery('.menu-box .main-menu .sub-menu>li.menu-item-has-children>a').append('<i class="fa fa-angle-right"></i>');

    jQuery('.menu-site .btn-show-menu').click(function () {
        jQuery(this).parents('.menu-site').find('.menu-box').css('width','100%');
    });
    jQuery('.menu-box .btn-hide-menu, .menu-box .bg-menu').click(function () {
        jQuery(this).parents('.menu-box ').css('width','0');
    });

    if($(window).width()<992){
        jQuery('.main-menu li.menu-item-has-children>a>i').click(function (e) {
            e.preventDefault();
            jQuery(this).parent().parent().children('.sub-menu').slideToggle('fast');
        });
    }

    jQuery(".has-subbox>a").click(function(){
        jQuery(this).parents(".has-subbox").find(".subbox").slideToggle("fast");
    });
});

jQuery(document).ready(function($) {
    
   

    jQuery(".slide-ads").owlCarousel({
        items:1,
        loop:true,
        dots:true,
        nav:false,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true
    });
    jQuery('.partner-slide').owlCarousel({
        loop:true,
        margin:20,
        nav:true,
        dots:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:2                
            },
            600:{
                items:3
            },
            992:{
                items:5
            },
            1200:{
                items:6
            }
        },
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true
    });

    jQuery('.slide-blog').owlCarousel({
        loop:false,
        margin:30,
        nav:true,
        dots:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:false,
                dots:true            
            },
            600:{
                items:2
            },
            992:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });

    jQuery('.list-business-provide').owlCarousel({
        loop:false,
        margin:20,
        nav:true,
        dots:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:2                
            },
            768:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });

    jQuery(".owl-prev").html('<i class="fa fa-angle-left"></i>');
    jQuery(".owl-next").html('<i class="fa fa-angle-right"></i>');

    $('.slide-sb-career').lightSlider({
        item:5,
        vertical:true,
        verticalHeight:320,
        pager:false,
        auto:true,
        loop:true,
        slideMargin:5
    });  
    $('.dvtt .lSAction>.lSPrev').prepend('<i class="fa fa-caret-up" aria-hidden="true"></i>');
    $('.dvtt .lSAction>.lSNext').prepend('<i class="fa fa-caret-down" aria-hidden="true"></i>');
});

jQuery(document).ready(function($) {
    jQuery("#qn_map area").click(function(e){
        e.preventDefault();
    });
    jQuery("#qn_map area").hover(
        function(){
            var c_class = jQuery(this).data("hover");
            jQuery(this).parents(".map-by-area").find(".content-onhover-map .item-content").hide();
            jQuery(this).parents(".map-by-area").find("."+c_class).show();
        },
        function(){

        }
    );
    jQuery("#map_holder").ready(function($) {
        jQuery("#map_holder").css("display","none");
    });
    
    jQuery("body").on("click","#map_outer path,#map_zoom path", function(){
        jQuery(this).parents(".tab-pane").find(".item-info-province").hide();
        jQuery(this).parents(".tab-pane").find(".item-info-province.item-default").show();
    });

    jQuery("body").on("click","#map_inner path", function(){
        var class_path = "item-default";
        jQuery(this).parent().find("path").each(function(){
            var fill = jQuery(this).attr("fill-opacity");
            if(fill == 1){
                class_path = jQuery(this).attr("class");
                return false;
            }
        });
        jQuery(this).parents(".tab-pane").find(".item-info-province").hide();
        jQuery(this).parents(".tab-pane").find(".item-info-province."+class_path).show();
    });

    jQuery(".menu-site").scrollToFixed();

    // // Scroll to fixed sidebar
    // jQuery(".sidebar").scrollToFixed({
    //     marginTop: $('.menu-site').outerHeight(true) + 10,
    //     limit: function() {
    //         return $('.end-scroll-sidebar').offset().top - $(this).outerHeight(true) - 10;
    //     },
    //     zIndex: 99
    // });

    //Select2
    jQuery(".my-select2").select2();

    // Click open modal 
    jQuery(".open-modal").click(function(e){
        e.preventDefault();
        var id_modal = jQuery(this).data("modal");
        jQuery(id_modal).show();
    });
    jQuery(".modal-basic .bg-modal,.modal-basic .close-md, .modal-basic .md-close").click(function(){
        jQuery(this).parents(".modal-basic").hide();
    });

    //Searchtab 
    jQuery(".search-tab .click-tab").click(function(e){
        e.preventDefault();
        var data_tab = jQuery(this).data("tab");
        jQuery(this).parents(".search-box").find(".tab-search").removeClass("active");
        jQuery(this).parents(".search-box").find(data_tab).addClass("active");
        jQuery(this).parents(".search-tab").find("li").removeClass("active");
        jQuery(this).parent().addClass("active");
    });


    //Back to top
    jQuery('body').append('<div id="toTop" class="btn"><i class="fa fa-angle-up"></i></div>');
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() >= 300) {
            jQuery('#toTop').fadeIn();
        } else {
            jQuery('#toTop').fadeOut();
        }
    }); 

    jQuery('#toTop').click(function(){
        jQuery("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    // Đánh giá
    jQuery(".show-rate-form").click(function(e){
        e.preventDefault();
        jQuery(this).hide();
        jQuery(this).parents("#rate-now").find(".rate-form").slideDown();
    });
    jQuery(".num-rate>i").click(function(){
        var this_index = jQuery(this).index();
        jQuery(this).parent().find("i").each(function(){
            if(jQuery(this).index()<=this_index){
                jQuery(this).removeClass("fa-star-o").addClass("fa-star");
            }else{
                jQuery(this).addClass("fa-star-o").removeClass("fa-star");
            }
        });
        jQuery(this).parents(".item-form").find(".star-num").val(this_index+1);
    });


    var width = $('.hl-profile-img').width();
    var h = width + 'px';
    $('.hl-profile-img>img').css('height', h);
});

jQuery(document).ready(function(){
    var type_ = jQuery(".msg-alert").data("type");
    var content_ = jQuery(".msg-alert").data("content");
    if(typeof type_ !== 'undefined' && type_.length>0 && typeof content_ !== 'undefined' && content_.length>0){
        new PNotify({
            text: content_,
            type: type_ 
        });
    }
});



jQuery(document).ready(function() {
    jQuery('.title-cover').click(function(event) {
        /* Act on the event */
        jQuery('.modal-content').addClass('modal-content-them')
    });
});