<?php require_once APPPATH.'/views/member/header.php'; ?>

<div class="top-page">
    <div class="container">
        <ul class="bread-crumb">
            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li><?php echo $current_page['page_title']; ?></li>
        </ul>
    </div>
</div>

<div class="contact-page basic-div">
	<div class="container">
		<h2 class="cate-title"><?php echo $current_page['page_title']; ?></h2>
		<div class="row">
			<div class="item-col col-xs-12 col-sm-6">
				<div class="contact-info">
					<?php echo $current_page['page_content']; ?>
				</div>
				<form action="">
					<div class="row">
						<div class="item-col col-xs-12">
							<input type="text" class="form-control" placeholder="Họ và tên" />
						</div>
						<div class="item-col col-xs-12 col-sm-6">
							<input type="text" class="form-control" placeholder="Số điện thoại" />
						</div>
						<div class="item-col col-xs-12 col-sm-6">
							<input type="text" class="form-control" placeholder="Địa chỉ email" />
						</div>
						<div class="item-col col-xs-12">
							<input type="text" class="form-control" placeholder="Tiêu đề" />
						</div>
						<div class="item-col col-xs-12">
							<textarea name="" id="" class="form-control" rows="5" placeholder="Nội dung"></textarea>
						</div>
					</div>
					<button class="btn" type="submit">Gửi đi</button>
				</form>
			</div>
			<div class="item-col col-xs-12 col-sm-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.7697841146!2d105.81902606440735!3d21.04189558599131!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab0ed9c7b4b1%3A0x5b46966e704b5fa!2zVmnhu4duIE5naGnDqm4gY-G7qXUgRGEgLSBHaeG6p3kgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1541738188289" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

<?php require_once APPPATH.'/views/member/footer.php'; ?>