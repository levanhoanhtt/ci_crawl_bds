<?php require_once APPPATH.'/views/member/header.php'; ?>

<div class="contact-page basic-div">
	<div class="container">
		<h2 class="cate-title"><?php echo $current_page['page_title']; ?></h2>
        <hr>
		<?php echo $current_page['page_content']; ?>
	</div>
</div>
<br/>
<?php require_once APPPATH.'/views/member/footer.php'; ?>