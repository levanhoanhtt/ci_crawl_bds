<?php
$sidebar_info = $this->get_data->options_info("sidebar");
$feedback_list = $this->get_data->options_info("feedback");
?>
<?php echo validation_errors(); ?>

    
<div class="box">
    <?php
    if (!empty($login)):
    ?>
    <p class="box-title">Hi, <?php echo $login['fullname']; ?></p>
    <div class="b-v">
        <p>Xin chào, <?php echo $login['fullname']; ?></p>
        <p>Loại tài khoản: <?php echo ($login['user_type']==1)?'<span class="label label-warning">VIP</span>':'<span class="label label-success">Thường</span>'; ?></p>
        <p>Ngày hết hạn: <?php echo ($login['end_date'])?$this->globals->get_dateformat($login['end_date'], 'd/m/Y'):'Chưa được gia hạn'; ?></p>
        <p><a href="<?php echo base_url('account/profile'); ?>">Thông tin cá nhân</a></p>
        <p><a href="<?php echo base_url('account/billing'); ?>">Lịch sử giao dịch</a></p>
        <p><a href="<?php echo base_url('account/logout'); ?>">Đăng xuất</a></p>
        <span >Hotline</span>
        <?php if ($sidebar_info->phone_number): ?>
            <div class="phone">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <p style="color:red">
                    0972 555 666
                    <!-- <?php echo $sidebar_info->phone_number; ?> -->
                </p>
            </div>
            <div class="phone">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <p style="color:red">
                    0988 999 666
                    <!-- <?php echo $sidebar_info->phone_number; ?> -->
                </p>
            </div>
            <div class="facebook">
                <p>Kết nối với chúng tôi:</p>
                <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
            </div>
        <?php endif; ?>
    </div>
    <?php else: ?>
        <p class="box-title">đăng nhập/ đăng ký</p>
        <div class="b-v">
            <form action="<?php echo base_url(); ?>login" method="post" role="form" >
                <div class="form-group">
                    <input type="text" name="username" class="form-control username bd-fix2" placeholder="Tên đăng nhập..."/>
                    <input type="password" name="password" class="form-control password bd-fix2" placeholder="Mật khẩu..."/>
                </div>

                <div class="b-b">
                    <button type="button" class="btn btn-default b-bnt login-btn">Đăng nhập</button>
                    <a href="#modal-forget-user" data-toggle="modal" class="btn btn-default b-bnt">Quên mật khẩu</a>
                </div>
                <p>Bạn chưa có tải khoản? <a  data-toggle="modal" href='#modal-register-user' class="red">Đăng ký</a> ngay</p>
                <span >Hotline</span>
                <?php if ($sidebar_info->phone_number): ?>
                     <div class="phone">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <p style="color:red">
                            0972 555 666
                            <!-- <?php echo $sidebar_info->phone_number; ?> -->
                        </p>
                    </div>
                    <div class="phone">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <p style="color:red">
                            0988 999 666
                            <!-- <?php echo $sidebar_info->phone_number; ?> -->
                        </p>
                    </div>
                    <div class="facebook">
                        <p>Kết nối với chúng tôi:</p>
                        <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                    </div>
                <?php endif; ?>
            </form>

        </div>
    <?php endif; ?>
</div>
<div class="box">
    <p class="box-title">Feedback khách hàng</p>
    <div class="b-v">
        <?php
        if ($feedback_list):
            foreach ($feedback_list as $item):
                ?>
                <div>
                    <img src="<?php echo $item->img; ?>" alt="">
                    <b><?php echo $item->name; ?></b>
                    <p style="font-family: Tahoma;">“<?php echo $item->content; ?>”</p>
                    <div class="chi"></div>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>