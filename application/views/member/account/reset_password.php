<?php require_once APPPATH.'/views/member/header.php'; ?>

    <div class="contact-page basic-div">
        <div class="container">
            <?php
                if ($check_exist==1):
            ?>
            <h2 class="cate-title"><?php echo $page_title; ?></h2>
            <hr/>
                    <?php
                    if($msg){
                        ?>
                        <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
                            <?php echo $msg['content']; ?>
                        </div>
                        <?php
                    }

                    if (validation_errors()!='')
                    {
                        ?>
                        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
                        <?php
                    }
                    ?>
            <form method="post">
                <div class="row">
                    <div class="col-md-5">
                        <input type="password" name="password" class="form-control" placeholder="Mật khẩu mới..."/>
                    </div>
                    <div class="col-md-5">
                        <input type="password" name="confirm_password" class="form-control" placeholder="Nhập lại mật khẩu mới..."/>
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Xác nhận</button>
                    </div>
                </div>
            </form>
            <?php else: ?>
             <br/>
            <div class="alert alert-info text-center">Liên kết không tồn tại hoặc đã hết hạn</div>
            <?php endif; ?>
        </div>
    </div>
    <br/>
<?php require_once APPPATH.'/views/member/footer.php'; ?>