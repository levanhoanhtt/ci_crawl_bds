<?php require_once APPPATH . '/views/member/header.php'; ?>

    <div class="contact-page basic-div">
        <div class="container">

            <?php
            if (!empty($login)):
                ?>
                <h2 class="cate-title"><?php echo $page_title; ?></h2>
                <hr>

                <div class="row">
                    <form method="post" enctype="multipart/form-data">
                        <div class="col-md-3">
                            <?php
                            if ($login['thumbnail']) {
                                $photo = $login['thumbnail'];
                            } else {
                                $photo = base_url('assets/sites/imgs/no-photo.png');
                            }
                            ?>
                            <p class="text-center"><img src="<?php echo $photo ?>" class="img-circle"
                                                        style="width: 100px; height: 100px;" alt=""></p>
                            <p><input type="file" name="thumbnail" class="form-control"/></p>
                            <p>Loại tài
                                khoản: <?php echo ($login['user_type'] == 1) ? '<span class="label label-warning">VIP</span>' : '<span class="label label-success">Thường</span>'; ?></p>
                            <p>Ngày hết
                                hạn: <?php echo ($login['end_date']) ? $this->globals->get_dateformat($login['end_date'], 'd/m/Y') : 'Chưa được gia hạn'; ?></p>
                            <p><a href="<?php echo base_url('account/profile'); ?>">Thông tin cá nhân</a></p>
                            <p><a href="<?php echo base_url('account/billing'); ?>">Lịch sử giao dịch</a></p>
                            <p><a href="<?php echo base_url('account/logout'); ?>">Đăng xuất</a></p>
                        </div>
                        <div class="col-md-9">

                            <table class="table table-bordered data_table">
                                <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th width="20%">Thời gian</th>
                                    <th width="20%">Ngày bắt đầu</th>
                                    <th width="20%">Kết thúc</th>
                                    <th width="">Nội dung</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($billing):
                                    $count = 0;
                                    foreach ($billing as $item):
                                        $count++;
                                        ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo isset($item['created_at']) ? $this->globals->get_dateformat($item['created_at'], 'd/m/Y H:i:s') : FALSE; ?></td>
                                            <td><?php echo ($item['start_time']) ? $this->globals->get_dateformat($item['start_time'], 'd/m/Y') : FALSE; ?></td>
                                            <td><?php echo ($item['end_time']) ? $this->globals->get_dateformat($item['end_time'], 'd/m/Y') : FALSE; ?></td>
                                            <td><?php echo $item['content']; ?></td>
                                        </tr>
                                    <?php endforeach; endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            <?php else: ?>
                <br/>
                <div class="alert alert-info text-center">
                    Vui lòng đăng nhập để thực hiện chức năng này! Đăng nhập <a href="<?php echo base_url(); ?>"><b>tại
                            đây</b></a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <br/>
<?php require_once APPPATH . '/views/member/footer.php'; ?>