<?php require_once APPPATH.'/views/member/header.php'; ?>

    <div class="contact-page basic-div">
        <div class="container">

            <?php
            if (!empty($login)):
            ?>
                <h2 class="cate-title"><?php echo $page_title; ?></h2>
                <hr>

                <div class="row">
                    <form method="post" enctype="multipart/form-data">
                        <div class="col-md-3">
                            <?php
                            if ($login['thumbnail'])
                            {
                                $photo = $login['thumbnail'];
                            }
                            else
                            {
                                $photo = base_url('assets/sites/imgs/no-photo.png');
                            }
                            ?>
                            <p class="text-center"><img src="<?php echo $photo ?>" class="img-circle" style="width: 100px; height: 100px;" alt=""></p>
                            <p><input type="file" name="thumbnail" class="form-control"/></p>
                            <p>Loại tài khoản: <?php echo ($login['user_type']==1)?'<span class="label label-warning">VIP</span>':'<span class="label label-success">Thường</span>'; ?></p>
                            <p>Ngày hết hạn: <?php echo ($login['end_date'])?$this->globals->get_dateformat($login['end_date'], 'd/m/Y'):'Chưa được gia hạn'; ?></p>
                            <p><a href="<?php echo base_url('account/profile'); ?>">Thông tin cá nhân</a></p>
                            <p><a href="<?php echo base_url('account/billing'); ?>">Lịch sử giao dịch</a></p>
                            <p><a href="<?php echo base_url('account/logout'); ?>">Đăng xuất</a></p>
                        </div>
                        <div class="col-md-9">
                            <?php
                            if($msg){
                                ?>
                                <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
                                    <?php echo $msg['content']; ?>
                                </div>
                                <?php
                            }

                            if (validation_errors()!='')
                            {
                                ?>
                                <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
                                <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Họ và tên</label>
                                        <input type="text" class="form-control" name="fullname" placeholder="Họ và tên..." value="<?php echo $login['fullname']; ?>"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Địa chỉ email</label>
                                        <input type="email" class="form-control" name="email_address" placeholder="Địa chỉ email..." value="<?php echo $login['email_address']; ?>"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Số điện thoại</label>
                                        <input type="tel" class="form-control" name="user_phone" placeholder="Số điện thoại..." value="<?php echo $login['user_phone']; ?>"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Địa chỉ</label>
                                        <input type="text" class="form-control" name="user_address" placeholder="Địa chỉ..." value="<?php echo $login['user_address']; ?>"/>
                                    </div>
                                </div>

                                <hr>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Mật khẩu</label>
                                        <input type="password" class="form-control" name="password" placeholder="Mật khẩu (Bỏ trống nếu không đổi)..."/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nhập lại mật khẩu</label>
                                        <input type="password" class="form-control" name="confirm_password" placeholder="Nhập lại mật khẩu..."/>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            <?php else: ?>
                <br/>
                <div class="alert alert-info text-center">
                    Vui lòng đăng nhập để thực hiện chức năng này! Đăng nhập <a href="<?php echo base_url(); ?>"><b>tại đây</b></a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <br/>
<?php require_once APPPATH.'/views/member/footer.php'; ?>