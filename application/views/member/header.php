<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <title><?php echo $page_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/fancybox.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/fancybox-thumbs.css'); ?>">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/style.css?ver='.rand()); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/owl.carousel.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/owl.theme.default.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
    
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fancybox/fancybox/jquery.fancybox-1.3.4.css" media="screen" /> -->
    
</head>

<body>
<?php
$main_menu = $this->get_data->main_menu();
// $main_menu_logout = $this->get_data->main_menu_logout();
$this->get_data->set_activity_user();

$header_info = $this->get_data->options_info("header");
?>
<header>
    <div class="header-top">
        <div class="container">
            <div class="row hd-1">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <div class="logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/sites/imgs/logo.png'); ?>" alt=""></a></div>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                    <p class="text-header"><?php echo $header_info->phone_number; ?> <a href="<?php echo $header_info->facebook_link; ?>"><img class="icon-facebook" src="<?php echo base_url('assets/sites/imgs/icon-fb.png'); ?>" alt=""></a></p>
                </div>
            </div>
        </div>
        <div class="menu-site">
            <div class="container">
                <button class="btn btn-show-menu hidden-md hidden-lg"><i class="fa fa-bars"></i></button>
                <div class="menu-box">
                    <div class="bg-menu hidden-md hidden-lg"></div>
                    <?php echo $main_menu; ?>
                    <button class="btn btn-hide-menu hidden-md hidden-lg"><i class="fa fa-times"></i></button>
                </div>
            </div>
        </div>
        <!-- end menu -->
    </div>
</header>
