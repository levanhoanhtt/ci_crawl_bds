<?php
require_once APPPATH.'/views/member/header.php';
require_once APPPATH.'/views/member/block/slideshow.php';
?>
 
    <div class="main-content">
        <div class="container">

            <h2 class="title"> <?php echo $page_title; ?></h2>

            <?php
            if ($this->get_data->is_user_login()):

            ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="mytable" class="table table-bordred ">
                            <thead>
                            <th width="5%">STT</th>
                            <th>Tiêu đề</th>
                            <th width="10%">Ngày đăng</th>
                            <th width="10%">Giá</th>
                            <th width="10%">Điện thoại</th>
                            <th width="10%">Loại tin</th>
                            <th width="7%"></th>
                            </thead>
                            <tbody>
                            <?php
                            if ($list_save_post):
                                $count = 0;
                                foreach ($list_save_post as $post):
                                    $count++;
                                    $post_info = $this->get_data->get_post_detail($post['post_id']);
                                    $contact = $post_info['contact'];
                                    preg_match('/([0-9]{10,11})/', $contact, $maches);
                                    $phone = $maches[0];
                                    $cate_arr = json_decode($post_info['cate_id'], true);
                                    $cate_name = $this->get_data->get_category($cate_arr[0]);
                            ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>

                                        <td>
                                            <p><a href="#popup_<?php echo $post_info['post_id']; ?>" data-post-id="<?php echo $post_info['post_id']; ?>" class="post-title <?php echo ($this->get_data->is_post_read($post_info['post_id']))?'read':FALSE; ?> post-<?php echo $post_info['post_id']; ?>" data-toggle="modal"><b><?php echo $post_info['post_title']; ?></b></a></p>
                                            <div class="mota">
                                                <b>Quận Huyện:</b> <?php echo ($this->get_data->get_district($post_info['district_id']))?$this->get_data->get_district($post_info['district_id']):'Không xác định'; ?> <b>Tỉnh/ Thành Phố:</b> <?php echo ($this->get_data->get_province($post_info['province_id']))?$this->get_data->get_province($post_info['province_id']):'Không xác định'; ?>
                                            </div>
                                        </td>
                                        <td><?php echo date('d/m/Y', $post_info['timestamp']); ?></td>
                                        <td><?php echo ($post_info['price'])?($this->get_data->get_price_string($post_info['price'])).$post_info['unit']:'Thỏa thuận'; ?></td>
                                        <td><?php echo $phone; ?></td>
                                        <td>Tin mới</td>
                                        <td>
                                            <a href="#" class="ico delete_post" data-post-id="<?php echo $post_info['post_id']; ?>"><img src="<?php echo base_url('assets/sites/imgs/xoa.png'); ?>" alt=""></a>
                                            <a href="#popup_<?php echo $post_info['post_id']; ?>" data-toggle="modal" data-post-id="<?php echo $post_info['post_id']; ?>" class="ico post-title"><img src="<?php echo base_url('assets/sites/imgs/do.png'); ?>" alt=""></a>
                                            <div class="modal fade popup" id="popup_<?php echo $post_info['post_id']; ?>" tabindex="-12" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                                            <h4 class="modal-title custom_align" id="Heading">Thông tin chi tiết</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                                    <h3 class="title"><?php echo $post_info['post_title']; ?></h3>
                                                                    <p style="text-transform: uppercase;">Nội dung tin đăng :</p>
                                                                    <div class="item-vip">
                                                                        <?php $content = preg_replace('/&nbsp;/', '', html_entity_decode($post_info['post_content']));
                                                                        echo $content;
                                                                        ?>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                    <div class="slide2">
                                                                        <div class="owl-carousel owl-theme slide-bds2">
                                                                            <!-- <div class="item">
                                                                                <img src="<?php echo $post_info['thumbnail']; ?>" alt="">
                                                                            </div> -->
                                                                            <?php if ($this->get_data->get_post_images($post_info['post_id'])):
                                                                                foreach ($this->get_data->get_post_images($post_info['post_id']) as $image):
                                                                                    ?>
                                                                                    <div class="item">
                                                                                        <img src="<?php echo $image['image_url']; ?>" alt="">
                                                                                    </div>
                                                                                <?php endforeach; endif; ?>
                                                                            </div>
                                                                        </div>

                                                                    <div class="bui-pop " >
                                                                        <div class="s-title">Giá tiền :
                                                                            <p class="mau2">
                                                                                <?php echo ($post_info['price'])?($this->get_data->get_price_string($post_info['price'])).$post_info['unit']:'Thỏa thuận'; ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="s-title">Liên hệ : 
                                                                            <p class="mau2">
                                                                                <?php echo $phone; ?> 
                                                                            </p>
                                                                        </div>
                                                                        <div class="s-title">Ngày đăng : 
                                                                            <p>
                                                                                <?php echo date('d/m/Y', $post_info['timestamp']); ?> 
                                                                            </p>
                                                                        </div>
                                                                        <div class="s-title">Loại tin : 
                                                                            <p>
                                                                                <?php echo ($cate_name)?$cate_name:'Không xác định'; ?>
                                                                            </p>  
                                                                        </div>
                                                                        <div class="s-title">Tỉnh / TP : 
                                                                            <p>
                                                                                <?php echo ($this->get_data->get_province($post_info['province_id']))?$this->get_data->get_province($post_info['province_id']):'Không xác định'; ?>
                                                                            </p>
                                                                        </div>
                                                                        <div class="s-title">Quận / Huyện : 
                                                                            <p>
                                                                                <?php echo ($this->get_data->get_district($post_info['district_id']))?$this->get_data->get_district($post_info['district_id']):'Không xác định'; ?>
                                                                            </p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href="#" class="ico delete_post" data-post-id="<?php echo $post_info['post_id']; ?>"><img src="<?php echo base_url('assets/sites/imgs/xoa.png'); ?>" alt=""></a>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>

                                        </td>
                                    </tr>
                            <?php
                            endforeach; endif;
                            ?>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <center>
                            <div class="number-1">
                                <span class="number-minus"><i class="fa fa-caret-left" aria-hidden="true"></i></span>
                                <input type="text" class="input-number" value="1" min="1">
                                <span class="number-plus"><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                            </div>

                        </center>

                    </div>
                </div>
            </div>

            <?php else: ?>
              <div class="alert alert-info text-center">
                  Vui lòng đăng nhập để thực hiện chức năng này! Đăng nhập <a href="<?php echo base_url(); ?>"><b>tại đây</b></a>
              </div>
            <?php endif; ?>
        </div>
    </div>
<?php require_once APPPATH.'/views/member/footer.php'; ?>