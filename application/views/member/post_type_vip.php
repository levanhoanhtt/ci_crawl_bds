<?php
require_once APPPATH . '/views/member/header.php';
if ($login):
    ?>

    <div class="seach">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="title"> <?php echo $page_title; ?></h2>
                </div>
            </div>
            <div class="jumbotron">
                <form method="get" action="<?php echo base_url('tin-vip-chinh-chu'); ?>">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="form-group">
                                <input type="text" name="keyword" class="form-control"
                                       value="<?php echo @$_GET['keyword']; ?>" placeholder="Nhập từ khóa tìm kiếm">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <div class="form-group">
                                <select class="form-control" name="province_id" id="province_id">
                                    <option value="0">Chọn Tỉnh / Thành phố</option>
                                    <?php
                                    if ($list_province):
                                        foreach ($list_province as $item):
                                            ?>
                                            <option value="<?php echo $item['matp']; ?>" <?php echo (isset($_GET['province_id']) && $_GET['province_id'] == $item['matp']) ? 'selected' : FALSE; ?>><?php echo $item['name']; ?></option>
                                        <?php
                                        endforeach; endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <div class="form-group">
                                <select class="form-control" name="district_id" id="district_id">
                                    <option value="0">Chọn Quận / huyện</option>
                                    <?php

                                    if (isset($list_dictrict)) {
                                        $oldDistrict = $this->input->get('district_id');
                                        foreach ($list_dictrict as $key => $value) {
                                            ?>
                                            <option value="<?php echo $value['maqh'] ?>" <?php if ($oldDistrict == $value['maqh']) echo "selected"; ?> ><?php echo $value['name']; ?></option>
                                            <?php
                                        }
                                    }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-seach"><i class="fa fa-search"
                                                                                           aria-hidden="true"></i>Tìm
                                    Kiếm
                                </button>
                            </div>
                        </div>
                    </div>
                    <small class="bd-show">Tìm kiếm nâng cao <i class="fa fa-caret-down" aria-hidden="true"></i></small>
                    <div class="row bd-hide">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="form-group">
                                <select class="form-control" id="category_id" name="category_id">
                                    <option value="0">Chọn danh mục</option>
                                    <?php
                                    if ($category_parent):
                                        foreach ($category_parent as $item):
                                            ?>
                                            <option value="<?php echo $item['cate_id']; ?>" <?php echo (isset($_GET['category_id']) && $_GET['category_id'] == $item['cate_id']) ? 'selected' : FALSE; ?>><?php echo $item['cate_name']; ?></option>
                                            <?php
                                            $category_child = $this->get_data->get_category_child($item['cate_id']);
                                            if ($category_child) {
                                                foreach ($category_child as $child) {
                                                    ?>
                                                    <option value="<?php echo $child['cate_id'] ?>" <?php echo (isset($_GET['category_id']) && $_GET['category_id'] == $child['cate_id']) ? 'selected' : FALSE; ?>>
                                                        - <?php echo $child['cate_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="form-group">
                                <select class="form-control" name="area">
                                    <option value="0">Chọn diện tích</option>
                                    <option value="0-30" <?php echo (isset($_GET['area']) && $_GET['area'] == '0-30') ? 'selected' : FALSE; ?>>
                                        Dưới 30 m2
                                    </option>
                                    <option value="30-50" <?php echo (isset($_GET['area']) && $_GET['area'] == '30-50') ? 'selected' : FALSE; ?>>
                                        30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;50 m2
                                    </option>
                                    <option value="50-70" <?php echo (isset($_GET['area']) && $_GET['area'] == '50-70') ? 'selected' : FALSE; ?>>
                                        50&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;70 m2
                                    </option>
                                    <option value="70-100" <?php echo (isset($_GET['area']) && $_GET['area'] == '70-100') ? 'selected' : FALSE; ?>>
                                        70&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;100 m2
                                    </option>
                                    <option value="100-150" <?php echo (isset($_GET['area']) && $_GET['area'] == '100-150') ? 'selected' : FALSE; ?>>
                                        100&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;150 m2
                                    </option>
                                    <option value="150-200" <?php echo (isset($_GET['area']) && $_GET['area'] == '150-200') ? 'selected' : FALSE; ?>>
                                        150&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;200 m2
                                    </option>
                                    <option value="200-250" <?php echo (isset($_GET['area']) && $_GET['area'] == '200-250') ? 'selected' : FALSE; ?>>
                                        200&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;250 m2
                                    </option>
                                    <option value="250-300" <?php echo (isset($_GET['area']) && $_GET['area'] == '250-300') ? 'selected' : FALSE; ?>>
                                        250&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;300 m2
                                    </option>
                                    <option value="300-350" <?php echo (isset($_GET['area']) && $_GET['area'] == '300-350') ? 'selected' : FALSE; ?>>
                                        300&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;350 m2
                                    </option>
                                    <option value="350-400" <?php echo (isset($_GET['area']) && $_GET['area'] == '350-400') ? 'selected' : FALSE; ?>>
                                        350&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;400 m2
                                    </option>
                                    <option value="400-600" <?php echo (isset($_GET['area']) && $_GET['area'] == '400-600') ? 'selected' : FALSE; ?>>
                                        400&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;600 m2
                                    </option>
                                    <option value="600-800" <?php echo (isset($_GET['area']) && $_GET['area'] == '600-800') ? 'selected' : FALSE; ?>>
                                        600&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;800 m2
                                    </option>
                                    <option value="800-1000" <?php echo (isset($_GET['area']) && $_GET['area'] == '800-1000') ? 'selected' : FALSE; ?>>
                                        800&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;1000 m2
                                    </option>
                                    <option value="1000-0" <?php echo (isset($_GET['area']) && $_GET['area'] == '1000-0') ? 'selected' : FALSE; ?>>
                                        Trên 1000 m2
                                    </option>
                                </select>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="form-group">
                                <select class="form-control" name="price">
                                    <option value="0">Chọn mức giá</option>
                                    <option value="-1">Không xác định</option>
                                    <option value="0-1" <?php echo (isset($_GET['price']) && $_GET['price'] == '0-1') ? 'selected' : false; ?>>
                                        &lt;= 1 triệu
                                    </option>
                                    <option value="1-3" <?php echo (isset($_GET['price']) && $_GET['price'] == '1-3') ? 'selected' : false; ?>>
                                        1 - 3 triệu
                                    </option>
                                    <option value="3-5" <?php echo (isset($_GET['price']) && $_GET['price'] == '3-5') ? 'selected' : false; ?>>
                                        3 - 5 triệu
                                    </option>
                                    <option value="5-10" <?php echo (isset($_GET['price']) && $_GET['price'] == '5-10') ? 'selected' : false; ?>>
                                        5 - 10 triệu
                                    </option>
                                    <option value="10-20" <?php echo (isset($_GET['price']) && $_GET['price'] == '10-20') ? 'selected' : false; ?>>
                                        10 - 20 triệu
                                    </option>
                                    <option value="20-30" <?php echo (isset($_GET['price']) && $_GET['price'] == '20-30') ? 'selected' : false; ?>>
                                        20 - 30 triệu
                                    </option>
                                    <option value="30-40" <?php echo (isset($_GET['price']) && $_GET['price'] == '30-40') ? 'selected' : false; ?>>
                                        30 - 40 triệu
                                    </option>
                                    <option value="40-70" <?php echo (isset($_GET['price']) && $_GET['price'] == '40-70') ? 'selected' : false; ?>>
                                        40 - 70 triệu
                                    </option>
                                    <option value="70-100" <?php echo (isset($_GET['price']) && $_GET['price'] == '70-100') ? 'selected' : false; ?>>
                                        70 - 100 triệu
                                    </option>
                                    <option value="100-500" <?php echo (isset($_GET['price']) && $_GET['price'] == '100-500') ? 'selected' : false; ?>>
                                        100 - 500 triệu
                                    </option>
                                    <option value="500-800" <?php echo (isset($_GET['price']) && $_GET['price'] == '500-800') ? 'selected' : false; ?>>
                                        500 - 800 triệu
                                    </option>
                                    <option value="800-1000" <?php echo (isset($_GET['price']) && $_GET['price'] == '800-1000') ? 'selected' : false; ?>>
                                        800 - 1 tỷ
                                    </option>
                                    <option value="1000-2000" <?php echo (isset($_GET['price']) && $_GET['price'] == '1000-2000') ? 'selected' : false; ?>>
                                        1 - 2 tỷ
                                    </option>
                                    <option value="2000-3000" <?php echo (isset($_GET['price']) && $_GET['price'] == '2000-3000') ? 'selected' : false; ?>>
                                        2 - 3 tỷ
                                    </option>
                                    <option value="3000-5000" <?php echo (isset($_GET['price']) && $_GET['price'] == '3000-5000') ? 'selected' : false; ?>>
                                        3 - 5 tỷ
                                    </option>
                                    <option value="5000-7000" <?php echo (isset($_GET['price']) && $_GET['price'] == '5000-7000') ? 'selected' : false; ?>>
                                        5 - 7 tỷ
                                    </option>
                                    <option value="7000-10000" <?php echo (isset($_GET['price']) && $_GET['price'] == '7000-10000') ? 'selected' : false; ?>>
                                        7 - 10 tỷ
                                    </option>
                                    <option value="10000-20000" <?php echo (isset($_GET['price']) && $_GET['price'] == '10000-20000') ? 'selected' : false; ?>>
                                        10 - 20 tỷ
                                    </option>
                                    <option value="20000-30000" <?php echo (isset($_GET['price']) && $_GET['price'] == '20000-30000') ? 'selected' : false; ?>>
                                        20 - 30 tỷ
                                    </option>
                                    <option value="30000-0" <?php echo (isset($_GET['price']) && $_GET['price'] == '30000-0') ? 'selected' : false; ?>>
                                        > 30 tỷ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">

                            <div class="form-group cot">
                                
                                <input class="form-control datepicker" name="date_end" type="text" value=""
                                       id="example-datetime-local-input" placeholder="Chọn ngày bắt đầu...">
                            </div>
                            <div class="form-group hai">
                                
                                <input class="form-control datepicker" name="date_start" type="text" value=""
                                       id="example-datetime-local-input" placeholder="Chọn ngày kết thúc...">
                            </div>
                        </div>

                    </div>

                </form>

            </div>
            <div class="filter-price text-right" style="padding: 15px 10px;">
                <span>Sắp xếp theo:</span>
                <select name="sort_by" class="click-change">
                    <?php 
                        $query_string = $_SERVER['QUERY_STRING'];
                        $query_char = (!empty($query_string))?'&':'?';
                        
                     ?>
                    <option value="<?php echo base_url('tin-vip-chinh-chu').'?'.$query_string.$query_char; ?>sort_by=time" <?php echo (isset($_GET['sort_by']) && $_GET['sort_by'] == "time") ? "selected" : false; ?> >
                        Tin mới nhất
                    </option>
                    <option value="<?php echo base_url('tin-vip-chinh-chu').'?'.$query_string.$query_char; ?>sort_by=price_desc" <?php echo (isset($_GET['sort_by']) && $_GET['sort_by'] == "price_desc") ? "selected" : false; ?>>
                        Giá cao nhất
                    </option>
                    <option value="<?php echo base_url('tin-vip-chinh-chu').'?'.$query_string.$query_char; ?>sort_by=price_asc" <?php echo (isset($_GET['sort_by']) && $_GET['sort_by'] == "price_asc") ? "selected" : false; ?>>
                        Giá thấp nhất
                    </option>
                </select>

            </div>
            <!-- end jumbotron -->
        </div>
        <!-- end jumbotron -->
    </div>
    </div>
    <!-- end tim kiếm -->

    <div class="main-content hidden-xs hidden-sm">
        <div class="container">
            <br>

            <?php if ($login['user_type'] == 1 && date('Y-m-d') >= $login['start_date'] && date('Y-m-d') <= $login['end_date']): ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="mytable" class="table table-bordred ">
                                <thead>
                                <th width="5%">STT</th>
                                <th>Tiêu đề</th>
                                <th width="10%">Ngày đăng</th>
                                <th width="10%">Giá</th>
                                <th width="10%">Điện thoại</th>
                                <th width="8%">Loại tin</th>
                                <th width="7%" style="display: none;"></th>
                                </thead>
                                <tbody>
                                <?php
                                if ($list_post):
                                    $count = 0;
                                    foreach ($list_post as $item):
                                        $count++;
                                        $contact = $item['contact'];
                                        preg_match('/([0-9]{10,11})/', $contact, $maches);
                                        $phone = $maches[0];
                                        $cate_arr = json_decode($item['cate_id'], true);
                                        $cate_name = $this->get_data->get_category($cate_arr[0]);
                                        ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>

                                            <td>
                                                <!-- <?php
                                                $title = $item['post_title'];

                                                $title1 = $this->globals->change_to_slug($title);
                                                $dem = strlen($title);
                                                if ($dem > 75) {
                                                    $title = mb_substr($title, 0, 60) . "...";
                                                }
                                                ?> -->
                                                
                                                <p class="multi-Line one-line"><a
                                                            href="#popup_<?php echo $item['post_id']; ?>"
                                                            data-post-id="<?php echo $item['post_id']; ?>"
                                                            class="post-title <?php echo ($this->get_data->is_post_read($item['post_id'])) ? 'read' : FALSE; ?> post-<?php echo $item['post_id']; ?>"
                                                            data-toggle="modal"><b><?php echo $item['post_title']; ?></b></a>
                                                </p>
                                                <div class="mota">
                                                    <b>Quận
                                                        Huyện:</b> <?php echo ($this->get_data->get_district($item['district_id'])) ? $this->get_data->get_district($item['district_id']) : 'Không xác định'; ?>
                                                    <b>Tỉnh/ Thành
                                                        Phố:</b> <?php echo ($this->get_data->get_province($item['province_id'])) ? $this->get_data->get_province($item['province_id']) : 'Không xác định'; ?>
                                                </div>
                                            </td>
                                            <?php 
                                                // $unit = $item['unit'];
                                                // $unit = str_replace('/', '', $unit);
                                            ?>
                                            <td><?php echo date('d/m/Y', $item['timestamp']); ?></td>
                                            <!-- <td><?php echo ($item['price']) ? ($this->get_data->get_price_string($item['price'])) .' / '. $unit : 'Thỏa thuận'; ?></td> -->
                                            <td style="color: red;">
                                                <?php 
                                                    if($item['price']!=0)
                                                    {
                                                        if($item['unit'] != null && $item['unit'] != '0')
                                                        {
                                                            $unit_change = $item['unit'];

                                                            // if( strpos($item['unit'] , '/' ) == false ){
                                                            //     $unit_change = str_replace('/','',$item['unit']);    
                                                            // }
                                                            echo $this->get_data->get_price_string($item['price'])." ".$unit_change;
                                                        }
                                                        else
                                                            echo $this->get_data->get_price_string($item['price']);

                                                    }
                                                    else
                                                        echo "Thỏa thuận";

                                                ?>
                                            </td>
                                            <td><?php echo $phone; ?></td>
                                            <td>Tin mới</td>
                                            <td style="display: none;">
                                                <a href="#" id="" data-post-id="<?php echo $item['post_id']; ?>"
                                                   class="ico save_post"><img
                                                            src="<?php echo base_url('assets/sites/imgs/luu.png'); ?>"
                                                            alt=""></a>
                                                <a href="#popup_<?php echo $item['post_id']; ?>" data-toggle="modal"
                                                   data-post-id="<?php echo $item['post_id']; ?>"
                                                   class="ico post-title"><img
                                                            src="<?php echo base_url('assets/sites/imgs/do.png'); ?>"
                                                            alt=""></a>
                                                

                                            </td>
                                            <div class="modal fade popup" id="popup_<?php echo $item['post_id']; ?>"
                                                     tabindex="-12" role="dialog" aria-labelledby="edit"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"><span
                                                                            class="glyphicon glyphicon-remove"
                                                                            aria-hidden="true"></span></button>
                                                                <h4 class="modal-title custom_align" id="Heading">Thông
                                                                    tin chi tiết</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                                        <h3 class="title"><?php echo $item['post_title']; ?></h3>
                                                                        <p style="text-transform: uppercase;">Nội dung tin đăng :</p>
                                                                        <div class="item-vip">
                                                                            <?php $content = preg_replace('/&nbsp;/', '', html_entity_decode($item['post_content']));
                                                                            if($content == strip_tags($content))
                                                                                {
                                                                                    $content = str_replace('.', '.<br>', $content);
                                                                                    $content = str_replace('.', '.<br>', $content);
                                                                    $content = str_replace('1.<br>', '1.', $content);
                                                                    $content = str_replace('2.<br>', '2.', $content);
                                                                    $content = str_replace('3.<br>', '3.', $content);
                                                                    $content = str_replace('4.<br>', '4.', $content);
                                                                    $content = str_replace('5.<br>', '5.', $content);
                                                                    $content = str_replace('6.<br>', '6.', $content);
                                                                    $content = str_replace('7.<br>', '7.', $content);
                                                                    $content = str_replace('8.<br>', '8.', $content);
                                                                    $content = str_replace('9.<br>', '9.', $content);
                                                                    $content = str_replace('0.<br>', '0.', $content);
                                                                                }
                                                                            echo $content;
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                        <div class="slide2">
                                                                            <div class="owl-carousel owl-theme slide-bds2">
                                                                            <?php if ($this->get_data->get_post_images($item['post_id'])):
                                                                                foreach ($this->get_data->get_post_images($item['post_id']) as $image):
                                                                                    ?>
                                                                                    <div class="item">
                                                                                        <a href="<?php echo $image['image_url']; ?>"
                                                                                         class="fancybox-thumb"
                                                                                         rel="fancybox-thumb"  data-fancybox="<?php echo "gallery".$count; ?>"  >
                                                                                         <img src="<?php echo $image['image_url']; ?>"
                                                                                         alt="" >
                                                                                     </a>
                                                                                 </div>
                                                                             <?php endforeach; endif; ?>
                                                                             </div>
                                                                        </div>
                                                                        <div class="bui-pop">
                                                                            <div class="s-title">Giá tiền :
                                                                                <span class="mau2">
                                                                                    <!-- <?php echo ($item['price']) ? ($this->get_data->get_price_string($item['price'])) . $item['unit'] : 'Thỏa thuận'; ?> -->
                                                                                    <?php 
                                                                                        if($item['price']!=0)
                                                                                        {
                                                                                            if($item['unit'] != null && $item['unit'] != '0')
                                                                                            {
                                                                                                $unit_change = $item['unit'];

                                                                                                // if( strpos($item['unit'] , '/' ) == false ){
                                                                                                //     $unit_change = str_replace('/','',$item['unit']);    
                                                                                                // }
                                                                                                echo $this->get_data->get_price_string($item['price'])." ".$unit_change;
                                                                                            }
                                                                                            else
                                                                                                echo $this->get_data->get_price_string($item['price']);

                                                                                        }
                                                                                        else
                                                                                            echo "Thỏa thuận";

                                                                                    ?>
                                                                                </span>
                                                                            </div> 
                                                                            <div class="s-title">Liên hệ :
                                                                                <span class="mau2">
                                                                                    <?php echo $phone; ?> 
                                                                                </span>
                                                                            </div> 
                                                                            <div class="s-title">Ngày đăng :
                                                                                <span >
                                                                                    <?php echo date('d/m/Y', $item['timestamp']); ?>
                                                                                </span>
                                                                            </div> 
                                                                            <div class="s-title">Loại tin :
                                                                                <span>
                                                                                    <?php echo ($cate_name) ? $cate_name : 'Không xác định'; ?>
                                                                                </span>
                                                                            </div> 
                                                                            <div class="s-title">Tỉnh / TP :
                                                                                <span>
                                                                                    <?php echo ($this->get_data->get_province($item['province_id'])) ? $this->get_data->get_province($item['province_id']) : 'Không xác định'; ?>
                                                                                </span>
                                                                            </div> 
                                                                            <div class="s-title">Quận / Huyện :
                                                                                <span>
                                                                                     <?php echo ($this->get_data->get_district($item['district_id'])) ? $this->get_data->get_district($item['district_id']) : 'Không xác định'; ?>
                                                                                </span>
                                                                            </div>
                                                                            <br>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer ">
                                                                <a href="#" id=""
                                                                   data-post-id="<?php echo $item['post_id']; ?>"
                                                                   class="ico save_post"><img
                                                                            src="<?php echo base_url('assets/sites/imgs/luu.png'); ?>"
                                                                            alt=""> Lưu tin này</a>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                        </tr>
                                    <?php endforeach; endif; ?>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <?php if ($agent != 1): ?>
                               <div>
                                        <ul class="pagination">
                                        <?php echo $pagination; ?>
                                        </ul>
                                    </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <div class="container">
                    <div class="alert alert-info text-center">
                        Khu vực này chỉ dành cho tài khoản VIP hoặc tài khoản của bạn đã hết hạn
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>

    <div style="clear: both;"></div>
    <div class="show-mobile hidden-lg hidden-md">
        <table width="100%">
            <tr class="item-mobile">
                <td class="number-show-mobile">STT</td>
                <td class="form-mobile">
                    <form><input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter"></form>
                </td>
                <td class="title-show-mobile">Tiêu Đề</td>
            </tr>
            <?php
            if ($list_post):
                $count = 0;
                foreach ($list_post as $item):
                    $count++;
                    $contact = $item['contact'];
                    preg_match('/([0-9]{10,11})/', $contact, $maches);
                    $phone = $maches[0];
                    $cate_arr = json_decode($item['cate_id'], true);
                    $cate_name = $this->get_data->get_category($cate_arr[0]);
                    $unit = ($item['unit'])?$item['unit']:'';
                    ?>
                    <tr>
                        <td class="number-stt"><?php echo $count; ?></td>
                        <td>
                            <form><input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter"></form>
                        </td>
                        <td>
                            <!-- <?php
                            $title = $item['post_title'];

                            $title1 = $this->globals->change_to_slug($title);
                            $dem = strlen($title);
                            if ($dem > 75) {
                                $title = mb_substr($title, 0, 60) . "...";
                            }
                            ?> -->
                            <div class="cover" style="font-style: italic;">
                                <a href="#m_popup_<?php echo $item['post_id']; ?>" data-toggle="modal"
                                   data-post-id="<?php echo $item['post_id']; ?>"
                                   class="title-cover" style="font-style: normal;"><?php echo $title ?></a>
                                <br>Quận huyện:<span
                                        class="mau1"><?php echo ($this->get_data->get_district($item['district_id'])) ? $this->get_data->get_district($item['district_id']) : 'Không xác định'; ?></span><br>Tỉnh/Thành
                                Phố:<span
                                        class="mau1"><?php echo ($this->get_data->get_province($item['province_id'])) ? $this->get_data->get_province($item['province_id']) : 'Không xác định'; ?></span>
                                        <!-- <br>Ngày Đăng:<span class="mau1"><?php echo date('d/m/Y', $item['timestamp']); ?></span><br> -->
                                Giá:<span
                                        class="mau2"><?php echo ($item['price']) ? ($this->get_data->get_price_string($item['price'])) . $unit  : 'Thỏa thuận'; ?></span>
                                <!-- <br><b>Điện Thoại:</b><span class="mau2"><?php echo $phone; ?></span><br>
                                Loại Tin:<span class="mau3">Tin mới.</span> -->
                            </div>
                        </td>
                        <td>
                            <div class="modal fade" id="m_popup_<?php echo $item['post_id']; ?>">
                                <div class="about-fade">
                                    <div class="modal-title-2">
                                        <h3>THÔNG TIN CHI TIẾT</h3>
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        </div>
                                    </div>
                                    <div class="modal-about">
                                        <div class="modal-about-left">
                                            <p><span class="a1"><?php echo $item['post_title'] ?></span><br><b>Nội dung tin đăng :</b><br>
                                            <div class="modal-scrop"><span class="a2">
                                                <?php $content = preg_replace('/&nbsp;/', '', html_entity_decode($item['post_content']));
                                                if($content == strip_tags($content))
                                                {
                                                    $content = str_replace('.', '.<br>', $content);
                                                    $content = str_replace('1.<br>', '1.', $content);
                                                    $content = str_replace('2.<br>', '2.', $content);
                                                    $content = str_replace('3.<br>', '3.', $content);
                                                    $content = str_replace('4.<br>', '4.', $content);
                                                    $content = str_replace('5.<br>', '5.', $content);
                                                    $content = str_replace('6.<br>', '6.', $content);
                                                    $content = str_replace('7.<br>', '7.', $content);
                                                    $content = str_replace('8.<br>', '8.', $content);
                                                    $content = str_replace('9.<br>', '9.', $content);
                                                    $content = str_replace('0.<br>', '0.', $content);
                                                }
                                                echo ($content);
                                                ?>
                                            </span>
                                            </div>
                                            </p>
                                        </div>
                                        <div class="modal-about-right">
                                            <p>Giá tiền :
                                                <span class="mau2"><?php echo ($item['price']) ? ($this->get_data->get_price_string($item['price'])) . $unit : 'Thỏa thuận'; ?></span>
                                                <br>
                                                Liên hệ :
                                                <span class="mau2"><?php echo $phone; ?></span> <br>
                                                Ngày đăng :
                                                <?php echo date('d/m/Y', $item['timestamp']); ?> <br>
                                                Loại tin :
                                                <?php echo ($cate_name) ? $cate_name : 'Không xác định'; ?><br>
                                                Tỉnh / TP :
                                                <?php echo ($this->get_data->get_province($item['province_id'])) ? $this->get_data->get_province($item['province_id']) : 'Không xác định'; ?>
                                                <br>
                                                Quận / Huyện :
                                                <?php echo ($this->get_data->get_district($item['district_id'])) ? $this->get_data->get_district($item['district_id']) : 'Không xác định'; ?>
                                            </p>
                                        </div>
                                        <!-- <div class="slide2 col-xs-12">
                                            <div class="owl-carousel owl-theme slide-bds2">
                                               
                                                <?php if ($this->get_data->get_post_images($item['post_id'])):
                                                    foreach ($this->get_data->get_post_images($item['post_id']) as $image):
                                                        ?>
                                                        <div class="item">
                                                            <a href="<?php echo $image['image_url']; ?>"
                                                               class="fancybox-thumb"
                                                               rel="fancybox-thumb">
                                                               <img src="<?php echo $image['image_url']; ?>"
                                                               alt="">
                                                           </a>
                                                       </div>
                                                   <?php endforeach; endif; ?>
                                               </div>
                                           </div>
                                       </div> -->

                                        <div class="slide2 col-xs-12">
                                            <div class="owl-carousel owl-theme slide-bds2">
                                            <?php if ($this->get_data->get_post_images($item['post_id'])):
                                                foreach ($this->get_data->get_post_images($item['post_id']) as $image):
                                                    ?>
                                                    <div class="item">
                                                        <a href="<?php echo $image['image_url']; ?>"
                                                         class="fancybox-thumb"
                                                         rel="fancybox-thumb"  data-fancybox="<?php echo "gallery".$count; ?>"  >
                                                         <img src="<?php echo $image['image_url']; ?>"
                                                         alt="" >
                                                     </a>
                                                 </div>
                                             <?php endforeach; endif; ?>
                                             </div>
                                        </div>
                                    </div>
                                    <div class="modal-about-footer">
                                        <a href="#" id=""
                                           data-post-id="<?php echo $item['post_id']; ?>"
                                           class="ico save_post">
                                           <img src="<?php echo base_url('assets/sites/imgs/luu.png'); ?>"
                                                    alt="">
                                        </a>
                                        
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; endif; ?>
        </table>
        
        <div class="clearfix"></div>
        <?php if ($agent == 1): ?>
            <center>
                <div class="number-1">
                    <?php echo $pagination; ?>
                </div>
            </center>
        <?php endif; ?>
    </div>


<?php else: ?>
    <div class="container">
        <div class="alert alert-info text-center">
            Vui lòng đăng nhập để thực hiện chức năng này! Đăng nhập <a href="<?php echo base_url(); ?>"><b>tại đây</b></a>
        </div>
    </div>

<?php endif; ?>
<?php require_once APPPATH . '/views/member/footer.php'; ?>
<!--<script type="text/javascript">-->
<!--    jQuery(document).ready(function () {-->
<!--        jQuery('.title-cover').click(function (event) {-->
<!--            /* Act on the event */-->
<!--            jQuery('.modal-content-2').addClass('modal-content-2-them')-->
<!--            jQuery('.nen').addClass('nen-them')-->
<!---->
<!--        });-->
<!--        jQuery('.dong').click(function (event) {-->
<!--            /* Act on the event */-->
<!--            jQuery('.modal-content-2').removeClass('modal-content-2-them')-->
<!--            jQuery('.nen').removeClass('nen-them')-->
<!--        });-->
<!--        jQuery('.nen').click(function (event) {-->
<!--            /* Act on the event */-->
<!--            jQuery('.modal-content-2').removeClass('modal-content-2-them')-->
<!--            jQuery('.nen').removeClass('nen-them')-->
<!--        });-->
<!---->
<!--    });-->
<!--</script>-->