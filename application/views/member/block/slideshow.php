<?php
$slideshow = $this->get_data->options_info("slideshow");
if ($slideshow):
?>
<div class="slide">
    <div class="owl-carousel owl-theme slide-bds">
        <?php foreach ($slideshow as $item): ?>
        <div class="item">
            <a href="<?php echo $item->link; ?>"><img src="<?php echo $item->img; ?>" alt=""></a>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>