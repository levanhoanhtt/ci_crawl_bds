<?php require_once APPPATH.'/views/member/header.php'; ?>

<div class="search-page basic-div">
    <div class="container">
        <!-- <div class="field-search">
            <h2 class="field-search-title">Tìm kiếm cho:</h2>
            <ul>
                <li><i class=""></i> Từ khóa: "abc"</li>
                <li>Lĩnh vực: "Giày"</li>
                <li>Khu vực: "Hà Nội"</li>
            </ul>
        </div> -->
        <div class="row">
            <div class="item-col col-xs-12 col-sm-8 col-md-8">
                <div id="mymap"></div>
                <div class="on-directions">
                    <input id="direction-source" type="text" class="hidden" placeholder="Điểm đi" />
                    <input id="direction-destination" type="text"  class="hidden" placeholder="Điểm đến" />
                    <select id="direction-mode" class="form-control">
                        <option value="DRIVING">Lái xe</option>
                        <option value="WALKING">Đi bộ</option>
                        <option value="TRANSIT">Xe buýt</option>
                    </select>
                </div>
            </div>
            <div class="item-col col-xs-12 col-sm-4 col-md-4">
                <div class="search-result">
                    <h4 class="list-result-title">Danh sách kết quả (<?php echo $count_business; ?>)</h4>
                    <?php 
                    if($list_business){
                        ?>                        
                        <div class="list-place visible">
                            <?php 
                            foreach ($list_business as $item) {
                                $list_review = $item['list_review'];
                                ?>
                                <div class="item-place" data-icon="<?php echo $item['address']['icon_link']; ?>">
                                    <div class="info-place" data-latitude="<?php echo $item['address']['latitude']; ?>" data-longitude="<?php echo $item['address']['longitude']; ?>">
                                        <div class="item-place-img">
                                            <a href="<?php echo base_url().'doanh-nghiep/'.$item['business_id'].'-'.$item['business_slug']; ?>">
                                                <img src="<?php echo base_url().$item['thumbnail']; ?>" alt="<?php echo $item['business_name']; ?>">
                                            </a>
                                        </div>
                                        <div class="item-place-info">
                                            <h4 class="item-place-name">
                                                <a href="<?php echo base_url().'doanh-nghiep/'.$item['business_id'].'-'.$item['business_slug']; ?>"><?php echo $item['business_name']; ?></a>
                                                <small><i>(<?php echo $item['address']['address_name']; ?>)</i></small>
                                            </h4>
                                            <div class="hide-on-map">
                                                <p class="rate-star">
                                                    <?php 
                                                    if(empty($list_review)){
                                                        ?>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <small>(chưa có đánh giá)</small>
                                                        <?php
                                                    }else{
                                                        $star = 0;
                                                        foreach ($list_review as $review) {
                                                            $star += $review['num_star'];
                                                        }
                                                        $star = ceil($star/count($list_review));
                                                        for ($i=1; $i <=5 ; $i++) { 
                                                            ?>
                                                            <i class="fa <?php echo ($i<=$star)?'fa-star':'fa-star-o'; ?>"></i>
                                                            <?php
                                                        }
                                                        ?>
                                                       <small>(<?php echo count($list_review); ?> đánh giá)</small>
                                                        <?php
                                                    }
                                                    ?>
                                                </p>
                                                <i class="fa fa-map-marker"></i> <?php echo $item['address']['address_detail']; ?>
                                            </div>
                                            <ul>
                                                <li class="rate-star">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <a href="<?php echo base_url().'doanh-nghiep/'.$item['business_id'].'-'.$item['business_slug']; ?>#rate-now"><small>(20 đánh giá)</small></a>
                                                </li>
                                                <li><i class="fa fa-cube"></i> <?php echo $item['career']['career_name']; ?> </li>
                                                <li><i class="fa fa-map-marker"></i> <?php echo $item['address']['address_detail']; ?></li>
                                                <li><i class="fa fa-phone"></i> <?php echo $item['phone']; ?></li>
                                                <li><i class="fa fa-fax"></i> <?php echo $item['fax']; ?></li>
                                                <li><i class="fa fa-envelope-o"></i> <?php echo $item['email_address']; ?></li>
                                                <li><i class="fa fa-users"></i> <?php echo $item['number_people']; ?></li>
                                            </ul>
                                            
                                            <div class="more-info-link">
                                                <a href="<?php echo base_url().'doanh-nghiep/'.$item['business_id'].'-'.$item['business_slug']; ?>">Chi tiết</a>
                                                <a href="#" class="direction pull-right">Chỉ đường</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }else{
                        ?>
                        <div style="margin-top: 10px">Không có két quả phù hợp!</div>
                        <?php
                    }
                    ?>
                </div>
                <div class="navigation">                        
                    <?php
                    $current_url = explode("?", $current_url);
                    $cur= "";
                    for ($i=1; $i < count($current_url); $i++) { 
                        $cur .= "?".$current_url[$i];
                    }
                    if($current_page>1){
                        $prev = $current_page-1;
                        ?>
                        <a href="<?php echo base_url()."tim-kiem-doanh-nghiep/".$prev.$cur; ?>"><i class="fa fa-angle-double-left"></i></a>
                        <?php
                    }
                    for($i=1;$i<=$total_page;$i++){
                        ?>
                        <a href="<?php echo base_url()."tim-kiem-doanh-nghiep/".$i.$cur; ?>" class="<?php echo ($i==$current_page)?'active':false; ?>">
                            <?php echo $i; ?>
                        </a>
                        <?php
                    }
                    if($current_page<$total_page){
                        $next = $current_page+1;
                        ?>
                        <a href="<?php echo base_url()."tim-kiem-doanh-nghiep/".$next.$cur; ?>"><i class="fa fa-angle-double-right"></i></a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if(!empty($current_center_map)){
    $tp_name = $current_center_map["name"];
    $zoom = 10;
}else{
    $tp_name = "Đà Nẵng";
    $zoom = 6;
}
?>
<script>
    var address = "<?php echo $tp_name; ?>";
    var m_zoom = parseInt("<?php echo $zoom; ?>");
    address = address.replace(" ", "+");
    var geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' + address + '&&key=AIzaSyAT_j6hTG_ca5pWwFViCMv1I-HfjOwCNCc');
    var output = JSON.parse(geocode),
        m_lat = output.results[0].geometry.location.lat,
        m_lng = output.results[0].geometry.location.lng;

    var map;
    var directionsDisplay;
    var directionsService;
    function initMap() {
        var myLatLng = {lat: m_lat, lng: m_lng};
        // Create a map object and specify the DOM element
        // for display.
        map = new google.maps.Map(document.getElementById('mymap'), {
          center: myLatLng,
          zoom: m_zoom
        });
        add_marker(map);

        // Tạo marker
        var infoWindows = [];
        function add_marker(map) {
            $('.item-place').each(function () {
                var lat = $(this).find(".info-place").data('latitude');
                var long = $(this).find(".info-place").data('longitude');
                var this_icon = $(this).data('icon');
                var name = $(this).html();
                var shop =[{
                        position: new google.maps.LatLng(lat, long)
                    }];
                var icons = {
                    icon: this_icon
                };
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, long),
                    map: map,
                    icon: icons.icon
                });
                google.maps.event.addListener(marker, 'click', function() {
                    var infowindow = new google.maps.InfoWindow({
                        content: `<div class="item-place">`+name+`</div>`
                    });
                    infowindow.open(map,marker);
                    for (var i=0;i<infoWindows.length;i++) {
                        infoWindows[i].close();
                    }
                    infoWindows.push(infowindow);
                });
            });
        }

        //DirectionsService - tính toán chỉ đường cho chúng ta.
        directionsService = new google.maps.DirectionsService();    
        //DirectionsRenderer - hiển thị chỉ đường trên bản đồ sau khi đã tính toán.
        directionsDisplay = new google.maps.DirectionsRenderer({map: map});  
        jQuery("body").on("change","#direction-source, #direction-destination, #direction-mode", function(){
            calculateAndDisplayRoute(directionsService, directionsDisplay); 
        });


        // Tính toán chỉ đường
        function calculateAndDisplayRoute(directionsService, directionsDisplay) { 
            // hàm route của DirectionsService sẽ thực hiện tính toán với các tham số truyền vào   
            directionsService.route({    
                origin: jQuery('#direction-source').val(),    // điểm xuất phát
                destination: jQuery('#direction-destination').val(),    // điểm đích
                travelMode: jQuery('#direction-mode').val(),     // phương tiện di chuyển
            }, function(response, status) {
                // response trả về bao gồm tất cả các thông tin về chỉ đường    
                if (status === google.maps.DirectionsStatus.OK) {    
                    directionsDisplay.setDirections(response); 
                    // hiển thị chỉ đường trên bản đồ (nét màu đỏ từ A-B)
                } else {    
                    // Hiển thị lỗi
                    // console.log("Error");
                }    
            });    
        }  
        jQuery("body").on("click",".direction, .search-result .info-place",function(e){
            e.preventDefault();
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
                var lat = jQuery(this).parents(".item-place").find(".info-place").data("latitude");
                var lng = jQuery(this).parents(".item-place").find(".info-place").data("longitude");
                var address_json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key=AIzaSyAT_j6hTG_ca5pWwFViCMv1I-HfjOwCNCc');
                var address = JSON.parse(address_json).results[1].formatted_address;
                jQuery("#direction-destination").val(address);
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            } else { 
                x.innerHTML = "Trình duyệt không hỗ trợ định vị.";
            }
        });

        function showPosition(position) {
            var lat = position.coords.latitude,
                lng = position.coords.longitude;
            var address_json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key=AIzaSyAT_j6hTG_ca5pWwFViCMv1I-HfjOwCNCc');
            var address = JSON.parse(address_json).results[1].formatted_address;
            jQuery("#direction-source").val(address);
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        }
    }
</script>
<?php require_once APPPATH.'/views/member/footer.php'; ?>