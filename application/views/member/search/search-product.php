<?php require_once APPPATH.'/views/member/header.php'; ?>

<div class="search-title">
	<div class="container">
		<h2 class="cate-title">Tìm kiếm sản phẩm: "<?php echo $key; ?>"</h2>
	</div>
</div>

<div class="main-page-cate basic-div">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<?php 
				if(isset($all_pro) && !empty($all_pro)){
					?>
					<div class="list-news list-item-last list-no-col">
						<?php
						foreach ($all_pro as $item) {
							?>
							<div class="item-new">
								<div class="row">
									<div class="col-xs-12 col-sm-4">
										<div class="item-new-img">
					                        <a href="<?php echo base_url().'san-pham/'.$item['pro_id'].'-'.$item['pro_slug']; ?>">
					                        	<img src="<?php echo base_url().$item['thumbnail']; ?>" alt="<?php echo $item['pro_name']; ?>">
					                        </a>
					                    </div>
									</div>
									<div class="col-xs-12 col-sm-8">
										<div class="item-new-info">
					                        <h4 class="item-new-title">
					                        	<a href="<?php echo base_url().'san-pham/'.$item['pro_id'].'-'.$item['pro_slug']; ?>"><?php echo $item['pro_name']; ?></a>
					                        </h4>
					                        <p><?php echo $item['excerpt']; ?></p>
					                        <a href="<?php echo base_url().'san-pham/'.$item['pro_id'].'-'.$item['pro_slug']; ?>" class="read-more">Chi tiết <i class="fa fa-angle-double-right"></i></a>
					                    </div>
									</div>
								</div>
			                </div>
							<?php
						}
						?>
					</div>
					<div class="navigation">                        
	                    <?php
	                    $current_url = explode("?", $current_url);
	                    $cur= "";
	                    for ($i=1; $i < count($current_url); $i++) { 
	                        $cur .= "?".$current_url[$i];
	                    }
	                    if($current_page>1){
	                        $prev = $current_page-1;
	                        ?>
	                        <a href="<?php echo base_url()."tim-kiem-san-pham/".$prev.$cur; ?>"><i class="fa fa-angle-double-left"></i></a>
	                        <?php
	                    }
	                    for($i=1;$i<=$total_page;$i++){
	                        ?>
	                        <a href="<?php echo base_url()."tim-kiem-san-pham/".$i.$cur; ?>" class="<?php echo ($i==$current_page)?'active':false; ?>">
	                            <?php echo $i; ?>
	                        </a>
	                        <?php
	                    }
	                    if($current_page<$total_page){
	                        $next = $current_page+1;
	                        ?>
	                        <a href="<?php echo base_url()."tim-kiem-san-pham/".$next.$cur; ?>"><i class="fa fa-angle-double-right"></i></a>
	                        <?php
	                    }
	                    ?>
	                </div>
					<?php
				}else{
					?>
					<div class="alert alert-warning">Không tìm thấy kết quả phù hợp</div>
					<?php
				}
				?>
				
			</div>
			<div class="col-xs-12 col-sm-4">
				<?php require_once APPPATH.'/views/member/sidebar.php'; ?>
			</div>
		</div>
	</div>
</div>

<?php require_once APPPATH.'/views/member/footer.php'; ?>