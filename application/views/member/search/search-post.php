<?php require_once APPPATH.'/views/member/header.php'; ?>

<div class="search-title">
	<div class="container">
		<h2 class="cate-title">Tìm kiếm cho: "<?php echo $key; ?>"</h2>
	</div>
</div>

<div class="main-page-cate basic-div">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<?php 
				if(isset($all_post) && !empty($all_post)){
					?>
					<div class="list-news list-item-last list-no-col">
						<?php
						foreach ($all_post as $item) {
							?>
							<div class="item-new">
								<div class="row">
									<div class="col-xs-12 col-sm-4">
										<div class="item-new-img">
					                        <a href="<?php echo base_url().'bai-viet/'.$item['post_id'].'-'.$item['post_slug']; ?>">
					                        	<img src="<?php echo base_url().$item['thumbnail']; ?>" alt="<?php echo $item['post_title']; ?>">
					                        </a>
					                    </div>
									</div>
									<div class="col-xs-12 col-sm-8">
										<div class="item-new-info">
					                        <h4 class="item-new-title">
					                        	<a href="<?php echo base_url().'bai-viet/'.$item['post_id'].'-'.$item['post_slug']; ?>"><?php echo $item['post_title']; ?></a>
					                        </h4>
					                        <div class="item-new-meta">
					                        	<small><i class="fa fa-clock-o"></i> <?php echo date("d/m/Y", $item['timestamp']); ?></small>
					                        </div>
					                        <p><?php echo $item['excerpt']; ?></p>
					                        <a href="<?php echo base_url().'bai-viet/'.$item['post_id'].'-'.$item['post_slug']; ?>" class="read-more">Chi tiết <i class="fa fa-angle-double-right"></i></a>
					                    </div>
									</div>
								</div>
			                </div>
							<?php
						}
						?>
					</div>
					<?php
				}
				?>
				<div class="navigation">                        
                    <?php
                    $current_url = explode("?", $current_url);
                    $cur= "";
                    for ($i=1; $i < count($current_url); $i++) { 
                        $cur .= "?".$current_url[$i];
                    }

                    if($current_page>1){
                        $prev = $current_page-1;
                        ?>
                        <a href="<?php echo base_url()."tim-kiem-tin-tuc/".$prev.$cur; ?>"><i class="fa fa-angle-double-left"></i></a>
                        <?php
                    }
                    for($i=1;$i<=$total_page;$i++){
                        ?>
                        <a href="<?php echo base_url()."tim-kiem-tin-tuc/".$i.$cur; ?>" class="<?php echo ($i==$current_page)?'active':false; ?>">
                            <?php echo $i; ?>
                        </a>
                        <?php
                    }
                    if($current_page<$total_page){
                        $next = $current_page+1;
                        ?>
                        <a href="<?php echo base_url()."tim-kiem-tin-tuc/".$next.$cur; ?>"><i class="fa fa-angle-double-right"></i></a>
                        <?php
                    }
                    ?>
                </div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<?php require_once APPPATH.'/views/member/sidebar.php'; ?>
			</div>
		</div>
	</div>
</div>

<?php require_once APPPATH.'/views/member/footer.php'; ?>