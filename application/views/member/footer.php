<?php
$footer_info = $this->get_data->options_info("footer");
?>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <h3 class="t-f"><?php echo $footer_info->area1_title; ?></h3>
                <?php echo $footer_info->area1; ?>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <h3 class="t-f"><?php echo $footer_info->area2_title; ?></h3>
                <?php echo $footer_info->area2; ?>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <h3 class="t-f"><?php echo $footer_info->area3_title; ?></h3>
                <?php echo $footer_info->area3; ?>
            </div>
        </div>
    </div>
</div>
<!-- end footer -->

<script type="text/javascript" src="<?php echo base_url('assets/sites/js/jquery-1.9.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/sites/js/owl.carousel.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/sites/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/sites/js/fancybox.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/sites/js/fancybox-thumbs.js'); ?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/sites/js/script.js?ver='.rand()); ?>"></script>

<script type="text/javascript">
            jQuery('.datepicker').datepicker({
                language: 'es',
                format: 'yyyy-mm-dd',
                autoclose: 1,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'top'
                },
                "setDate": new Date(),
                "autoclose": true,
                todayBtn: true,
                todayHighlight: true
            });
        </script>
        
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.bd-show').click(function(){
        console.log('jhsdjh');
        jQuery('.bd-hide').slideToggle();
    });
        jQuery('.bd-fix2').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            jQuery('.login-btn').click();
        }
        event.stopPropagation();
    });
     
    jQuery('.bd').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            jQuery('.btn-register').click();
        }
    });



        jQuery('.data_table').DataTable(
            {
                "language": {
                    "lengthMenu": "Hiển thị _MENU_ dòng trên trang",
                    "zeroRecords": "Không tìm thấy dữ liệu",
                    "info": "Hiển thị _PAGE_ của _PAGES_",
                    "search": "Tìm kiếm:",
                    "paginate": {
                        "previous": "Trước",
                        "next": "Sau"

                    }
                }
            }
        );

        jQuery('#province_id').on('change', function(){
            var province_id = jQuery('#province_id').val();
            jQuery.ajax(
                {
                    url: "<?php echo base_url('admin/post/load_district'); ?>",
                    type: "POST",
                    data: {province_id: province_id},
                    success: function(resp){
                        var district = JSON.parse(resp);
                        var option = '<option value="0">Chọn Quận/Huyện</option>';
                        var selected = '';
                        if (Array.isArray(district))
                        {
                            for (var i = 0; i<district.length - 1; i++)
                            {
                                var district_item = district[i];

                                option = option+'<option value="'+district_item.maqh+'">'+district_item.name+'</option>';
                            }
                        }

                        jQuery('#district_id').html(option);
                    }
                }
            );
        });

        jQuery('.save_post').on('click', function(){

            <?php
            if ($this->get_data->is_user_login()):
            ?>
            var post_id = jQuery(this).attr('data-post-id');

            jQuery.ajax(
                {
                    url: "<?php echo base_url('post_type/save_post'); ?>",
                    type: "POST",
                    data: {post_id: post_id},
                    success: function(resp){
                        var resp_oj = JSON.parse(resp);
                        if (resp_oj.status==2)
                        {
                        	if (confirm('Lưu tin thành công! Bạn có muốn xem danh sách lưu tin?'))
                        	{
                        		window.location.href='<?php echo base_url('tin-da-luu'); ?>';
                        	}
                            
                        }
                        else if (resp_oj.status==1)
                        {
                            alert('Lưu tin không thành công. Vui lòng thử lại');
                        }
                        else
                        {
                            alert('Tin này đã bị trùng! Vui lòng lưu tin khác');
                        }
                    }
                }
            );

            <?php else: ?>
            alert('Vui lòng đăng nhập để thực hiện chức năng này!');
            <?php endif; ?>
            return false;
        });

        jQuery('.delete_post').on('click', function(){

            <?php
            if ($this->get_data->is_user_login()):
            ?>
            var post_id = jQuery(this).attr('data-post-id');

            if (confirm('Bạn có chắc chắn muốn xóa?'))
            {
                jQuery.ajax(
                    {
                        url: "<?php echo base_url('post_type/delete_post'); ?>",
                        type: "POST",
                        data: {post_id: post_id},
                        success: function(resp){
                            var resp_oj = JSON.parse(resp);
                            if (resp_oj.status==1)
                            {
                                alert('Xóa tin thành công');
                                window.location.reload();
                            } else
                            {
                                alert('Xóa tin không thành công! Vui lòng thử lại.');
                            }
                        }
                    }
                );

            }

            <?php else: ?>
            alert('Vui lòng đăng nhập để thực hiện chức năng này!');
            <?php endif; ?>
            return false;
        });

        jQuery('.post-title').on('click', function(){
            <?php
            if ($this->get_data->is_user_login()):
            ?>
            var post_id = jQuery(this).attr('data-post-id');

            jQuery.ajax(
                {
                    url: "<?php echo base_url('post_type/read_post'); ?>",
                    type: "POST",
                    data: {post_id: post_id},
                    success: function(resp){
                        console.log(resp);
                        var resp_oj = JSON.parse(resp);
                        if (resp_oj.status == 1)
                        {
                            jQuery('.post-'+post_id).addClass('read');
                        }
                    }
                }
            );

            <?php else: ?>
            alert('Vui lòng đăng nhập để thực hiện chức năng này!');
            <?php endif; ?>

        });

        jQuery('.login-btn').on('click', function(){

            var username = jQuery('.username').val();
            var password = jQuery('.password').val();
            if (username.trim()!='' && password.trim()!='')
            {
                jQuery.ajax(
                    {
                        url: "<?php echo base_url('account/login'); ?>",
                        type: "POST",
                        data: {username: username, password: password},
                        success: function(resp){
                            var resp_oj = JSON.parse(resp);
                            if (resp_oj.status==2)
                            {
                                alert('Đăng nhập thành công! Hệ thống sẽ tự động chuyển hướng');
                                window.location.reload();
                            }
                            else if (resp_oj.status==3) {
                                alert('Tài khoản này đang đăng nhập ở nơi khác')
                            }
                            else if (resp_oj.status==1)
                            {
                                alert('Tên đăng nhập hoặc mật khẩu không chính xác');
                            }
                            else
                            {
                                alert('Vui lòng nhập đầy đủ thông tin đăng nhập');
                            }
                        }
                    }
                );
            }
            else
            {
                alert('Vui lòng nhập đầy đủ thông tin đăng nhập');
            }

        });

        jQuery('.btn-forget').on('click', function(){
            var email = jQuery(this).parents('.form-forget').find('.email_address').val();

            if (email.trim()!='')
            {
                jQuery.ajax(
                    {
                        url: "<?php echo base_url('account/forget_password'); ?>",
                        type: "POST",
                        data: {email_address: email},
                        success: function(resp){
                            var resp_oj = JSON.parse(resp);
                            if (resp_oj.status==2)
                            {
                                alert('Hệ thống đã gửi 1 email cho bạn! Bạn vui lòng kiểm tra email.');
                            }
                            else if (resp_oj.status==1)
                            {
                                alert('Email bạn nhập không tồn tại trong hệ thống');
                            }
                            else
                            {
                                alert('Vui lòng nhập địa chỉ email');
                            }
                        }
                    }
                );
            }
            else
            {
                alert('Vui lòng nhập địa chỉ email');
            }
        });

        jQuery('.btn-register').on('click', function(){
            var fullname = jQuery(this).parents('.form-register').find('.fullname').val();
            var email_address = jQuery(this).parents('.form-register').find('.email_address').val();
            var user_phone = jQuery(this).parents('.form-register').find('.user_phone').val();
            var user_address = jQuery(this).parents('.form-register').find('.user_address').val();
            var username = jQuery(this).parents('.form-register').find('.username').val();
            var password = jQuery(this).parents('.form-register').find('.password').val();
            var confirm_password = jQuery(this).parents('.form-register').find('.confirm_password').val();

            jQuery.ajax(
                {
                    url: "<?php echo base_url('account/register'); ?>",
                    type: "POST",
                    data: {
                        fullname: fullname,
                        email_address: email_address,
                        user_phone: user_phone,
                        user_address: user_address,
                        username: username,
                        password: password,
                        confirm_password: confirm_password
                    },
                    success: function(resp){

                        var resp_oj = JSON.parse(resp);
                        console.log(resp_oj);
                        if (resp_oj.status==2)
                        {
                            alert('Đăng ký tài khoản thành công! Bạn có thể đăng nhập ngay bây giờ!');
                            window.location.reload();
                        }
                        else if (resp_oj.status==1)
                        {
                            alert('Đăng ký tài khoản không thành công! Vui lòng thử lại');
                        }
                        else
                        {
                            jQuery('.fullname').html(resp_oj.errors.fullname);
                            jQuery('.email_address').html(resp_oj.errors.email_address);
                            jQuery('.password').html(resp_oj.errors.password);
                            jQuery('.user_phone').html(resp_oj.errors.user_phone);
                            jQuery('.username').html(resp_oj.errors.username);
                            jQuery('.confirm_password').html(resp_oj.errors.confirm_password);
                            jQuery('.user_address').html(resp_oj.errors.user_address);
                        }
                    }
                }
            );

        });


    });
    

    $(document).ready(function() {
        
        for(i = 0; i < 50; i++)
        {


            $('[data-fancybox="gallery'+i+'"]').fancybox({
                prevEffect  : 'none',
                nextEffect  : 'none',
                
                helpers : {
                    title   : {
                        type: 'outside'
                    },
                    thumbs  : {
                        width   : 50,
                        height  : 50
                    }
                }
            });
        }
    });
</script>
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
 -->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancybox/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
 --><!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/fancybox/fancybox/jquery.fancybox-1.3.4.pack.js"></script> -->
<script type="text/javascript">
    // jQuery(document).ready(function() {

    //   var i= jQuery('#mytable .title').html();
    //   var y= i.length;
    //   console.log(y);
    //   var splitted = i.split(" ", 24);

    //   splitted= splitted.join(' ');
    //   var a= splitted.length
    //   console.log(a);

    //   if (y>161) {
    //     jQuery('#mytable .title').html(splitted + '...');
    //   }
     
    // });
</script>

</body>
</html>
