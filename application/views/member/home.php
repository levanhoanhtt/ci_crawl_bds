<?php
require_once APPPATH.'/views/member/header.php';
require_once APPPATH.'/views/member/block/slideshow.php';
?>
<?php echo validation_errors(); ?>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 ">
                <?php  
                    $data_home = json_decode($home_option['opt_value'] , true);

                ?>
                <!-- main content -->
                <?php echo $data_home['main-content']; ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <?php require_once APPPATH.'/views/member/sidebar.php'; ?>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jumbotron sale">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 main_banner_content">
                        <?php echo $data_home['main_banner_content'] ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 banner_pricetext">
                        <?php echo $data_home['banner_pricetext']; ?>
                        <a href="<?php echo $data_home['banner_link'] ?>" class="btn btn-dk">Đăng ký dùng thử</a>
                    </div>
                </div>
            </div>
            
        </div>



        <!-- Modal Dùng Thử -->
        <div class="modal fade" id="register_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">ĐĂNG KÝ DÙNG THỬ</h4>
                    </div>
                    <form method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">Họ và tên</label>
                                <input type="text" name="your_name" class="form-control" placeholder="Họ và tên..." required/>
                            </div>
                            <div class="form-group">
                                <label for="">Số điện thoại</label>
                                <input type="tel" name="your_phone" class="form-control" placeholder="Số điện thoại..." required/>
                            </div>
                            <div class="form-group">
                                <label for="">Lĩnh vực</label>
                                <select class="form-control" name="your_type" required>
                                    <option value="Môi giới nhà thổ cư">Môi giới nhà thổ cư</option>
                                    <option value="Môi giới cho thuê">Môi giới cho thuê</option>
                                    <option value="Môi giới nhà dự án">Môi giới nhà dự án</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Đăng ký ngay</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-forget-user">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Quên mật khẩu</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST" role="form" class="form-forget">

                            <div class="form-group">
                                <label for="fullname">Địa chỉ email</label>
                                <input type="email" class="form-control email_address" name="email_address" placeholder="Địa chỉ email...">
                            </div>

                            <button type="button" class="btn btn-primary text-center btn-forget">Xác nhận</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
       
        <div class="modal fade" id="modal-register-user">
            <div class="modal-dialog">
                <div class="modal-content " style="max-height: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Đăng ký thành viên</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST" role="form" class="form-register">

                            <div class="form-group">
                                <label for="fullname">Họ và tên</label>
                                <input type="text" class="form-control fullname bd" name="fullname" placeholder="Họ và tên...">
                                <p class="fullname" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="email_address">Địa chỉ email</label>
                                <input type="text" class="form-control email_address bd" name="email_address" placeholder="Địa chỉ email...">
                                <p class="email_address" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="user_phone">Số điện thoại</label>
                                <input type="text" class="form-control user_phone bd" name="user_phone"  placeholder="Số điện thoại...">
                                <p class="user_phone" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="user_address">Địa chỉ</label>
                                <input type="text" class="form-control user_address bd" name="user_address" placeholder="Địa chỉ...">
                                <p class="user_address" style="color: red;"></p>
                            </div>
                            <hr/>
                            <div class="form-group ">
                                <label for="re_username">Tên đăng nhập</label>
                                <input type="text" class="form-control username bd" name="username" placeholder="Tài khoản...">
                                <p class="username" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="pw1">Mật khẩu</label>
                                <input type="password" class="form-control password bd" name="password" placeholder="Mật khẩu...">
                                <p class="password" style="color: red;"></p>
                            </div>

                            <div class="form-group">
                                <label for="pw1">Nhập lại mật khẩu</label>
                                <input type="password" class="form-control confirm_password bd" name="confirm_password" placeholder="Nhập lại mật khẩu...">
                                <p class="confirm_password" style="color: red;"></p>
                            </div>

                            <button type="button" class="btn btn-primary text-center btn-register">Đăng ký</button>

                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end maincontet -->
<?php require_once APPPATH.'/views/member/footer.php'; ?>