<?php require_once APPPATH.'/views/admin/header.php'; ?>
<section class="panel">
	<?php 
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}

	if (validation_errors()!='')
	{
		?>
		<div class="alert alert-danger"><?php echo validation_errors(); ?></div>
		<?php
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title"><?php echo $page_title; ?></h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
				<div class="col-sm-8 col-md-8">
					<input type="hidden" name="id" class="form-control" id="id">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="user_id">Tên người dùng</label>
				<div class="col-sm-8 col-md-8">
					<select class="form-control user_id" id="user_id" name="user_id" >
						<?php if ($list_users): foreach ($list_users as $item): ?>
							<option value="<?php echo $item['user_id']; ?>"><?php echo $item['fullname']; ?></option>
						<?php endforeach; endif; ?>	
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="amount">Số tiền</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="amount" class="form-control" id="amount" placeholder="Số tiền">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="content">Nội dung</label>
				<div class="col-sm-8 col-md-8">
					<textarea class="form-control content" id="content" name="content" rows="4" placeholder="Nội dung..." ></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Thêm</button>
					<a href="<?php echo base_url() ?>admin/user_payments" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>