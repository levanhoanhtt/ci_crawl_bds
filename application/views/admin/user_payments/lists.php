<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title"> <?php echo $page_title; ?> </h2>
	</header>
	<div class="panel-body">
		<div class="add-new text-right">
			<a href="<?php echo base_url() ?>admin/user_payments/export_user" class="btn btn-custom">Xuất Excel</a>
			<a href="<?php echo base_url() ?>admin/user_payments/add" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
		</div>
		<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="text-center">STT</th>
					<th class="text-center">Tên người dùng</th>
					<th class="text-center">Số tiền</th>
					<th class="text-center">Ngày tạo</th>
					<th class="text-center">Nội dung</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</thead>
			<tbody>
				<?php
                $count = 0;
				foreach ($all_user_payments as $item) {
				    $count++;
					?>
					<tr>
						<td class="text-center"><?php echo $count; ?></td>
						<td class="text-center"><?php echo $this->get_data->get_user_name($item["user_id"]) ?></td>
						<td class="text-center"><?php echo $item["amount"]; ?></td>
						<td class="text-center"><?php echo $item["create_at"]; ?></td>
						<td class="text-center"><?php echo $item["content"]; ?></td>
						<td class="text-center table-action">
							<a href="<?php echo base_url() ?>admin/user_payments/del/<?php echo $item['id']; ?>" onclick="return confirm('Xóa người dùng này?');"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th class="text-center">STT</th>
					<th class="text-center">Tên người dùng</th>
					<th class="text-center">Số tiền</th>
					<th class="text-center">Ngày tạo</th>
					<th class="text-center">Nội dung</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</tfoot>
		</table>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>