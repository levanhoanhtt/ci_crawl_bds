				</section>
			</div>
		</section>

		<script type="text/javascript">
			var home = "<?php echo base_url(); ?>";
		</script>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/flot/jquery.flot.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/raphael/raphael.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/morris/morris.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/gauge/gauge.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/ios7-switch/ios7-switch.js"></script>

		<!-- Specific Page Vendor -->
		<script src="<?php echo base_url(); ?>assets/vendor/select2/select2.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/js/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/js/theme.init.js"></script>

		<!-- Datatable -->
		<script src="<?php echo base_url(); ?>assets/js/tables/examples.datatables.default.js"></script>

		<!-- Menu -->
		<script src="<?php echo base_url(); ?>assets/vendor/jquery-nestable/jquery.nestable.js"></script>
		
		<!-- Modals -->
		<script src="<?php echo base_url(); ?>assets/js/ui-elements/examples.modals.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>

        <script type="text/javascript">
            jQuery('.datepicker').datepicker({
                language: 'es',
                format: 'yyyy-mm-dd',
                autoclose: 1,
                widgetPositioning: {
                    horizontal: 'right',
                    vertical: 'top'
                },
                "setDate": new Date(),
        		"autoclose": true,
        		todayBtn: true,
           	 	todayHighlight: true
            });
        </script>

		<script type="text/javascript">
			(function( $ ) {
				'use strict';
				var datatableInit = function() {
					$('#no-sort-dttb').dataTable({
						"ordering": false
					});
				};
				$(function() {
					datatableInit();
				});
			}).apply( this, [ jQuery ]);
		</script>

		
		<script src="<?php echo base_url(); ?>assets/js/file_get_contents.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/admin.js?ver=<?php echo rand(); ?>"></script>
		<script src="<?php echo base_url(); ?>assets/js/together.js"></script>
                <script type="text/javascript">
                    jQuery(document).ready(function () {

                        var province_id = jQuery('#province_id').val();
                        jQuery.ajax(
                            {
                                url: "<?php echo base_url('admin/post/load_district'); ?>",
                                type: "POST",
                                data: {province_id: province_id},
                                success: function(resp){
                                    var district = JSON.parse(resp);
                                    var option = '<option value="0">Chọn Quận/Huyện</option>';
                                    var selected = '';

                                    if (Array.isArray(district))
                                    {
                                        for (var i = 0; i<district.length - 1; i++)
                                        {
                                            var district_item = district[i];
                                            if (typeof district_id_selected != "undefined" && district_id_selected==district_item.maqh)
                                            {
                                                selected = 'selected';
                                            }
                                            else
                                            {
                                                selected = '';
                                            }
                                            option = option+'<option '+selected+' value="'+district_item.maqh+'">'+district_item.name+'</option>';
                                        }
                                    }

                                    jQuery('#district_id').html(option);
                                }
                            }
                        );

                        jQuery('#province_id').on('change', function(){
                            var province_id = jQuery('#province_id').val();
                            jQuery.ajax(
                                {
                                    url: "<?php echo base_url('admin/post/load_district'); ?>",
                                    type: "POST",
                                    data: {province_id: province_id},
                                    success: function(resp){
                                        var district = JSON.parse(resp);
                                        var option = '<option value="0">Chọn Quận/Huyện</option>';
                                        var selected = '';
                                        if (Array.isArray(district))
                                        {
                                            for (var i = 0; i<district.length - 1; i++)
                                            {
                                                var district_item = district[i];
                                                if (typeof district_id_selected != "undefined" && district_id_selected==district_item.maqh)
                                                {
                                                    selected = 'selected';
                                                }
                                                else
                                                {
                                                    selected = '';
                                                }
                                                option = option+'<option '+selected+' value="'+district_item.maqh+'">'+district_item.name+'</option>';
                                            }
                                        }

                                        jQuery('#district_id').html(option);
                                    }
                                }
                            );
                        });
                    });
                </script>
	</body>
</html>