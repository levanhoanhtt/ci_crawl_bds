<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}

	if (validation_errors()!='')
	{
		?>
		<div class="alert alert-danger"><?php echo validation_errors(); ?></div>
		<?php
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Thêm mới người dùng</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="username">Tên đăng nhập</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="username" class="form-control" id="username" value="<?php echo ($data_post)?$data_post['username']:false; ?>" placeholder="Tên đăng nhập">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="pw1">Mật khẩu</label>
				<div class="col-sm-8 col-md-8">
					<input type="password" name="pw1" class="form-control" id="pw1" placeholder="Mật khẩu">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="fullname">Họ và tên</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="fullname" class="form-control" id="fullname" value="<?php echo ($data_post)?$data_post['fullname']:false; ?>" placeholder="Họ và tên">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="email_address">Địa chỉ email</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="email_address" class="form-control" id="email_address" value="<?php echo ($data_post)?$data_post['email_address']:false; ?>" placeholder="Địa chỉ email">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="user_phone">Số điện thoại</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="user_phone" class="form-control" id="user_phone" value="<?php echo ($data_post)?$data_post['user_phone']:false; ?>" placeholder="Số điện thoại">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="address">Địa chỉ</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="address" class="form-control" id="address" value="<?php echo ($data_post)?$data_post['user_address']:false; ?>" placeholder="Địa chỉ">
				</div>
			</div>
			<!-- <div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="vip_user">Người dùng Vip</label>
				<div class="col-sm-8 col-md-8">
					<div class="switch switch-sm switch-success">
						<input type="checkbox" name="vip_user" data-plugin-ios-switch="" <?php echo ($data_post && $data_post['vip_user'])?"checked":false; ?> style="display: none;">
					</div>
				</div>
			</div> -->

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="user_type">Loại tài khoản</label>
				<div class="col-sm-8 col-md-8">
					<select name="user_type" class="form-control" id="user_type">
						<option value="0">Tài khoản thường</option>
						<option value="1">Tài khoản VIP</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Kích hoạt</label>
				<div class="col-sm-8 col-md-8">
					<div class="switch switch-sm switch-success">
						<input type="checkbox" name="active" data-plugin-ios-switch="" <?php echo ($data_post && $data_post["active"])?"checked":false; ?> style="display: none;">
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Thêm</button>
					<a href="<?php echo base_url() ?>admin/user" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>