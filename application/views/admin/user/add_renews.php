<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}

	if (validation_errors()!='')
	{
		?>
		<div class="alert alert-danger"><?php echo validation_errors(); ?></div>
		<?php
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Gia Hạn tài khoản</h2>
	</header>
	<div class="panel-body" style="-webkit-flex: 1; -ms-flex: 1;  -moz-flex: 1; flex: 1;  flex: 1;">
		<form class="form-horizontal form-bordered" method="post" >
			<div class="form-group">
				<input type="hidden" name="id">
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="user_id">Tên người dùng</label>
				<div class="col-sm-8 col-md-8">
					<select class="form-control" name="user_id" id="user_id" required>
						<?php if ($list_users): foreach ($list_users as $item): ?>
							<option value="<?php echo $item['user_id']; ?>" <?php echo ($current_id==$item['user_id'])?'selected':FALSE; ?>><?php echo $item['fullname']; ?></option>
						<?php endforeach; endif; ?>	
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="start_time">Từ ngày</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="start_time" id="" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="end_time">Đến ngày</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="end_time" id="" class="form-control datepicker" value="" required>
				</div>
			</div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="start_time">Nội dung</label>
                <div class="col-sm-8 col-md-8">
                    <textarea class="form-control" name="content" placeholder="Nhập ghi chú (Nếu có)..."></textarea>
                </div>
            </div>

			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Thêm</button>
					<a href="<?php echo base_url() ?>admin/user/renew_logs" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>