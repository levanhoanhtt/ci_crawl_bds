<?php require_once APPPATH.'/views/admin/header.php'; ?>
<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
    {
        ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <?php
    }
	
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Cập nhật người dùng</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="username">Tên đăng nhập</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="username" class="form-control" id="username" value="<?php echo $current_edit['username']; ?>" placeholder="Tên đăng nhập">
				</div>
			</div>
	
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="fullname">Họ và tên</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="fullname" class="form-control" id="fullname" value="<?php echo $current_edit['fullname']; ?>" placeholder="Họ và tên">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="email_address">Địa chỉ email</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="email_address" class="form-control" id="email_address" value="<?php echo $current_edit['email_address']; ?>" placeholder="Địa chỉ email">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="user_phone">Số điện thoại</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="user_phone" class="form-control num-only" id="user_phone" value="<?php echo $current_edit['user_phone']; ?>" placeholder="Số điện thoại">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="address">Địa chỉ</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="address" class="form-control" id="address" value="<?php echo $current_edit['user_address']; ?>" placeholder="Địa chỉ">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="pwd">Mật khẩu mới (Nhập nếu thay đổi mật khẩu)</label>
				<div class="col-sm-8 col-md-8">
					<input type="password" name="pwd" class="form-control" id="pwd" value="" placeholder="Nhập mật khẩu mới nếu thay đổi" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="user_type">Loại tài khoản</label>
				<div class="col-sm-8 col-md-8">
					<select name="user_type" class="form-control" id="user_type">
						<option value="0" <?php echo ($current_edit['user_type']==0)?"selected":false; ?>>Tài khoản thường</option>
						<option value="1" <?php echo ($current_edit['user_type']==1)?"selected":false; ?>>Tài khoản VIP</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Kích hoạt</label>
				<div class="col-sm-8 col-md-8">
					<div class="switch switch-sm switch-success">
						<input type="checkbox" name="active" data-plugin-ios-switch="" <?php echo ($current_edit['active']==1)?"checked":false; ?> style="display: none;">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Lưu</button>
					<a href="<?php echo base_url() ?>admin/user" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>