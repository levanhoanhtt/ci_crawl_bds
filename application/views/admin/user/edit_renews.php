<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
	{
		?>
		<div class="alert alert-danger"><?php echo validation_errors(); ?></div>
		<?php
	}
	
	?>
	<header class="panel-heading">
		<h2 class="panel-title"> <?php echo $page_title; ?> </h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="user_id">Tên người dùng</label>
				<div class="col-sm-8 col-md-8">
					<select class="form-control" name="user_id" id="user_id" required>
						<?php if ($list_users): foreach ($list_users as $item): ?>
							<option value="<?php echo $item['user_id']; ?>" <?php echo ($item['user_id'] == $current_edit['user_id']) ? 'selected' : FALSE ?> ><?php echo $item['fullname']; ?></option>
						<?php endforeach; endif; ?>	
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="start_time">Từ ngày</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" value=" <?php echo $current_edit['start_time'] ?> " name="start_time" id="" class="form-control datepicker"  required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="end_time">Đến ngày</label>
				<div class="col-sm-8 col-md-8">
					<input type="text"  value=" <?php echo $current_edit['end_time'] ?> "  name="end_time" id="" class="form-control datepicker" required>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Lưu</button>
					<a href="<?php echo base_url() ?>admin/user/renew_logs" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>