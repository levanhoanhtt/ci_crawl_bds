<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<!-- <?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	?> -->
	<header class="panel-heading">
		<h2 class="panel-title"> <?php echo $page_title ?> </h2>
	</header>
	<div class="panel-body">
		<div class="add-new text-right">
			<a href="<?php echo base_url() ?>admin/user/export_renew_log" class="btn btn-custom">Xuất Excel</a>
			<a href="<?php echo base_url() ?>admin/user/add_renews" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
		</div>
		<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="text-center">STT</th>
					<th class="text-center">Tên người dùng</th>
					<th class="text-center">Từ ngày</th>
					<th class="text-center">Đến ngày</th>
					<th class="text-center">Thời gian chỉnh sửa</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($renew_logs as $item ) { ?>
					<tr>
						<td class="text-center"> <?php echo $item['id'] ?> </td>
						<td class="text-center"> <?php echo $this->get_data->get_user_name($item['user_id']) ?> </td>
						<td class="text-center"> <?php echo $item['start_time'] ?> </td>
						<td class="text-center"> <?php echo $item['end_time'] ?> </td>
						<td class="text-center"> <?php echo $item['created_at'] ?> </td>
						<td class="text-center table-action">
							<a href="<?php echo base_url() ?>admin/user/edit_renews/<?php echo $item['id']; ?>"><i class="fa fa-edit"></i></a>
							<a href="<?php echo base_url() ?>admin/user/del_renews/<?php echo $item['id']; ?>" onclick="return confirm('Xóa người dùng này?');"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<th class="text-center">STT</th>
					<th class="text-center">Tên người dùng</th>
					<th class="text-center">Từ ngày</th>
					<th class="text-center">Đến ngày</th>
					<th class="text-center">Thời gian chỉnh sửa</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</tfoot>
		</table>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>