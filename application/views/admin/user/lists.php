<?php require_once APPPATH . '/views/admin/header.php'; ?>
    <section class="panel">
        <?php
        if ($this->session->flashdata('msg')) {
            $alert = $this->session->flashdata('msg');
            ?>
            <div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
                <?php echo $alert['content']; ?>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
        <header class="panel-heading">
            <h2 class="panel-title">Danh sách người dùng</h2>
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="text-left col-md-6">
                    Người dùng thường: <span class="badge"><?= $count_normal ?></span> - Người dùng vip: <span class="badge"><?= $count_vip ?></span>
                </div>
                <div class="add-new text-right col-md-6">
                    <a href="<?php echo base_url() ?>admin/user/export_user" class="btn btn-custom">Xuất Excel</a>
                    <a href="<?php echo base_url() ?>admin/user/add" class="btn btn-custom"><i class="fa fa-plus"></i>
                        Thêm mới</a>
                </div>
            </div>
            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                <thead>
                <tr>
                    <th class="text-center">Họ và tên</th>
                    <th class="text-center">Số điện thoại</th>
                    <th class="text-center">Tài khoản</th>
                    <th class="text-center">Loại tài khoản</th>
                    <th class="text-center">Kích hoạt</th>
                    <th class="text-center">Từ ngày</th>
                    <th class="text-center">Đến ngày</th>
                    <th class="text-center">Đăng nhập cuối</th>
                    <th class="text-center table-action">Hành động</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($all_user as $item) {
                    ?>
                    <tr>
                        <td><?php echo $item["fullname"]; ?></td>
                        <td><?php echo $item["user_phone"]; ?></td>
                        <td><?php echo $item["username"]; ?></td>
                        <td>
                            <?php echo ($item["user_type"] == 1) ? 'VIP' : 'Thường'; ?>
                            <?php
                            if ($item["user_type"] == 1 && $item["user_type_active"] == 0) {
                                ?>
                                <br/>
                                <a href="<?php echo base_url(); ?>admin/user/confirm_type/<?php echo $item['user_id']; ?>">Xác
                                    nhận chủ doanh nghiệp</a>
                                <?php
                            }
                            ?>
                        </td>
                        <td class="text-center">
							<span class="label label-<?php echo ($item['active'] == 0) ? "warning" : "success"; ?>">
								<?php echo ($item['active'] == 0) ? "Không kích hoạt" : "Kích hoạt"; ?>
							</span>
                        </td>
                        <td><?php echo ($item['start_date']) ? $this->globals->get_dateformat($item['start_date'], 'd/m/Y') : FALSE; ?></td>
                        <td><?php echo ($item['end_date']) ? $this->globals->get_dateformat($item['end_date'], 'd/m/Y') : FALSE; ?></td>
                        <td><?php echo ($item["last_login"] && $item['last_login'] != '0000-00-00 00:00:00') ? $this->globals->get_dateformat($item["last_login"], 'd-m-Y H:i:s') : FALSE; ?> </td>
                        <td class="text-center table-action">
                            <a href="<?php echo base_url() ?>admin/user/edit/<?php echo $item['user_id']; ?>"><i
                                        class="fa fa-edit"></i></a>
                            <a style="margin-left: 0px; display: block;"
                               href="<?php echo base_url() ?>admin/user/add_renews/<?php echo $item['user_id']; ?>"><i
                                        class="fa fa-usd"></i></a>
                            <a style="margin-left: 0px;"
                               href="<?php echo base_url() ?>admin/user/del/<?php echo $item['user_id']; ?>"
                               onclick="return confirm('Xóa người dùng này?');"><i class="fa fa-trash-o"></i></a>

                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <th class="text-center">Họ và tên</th>
                    <th class="text-center">Số điện thoại</th>
                    <th class="text-center">Tài khoản</th>
                    <th class="text-center">Loại tài khoản</th>
                    <th class="text-center">Kích hoạt</th>
                    <th class="text-center">Từ ngày</th>
                    <th class="text-center">Đến ngày</th>
                    <th class="text-center">Đăng nhập cuối</th>
                    <th class="text-center table-action">Hành động</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </section>

<?php require_once APPPATH . '/views/admin/footer.php'; ?>