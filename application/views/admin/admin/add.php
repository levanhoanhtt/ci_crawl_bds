<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}

    if (validation_errors()!='')
    {
        ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <?php
    }
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Thêm mới quản trị viên</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="col-xs-12 col-sm-4">
				<div class="form-group">
					<div class="col-xs-12">
						<label>Ảnh</label><br />
	              		<img src="" class="imgProduct img-thumbnail"/>
	              		<div class="input-box box-img-admin">
	              			<input id="banner" type="text" name="thumbnail" class="form-control finder-img" readonly placeholder="Đường dẫn ảnh">
	              			<button type="button" class="btn btn-success choose_image_product"><i class="fa fa-edit"></i></button>
	              		</div>
	              	</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-8">
				<div class="form-group">
					<div class="col-xs-12">
						<label for="username">Tên đăng nhập</label>
						<input type="text" name="username" class="form-control" id="username" value="<?php echo ($data_post)?$data_post['username']:false; ?>" placeholder="Tên đăng nhập">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<label for="pw1">Mật khẩu</label>
						<input type="password" name="pw1" class="form-control" id="pw1" placeholder="Mật khẩu">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<label for="fullname">Họ và tên</label>
						<input type="text" name="fullname" class="form-control" id="fullname" value="<?php echo ($data_post)?$data_post['fullname']:false; ?>" placeholder="Họ và tên">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<label for="email_address">Địa chỉ email</label>
						<input type="text" name="email_address" class="form-control" id="email_address" value="<?php echo ($data_post)?$data_post['email_address']:false; ?>" placeholder="Địa chỉ email">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<label for="user_type">Phân loại</label>
						<select name="user_type" class="form-control" id="user_type">
							<option value="0">Biên tập viên</option>
							<option value="1">Quản lý cấp cao</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-8 col-md-8">
						<button class="btn btn-custom" type="submit">Thêm</button>
						<a href="<?php echo base_url() ?>admin/admin" class="btn btn-primary">Quay lại danh sách</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>