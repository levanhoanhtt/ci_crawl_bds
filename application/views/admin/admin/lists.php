<?php require_once APPPATH.'/views/admin/header.php'; ?>
<?php 
$current_login = $current_login['admin_info'];
?>
<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Danh sách quản trị viên</h2>
	</header>
	<div class="panel-body">
		<div class="add-new text-right">
			<!-- <a href="<?php echo base_url() ?>admin/admin/export_user" class="btn btn-custom">Xuất Excel</a> -->
			<a href="<?php echo base_url() ?>admin/admin/add" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
		</div>
		<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="text-center">Ảnh</th>
					<th class="text-center">Họ và tên</th>
					<th class="text-center">Email</th>
					<th class="text-center">Tài khoản</th>
					<th class="text-center">Phân loại</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($all_user as $item) {
					?>
					<tr>
						<td class="text-center">
							<?php 
							if(!empty($item['thumbnail'])){
								?>
								<img src="<?php echo $item['thumbnail']; ?>" alt="<?php echo $item['admin_fullname']; ?>" class="thumb-on-list">
								<?php
							}
							?>
						</td>
						<td><?php echo $item["admin_fullname"]; ?></td>
						<td><?php echo $item["admin_email"]; ?></td>
						<td><?php echo $item["admin_account"]; ?></td>
						<td>
							<?php echo ($item["admin_role"]==1)?'Quản trị cấp cao':'Biên tập viên'; ?>
						</td>
						<td class="text-center table-action">
							<?php
							if($current_login['admin_id'] == $item['admin_id']){
								?>
								<a href="<?php echo base_url() ?>admin/home/profile"><i class="fa fa-edit"></i></a>
								<?php
							}
							if($current_login['admin_role'] == 1 && $current_login['admin_id'] != $item['admin_id']){
								?>
								<a href="<?php echo base_url() ?>admin/admin/edit/<?php echo $item['admin_id']; ?>"><i class="fa fa-edit"></i></a>
								<a href="<?php echo base_url() ?>admin/admin/del/<?php echo $item['admin_id']; ?>" onclick="return confirm('Xóa quản trị viên này?');"><i class="fa fa-trash-o"></i></a>
								<?php
							}
							?>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th class="text-center">Ảnh</th>
					<th class="text-center">Họ và tên</th>
					<th class="text-center">Email</th>
					<th class="text-center">Tài khoản</th>
					<th class="text-center">Phân loại</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</tfoot>
		</table>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>