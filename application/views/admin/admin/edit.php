<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
    {
        ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <?php
    }
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Cập nhật thông tin quản trị viên</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<div class="col-xs-12">
							<b>Ảnh</b><br />
		              		<img src="<?php echo base_url().$current_edit['thumbnail']; ?>" class="imgProduct img-thumbnail <?php echo !empty($current_edit['thumbnail'])?'active':false; ?>"/>
		              		<div class="input-box box-img-admin">
		              			<input id="banner" type="text" name="thumbnail" class="form-control finder-img" value="<?php echo $current_edit['thumbnail']; ?>" readonly placeholder="Đường dẫn ảnh">
		              			<button type="button" class="btn btn-success choose_image_product"><i class="fa fa-edit"></i></button>
		              		</div>
		              	</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-8">
					<div class="form-group">
						<div class="col-xs-12">
							<b>Tài khoản</b>
							<input type="text" class="form-control" value="<?php echo $current_edit['admin_account']; ?>" readonly />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<b>Họ và tên</b>
							<input type="text" class="form-control" name="admin_fullname" value="<?php echo $current_edit['admin_fullname']; ?>" placeholder="Nhập họ và tên"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<b>Địa chỉ email</b>
							<input type="email" class="form-control" name="admin_email" value="<?php echo $current_edit['admin_email']; ?>"  placeholder="Nhập đại chỉ email"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<b>Phân loại</b>
							<select name="admin_role" class="form-control" id="admin_role">
								<option value="0" <?php echo $current_edit['admin_role']==0?'selected':false; ?>>Biên tập viên</option>
								<option value="1" <?php echo $current_edit['admin_role']==1?'selected':false; ?>>Quản trị cấp cao</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<button class="btn btn-custom" type="submit">Lưu</button>
							<a href="<?php echo base_url() ?>admin/admin" class="btn btn-primary">Quay lại danh sách</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>