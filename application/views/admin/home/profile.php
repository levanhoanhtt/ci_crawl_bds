<?php require_once APPPATH.'/views/admin/header.php'; ?>
<section class="panel">
	<?php 
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
    {
        ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <?php
    }
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Cập nhật thông tin người dùng</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontals" method="post">
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<div class="form-group">
						<div class="col-xs-12">
		              		<img src="<?php echo $admin_info['thumbnail']; ?>" class="imgProduct img-thumbnail active"/>
		              		<div class="input-box box-img-admin">
		              			<input id="banner" type="text" name="thumbnail" class="form-control finder-img" value="<?php echo $admin_info['thumbnail']; ?>" readonly placeholder="Đường dẫn ảnh">
		              			<button type="button" class="btn btn-success choose_image_product"><i class="fa fa-edit"></i></button>
		              		</div>
		              	</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-8">
					<div class="form-group">
						<div class="col-xs-12">
							<b>Tài khoản</b>
							<input type="text" class="form-control" value="<?php echo $admin_info['admin_account']; ?>" readonly />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<b>Họ và tên</b>
							<input type="text" class="form-control" name="admin_fullname" value="<?php echo $admin_info['admin_fullname']; ?>" placeholder="Nhập họ và tên"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<b>Địa chỉ email</b>
							<input type="email" class="form-control" name="admin_email" value="<?php echo $admin_info['admin_email']; ?>"  placeholder="Nhập đại chỉ email"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<b>Phân loại</b>
							<input type="text" class="form-control" value="<?php echo ($admin_info["admin_role"]==1)?'Quản trị cấp cao':'Biên tập viên'; ?>" readonly />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<b>Mật khẩu</b> (Nhập nếu thay đổi)
							<input type="password" class="form-control" name="admin_password" value="" placeholder="Nhập mật khẩu mới nếu thay đổi" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<button class="btn btn-custom" type="submit">Lưu</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>