<?php
$login = $this->session->get_userdata();
if (isset($login['admin_login']) && isset($login['admin_login_url']) && $login['admin_login_url']==base_url())
{
    redirect('admin/home');
}
?>
<!doctype html>
<html class="fixed">
	<head>
		
		<title><?php echo $page_title; ?></title>
		<!-- Basic -->
		<meta charset="UTF-8">
		
		<meta name="keywords" content="" />
		<meta name="author" content="">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/theme-custom.css">

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin.css?ver=<?php echo rand(); ?>" />

		<!-- Head Libs -->
		<script src="<?php echo base_url() ?>assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="<?php echo base_url() ?>admin" class="logo pull-left">
					<img src="<?php echo base_url() ?>assets/sites/imgs/logo.png" height="55" alt="Nguồn nhà đất" />
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Đăng nhập hệ thống</h2>
					</div>
					<div class="panel-body">
						<?php 
						if (validation_errors()!='')
                        {
                            ?>
                            <div class="alert alert-warning"><?php echo validation_errors(); ?></div>
                            <?php
                        }

                        if ($this->session->flashdata('msg_login_warning'))
                        {
                            ?>
                            <div class="alert alert-warning">
                            	<?php 
                            	echo $this->session->flashdata('msg_login_warning');
                            	?>
                            </div>
                            <?php
                        }

                        ?>
						<form action="" method="post">
							<div class="form-group mb-lg">
								<label>Tài khoản</label>
								<div class="input-group input-group-icon">
									<input name="username" type="text" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Mật khẩu</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="pwd" type="password" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							
							<div class="text-right">
								<button type="submit" class="btn btn-custom hidden-xs">Đăng nhập</button>
								<button type="submit" class="btn btn-custom btn-block btn-lg visible-xs mt-lg">Đăng nhập</button>
							</div>

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">Copyright &copy; <?php echo date('Y') ?> by Nguồn Nhà Đất. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.js"></script>
		<script src="<?php echo base_url() ?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url() ?>assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url() ?>assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo base_url() ?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url() ?>assets/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url() ?>assets/js/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url() ?>assets/js/theme.init.js"></script>

	</body>
</html>