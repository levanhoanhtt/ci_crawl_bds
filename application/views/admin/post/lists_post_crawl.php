<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title"> <?php echo $page_title; ?> </h2>
	</header>
	<div class="panel-body">
        <div class="row">
            <div class="col-md-8">
                <div class="text-right add-new">
                    <form action="" method="GET" role="form">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group" >
                                    <select class="form-control" name="province_id" id="province_id">
                                        <option value="0">Chọn Tỉnh / Thành phố</option>
                                        <?php
                                        if ($list_province):
                                            foreach ($list_province as $item):
                                                ?>
                                                <option value="<?php echo $item['matp']; ?>" <?php echo (isset($_GET['province_id']) && $_GET['province_id']==$item['matp'])?'selected':FALSE; ?>><?php echo $item['name']; ?></option>
                                                <?php
                                            endforeach; endif;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" name="district_id" id="district_id">
                                        <option value="0">Chọn Quận / huyện</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control" name="price">
                                        <option value="0">Chọn mức giá</option>
                                        <option value="0-1" <?php echo (isset($_GET['price']) && $_GET['price']=='0-1')?'selected':FALSE; ?>>&lt;= 1 triệu/tháng</option>
                                        <option value="1-3" <?php echo (isset($_GET['price']) && $_GET['price']=='1-3')?'selected':FALSE; ?>>1 - 3 triệu/tháng</option>
                                        <option value="3-5" <?php echo (isset($_GET['price']) && $_GET['price']=='3-5')?'selected':FALSE; ?>>3 - 5 triệu/tháng</option>
                                        <option value="5-10" <?php echo (isset($_GET['price']) && $_GET['price']=='5-10')?'selected':FALSE; ?>>5 - 10 triệu/tháng</option>
                                        <option value="10-20" <?php echo (isset($_GET['price']) && $_GET['price']=='10-20')?'selected':FALSE; ?>>10 - 20 triệu/tháng</option>
                                        <option value="20-50" <?php echo (isset($_GET['price']) && $_GET['price']=='20-50')?'selected':FALSE; ?>>20 - 50 triệu/tháng</option>
                                        <option value="50-0" <?php echo (isset($_GET['price']) && $_GET['price']=='50-0')?'selected':FALSE; ?>>&gt; 50 triệu/tháng</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-seach btn-block"><i class="fa fa-search" aria-hidden="true"></i> Lọc</button>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="add-new text-right">
                    <a href="<?php echo base_url() ?>admin/post/export_post" class="btn btn-custom">Xuất Excel</a>
                </div>
            </div>
        </div>

		<table class="table table-bordered table-striped mb-none" id="data_ajax">
			<thead>
				<tr>
					<th class="text-center has-thumb">STT</th>
					<th class="text-center">Tiêu đề</th>
					<th class="text-center">Danh mục</th>
					<th class="text-center">Giá</th>
					<th class="text-center">Loại tin</th>
					<th class="text-center">Khu vực</th>
					<th class="text-center">Ngày đăng</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
			<tfoot>
				<tr>
					<th class="text-center">STT</th>
					<th class="text-center">Tiêu đề</th>
					<th class="text-center">Danh mục</th>
					<th class="text-center">Giá</th>
					<th class="text-center">Loại tin</th>
					<th class="text-center">Khu vực</th>
					<th class="text-center">Ngày đăng</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</tfoot>
		</table>
	</div>
</section>


<?php require_once APPPATH.'/views/admin/footer.php'; ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#data_ajax').DataTable( {
            "processing": true,
            "serverSide": false,
            "ajax": "<?php echo base_url('admin/post/manager_post_crawl_data/?'.$_SERVER['QUERY_STRING']); ?>"
        } );
    } );
</script>
