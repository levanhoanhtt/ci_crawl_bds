<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
    {
        ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <?php
    }
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Cập nhật tin tự động</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="thumbnail">Ảnh đại diện</label>
				<div class="col-sm-8 col-md-8">
					<div class="row">
                      	<div class="col-xs-12 col-sm-7 col-md-8">   
                      		<div class="input-box">
                      			<input id="thumbnail" type="text" name="thumbnail" class="form-control finder-img" value="<?php echo $current_post['thumbnail']; ?>" readonly placeholder="Đường dẫn ảnh">
                      			<button type="button" class="btn btn-success choose_image_product">Thêm ảnh</button>
                      		</div>
                      	</div>
                      	<div class="col-xs-12 col-sm-5 col-md-4">   
                      		<img src="<?php echo $current_post['thumbnail']; ?>" class="imgProduct img-thumbnail <?php echo $current_post['thumbnail']?'active':false;?>" width="200"/>
                      	</div>
                    </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="post_title">Tiêu đề</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="post_title" class="form-control to-slug" id="post_title" value="<?php echo $current_post['post_title']; ?>" placeholder="Tiêu đề bài viết">
				</div>
			</div>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="post_slug">Đường dẫn tĩnh</label>
                <div class="col-sm-8 col-md-8">
                    <input type="text" name="post_slug" class="form-control main-slug" id="post_slug" value="<?php echo $current_post['post_slug']; ?>" placeholder="Đường dẫn tĩnh...">
                </div>
            </div>
			<div class="form-group multi-selected">
				<label class="col-sm-3 col-md-3 control-label">Danh mục tin tức</label>
				<div class="col-sm-8 col-md-8">
					<div class="box-checkbox">
						<?php 
						$cate_id = json_decode($current_post["cate_id"]);
						if(!is_array($cate_id)) $cate_id = [];
						foreach ($all_cate as $item) {
							?>
							<div class="checkbox-custom checkbox-default">
							<input type="checkbox" id="cate_<?php echo $item['cate_id']; ?>" name="cate_id[]" value="<?php echo $item['cate_id']; ?>" <?php echo in_array($item["cate_id"], $cate_id)?"checked":false; ?> />
								<label for="cate_<?php echo $item['cate_id']; ?>"><?php echo $item["display"]; ?></label>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="post_content">Nội dung</label>
				<div class="col-sm-8 col-md-8">
					<textarea name="post_content" id="post_content" class="form-control" rows="5"><?php echo $current_post['post_content']; ?></textarea>
				</div>
				<script>
					CKEDITOR.replace("post_content");
				</script>
			</div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="post_content">Thư viện ảnh</label>
                <div class="col-sm-8 col-md-8">
                    <div class="list-image-box">
                        <?php
                        if (!empty($post_images)):
                            foreach ($post_images as $item):
                                ?>

                                <div class="form-group item-image">

                                    <div class="col-xs-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-7 col-md-9">
                                                <div class="input-box">
                                                    <input id="thumbnail" type="text" name="images[]" class="form-control finder-img" value="<?php echo $item['image_url']; ?>" readonly placeholder="Đường dẫn ảnh...">
                                                    <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                                                </div><br />

                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-3">
                                                <div class="box-img text-right">
                                                    <img src="<?php echo $item['image_url']; ?>" class="imgProduct img-thumbnail active"/>
                                                    <button type="button" class="remove-image btn btn-danger"><i class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endforeach; endif;
                        ?>
                    </div>
                    <hr/>
                    <button type="button" class="more-image btn btn-primary"><i class="fa fa-plus"></i> Thêm ảnh</button>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Tỉnh/Thành phố</label>
                <div class="col-sm-8 col-md-8">
                    <select class="form-control" name="province_id" id="province_id">
                        <option value="0">Chọn Tỉnh/Thành phố</option>
                        <?php
                        if ($list_province):
                            foreach ($list_province as $item):
                                ?>
                                <option value="<?php echo $item['matp']; ?>" <?php echo ($current_post['province_id']==$item['matp'])?'selected':FALSE; ?>><?php echo $item['name']; ?></option>
                                <?php
                            endforeach; endif;
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Quận/Huyện</label>
                <div class="col-sm-8 col-md-8">
                    <select class="form-control" name="district_id" id="district_id">
                        <option value="0">Chọn Quận/Huyện</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Địa chỉ</label>
                <div class="col-sm-8 col-md-8">
                    <input type="text" name="address" class="form-control" value="<?php echo ($current_post['address'])?$current_post['address']:false; ?>" placeholder="Địa chỉ..."/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Giá thuê/bán</label>
                <div class="col-sm-5 col-md-5">
                    <input type="text" name="price" class="form-control" value="<?php echo ($current_post['price'])?$current_post['price']:false; ?>" placeholder="Giá thuê/bán..."/>
                </div>
                <div class="col-md-3 col-sm-3">
                    <select class="form-control" name="unit">
                        <option value="" <?php echo ($current_post['unit']=='')?'selected':FALSE; ?>>Không đơn vị</option>
                        <option value="/m2" <?php echo ($current_post['unit']=='/m2')?'selected':FALSE; ?>>/m2</option>
                        <option value="/tháng" <?php echo ($current_post['unit']=='/tháng')?'selected':FALSE; ?>>/tháng</option>
                        <option value="/Triệu" <?php echo ($current_post['unit'] == '/Triệu') ? 'selected' : FALSE; ?>>
                                    /Triệu
                        </option>
                        <option value="/Tỷ" <?php echo ($current_post['unit'] == '/Tỷ') ? 'selected' : FALSE; ?>>
                            /Tỷ
                        </option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Diện tích</label>
                <div class="col-sm-8 col-md-8">
                    <input type="text" name="area" class="form-control" value="<?php echo ($current_post['area'])?$current_post['area']:false; ?>" placeholder="Diện tích..."/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Thông tin liên hệ</label>
                <div class="col-sm-8 col-md-8">
                    <textarea class="form-control" rows="5" name="contact" placeholder="Thông tin liên hệ..."><?php echo ($current_post['contact'])?$current_post['contact']:false; ?></textarea>
                </div>
            </div>

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Tin chủ nhà</label>
				<div class="col-sm-8 col-md-8">
					<div class="switch switch-sm switch-success">
						<input type="checkbox" name="for_vip_user" data-plugin-ios-switch="" <?php echo ($current_post["crawler_type"]==1)?"checked":false; ?> style="display: none;">
                        <p><i>Không chọn sẽ là tin môi giới</i></p>
                    </div>
				</div>
			</div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label">Chuyển sang tin tự đăng</label>
                <div class="col-sm-8 col-md-8">
                    <div class="switch switch-sm switch-success">
                        <input type="checkbox" name="crawler_type_switch" data-plugin-ios-switch="" <?php echo ($current_post["crawler_type"]==0)?"checked":false; ?> style="display: none;">
                    </div>
                </div>
            </div>

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Kích hoạt</label>
				<div class="col-sm-8 col-md-8">
					<div class="switch switch-sm switch-success">
						<input type="checkbox" name="active" data-plugin-ios-switch="" <?php echo ($current_post["active"]==1)?"checked":false; ?> style="display: none;">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Lưu</button>
					<a href="<?php echo base_url() ?>admin/post/manager_post_crawl" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    var district_id_selected = '<?php echo $current_post['district_id']; ?>';
</script>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>