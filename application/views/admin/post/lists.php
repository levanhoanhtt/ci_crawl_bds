<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
    <?php
    if ($this->session->flashdata('msg')) {
        $alert = $this->session->flashdata('msg');
        ?>
        <div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
            <?php echo $alert['content']; ?>
        </div>
        <?php
        unset($_SESSION['msg']);
    }
    ?>
    <header class="panel-heading">
        <h2 class="panel-title">Danh mục tin đăng</h2>
    </header>

    <div class="panel-body">

        <div class="row">
            <div class="col-md-10">
                <div class="text-right add-new">
                    <?php 
                    $uri = ($for_vip==1)?'/post_vip':FALSE;
                     ?>
                    <form action="<?php echo base_url('admin/post'.$uri); ?>" method="GET" role="form">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" >
                                    <select class="form-control" name="province_id" id="province_id">
                                        <option value="0">Chọn Tỉnh / Thành phố</option>
                                        <?php
                                        if ($list_province) :
                                            foreach ($list_province as $item) :
                                                ?>
                                                <option value="<?php echo $item['matp']; ?>" <?php echo (isset($_GET['province_id']) && $_GET['province_id']==$item['matp'])?'selected':false;
                                                echo (!$_GET['province_id'] && $item['matp']==1)?'selected':false; ?>><?php echo $item['name']; ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" name="district_id" id="district_id">
                                            <option value="0">Chọn Quận / huyện</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" name="price">
                                            <option value="0">Chọn mức giá</option>
                                            <option value="-1">Không xác định</option>
                                            <option value="0-1" <?php echo (isset($_GET['price']) && $_GET['price'] == '0-1') ? 'selected' : false; ?>>
                                                &lt;= 1 triệu
                                            </option>
                                            <option value="1-3" <?php echo (isset($_GET['price']) && $_GET['price'] == '1-3') ? 'selected' : false; ?>>
                                                1 - 3 triệu
                                            </option>
                                            <option value="3-5" <?php echo (isset($_GET['price']) && $_GET['price'] == '3-5') ? 'selected' : false; ?>>
                                                3 - 5 triệu
                                            </option>
                                            <option value="5-10" <?php echo (isset($_GET['price']) && $_GET['price'] == '5-10') ? 'selected' : false; ?>>
                                                5 - 10 triệu
                                            </option>
                                            <option value="10-20" <?php echo (isset($_GET['price']) && $_GET['price'] == '10-20') ? 'selected' : false; ?>>
                                                10 - 20 triệu
                                            </option>
                                            <option value="20-30" <?php echo (isset($_GET['price']) && $_GET['price'] == '20-30') ? 'selected' : false; ?>>
                                                20 - 30 triệu
                                            </option>
                                            <option value="30-40" <?php echo (isset($_GET['price']) && $_GET['price'] == '30-40') ? 'selected' : false; ?>>
                                                30 - 40 triệu
                                            </option>
                                            <option value="40-70" <?php echo (isset($_GET['price']) && $_GET['price'] == '40-70') ? 'selected' : false; ?>>
                                                40 - 70 triệu
                                            </option>
                                            <option value="70-100" <?php echo (isset($_GET['price']) && $_GET['price'] == '70-100') ? 'selected' : false; ?>>
                                                70 - 100 triệu
                                            </option>
                                            <option value="100-500" <?php echo (isset($_GET['price']) && $_GET['price'] == '100-500') ? 'selected' : false; ?>>
                                                100 - 500 triệu
                                            </option>
                                            <option value="500-800" <?php echo (isset($_GET['price']) && $_GET['price'] == '500-800') ? 'selected' : false; ?>>
                                                500 - 800 triệu
                                            </option>
                                            <option value="800-1000" <?php echo (isset($_GET['price']) && $_GET['price'] == '800-1000') ? 'selected' : false; ?>>
                                                800 - 1 tỷ
                                            </option>
                                            <option value="1000-2000" <?php echo (isset($_GET['price']) && $_GET['price'] == '1000-2000') ? 'selected' : false; ?>>
                                                1 - 2 tỷ
                                            </option>
                                            <option value="2000-3000" <?php echo (isset($_GET['price']) && $_GET['price'] == '2000-3000') ? 'selected' : false; ?>>
                                                2 - 3 tỷ
                                            </option>
                                            <option value="3000-5000" <?php echo (isset($_GET['price']) && $_GET['price'] == '3000-5000') ? 'selected' : false; ?>>
                                                3 - 5 tỷ
                                            </option>
                                            <option value="5000-7000" <?php echo (isset($_GET['price']) && $_GET['price'] == '5000-7000') ? 'selected' : false; ?>>
                                                5 - 7 tỷ
                                            </option>
                                            <option value="7000-10000" <?php echo (isset($_GET['price']) && $_GET['price'] == '7000-10000') ? 'selected' : false; ?>>
                                                7 - 10 tỷ
                                            </option>
                                            <option value="10000-20000" <?php echo (isset($_GET['price']) && $_GET['price'] == '10000-20000') ? 'selected' : false; ?>>
                                                10 - 20 tỷ
                                            </option>
                                            <option value="20000-30000" <?php echo (isset($_GET['price']) && $_GET['price'] == '20000-30000') ? 'selected' : false; ?>>
                                                20 - 30 tỷ
                                            </option>
                                            <option value="30000-0" <?php echo (isset($_GET['price']) && $_GET['price'] == '30000-0') ? 'selected' : false; ?>>
                                                > 30 tỷ
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p class="text-left" style="height: 22px;"></p>
                                        <select class="form-control" name="area">
                                            <option value="0">Chọn diện tích</option>
                                            <option value="0-30" <?php echo (isset($_GET['area']) && $_GET['area']=='0-30')?'selected':false; ?>>Dưới 30 m2</option>
                                            <option value="30-50" <?php echo (isset($_GET['area']) && $_GET['area']=='30-50')?'selected':false; ?>>30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;50 m2</option>
                                            <option value="50-70" <?php echo (isset($_GET['area']) && $_GET['area']=='50-70')?'selected':false; ?>>50&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;70 m2</option>
                                            <option value="70-100" <?php echo (isset($_GET['area']) && $_GET['area']=='70-100')?'selected':false; ?>>70&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;100 m2</option>
                                            <option value="100-150" <?php echo (isset($_GET['area']) && $_GET['area']=='100-150')?'selected':false; ?>>100&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;150 m2</option>
                                            <option value="150-200" <?php echo (isset($_GET['area']) && $_GET['area']=='150-200')?'selected':false; ?>>150&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;200 m2</option>
                                            <option value="200-250" <?php echo (isset($_GET['area']) && $_GET['area']=='200-250')?'selected':false; ?>>200&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;250 m2</option>
                                            <option value="250-300" <?php echo (isset($_GET['area']) && $_GET['area']=='250-300')?'selected':false; ?>>250&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;300 m2</option>
                                            <option value="300-350" <?php echo (isset($_GET['area']) && $_GET['area']=='300-350')?'selected':false; ?>>300&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;350 m2</option>
                                            <option value="350-400" <?php echo (isset($_GET['area']) && $_GET['area']=='350-400')?'selected':false; ?>>350&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;400 m2</option>
                                            <option value="400-600" <?php echo (isset($_GET['area']) && $_GET['area']=='400-600')?'selected':false; ?>>400&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;600 m2</option>
                                            <option value="600-800" <?php echo (isset($_GET['area']) && $_GET['area']=='600-800')?'selected':false; ?>>600&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;800 m2</option>
                                            <option value="800-1000" <?php echo (isset($_GET['area']) && $_GET['area']=='800-1000')?'selected':false; ?>>800&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;1000 m2</option>
                                            <option value="1000-0" <?php echo (isset($_GET['area']) && $_GET['area']=='1000-0')?'selected':false; ?>>Trên 1000 m2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p class="text-left" style="margin-bottom: 5px;">Ngày bắt đầu</p>
                                        <input class="form-control datepicker" name="date_start" type="text" value="" id="example-datetime-local-input" placeholder="Chọn ngày bắt đầu...">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <p class="text-left" style="margin-bottom: 5px;">Ngày kết thúc</p>
                                        <input class="form-control datepicker" name="date_end" type="text" value="" id="example-datetime-local-input" placeholder="Chọn ngày kết thúc..."/>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="keyword" class="form-control" placeholder="Nhập từ khóa tìm kiếm..." value="<?php echo @$_GET['keyword']; ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="category_id" name="category_id">
                                            <option value="0">Chọn danh mục</option>
                                            <?php
                                            if ($category_parent) :
                                                foreach ($category_parent as $item) :
                                                    ?>
                                                    <option value="<?php echo $item['cate_id']; ?>" <?php echo (isset($_GET['category_id']) && $_GET['category_id']==$item['cate_id'])?'selected':false; ?>><?php echo $item['cate_name']; ?></option>
                                                    <?php
                                                    $category_child = $this->get_data->get_category_child($item['cate_id']);
                                                    if ($category_child) {
                                                        foreach ($category_child as $child) {
                                                            ?>
                                                            <option value="<?php echo $child['cate_id'] ?>" <?php echo (isset($_GET['category_id']) && $_GET['category_id']==$child['cate_id'])?'selected':false; ?>>- <?php echo $child['cate_name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- -------------- Loại -------------- -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="category_id" name="crawler_type">
                                            <option value="0">Chọn nguồn</option>
                                            <option <?php echo (isset($_GET['crawler_type']) && $_GET['crawler_type']=='1')?'selected':false; ?> value="1">Chính chủ</option>
                                            <option <?php echo (isset($_GET['crawler_type']) && $_GET['crawler_type']=='2')?'selected':false; ?> value="2">Môi giới</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <!-- -------------- Loại -------------- -->



                                
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-seach btn-block"><i class="fa fa-search" aria-hidden="true"></i> Lọc</button>
                                </div>
                                <div class="col-md-4">
                                    <a href="<?= base_url('admin/post') ?>" class="btn btn-warning btn-seach btn-block">Xoá lọc</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="add-new text-right">
                        <a href="<?php echo base_url() ?>admin/post/add" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
                        <hr/>
                        <a href="<?php echo base_url() ?>admin/post/export_post/<?php echo $for_vip; ?>" class="btn btn-custom"><i class="fa fa-download"></i> Xuất Excel</a>
                    </div>
                </div>
            </div>

            <br/>
         
            <table class="table table-bordered table-striped mb-none" id="">
              <thead>
               <tr>
                <th class="text-center" width="5%">STT</th>
                <th class="text-center">Tiêu đề</th>
                <th class="text-center">Khu vực</th>
                <th class="text-center">Giá</th>
                <th class="text-center">Ngày đăng</th>
                <th class="text-center">Kích hoạt</th>
                <th class="text-center">Xử lý</th>
                <th class="text-center">Loại Tin Đăng</th>
                <th class="text-center table-action">Hành động</th>
                
            </tr>
        </thead>
        <tbody>
            
            <?php 
            if ($post_list):
                
                foreach ($post_list as $item):
                   
             ?>
             <tr>
                <td class="text-center"><?php echo $item[0]; ?></td>
                <td>
                    <?php echo $item[1]; ?>
                </td>
                <td>
                    <?php echo $item[2]; ?>
                </td>
                <td>
                    <?php echo $item[3]; ?>
                </td>
                <td>
                    <?php echo $item[4]; ?>
                </td>
                <td>
                    <?php echo $item[5]; ?>
                </td>
                <td>
                    <?php echo $item[6]; ?>
                </td>
                 <td>
                    <?php echo $item[7]; ?>
                </td>
                 <td>
                    <?php echo $item[8]; ?>
                </td>
                
             </tr>
            <?php 
            endforeach; endif;
             ?> 
        </tbody>
        <tfoot>
           <tr>
            <th class="text-center">STT</th>
            <th class="text-center">Tiêu đề</th>
            <th class="text-center">Khu vực</th>
            <th class="text-center">Giá</th>
           
            <th class="text-center">Ngày đăng</th>
            <th class="text-center">Kích hoạt</th>
            <th class="text-center">Xử lý</th>
            <th class="text-center">Loại Tin Đăng</th>
            <th class="text-center table-action">Hành động</th>
            
        </tr>
    </tfoot>
</table>
<hr>
<ul class="pagination">
<?php echo $pagination; ?>
</ul>
</div>
</section>


<?php require_once APPPATH.'/views/admin/footer.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {


        // var myData = {};

        // var table = jQuery('#data_ajax').DataTable({
        //     serverSide: true,
        //     paging: true,
        //     processing: true,
        //     searching: true,
        //     ajax: "<?php echo base_url('admin/post/get_list_post_data/'.$for_vip.'/?'.$_SERVER['QUERY_STRING']); ?>",
        //     info: false,
        //     deferRender: true,
        //     pageLength: 50,
        //     initComplete:function() {

        //     }
        // });
    } );
</script>

