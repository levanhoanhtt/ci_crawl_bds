<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
    {
        ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <?php
    }
    ?>
    <header class="panel-heading">
        
      <h2 class="panel-title">Thêm mới tin đăng</h2>
  </header>
  <div class="panel-body">
      <form class="form-horizontal form-bordered" action="" method="post" enctype="multipart/form-data" id="formUpload">
         <div class="form-group">
            <label class="col-sm-3 col-md-3 control-label" for="thumbnail">Ảnh đại diện</label>
            <div class="col-sm-8 col-md-8">
               <div class="row">
                 <div class="col-xs-12 col-sm-7 col-md-8">   
                    <div class="input-box">
                       <input id="thumbnail" type="text" name="thumbnail" class="form-control finder-img" value="" readonly placeholder="Đường dẫn ảnh">
                       <button type="button" class="btn btn-success choose_image_product">Thêm ảnh</button>
                   </div>
               </div>
               <div class="col-xs-12 col-sm-5 col-md-4">   
                <img src="" class="imgProduct img-thumbnail" width="200"/>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 col-md-3 control-label" for="post_title">Tiêu đề</label>
    <div class="col-sm-8 col-md-8">
       <input type="text" name="post_title" class="form-control to-slug" id="post_title" value="<?php echo ($data_post)?$data_post['post_title']:false; ?>" placeholder="Tiêu đề bài viết">
   </div>
</div>
<div class="form-group">
    <label class="col-sm-3 col-md-3 control-label" for="post_slug">Đường dẫn tĩnh</label>
    <div class="col-sm-8 col-md-8">
        <input type="text" name="post_slug" class="form-control main-slug" id="post_slug" value="<?php echo ($data_post)?$data_post['post_slug']:false; ?>" placeholder="Đường dẫn tĩnh...">
    </div>
</div>
<div class="form-group multi-selected">
    <label class="col-sm-3 col-md-3 control-label">Danh mục tin tức</label>
    <div class="col-sm-8 col-md-8">
       <div class="box-checkbox">
          <?php 
          foreach ($all_cate as $item) {
             ?>
             <div class="checkbox-custom checkbox-default">
                 <input type="checkbox" id="cate_<?php echo $item['cate_id']; ?>" name="cate_id[]" value="<?php echo $item['cate_id']; ?>" />
                 <label for="cate_<?php echo $item['cate_id']; ?>"><?php echo $item["display"]; ?></label>
             </div>
             <?php
         }
         ?>
     </div>
 </div>
</div>

<div class="form-group">
    <label class="col-sm-3 col-md-3 control-label" for="post_content">Nội dung</label>
    <div class="col-sm-8 col-md-8">
       <textarea name="post_content" id="post_content" class="form-control" rows="5"><?php echo ($data_post)?$data_post["post_content"]:false; ?></textarea>
   </div>
   <script>
       CKEDITOR.replace("post_content");
   </script>
</div>

<div class="form-group">
    <label class="col-sm-3 col-md-3 control-label" for="post_content">Thư viện ảnh</label>
    <div class="col-sm-8 col-md-8">
        <div class="box-upload" id="">
                <div class="progress">
                    <div class="progress-bar">0%</div>
                </div>
                <input type="file" name="img_file[]" multiple="multiple" onchange="previewImg(event);" id="img_file" accept="image/*">

                <div class="box-preview-img">
                    <span class="span-1"></span>
                    <span class="span-2"></span>
                </div>
                <div class="buido">

                </div>
                <button type="reset" class="btn-reset">Làm mới</button>
                <div class="output"></div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 col-md-3 control-label" for="">Tỉnh/Thành phố</label>
            <div class="col-sm-8 col-md-8">
                <select class="form-control" name="province_id" id="province_id" readonly="readonly">
                    <option value="0">Chọn Tỉnh/Thành phố</option>
                    <?php
                    if ($list_province):
                        foreach ($list_province as $item):
                            ?>
                            <option value="<?php echo $item['matp']; ?>" <?php echo ($item['matp']==1)?'selected':FALSE ?>><?php echo $item['name']; ?></option>
                            <?php
                        endforeach; endif;
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Quận/Huyện</label>
                <div class="col-sm-8 col-md-8">
                    <select class="form-control" name="district_id" id="district_id">
                        <option value="0">Chọn Quận/Huyện</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Địa chỉ</label>
                <div class="col-sm-8 col-md-8">
                    <input type="text" name="address" class="form-control" value="<?php echo ($data_post)?$data_post['address']:false; ?>" placeholder="Địa chỉ..."/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Giá thuê/bán</label>
                <div class="col-sm-5 col-md-5">
                    <input type="text" name="price" class="form-control" value="<?php echo ($data_post)?$data_post['price']:false; ?>" placeholder="Giá thuê/bán..."/>
                </div>
                <div class="col-md-3 col-sm-3">
                    <select class="form-control" name="unit">
                        <option value="0" <?php echo ($data_post && $data_post['unit']=='')?'selected':FALSE; ?>>Thương lượng</option>
                        <!-- <option value="/m2" <?php echo ($data_post && $data_post['unit']=='/m2')?'selected':FALSE; ?>>/m2</option> -->
                        <option value="/tháng" <?php echo ($data_post && $data_post['unit']=='/tháng')?'selected':FALSE; ?>>Triệu/tháng</option> 
                        <option value="Triệu" <?php echo ($data_post && $data_post['unit']=='Triệu')?'selected':FALSE; ?>>Triệu</option>
                        <option value="Tỷ" <?php echo ($data_post && $data_post['unit']=='Tỷ')?'selected':FALSE; ?>>Tỷ</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Diện tích</label>
                <div class="col-sm-8 col-md-8">
                    <input type="text" name="area" class="form-control" value="<?php echo ($data_post)?$data_post['area']:false; ?>" placeholder="Diện tích..."/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="">Thông tin liên hệ</label>
                <div class="col-sm-8 col-md-8">
                   <!--  <textarea class="form-control" rows="5" name="contact" placeholder="Thông tin liên hệ..."><?php echo ($data_post)?$data_post['contact']:false; ?></textarea> -->
                   <input type="text" name="contact" value="<?php echo ($data_post)?$data_post['contact']:false; ?>" class="form-control" placeholder="Thông tin liên hệ...">
                </div>
            </div>

           <!--  <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label">Tin VIP</label>
                <div class="col-sm-8 col-md-8">
                   <div class="switch switch-sm switch-success">
                      <input type="checkbox" name="for_vip_user" data-plugin-ios-switch="" <?php echo ($data_post && $data_post['for_vip_user'])?"checked":false; ?> style="display: none;">
                      <p><i>Không chọn sẽ là tin thường</i></p>
                  </div>
              </div>
          </div> -->

         <!--  <div class="form-group">
            <label class="col-sm-3 col-md-3 control-label">Kích hoạt</label>
            <div class="col-sm-8 col-md-8">
               <div class="switch switch-sm switch-success">
                  <input type="checkbox" name="active" data-plugin-ios-switch="" <?php echo ($data_post && $data_post['active'])?"checked":false; ?> style="display: none;">
              </div>
          </div>
      </div> -->
      <div class="form-group">
        <label class="col-sm-3 col-md-3 control-label">Xử lý</label>
        <div class="col-sm-8 col-md-8">
            <div class="switch switch-sm switch-success">
                <input type="checkbox" name="handling" data-plugin-ios-switch="" <?php echo ($data_post && $data_post['handling'])?"checked":false; ?> style="display: none;">
                <p><i>Không chọn sẽ là chưa xử lý</i></p>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-3 col-md-3"></div>
        <div class="col-sm-8 col-md-8">
           <button class="btn btn-custom" type="submit">Thêm</button>
           <a href="<?php echo base_url() ?>admin/post" class="btn btn-primary">Quay lại danh sách</a>
       </div>
   </div>
</form>
</div>
</section>
<style type="text/css">
    input.bui {

        display: block;
        width: 85%;

    }
    body {font-family: 'Helvetica Neue', Arial, Helvetica,'Nimbus Sans L', sans-serif, 'Calibri'; font-size: 14px; color: #444; background-color: #ecf0f5;}
    h1, h2, h3, h4, h5, h6 {font-weight: normal;}
    .clearfix {clear: both;}
    button:hover {cursor: pointer; opacity: 0.8;}

    /* Box upload */
    .box-upload {width: 500px; margin: 20px auto; border: 1px solid #e5e5e5; background-color: #fff; padding: 10px;}
    .box-upload h2 {text-align: center; margin-bottom: 20px;}
    .progress {padding: 2px; border: 1px solid #e5e5e5; border-radius: 4px; margin-bottom: 10px; display: none;}
    .progress-bar {background-color: #428bca; color: #fff; text-align: center; border-radius: 4px; padding: 2px 0; width: 0;}
    input[type=file] {display: block; font-size: 14px;}
    .box-preview-img{margin-top: 10px; display: none;}
    .box-preview-img p {font-weight: bold;}
    .box-preview-img img{width: 50px; height: 50px; border: 1px solid #e5e5e5; margin-right: 5px; margin-top: 5px;}
    button.btn-reset {background-color: #fff; border: 1px solid #ccc; color: #444;}
    .output {display: none; background-color: #d9534f; color: #fff; padding: 7px; border-radius: 4px; margin-top: 10px;}
    .success {background-color: #5cb85c;}
</style>
<script type="text/javascript">
    // Xem hình ảnh trước khi upload
    function previewImg(event) {
    // Gán giá trị các file vào biến files
    var files = document.getElementById('img_file').files;
    
    // var oldfile = 
    
    // Show khung chứa ảnh xem trước
    $('#formUpload .box-preview-img').show();
    
    // Thêm chữ "Xem trước" vào khung


    //$('#formUpload .box-preview-img').html('<p>Xem trước</p>');
        $('#formUpload .box-preview-img span.span-1').append($('#formUpload .box-preview-img span.span-2').html());
        $('#formUpload .box-preview-img span.span-2').html('');


    // Dùng vòng lặp for để thêm các thẻ img vào khung chứa ảnh xem trước
    for (i = 0; i < files.length; i++)
    {
        // Thêm thẻ img theo i
        $('#formUpload .box-preview-img span.span-2').append('<img src="" id="' + i +'">');

        // Thêm src vào mỗi thẻ img theo id = i
        $('#formUpload .box-preview-img span.span-2 img:eq('+i+')').attr('src', URL.createObjectURL(event.target.files[i]));

    }   
}

// Nút reset form upload
$('#formUpload .btn-reset').on('click', function() {
    // Làm trống khung chứa hình ảnh xem trước
    $('#formUpload .box-preview-img').html('');

    // Hide khung chứa hình ảnh xem trước
    $('#formUpload .box-preview-img').hide();

    // Hide khung kết quả
    $('#formUpload .output').hide();
});

// Xử lý ảnh và upload

    $('#img_file').on('change', function(){
        $img_file = $('#formUpload #img_file').val();

        // Cắt đuôi của file để kiểm tra
        $type_img_file = $('#formUpload #img_file').val().split('.').pop().toLowerCase();

        // Nếu không có ảnh nào
        if ($img_file == '')
        {
            // Show khung kết quả
            $('#formUpload .output').show();

            // Thông báo lỗi
            $('#formUpload .output').html('Vui lòng chọn ít nhất một file ảnh.');
        }
        // Ngược lại nếu file ảnh không hợp lệ với các đuôi bên dưới
        else if ($.inArray($type_img_file, ['png', 'jpeg', 'jpg', 'gif']) == -1)
        {
            // Show khung kết quả
            $('#formUpload .output').show();

            // Thông báo lỗi
            $('#formUpload .output').html('File ảnh không hợp lệ với các đuôi .png, .jpeg, .jpg, .gif.');
        }
        // Ngược lại
        else
        {

            // Tiến hành upload
            $('#formUpload').ajaxSubmit({
                url: '<?php echo base_url('admin/post/upload_images'); ?>',
                // Trước khi upload
                // type : "get",
                // data : {a:"123123"},
                beforeSubmit: function() {
                    target:   '#formUpload .output',

                        // Ẩn khung kết quả
                        $('#formUpload .output').hide();

                    // Show thanh tiến trình
                    $("#formUpload .progress").show();

                    // Đặt mặc định độ dài thanh tiến trình là 0
                    $("#formUpload .progress-bar").width('0');
                },

                // Trong quá trình upload
                uploadProgress: function(event, position, total, percentComplete){
                    // Kéo dãn độ dài thanh tiến trình theo % tiến độ upload
                    $("#formUpload .progress-bar").css('width', percentComplete + '%');

                    // Hiển thị con số % trên thanh tiến trình
                    $("#formUpload .progress-bar").html(percentComplete + '%');
                },
                // Sau khi upload xong
                success: function(re) {
                    // Show khung kết quả
                    $('#formUpload .output').show();

                    // Thêm class success vào khung kết quả
                    $('#formUpload .output').addClass('success');

                    // Thông báo thành công
                    $('#formUpload .output').html('Upload ảnh thành công.');

                    // Trả về đường dẫn
                    $('.buido').append(re);
                },
                // Nếu xảy ra lỗi
                error : function() {
                    // Show khung kết quả
                    $('#formUpload .output').show();

                    // Thông báo lỗi
                    $('#formUpload .output').html('Không thể upload ảnh vào lúc này, hãy thử lại sau.');
                }
            });

        }
    });

</script>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>
