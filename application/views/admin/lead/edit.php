<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
	{
		?>
		<div class="alert alert-danger"><?php echo validation_errors(); ?></div>
		<?php
	}
	
	?>
	<header class="panel-heading">
		<h2 class="panel-title"> <?php echo $page_title; ?> </h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="your_name">Họ và Tên</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="your_name" class="form-control" id="your_name" value=" <?php echo $current_edit['your_name'] ?> " >
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="your_phone">Số điện thoại</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="your_phone" class="form-control" id="your_phone" value=" <?php echo $current_edit['your_phone'] ?> ">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="your_type">Lĩnh vực</label>
				<div class="col-sm-8 col-md-8">
					<select name="your_type" class="form-control" id="your_type">
					<option value="Môi giới nhà thổ cư" <?php echo ($current_edit['your_type']== "Môi giới nhà thổ cư")?"selected":false ?> >Môi giới nhà thổ cư</option>
					<option value="Môi giới cho thuê" <?php echo ($current_edit['your_type']== "Môi giới nhà cho thuê")?"selected":false ?>>Môi giới cho thuê</option>
					<option value="Môi giới nhà dự án" <?php echo ($current_edit['your_type']== "Môi giới nhà dự án")?"selected":false ?> >Môi giới nhà dự án</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="note">Ghi chú</label>
				<div class="col-sm-8 col-md-8">
					<textarea class="form-control note" id="note" name="note" rows="4"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="status">Trạng thái</label>
				<div class="col-sm-8 col-md-8">
					<select name="status" class="form-control" id="status">
						<option value="0" <?php echo ($current_edit['status']==0)?"selected":false ?> >0</option>
						<option value="1" <?php echo ($current_edit['status']==1)?"selected":false ?> >1</option>
					</select>
				</div>
			</div>

			
			
			
			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Lưu</button>
					<a href="<?php echo base_url() ?>admin/lead" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>