<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title"><?php echo $page_title ?></h2>
	</header>
	<div class="panel-body">
		<div class="add-new text-right">
			<a href="<?php echo base_url() ?>admin/lead/export_user" class="btn btn-custom">Xuất Excel</a>
			<a href="<?php echo base_url() ?>admin/lead/add" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
		</div>
		<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th class="text-center">Họ và Tên</th>
					<th class="text-center">Số điện thoại</th>
					<th class="text-center">Lĩnh vực</th>
					<th class="text-center">Ghi chú</th>
					<th class="text-center">Trạng thái</th>
					<th class="text-center">Ngày tạo</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($users_lead as $item) {
					?>
					<tr>
						<td><?php echo $item["your_name"]; ?></td>
						<td><?php echo $item["your_phone"]; ?></td>
						<td><?php echo $item["your_type"]; ?></td>
						<td><?php echo $item["note"]; ?></td>
						<td><?php echo ($item["status"]==1)?'<span class="label label-success">Đã xử lý</span>':'<span class="label label-warning">Chưa xử lý</span>'; ?>
                        <br/>
                        <a href="<?php echo base_url('admin/lead/switch_status/'.$item['id']); ?>" onclick="return (confirm('Bạn có chắc chắn muốn chuyển?'));"><span class="label label-danger">Chuyển</span></a>
                        </td>
						<td><?php echo $this->globals->get_dateformat($item["create_at"], 'd/m/Y H:i:s'); ?></td>
						<td class="text-center table-action">
							<a href="<?php echo base_url() ?>admin/lead/edit/<?php echo $item['id']; ?>"><i class="fa fa-edit"></i></a>
							<a href="<?php echo base_url() ?>admin/lead/del/<?php echo $item['id']; ?>" onclick="return confirm('Xóa người dùng này?');"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th class="text-center">Họ và Tên</th>
					<th class="text-center">Số điện thoại</th>
					<th class="text-center">Lĩnh vực</th>
					<th class="text-center">Ghi chú</th>
					<th class="text-center">Trạng thái</th>
					<th class="text-center">Ngày tạo</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</tfoot>
		</table>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>