<?php 
$login = $this->session->get_userdata();
$this->session->set_userdata('_current_link', current_url());
if (!isset($login['admin_login']) || !isset($login['admin_login_url']) || $login['admin_login_url']!=base_url())
{
   redirect('admin/home/login');
}
$array_role_0 = ['post','category'];
$info_admin_check = $login['admin_info'];
if($info_admin_check['admin_role']==0){
	$uri = explode('/',uri_string());
	if(!empty($uri[1])){
		if(!in_array($uri[1], $array_role_0)){
			redirect('admin');
		}
	}
}

$controller = $this->router->fetch_class();
$method = $this->router->fetch_method();
?>

<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title><?php echo $page_title; ?></title>
		<meta name="keywords" content="<?php echo $page_title; ?>" />

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css"> -->

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/morris/morris.css" />

		<!-- Datatable -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme-custom.css" />

		<!-- Style -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin.css?ver=<?php echo rand(); ?>" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/together.css" />

		<!-- jQuery -->
		<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.js"></script>

		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>assets/vendor/modernizr/modernizr.js"></script>
		
		
		<!-- ckeditor -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>

		

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="<?php echo base_url() ?>admin" class="logo" style="font-size: 25px; margin-top: 15px; font-weight: bold;">
						ADMINISTRATOR
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
				<!-- start: search & user box -->
				<div class="header-right">
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<?php echo $login['admin_info']['thumbnail']; ?>" alt="<?php echo $login['admin_info']['admin_fullname']; ?>" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
								<span class="name"><?php echo $login['admin_info']['admin_fullname']; ?></span>
								<span class="role"><?php echo ($login['admin_info']["admin_role"]==0)?"Biên tập viên":"Quản trị viên"; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>admin/home/profile"><i class="fa fa-user"></i> Thông tin cá nhân</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>admin/home/logout"><i class="fa fa-power-off"></i> Đăng xuất</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title"><a href="<?=base_url();?>" target="_blank"><i class="fa fa-eye"></i> Xem trang</a></div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-active">
										<a href="<?php echo base_url() ?>admin">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Trang tổng quan</span>
										</a>
									</li>
									<?php
									if($info_admin_check['admin_role']==1){
										?>

										<li class="nav-parent <?php echo ($controller=='user' || $controller=='admin' || $controller=='user_payments')?'nav-expanded':FALSE; ?>">
											<a>
												<i class="fa fa-users" aria-hidden="true"></i>
												<span>Người dùng</span>
											</a>
											<ul class="nav nav-children">
												<li><a href="<?php echo base_url() ?>admin/user">Danh sách người dùng</a></li>
												<li><a href="<?php echo base_url() ?>admin/admin">Danh sách quản trị viên</a></li>
											</ul>
										</li>

										<li class="nav-parent <?php echo ($controller=='post' || $controller=='category' || $controller=='broker')?'nav-expanded':FALSE; ?>">
											<a>
												<i class="fa fa-file-o" aria-hidden="true"></i>
												<span>Quản lý tin đăng</span>
											</a>
											<ul class="nav nav-children">
												<li><a href="<?php echo base_url() ?>admin/post">Danh sách tin thường</a></li>
												<li><a href="<?php echo base_url() ?>admin/post/post_vip">Danh sách tin VIP</a></li>
                                                <li><a href="<?php echo base_url() ?>admin/category">Quản lý chuyên mục</a></li>
                                                <li><a href="<?php echo base_url() ?>admin/broker">Danh sách môi giới</a></li>
											</ul>
										</li>

										<li class="nav-parent <?php echo ($controller=='page')?'nav-expanded':FALSE; ?>">
											<a>
												<i class="fa fa-file-o" aria-hidden="true"></i>
												<span>Quản lý trang</span>
											</a>
											<ul class="nav nav-children">
												<li><a href="<?php echo base_url() ?>admin/page/add">Thêm mới</a></li>
												<li><a href="<?php echo base_url() ?>admin/page">Danh sách</a></li>
											</ul>
										</li>
										<li class="nav-parent <?php echo ($controller=='options')?'nav-expanded':FALSE; ?>">
											<a>
												<i class="fa fa-cogs" aria-hidden="true"></i>
												<span>Thiết lập</span>
											</a>
											<ul class="nav nav-children">
												<li><a href="<?php echo base_url() ?>admin/options/slideshow">Thiết lập Slideshow</a></li>
												<li><a href="<?php echo base_url() ?>admin/options/menu">Thiết lập Menu</a></li>
												<li><a href="<?php echo base_url() ?>admin/options/header">Thiết lập Header</a></li>
												<li><a href="<?php echo base_url() ?>admin/options/homepagesetting">Thiết lập Trang Chủ</a></li>
	                                            <li><a href="<?php echo base_url() ?>admin/options/footer">Thiết lập Footer</a></li>
	                                            <li><a href="<?php echo base_url() ?>admin/options/sidebar">Thiết lập Sidebar</a></li>
	                                            <li><a href="<?php echo base_url() ?>admin/options/feedback">Thiết lập Feedback</a></li>
	                                            <li><a href="<?php echo base_url() ?>admin/options/systems">Thiết lập hệ thống</a></li>

											</ul>
										</li>

										<?php
									}else{
										?>
										<li class="nav-parent <?php echo ($controller=='user' || $controller=='admin' || $controller=='user_payments')?'nav-expanded':FALSE; ?>">
											<a>
												<i class="fa fa-users" aria-hidden="true"></i>
												<span>Người dùng</span>
											</a>
											<ul class="nav nav-children">
												
												<li><a href="<?php echo base_url() ?>admin/admin">Danh sách quản trị viên</a></li>
											</ul>
										</li>
										
										
										<li class="nav-parent <?php echo ($controller=='post' || $controller=='category' || $controller=='broker')?'nav-expanded':FALSE; ?>">
											<a>
												<i class="fa fa-file-o" aria-hidden="true"></i>
												<span>Quản lý tin đăng</span>
											</a>
											<ul class="nav nav-children">
												<li><a href="<?php echo base_url() ?>admin/post">Danh sách tin thường</a></li>
												<li><a href="<?php echo base_url() ?>admin/post/post_vip">Danh sách tin VIP</a></li>
                                               
											</ul>
										</li>

										<?php
									}
									?>
								</ul>
							</nav>
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $head_title; ?></h2>
					</header>