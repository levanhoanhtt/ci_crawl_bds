<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Danh sách trang</h2>
	</header>
	<div class="panel-body">
        <a href="<?php echo base_url() ?>admin/page/add" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
        <hr/>
		<table class="table table-bordered table-striped mb-none" id="datatable-default">
			<thead>
				<tr>
					<th>Tiêu đề</th>
					<th width="15%">Hoạt động</th>
					<th width="15%">Hành động</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($all_page as $item) {
				 	?>
				 	<tr>
				 		<td>
				 			<?php echo $item['page_title']; ?>
				 			<br />
				 			<a href="<?php echo $this->permalink->get_page_link($item['page_id']); ?>" target="_blank"><i class="fa fa-eye"></i> Xem</a>
				 		</td>
				 		<td class="text-center">
							<span class="label label-<?php echo ($item['active']==0)?"warning":"success"; ?>">
								<?php echo ($item['active']==0)?"Không kích hoạt":"Kích hoạt"; ?>
							</span>
						</td>
				 		<td class="text-center">
				 			<a href="<?php echo base_url() ?>admin/page/edit/<?php echo $item['page_id']; ?>"><i class="fa fa-edit"></i></a>
							<a href="<?php echo base_url() ?>admin/page/del/<?php echo $item['page_id']; ?>" onclick="return confirm('Xóa trang này?');"><i class="fa fa-trash-o"></i></a>
				 		</td>
				 	</tr>
				 	<?php
				} 
				?>
			</tbody>
			<tfoot>
				<tr>
					<th>Tiêu đề</th>
					<th>Hoạt động</th>
					<th>Hành động</th>
				</tr>
			</tfoot>
		</table>
		<div class="add-new text-right">
			<a href="<?php echo base_url() ?>admin/page/add" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
		</div>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>