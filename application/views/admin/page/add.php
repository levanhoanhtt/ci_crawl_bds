<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
    {
        ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <?php
    }
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Thêm mới trang</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="page_title">Tiêu đề trang</label>
				<div class="col-sm-8 col-md-8">
					<input type="text" name="page_title" class="form-control to-slug" id="page_title" value="<?php echo ($data_post)?$data_post['page_title']:false; ?>" placeholder="Tiêu đề trang...">
				</div>
			</div>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="page_slug">Đường dẫn tĩnh</label>
                <div class="col-sm-8 col-md-8">
                    <input type="text" name="page_slug" class="form-control main-slug" id="page_slug" value="<?php echo ($data_post)?$data_post['page_slug']:false; ?>" placeholder="Đường dẫn tĩnh...">
                </div>
            </div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="page_content">Nội dung</label>
				<div class="col-sm-8 col-md-8">
					<textarea name="page_content" id="page_content" class="form-control" rows="5"><?php echo ($data_post)?$data_post['page_content']:false; ?></textarea>
				</div>
				<script>
					CKEDITOR.replace("page_content");
				</script>
			</div>

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Giao diện</label>
				<div class="col-sm-8 col-md-8">
					<select name="template" id="" class="form-control">
						<option value="0">Giao diện mặc định</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Kích hoạt</label>
				<div class="col-sm-8 col-md-8">
					<div class="switch switch-sm switch-success">
						<input type="checkbox" name="active" data-plugin-ios-switch="" <?php echo ($data_post && $data_post['active'])?"checked":false; ?> style="display: none;">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Thêm</button>
					<a href="<?php echo base_url() ?>admin/page" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>