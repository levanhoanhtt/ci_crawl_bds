<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title"><?php echo $page_title ?></h2>
	</header>
	<div class="panel-body">
		<div class="add-new text-right">
			<a href="<?php echo base_url('admin/broker/export_broker'); ?>" class="btn btn-custom">Xuất Excel</a>
			<a href="<?php echo base_url('admin/broker/add'); ?>" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
		</div>
		<div class="row" style="padding-bottom: 40px;">
			<form action="<?php echo base_url('admin/broker') ?>" method="get" >
				<div class="col-md-4 col-md-offset-2 form-group">
					<input type="text" placeholder="Từ khóa..." name="search" class="form-control">
				</div>
				<div class="col-md-4 form-group">
					<button class="btn btn-primary" style="width: 120px;">Tìm kiếm</button>
				</div>
			</form>
		</div>
		<table class="table table-bordered table-striped mb-none">
			<thead>
				<tr>
					<th width="20px">STT</th>
					<th class="text-center">Họ và Tên</th>
					<th class="text-center">Số điện thoại</th>
					<th class="text-center">Email</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$count = 0;
				foreach ($brokers_broker as $item) {
					$count++;
					?>
					<tr>
						<td><?= $count ?></td>
						<td><?php echo $item["name"]; ?></td>
						<td><?php echo strip_tags($item["phone"]); ?></td>
						<td><?php echo strip_tags($item["email"]); ?></td>
						<td class="text-center table-action">
							<a href="<?php echo base_url() ?>admin/broker/edit/<?php echo $item['id']; ?>"><i class="fa fa-edit"></i></a>
							<a href="<?php echo base_url() ?>admin/broker/del/<?php echo $item['id']; ?>" onclick="return confirm('Xóa người dùng này?');"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th>STT</th>
					<th class="text-center">Họ và Tên</th>
					<th class="text-center">Số điện thoại</th>
					<th class="text-center">Email</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</tfoot>
		</table>
		<ul class="pagination">
			<?php echo $this->pagination->create_links(); ?>
		</ul>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>
<script type="text/javascript">
	$('#datatable-default-bro').dataTable({
	    "bPaginate": false
	});
</script>