<?php require_once APPPATH . '/views/admin/header.php'; ?>
    <section class="panel">
        <?php
        if ($msg) {
            ?>
            <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
                <?php echo $msg['content']; ?>
            </div>
            <?php
        }

        if (validation_errors() != '') {
            ?>
            <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
            <?php
        }
        ?>
        <header class="panel-heading">
            <h2 class="panel-title"><?php echo $page_title; ?></h2>
        </header>
        <div class="panel-body">
            <form class="form-horizontal form-bordered" method="post">
                <div class="form-group">
                    <div class="col-sm-8 col-md-8">
                        <input type="hidden" name="id" class="form-control" id="id">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 control-label" for="your_name">Họ và Tên</label>
                    <div class="col-sm-8 col-md-8">
                        <input type="text" name="your_name" class="form-control" id="your_name"
                               value="<?php echo isset($data) ? $data['name'] : ''; ?>" placeholder="Họ và tên">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 control-label" for="your_phone">Số điện thoại</label>
                    <div class="col-sm-8 col-md-8">
                        <input type="text" name="your_phone" class="form-control" id="your_phone" placeholder="SĐT">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 col-md-3"></div>
                    <div class="col-sm-8 col-md-8">
                        <button class="btn btn-custom" type="submit">Thêm</button>
                        <a href="<?php echo base_url() ?>admin/lead" class="btn btn-primary">Quay lại danh sách</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
<?php require_once APPPATH . '/views/admin/footer.php'; ?>