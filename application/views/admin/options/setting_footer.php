<?php require_once APPPATH.'/views/admin/header.php'; ?>

<?php
if($msg){
    ?>
    <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
        <?php echo $msg['content']; ?>
    </div>
    <?php
}
if (validation_errors()!='')
{
    ?>
    <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
    <?php
}
?>
    <section class="panel">

        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
            </div>
            <h2 class="panel-title">Cập nhật thông tin chân trang</h2>
        </header>
        <div class="panel-body">
            <?php
            $current_opt = json_decode($current_opt['opt_value']);
            ?>
            <form class="form-horizontal form-bordered" method="post">
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 control-label" for="area1">Khu vực 1</label>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="area1_title" value="<?=$current_opt->area1_title;?>" placeholder="Tiêu đề 1..."/>
                        </div>
                        <div class="form-group">
                            <textarea name="area1" id="area1" class="form-control" rows="5"><?=$current_opt->area1;?></textarea>
                        </div>
                    </div>
                    <script>
                        CKEDITOR.replace("area1");
                    </script>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 control-label" for="area2">Khu vực 2</label>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="area2_title" value="<?=$current_opt->area2_title;?>" placeholder="Tiêu đề 2..."/>
                        </div>
                        <div class="form-group">
                            <textarea name="area2" id="area2" class="form-control" rows="5"><?=$current_opt->area2;?></textarea>
                        </div>
                    </div>
                    <script>
                        CKEDITOR.replace("area2");
                    </script>
                </div>

                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 control-label" for="area2">Khu vực 3</label>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="area3_title" value="<?=$current_opt->area3_title;?>" placeholder="Tiêu đề 3..."/>
                        </div>
                        <div class="form-group">
                            <textarea name="area3" id="area3" class="form-control" rows="5"><?=$current_opt->area3;?></textarea>
                        </div>
                    </div>
                    <script>
                        CKEDITOR.replace("area3");
                    </script>
                </div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-3"></div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <button class="btn btn-custom">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>