<?php require_once APPPATH.'/views/admin/header.php'; ?>

<?php
if($msg){
    ?>
    <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
        <?php echo $msg['content']; ?>
    </div>
    <?php
}
if (validation_errors()!='')
{
    ?>
    <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
    <?php
}
?>
    <section class="panel">

        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
            </div>
            <h2 class="panel-title">Cập nhật thông tin đầu trang</h2>
        </header>
        <?php
        $current_opt = json_decode($current_opt['opt_value']);
        ?>
        <div class="panel-body">
            <form class="form-horizontal form-bordered" method="post">
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 control-label" for="phone_number">Số điện thoại</label>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <input type="text" name="phone_number" class="form-control" id="phone_number" value="<?php echo (!empty($current_opt->phone_number))?$current_opt->phone_number:false; ?>" placeholder="Nhập số điện thoại..." />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 control-label" for="email_address">Link Facebook</label>
                    <div class="col-sm-8 col-md-8">
                        <input type="text" name="facebook_link" class="form-control" id="facebook_link" value="<?php echo (!empty($current_opt->facebook_link))?$current_opt->facebook_link:false; ?>" placeholder="Nhập link facebook..." />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 col-md-3 control-label" for="email_address">Giá trên ngày</label>
                    <div class="col-sm-8 col-md-8">
                        <input type="text" name="money_fee" class="form-control" id="money_fee" value="<?php echo (!empty($current_opt->money_fee))?$current_opt->money_fee:false; ?>" placeholder="Nhập giá trên ngày..." />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-3"></div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <button class="btn btn-custom">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>