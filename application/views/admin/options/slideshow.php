<?php require_once APPPATH.'/views/admin/header.php'; ?>

<?php
if($msg){
    ?>
    <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
        <?php echo $msg['content']; ?>
    </div>
    <?php
}
if (validation_errors()!='')
{
    ?>
    <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
    <?php
}
?>
    <section class="panel">

        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
            </div>
            <h2 class="panel-title">Thiết lập slideshow</h2>
        </header>
        <div class="panel-body">
            <form class="form-horizontal form-bordered" method="post">

                <div class="list-partner-box">
                    <?php
                    $list_slideshow = json_decode($current_opt['opt_value']);
                    if(!empty($list_slideshow)){
                        $d = 0;
                        foreach ($list_slideshow as $item){
                            if(!empty($item)){
                                $d++;
                                ?>
                                <div class="form-group item-partner">
                                    <div class="col-xs-12 col-sm-3"></div>
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-7 col-md-8">
                                                <div class="input-box">
                                                    <input id="thumbnail" type="text" name="thumbnail[]" class="form-control finder-img" value="<?=$item->img?>" readonly placeholder="Đường dẫn ảnh">
                                                    <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                                                </div><br />
                                                <input type="text" class="form-control" placeholder="Liên kết" name="link[]" value="<?=$item->link?>" />
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-4">
                                                <div class="box-img text-right">
                                                    <img src="<?=$item->img?>" class="imgProduct img-thumbnail active"/>
                                                    <button type="button" class="remove-partner btn btn-danger"><i class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        if($d ==0){
                            ?>
                            <div class="form-group item-partner">
                                <div class="col-xs-12 col-sm-3"></div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-7 col-md-8">
                                            <div class="input-box">
                                                <input id="thumbnail" type="text" name="thumbnail[]" class="form-control finder-img" value="" readonly placeholder="Đường dẫn ảnh">
                                                <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                                            </div><br />
                                            <input type="text" class="form-control" placeholder="Liên kết" name="link[]" value="" />
                                        </div>
                                        <div class="col-xs-12 col-sm-5 col-md-4">
                                            <div class="box-img text-right">
                                                <img src="" class="imgProduct img-thumbnail"/>
                                                <button type="button" class="remove-partner btn btn-danger"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }else{
                        ?>
                        <div class="form-group item-partner">
                            <div class="col-xs-12 col-sm-3"></div>
                            <div class="col-xs-12 col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-7 col-md-8">
                                        <div class="input-box">
                                            <input id="thumbnail" type="text" name="thumbnail[]" class="form-control finder-img" value="" readonly placeholder="Đường dẫn ảnh">
                                            <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 col-md-4">
                                        <div class="box-img text-right">
                                            <img src="" class="imgProduct img-thumbnail"/>
                                            <button type="button" class="remove-partner btn btn-danger"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-3"></div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <button class="btn btn-custom">Lưu</button>
                        <button type="button" class="more-partner btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>