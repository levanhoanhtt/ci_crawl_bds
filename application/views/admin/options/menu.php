<?php require_once APPPATH.'/views/admin/header.php'; ?>

<?php 
if($msg){
	?>
	<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
		<?php echo $msg['content']; ?>
	</div>
	<?php
}
?>
<section class="panel">
	
	<header class="panel-heading">
		<div class="panel-actions">
			<a href="#" class="fa fa-caret-down"></a>
		</div>
		<h2 class="panel-title">Thêm menu</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
				<label class="col-xs-12 col-sm-4 control-label" for="menu_name">Tiêu đề hiển thị</label>
				<div class="col-xs-12 col-sm-8 col-md-6">
					<input type="text" name="menu_name" class="form-control" id="menu_name" value="" placeholder="Tiêu đề hiển thị" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-12 col-sm-4 control-label" for="menu_link">Đường dẫn</label>
				<div class="col-xs-12 col-sm-8 col-md-6">
					<input type="text" name="menu_link" class="form-control" id="menu_link" value="" placeholder="Đường dẫn" required>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-sm-4"></div>
				<div class="col-xs-12 col-sm-8 col-md-6">
					<input type="submit" class="btn btn-custom" name="add_menu" value="Thêm" />
				</div>
			</div>
		</form>
	</div>
</section>
<section class="panel">
	<header class="panel-heading">
		<div class="panel-actions">
			<a href="#" class="fa fa-caret-down"></a>
		</div>
		<h2 class="panel-title">Cập nhật menu</h2>
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-offset-2 col-sm-offset-2 col-lg-offset-2">
				<div class="dd list-menu" id="nestable">
					<?php 
					echo $show_menu;
					?>
				</div>
				<form action="" method="post">
					<textarea id="nestable-output" name="menu_content" rows="3" class="form-control hidden"></textarea>
					<input type="submit" class="btn btn-custom" id="save-menu" name="save_menu" value="Lưu" disabled />
				</form>
			</div>
		</div>
	</div>
</section>
<!-- Modal Form -->
<div id="modal-menu" class="modal-block modal-block-primary mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Cập nhật menu</h2>
		</header>
		<div class="panel-body">
			<div class="form-group row">
				<label class="col-sm-3 control-label">Tên hiển thị</label>
				<div class="col-sm-9">
					<input type="text" name="name" class="form-control edit-name" placeholder="Tên hiển thị" required/>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-3 control-label">Đường dẫn</label>
				<div class="col-sm-9">
					<input type="text" name="name" class="form-control edit-link" placeholder="Đường dẫn" required/>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-custom modal-dismiss save-edit-menu">Sửa</button>
					<button class="btn btn-default modal-dismiss exit-modal">Hủy</button>
				</div>
			</div>
		</footer>
	</section>
</div>
<script>
	jQuery(document).ready(function($) {
		jQuery(".edit-menu").click(function(e){
			e.preventDefault();
			jQuery(this).parent().parent().addClass("editing");
			var name = jQuery(this).parent().parent().data("name"),
				link = jQuery(this).parent().parent().data("link");
			jQuery("#modal-menu .edit-name").val(name);
			jQuery("#modal-menu .edit-link").val(link);
		});

		jQuery(".exit-modal").click(function(){
			jQuery(this).parents("body").find(".editing").removeClass("editing");
		});

		(function( $ ) {
			jQuery(".save-edit-menu").click(function(){
				var name = jQuery(this).parents("#modal-menu").find(".edit-name").val(),
					link = jQuery(this).parents("#modal-menu").find(".edit-link").val();
				jQuery(".dd-item.editing").data("name",name);
				jQuery(".dd-item.editing").data("link",link);
				jQuery(".dd-item.editing>.dd-handle").html(name);
				jQuery(".dd-item.editing").removeClass("editing");
				updateOutput($('#nestable').data('output', $('#nestable-output')));
				jQuery("#save-menu").prop("disabled", false); // Element(s) are now enabled.
			});

			jQuery(".remove-menu").click(function(e){
		    	e.preventDefault();
		    	if(confirm("Xóa menu này và các phần tử bên trong?")){
					jQuery(this).parent().remove();
					updateOutput($('#nestable').data('output', $('#nestable-output')));
					$("#save-menu").prop("disabled", false); // Element(s) are now enabled.
		    	}
		    });

			'use strict';
				
			/*
			Update Output
			*/
			var updateOutput = function (e) {
				var list = e.length ? e : $(e.target),
					output = list.data('output');
				if (window.JSON) {
					output.val(window.JSON.stringify(list.nestable('serialize')));
				} else {
					output.val('JSON browser support required for this demo.');
				}
			};

			/*
			Nestable 1
			*/
			$('#nestable').nestable({
				group: 1
			}).on('change', function(){
				$("#save-menu").prop("disabled", false); // Element(s) are now enabled.
				updateOutput($('#nestable').data('output', $('#nestable-output')));
			});

			/*
			Output Initial Serialised Data
			*/
			$(function() {
				updateOutput($('#nestable').data('output', $('#nestable-output')));
			});
		}).apply(this, [ jQuery ]);
	});
</script>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>