<?php require_once APPPATH.'/views/admin/header.php'; ?>
<?php

if($msg){

    ?>

    <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">

        <?php echo $msg['content']; ?>

    </div>

    <?php

}

if (validation_errors()!='')

{

    ?>

    <div class="alert alert-danger"><?php echo validation_errors(); ?></div>

    <?php

}

?>

    <section class="panel">



        <header class="panel-heading">

            <div class="panel-actions">

                <a href="#" class="fa fa-caret-down"></a>

            </div>

            <h2 class="panel-title">Cập nhật thông tin trang chủ</h2>

        </header>

        <div class="panel-body">

            <?php
            $current_opt = json_decode($current_opt['opt_value'] , true);

            ?>

            <form class="form-horizontal form-bordered" method="post">

                <div class="form-group">

                    <label class="col-xs-12 col-sm-3 control-label" for="area1">Nội dung chính</label>

                    <div class="col-xs-12 col-sm-8 col-md-8">

                        <div class="form-group">

                            <textarea name="area1" id="area1" class="form-control" rows="5"><?= $current_opt['main-content']; ?></textarea>

                        </div>

                    </div>

                    <script>

                        CKEDITOR.replace("area1");

                    </script>

                </div>
                

                <div class="form-group">

                    <label class="col-xs-12 col-sm-3 control-label" for="area1">Nội dung dưới</label>

                    <div class="col-xs-12 col-sm-8 col-md-8">

                        <div class="form-group">
                            <textarea name="area2" id="area2" class="form-control" rows="5"><?=$current_opt['main_banner_content'];?></textarea>
                            <label class=" control-label" for="area3">Giá </label>
                            <textarea name="area3" id="area2" class="form-control" rows="5"><?=$current_opt['banner_pricetext'];?></textarea>
                            <!-- ảnh nền  -->
                            <label class=" control-label" for="area1">Ảnh nền </label>
                            <div class="row" style="padding: 15px;">
                                <div class="col-xs-6 col-sm-3 col-md-3">
                                    <div class="input-box" style="position: unset;">
                                        <input id="thumbnail" type="hidden" name="banner" class="form-control finder-img" value="<?=$current_opt['banner_image'];?>" readonly placeholder="Đường dẫn ảnh">
                                        <button type="button" class="btn btn-success choose_image_product" style="position: unset; transform: translateY(0%);">Tải ảnh lên <i class="fa fa-upload"></i></button>
                                    </div><br />

                                </div>
                                <div class="col-xs-6 col-sm-9 col-md-9">
                                    <div class="box-img text-right">
                                        <img src="<?=$current_opt['banner_image'];?>" width="60" height="60" class="imgProduct img-thumbnail" style="display:block;" />
                                        <!-- <button type="button" class="remove-feedback btn btn-danger"><i class="fa fa-times"></i></button> -->
                                    </div>
                                </div>
                            </div>
                            
                            <label class=" control-label" for="area1">Link liên kết</label>
                            <input type="text" name="link_banner" class="form-control" id="link_banner" value="<?=$current_opt['banner_link'];?>" placeholder="Nhập số link liên kết..." />
                        </div>


                    </div>

                    <script>

                        CKEDITOR.replace("area2");
                        CKEDITOR.replace("area3");

                    </script>

                </div>

                <div class="form-group">

                    <div class="col-xs-12 col-sm-3"></div>

                    <div class="col-xs-12 col-sm-8 col-md-8">

                        <button class="btn btn-custom">Lưu</button>

                    </div>

                </div>

            </form>

        </div>

    </section>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>