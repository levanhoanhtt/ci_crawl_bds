<?php require_once APPPATH.'/views/admin/header.php'; ?>

<?php
if($msg){
    ?>
    <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
        <?php echo $msg['content']; ?>
    </div>
    <?php
}
if (validation_errors()!='')
{
    ?>
    <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
    <?php
}
?>
    <section class="panel">

        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
            </div>
            <h2 class="panel-title">Thiết lập hệ thống</h2>
        </header>
        <div class="panel-body">
            <form class="form-horizontal form-bordered" method="post">

                <div class="form-group">
                    <div class="col-xs-12 col-sm-3">
                        Link cũ:
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <input type="text" class="form-control" name="old_link" placeholder="Link cũ..."/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-3">
                        Link mới:
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <input type="text" class="form-control" name="new_link" placeholder="Link mới..."/>
                    </div>
                </div>

                <hr/>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-3"></div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <button class="btn btn-custom">Xác nhận</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>