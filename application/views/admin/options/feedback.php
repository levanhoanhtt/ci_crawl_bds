<?php require_once APPPATH.'/views/admin/header.php'; ?>

<?php
if($msg){
    ?>
    <div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
        <?php echo $msg['content']; ?>
    </div>
    <?php
}
if (validation_errors()!='')
{
    ?>
    <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
    <?php
}
?>
    <section class="panel">

        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
            </div>
            <h2 class="panel-title">Thiết lập cảm nhận khách hàng</h2>
        </header>
        <div class="panel-body">
            <form class="form-horizontal form-bordered" method="post">

                <div class="list-feedback-box">
                    <?php
                    $list_feedback = json_decode($current_opt['opt_value']);
                    if(!empty($list_feedback)){
                        $d = 0;
                        foreach ($list_feedback as $item){
                            if(!empty($item)){
                                $d++;
                                ?>
                                <div class="form-group item-partner">
                                    <div class="col-xs-12 col-sm-3"></div>
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-7 col-md-8">
                                                <div class="input-box">
                                                    <input id="thumbnail" type="text" name="thumbnail[]" class="form-control finder-img" value="<?=$item->img?>" readonly placeholder="Đường dẫn ảnh">
                                                    <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                                                </div><br />
                                                <input type="text" class="form-control" placeholder="Tên khách hàng..." name="name[]" value="<?=$item->name?>" />
                                                <br/>
                                                <textarea class="form-control" placeholder="Nội dung..." name="content[]"><?=$item->content?></textarea>
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-4">
                                                <div class="box-img text-right">
                                                    <img src="<?=$item->img?>" class="imgProduct img-thumbnail active"/>
                                                    <button type="button" class="remove-feedback btn btn-danger"><i class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        if($d ==0){
                            ?>
                            <div class="form-group item-partner">
                                <div class="col-xs-12 col-sm-3"></div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-7 col-md-8">
                                            <div class="input-box">
                                                <input id="thumbnail" type="text" name="thumbnail[]" class="form-control finder-img" value="" readonly placeholder="Đường dẫn ảnh">
                                                <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                                            </div><br />
                                            <input type="text" class="form-control" placeholder="Tên khách hàng..." name="name[]" value="" />
                                            <br/>
                                            <textarea class="form-control" placeholder="Nội dung..." name="content[]"></textarea>
                                        </div>
                                        <div class="col-xs-12 col-sm-5 col-md-4">
                                            <div class="box-img text-right">
                                                <img src="" class="imgProduct img-thumbnail"/>
                                                <button type="button" class="remove-feedback btn btn-danger"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }else{
                        ?>
                        <div class="form-group item-partner" style="display: none">
                            <div class="col-xs-12 col-sm-3"></div>
                            <div class="col-xs-12 col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-7 col-md-8">
                                        <div class="input-box">
                                            <input id="thumbnail" type="text" name="thumbnail[]" class="form-control finder-img" value="" readonly placeholder="Đường dẫn ảnh">
                                            <button type="button" class="btn btn-success choose_image_product"><i class="fa fa-upload"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 col-md-4">
                                        <div class="box-img text-right">
                                            <img src="" class="imgProduct img-thumbnail"/>
                                            <button type="button" class="remove-feedback btn btn-danger"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <hr/>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-3"></div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <button class="btn btn-custom">Lưu</button>
                        <button type="button" class="more-feedback btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
<?php require_once APPPATH.'/views/admin/footer.php'; ?>