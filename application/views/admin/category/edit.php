<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	if($msg){
		?>
		<div class="alert alert-<?php echo $msg['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $msg['content']; ?>
		</div>
		<?php
	}
	if (validation_errors()!='')
    {
        ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <?php
    }
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Cập nhật chuyên mục tin tức</h2>
	</header>
	<div class="panel-body">
		<form class="form-horizontal form-bordered" method="post">
			<div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="cate_name">Tên chuyên mục</label>
                <div class="col-sm-8 col-md-8">
                    <input type="text" name="cate_name" class="form-control to-slug" id="cate_name" value="<?php echo $current_cate['cate_name']; ?>" placeholder="Tên chuyên mục">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="cate_slug">Slug</label>
                <div class="col-sm-8 col-md-8">
                    <input type="text" name="cate_slug" class="form-control main-slug" id="cate_slug" value="<?php echo $current_cate['cate_slug']; ?>" placeholder="Slug">
                </div>
            </div>

            <div class="form-group">
				<label class="col-sm-3 col-md-3 control-label" for="thumbnail">Ảnh đại diện</label>
				<div class="col-sm-8 col-md-8">
					<div class="row">
                      	<div class="col-xs-12 col-sm-7 col-md-8">   
                      		<div class="input-box">
                      			<input id="thumbnail" type="text" name="thumbnail" class="form-control finder-img" value="<?php echo $current_cate['banner']; ?>" readonly placeholder="Đường dẫn ảnh">
                      			<button type="button" class="btn btn-success choose_image_product">Thêm ảnh</button>
                      		</div>
                      	</div>
                      	<div class="col-xs-12 col-sm-5 col-md-4">   
                      		<img src="<?php echo base_url().$current_cate['banner']; ?>" class="imgProduct img-thumbnail <?php echo $current_cate['banner']?'active':false; ?>" width="200"/>
                      	</div>
                    </div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Danh mục cha</label>
				<div class="col-sm-8 col-md-8">
					<select data-plugin-selectTwo class="form-control" name="parent_id">
						<option value="0">Không</option>
						<?php 
						foreach ($all_cate as $item) {
							if($item['cate_id'] == $current_cate['cate_id'] || $item['parent_id'] == $current_cate['cate_id']) continue;
							$check_child = 0;
							foreach ($child_cate as $child) {
								if($item['cate_id'] == $child['cate_id']){
									$check_child=1;
									break;
								}
							}
							if($check_child==1) continue;
							?>
							<option value="<?php echo $item['cate_id'] ?>" <?php echo ($current_cate['parent_id'] == $item['cate_id'])?"selected":false; ?>><?php echo $item['display']; ?></option>
							<?php
						}
						?>
					</select>
				</div>
			</div>

            <div class="form-group">
                <label class="col-sm-3 col-md-3 control-label" for="cate_name">Link lấy tin</label>
                <div class="col-sm-8 col-md-8">
                    <textarea class="form-control" name="crawl_link" rows="10" placeholder="Nhập link cần lấy tin (Mỗi link 1 dòng)"><?php echo ($current_cate && $current_cate['crawl_link'])?$current_cate['crawl_link']:FALSE; ?></textarea>
                </div>
            </div>

			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Kích hoạt</label>
				<div class="col-sm-8 col-md-8">
					<div class="switch switch-sm switch-success">
						<input type="checkbox" name="active" data-plugin-ios-switch="" <?php echo ($current_cate['active'] == 1)?"checked":false; ?> style="display: none;">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-3 col-md-3"></div>
				<div class="col-sm-8 col-md-8">
					<button class="btn btn-custom" type="submit">Lưu</button>
					<a href="<?php echo base_url() ?>admin/category" class="btn btn-primary">Quay lại danh sách</a>
				</div>
			</div>
		</form>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>