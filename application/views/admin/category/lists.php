<?php require_once APPPATH.'/views/admin/header.php'; ?>

<section class="panel">
	<?php 
	if($this->session->flashdata('msg')){
		$alert = $this->session->flashdata('msg');
		?>
		<div class="alert alert-<?php echo $alert['type']; ?>" style="margin-bottom: 15px;">
			<?php echo $alert['content']; ?>
		</div>
		<?php
		unset($_SESSION['msg']);
	}
	?>
	<header class="panel-heading">
		<h2 class="panel-title">Danh mục tin đăng</h2>
	</header>
	<div class="panel-body">
		<table class="table table-bordered table-striped mb-none" id="no-sort-dttb">
			<thead>
				<tr>
					<th class="text-center">Tên danh mục</th>
					<th class="text-center">Kích hoạt</th>
					<th class="text-center table-action">Hành động</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($all_cate as $item) {
					?>
					<tr>
						<td>
							<?php echo $item['display']; ?>
						</td>
						<td class="text-center">
							<span class="label label-<?php echo ($item['active']==0)?"warning":"success"; ?>">
								<?php echo ($item['active']==0)?"Không kích hoạt":"Kích hoạt"; ?>
							</span>
						</td>
						<td class="text-center table-action">
							<a href="<?php echo base_url() ?>admin/category/edit/<?php echo $item['cate_id']; ?>"><i class="fa fa-edit"></i></a>
							<a href="<?php echo base_url() ?>admin/category/del/<?php echo $item['cate_id']; ?>" onclick="return confirm('Xóa ngành danh mục này và tất cả các danh mục con?');"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th class="text-center">Tên danh mục</th>
					<th class="text-center">Kích hoạt</th>
					<th class="text-center">Hành động</th>
				</tr>
			</tfoot>
		</table>
		<div class="add-new text-right">
			<a href="<?php echo base_url() ?>admin/category/add" class="btn btn-custom"><i class="fa fa-plus"></i> Thêm mới</a>
		</div>
	</div>
</section>

<?php require_once APPPATH.'/views/admin/footer.php'; ?>