<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	protected $_data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Mpost');
        $this->load->model('Mcategory');
        $this->load->model('Muser');
        $this->load->model('Mpost');
        $this->load->library('Globals');
        $this->load->library('Get_data');

        $login = $this->session->get_userdata();
        if(!empty($login['username'])){
            $info_user = $this->Muser->get_user_info($login['username']);
            $this->session->set_userdata('user_info',$info_user);
            $this->_data['login'] = $login['user_info'];
        }else{
            $this->_data['login'] = array();
        }
    }

	public function index()
	{
        $id = explode("-",$this->uri->segment(2));
        $id = $id[0];
        if($id){
            $check = $this->Mcategory->mem_check_cate_id($id);
            if($check==0){
                redirect(base_url(),'refresh');
            }
        }else{
            redirect(base_url(),'refresh');
        }

        $current_cate = $this->Mcategory->mem_get_cate_by("cate_id",$id);
        $current_cate['list_parent'] = $this->get_all_parent($id);
        $this->_data['page_title'] = $current_cate['cate_name'];

        die('Coming Soon');

	}

    // Đệ quy category
    public function get_cate_recursive($parent_id=0,$result=''){
        if(!is_array($result)){
            $result = [];
        }
        $get_cate = $this->Mcategory->ad_get_cate_child_active($parent_id);
        foreach ($get_cate as $item) {
            $result[] = $item['cate_id'];
            $result = $this->get_cate_recursive($item['cate_id'],$result);
        }
        return $result;
    }

    // Lấy ra các cha cho breadcrumb
    public function get_all_parent($cate_id,$result=''){
        if(!is_array($result)){
            $result = [];
        }

        $this_cate = $this->Mcategory->mem_get_cate_by("cate_id",$cate_id);
        if($this_cate['parent_id'] <> 0){
            $get_cate = $this->Mcategory->mem_get_cate_by("cate_id",$this_cate['parent_id']);
            $result[] = $get_cate;
            $result = $this->get_all_parent($get_cate['cate_id'],$result);
        }
        return $result;
    }

    // Get all post of cate member
    public function mem_get_post_by_cate($cate_id){
        $all_post = $this->Mpost->mem_get_all_post();
        $cate_get_post[] = $cate_id;
        $cate_get_post = $this->get_cate_recursive($cate_id,$cate_get_post);
        $posts = [];
        foreach ($cate_get_post as $item) {
            $item = (string)$item;
            foreach ($all_post as $key => $post) {
                $post['excerpt'] = $this->globals->custom_cut($post['post_content'],30);
                $cate_arr = json_decode($post['cate_id']);
                if(in_array($item,$cate_arr)){
                    $posts[] = $post;
                    unset($all_post[$key]);
                }
            }
        }
        return $posts;
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */