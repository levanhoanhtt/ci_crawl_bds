<?php
require 'vendor/autoload.php';
use Elasticsearch\ClientBuilder;

class Elasticsearch  extends CI_Controller

{
    public function __construct(){
        parent::__construct();

        $this->load->model('Mpost');

    }
    
    public function sync(){
        $postModel = new Mpost();
        $hosts = [
            '127.0.0.1:9200',         // IP + Port
        ];

        $client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();

//
//
        $allPostCount = $postModel->get_count_post(['sync_elastic'=>0, 'status_crawler' => 1],'',[]);
        $currentPost = 0;
        $perTime = 100000;
        //mapping

//        $client->indices()->create([
//            'index' => 'bds'
//        ]);
//        $configParams = [
//            'index' => 'bds',
//            'body' => [
//                'settings' => [
//                    'number_of_shards' => 3,
//                    'number_of_replicas' => 2
//                ],
//                'mappings' => [
//                    'bds_post' => [
//                        '_source' => [
//                            'enabled' => false
//                        ],
//                        'properties' => [
//                            'post_title' => [
//                                'type' => 'string',
//                                'analyzer' => 'standard'
//                            ],
//                        ]
//                    ]
//                ]
//            ]
//        ];
//        $client->indices()->create($configParams);
        while($allPostCount > $currentPost){
            $allPosts = $this->Mpost->get_all_post(['sync_elastic'=>0, 'status_crawler' => 1],'',[],$perTime,$currentPost);
            foreach($allPosts as $post){
                //Sửa lại định dạng dữ liệu
                $post['post_id'] = (int)$post['post_id'];
                $post['timestamp'] = (int)$post['timestamp'];
                $post['province_id'] = (int)$post['province_id'];
                $post['district_id'] = (int)$post['district_id'];
                $post['price'] = (float)$post['price'];
                $post['feadtured_post'] = (int)$post['feadtured_post'];
                $post['for_vip_user'] = (int)$post['for_vip_user'];
                $post['active'] = (int)$post['active'];
                $post['handling'] = (int)$post['handling'];
                $post['crawler_type'] = (int)$post['crawler_type'];
                $post['clone'] = (int)$post['clone'];
                $post['broker'] = (int)$post['broker'];
                $post['status_crawler'] = (int)$post['status_crawler'];
                $post['sync_elastic'] = (int)$post['sync_elastic'];

                //Khai bao noi dung index
                $params['body']['upsert'] = $post;
                $params['body']['doc'] = $post;
                $params['index'] = 'bds';
                $params['type'] = 'bds_post';
                $params['id'] = $post['post_id'];



                $response = $client->update($params);

                //Update bản ghi trong Mysql
                $this->Mpost->update($post['post_id'],['sync_elastic' => 1]);

                echo 'Insert :'.$post['post_title']."\n";
            }
            $currentPost += $perTime;
            echo 'Insert :'.$currentPost."\n";
        }

    }

}
