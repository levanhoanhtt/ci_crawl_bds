<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller
{

    protected $_data;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');

        $this->load->model('Muser');
        $this->load->model('Muser_payments');
        $this->load->library('Globals');
        $this->load->library('Get_data');

        $login = $this->session->get_userdata();
        if (!empty($login['username'])) {
            $info_user = $this->Muser->get_user_info($login['username']);
            $login['user_info'] = $info_user;
            $this->session->set_userdata('user_info', $info_user);
            $this->_data['login'] = $login['user_info'];
        } else {
            $this->_data['login'] = array();
        }

    }

    public function index()
    {
        redirect('account/profile');
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $response = array();
        if (!empty($username) && !empty($password)) {
            $password = md5($password);
            $a = $this->Muser->check_users_log($username);
            if (($this->Muser->check_login($username, $password) > 0) && ($this->Muser->check_users_log($username) == 0)) {
                $this->session->set_userdata('username', $username);

                $response['status'] = 2;
                $data = array(
                    'id' => '',
                    'username' => $username,
                    'time' => date("Y/m/d h:i:s")
                );
                $data_update = array('last_login' => date("Y-m-d h:i:s"));
                $this->Muser->insert_users_log($data);
                $this->Muser->update_login($data['username'], $data_update);

            } else if (($this->Muser->check_users_log($username)) > 0) {
                $response['status'] = 3;
            } else {
                $response['status'] = 1;
            }
        } else {
            $response['status'] = 0;
        }

        echo json_encode($response);
    }

    public function check_email($email)
    {
        $user_id = $this->get_data->get_current_user();
        if ($this->Muser->check_email($email, $user_id) == 0) {
            return true;
        }
        return false;
    }

    public function profile()
    {
        $this->_data['page_title'] = 'Thông tin cá nhân';

        $user_id = $this->get_data->get_current_user();

        $msg = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('fullname', 'Họ và tên', 'required|trim');
            $this->form_validation->set_rules('email_address', 'Email', 'required|trim|valid_email|callback_check_email');
            $this->form_validation->set_message('check_email', 'Địa chỉ email đã có người sử dụng');
            $this->form_validation->set_rules('user_phone', 'Số điện thoại', 'required|trim');

            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', 'Mật khẩu', 'required|trim');
                $this->form_validation->set_rules('confirm_password', 'Nhập lại mật khẩu', 'required|trim|matches[password]');
            }

            if ($this->form_validation->run()) {
                $data_update = array(
                    'fullname' => $this->input->post('fullname'),
                    'email_address' => $this->input->post('email_address'),
                    'user_phone' => $this->input->post('user_phone'),
                    'user_address' => $this->input->post('user_address'),
                );

                if ($this->input->post('password')) {
                    $data_update['password'] = md5($this->input->post('password'));
                }

                if (isset($_FILES['thumbnail'])) {
                    $file_name = $_FILES['thumbnail']['name'];
                    $ext = end(explode('.', $file_name));
                    $config['upload_path'] = FCPATH . '/uploads/avatar/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = 2048;
                    $config['file_name'] = md5(uniqid()) . '.' . $ext;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('thumbnail')) {
                        $data_upload = $this->upload->data();
                        $data_update['thumbnail'] = base_url('uploads/avatar/' . $data_upload['file_name']);
                    }
                }


                $update_stt = $this->Muser->update($user_id, $data_update);

                if ($update_stt) {
                    $msg = array(
                        "type" => "success",
                        "content" => "Cập nhật thành công"
                    );
                } else {
                    $msg = array(
                        "type" => "danger",
                        "content" => "Cập nhật không thành công!"
                    );
                }

                $this->session->set_flashdata('update_msg', $msg);

                redirect('account/profile');
            }

        }

        $this->_data['msg'] = $this->session->flashdata('update_msg');

        $this->load->view('member/account/profile', $this->_data);
    }

    public function billing()
    {
        $this->_data['page_title'] = 'Lịch sử giao dịch';

        $user_id = $this->get_data->get_current_user();

        $billing = $this->Muser->list_renew($user_id);

        $this->_data['billing'] = $billing;

        $this->load->view('member/account/billing', $this->_data);
    }

    public function logout()
    {
        $this->Muser->del_user_log($_SESSION['username']);
        $this->session->unset_userdata('username');
        redirect('');
    }

    public function forget_password()
    {
        $email = $this->input->post('email_address');
        $response = array();
        if (!empty($email)) {
            if ($this->Muser->get_user_by('email_address', $email)) {
                $row = $this->Muser->get_user_by('email_address', $email);
                $token = md5(uniqid());
                $data_update = array(
                    'key_forget_password' => $token,
                );
                $update_stt = $this->Muser->update($row['user_id'], $data_update);
                if ($update_stt) {
                    $subject = 'Nguồn Nhà Đất - Lấy lại mật khẩu';
                    $content = '<p>Chào bạn:</p>';
                    $content .= '<p>Để đặt lại mật khẩu bạn vui lòng click vào link sau:</p>';
                    $content .= '<p>' . base_url('account/reset_password/' . $email . '/' . $token) . '</p>';
                    $content .= '<p>Cảm ơn!</p>';
                    $this->globals->sendMail($email, $subject, $content);

                    $response['status'] = 2;
                }

            } else {
                $response['status'] = 1;
            }
        } else {
            $response['status'] = 0;
        }

        echo json_encode($response);
    }

    public function reset_password($email, $token)
    {
        $this->_data['page_title'] = 'Đặt lại mật khẩu';

        $check_email = $this->Muser->get_user_by('email_address', $email);
        $check_token = $this->Muser->get_user_by('key_forget_password', $token);

        if ($check_email && $check_token) {
            $this->_data['check_exist'] = 1;

            $msg = '';

            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $this->form_validation->set_rules('password', 'Mật khẩu', 'required|trim');
                $this->form_validation->set_rules('confirm_password', 'Nhập lại mật khẩu', 'required|trim|matches[password]');

                if ($this->form_validation->run()) {
                    $data_update = array(
                        'password' => md5($this->input->post('password')),
                        'key_forget_password' => ''
                    );

                    $update_stt = $this->Muser->update($check_email['user_id'], $data_update);

                    if ($update_stt) {
                        $msg = array(
                            "type" => "success",
                            "content" => "Cập nhật thành công"
                        );
                    } else {
                        $msg = array(
                            "type" => "danger",
                            "content" => "Cập nhật không thành công!"
                        );
                    }

                    $this->session->set_flashdata('update_msg', $msg);

                }

            }

            $this->_data['msg'] = $this->session->flashdata('update_msg');

        } else {
            $this->_data['check_exist'] = 0;
        }

        $this->load->view('member/account/reset_password', $this->_data);
    }

    public function register()
    {
        $response = array();
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('fullname', 'Họ và tên', 'required|trim');
            $this->form_validation->set_rules('user_phone', 'Số điện thoại', 'required|trim');
            $this->form_validation->set_rules('user_address', 'Địa chỉ', 'required|trim');
            $this->form_validation->set_rules('username', 'Tên đăng nhập', 'required|trim|is_unique[users.username]');
            $this->form_validation->set_rules('password', 'Mật khẩu', 'required|trim');
            $this->form_validation->set_rules('confirm_password', 'Nhập lại mật khẩu', 'required|trim|matches[password]');
            $this->form_validation->set_rules('email_address', 'Địa chỉ email', 'required|trim|valid_email|is_unique[users.email_address]');

            if ($this->form_validation->run()) {
                $data = array(
                    'fullname' => $this->input->post('fullname'),
                    'user_phone' => $this->input->post('user_phone'),
                    'user_address' => $this->input->post('user_address'),
                    'username' => $this->input->post('username'),
                    'email_address' => $this->input->post('email_address'),
                    'password' => md5($this->input->post('password')),
                    'active' => 1
                );
                $insert_id = $this->Muser->insertUser($data);
                if ($insert_id > 0) {
                    $response['status'] = 2;
                } else {
                    $response['status'] = 1;
                }
            } else {
                $response['errors'] = array(
                    'fullname' => form_error('fullname'),
                    'user_phone' => form_error('user_phone'),
                    'user_address' => form_error('user_address'),
                    'username' => form_error('username'),
                    'password' => form_error('password'),
                    'confirm_password' => form_error('confirm_password'),
                    'email_address' => form_error('email_address')
                );
            }
        }

        echo json_encode($response);
    }

}