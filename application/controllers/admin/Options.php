<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Options extends CI_Controller {

	protected $_data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Moptions');
        $this->load->library('Globals');
    }

	public function index()
	{
		$this->_data['page_title'] = "Danh sách thuộc tính";
		$this->_data['head_title'] = "Quản lý thuộc tính";
		$this->load->view('admin/options/home.php', $this->_data);
	}

	//Menu
	public function menu(){
		$this->_data['page_title'] = "Thiết lập menu";
		$this->_data['head_title'] = "Quản lý thuộc tính";
		$opt = $this->Moptions->get_option("menu");
		$main_menu = json_decode($opt["opt_value"]);

		$msg = '';

		if($this->input->post("add_menu")){
			$this->form_validation->set_rules("menu_name","Tên hiển thị","required|trim");
			$this->form_validation->set_rules("menu_link","Đường dẫn","required|trim");
			if($this->form_validation->run() == true){
				
				$new_item = array(
					"name" => $this->input->post("menu_name"),
					"link" => $this->input->post("menu_link")
				);
				$new_item = (object) $new_item;
				$main_menu[] = $new_item;
				$data = array(
					"opt_value" => json_encode($main_menu)
				);
				if($this->Moptions->update("menu",$data)){
					$msg = array(
						"type" => "success",
						"content" => "Thêm menu thành công!"
					);
				}else{
					$msg = array(
						"type" => "danger",
						"content" => "Thêm menu không thành công!"
					);
				}
			}
		}
		if($this->input->post("save_menu")){
			$new_menu = $this->input->post("menu_content");
			$data = array(
				"opt_value" => $new_menu
			);
			if($this->Moptions->update("menu",$data)){
				$msg = array(
					"type" => "success",
					"content" => "Cật nhật menu thành công!"
				);
			}
			else
			{
				$msg = array(
					"type" => "danger",
					"content" => "Cập nhật menu không thành công!"
				);
			}
		}

		$opt = $this->Moptions->get_option("menu");
		$last_menu = json_decode($opt["opt_value"]);
		$this->_data['msg'] = $msg;
		$this->_data['show_menu'] = $this->show_menu($last_menu);
		$this->load->view('admin/options/menu.php', $this->_data);
	}

	// Đệ quy menu
	public function show_menu($arr_menu,$result=''){
		if($arr_menu){
			$result.='<ol class="dd-list">';
				foreach ($arr_menu as $key => $item) {
					$item = get_object_vars($item);
					$name = $item['name'];
					$link = $item['link'];
					$result.='<li class="dd-item" data-link="'.$link.'" data-name="'.$name.'">';
					$result.='<div class="dd-handle">'.$name.'</div>';
					if(array_key_exists("children",$item)){
						$result=$this->show_menu($item["children"],$result);
					} 
					$result.='<div class="menu-actions"><a href="#modal-menu" class="edit-menu modal-with-form"><i class="fa fa-edit"></i></a><a href="#" class="remove-menu"><i class="fa fa-times"></i></a></div></li>';
				}
			$result.='</ol>';
		}
		return $result;
	}


	// Thiết lập sidebar
	public function sidebar(){

		$this->_data['page_title'] = "Thiết lập sidebar";
		$this->_data['head_title'] = "Quản lý thuộc tính";

		$msg = '';
		if($this->input->server('REQUEST_METHOD')=='POST'){
			$sidebar = array(
			    'phone_number' => $this->input->post('phone_number')
            );
			$data = array(
				"opt_value" => json_encode($sidebar)
			);
			if($this->Moptions->update("sidebar",$data)){
				$msg = array(
					"type" => "success",
					"content" => "Cập nhật sidebar thành công!"
				);
			}else{
				$msg = array(
					"type" => "danger",
					"content" => "Cật nhật không thành công!"
				);
			}
		}

		$opt = $this->Moptions->get_option("sidebar");

		$this->_data['current_opt'] = $opt["opt_value"];
		$this->_data['msg'] = $msg;
		$this->load->view('admin/options/sidebar', $this->_data);
	}

    // Thiết đầu trang
    public function header(){
        $this->_data['page_title'] = "Thiết lập đầu trang";
        $this->_data['head_title'] = "Quản lý thuộc tính";
        $msg = '';

        $this->form_validation->set_rules("phone_number","Số điện thoại","trim");
        $this->form_validation->set_rules("facebook_link","Facebook Link","trim");
        if($this->form_validation->run() == true){
            $arr = array(
                'phone_number' => $this->input->post("phone_number"),
                'facebook_link' => $this->input->post("facebook_link"),
                'money_fee' => $this->input->post('money_fee')
            );
            $data = array(
                "opt_value" => json_encode($arr)
            );
            if($this->Moptions->update("header",$data)){
                $msg = array(
                    "type" => "success",
                    "content" => "Cập nhật thành công!"
                );
            }else{
                $msg = array(
                    "type" => "danger",
                    "content" => "Cập nhật không thành công!"
                );
            }
        }

        $this->_data['current_opt'] = $this->Moptions->get_option("header");
        $this->_data['msg'] = $msg;
        $this->load->view('admin/options/setting_header.php', $this->_data);
    }

    // Thiết chân trang
    public function footer(){
        $this->_data['page_title'] = "Thiết lập chân trang";
        $this->_data['head_title'] = "Quản lý thuộc tính";
        $msg = '';

        $this->form_validation->set_rules("area1","Khu vực 1","trim");
        $this->form_validation->set_rules("area2","Khu vực 2","trim");
        $this->form_validation->set_rules("area3","Khu vực 3","trim");
        if($this->form_validation->run() == true){
            $arr = array(
                'area1' => $this->input->post("area1"),
                'area2' => $this->input->post("area2"),
                'area3' => $this->input->post("area3"),
                'area1_title' => $this->input->post("area1_title"),
                'area2_title' => $this->input->post("area2_title"),
                'area3_title' => $this->input->post("area3_title"),
            );
            $data = array(
                "opt_value" => json_encode($arr)
            );
            if($this->Moptions->update("footer",$data)){
                $msg = array(
                    "type" => "success",
                    "content" => "Cập nhật thành công!"
                );
            }else{
                $msg = array(
                    "type" => "danger",
                    "content" => "Cập nhật không thành công!"
                );
            }
        }

        $this->_data['current_opt'] = $this->Moptions->get_option("footer");
        $this->_data['msg'] = $msg;
        $this->load->view('admin/options/setting_footer.php', $this->_data);
    }

    // Thiết lập slideshow
    public function slideshow(){
        $current_opt = $this->Moptions->get_option("slideshow");
        $this->_data['page_title'] = $current_opt['opt_name'];
        $this->_data['head_title'] = "Thiết lập SlideShow";
        $msg = '';
        if($this->input->post("thumbnail")){
            $img = $this->input->post("thumbnail");
            $link = $this->input->post("link");
            $list_slideshow = [];
            for($i=0;$i<count($img); $i++){
                $list_slideshow[] = array(
            		'img' => $img[$i],
            		'link' => $link[$i],
            	);
            }

            $data = array(
                "opt_value" => json_encode($list_slideshow)
            );
            if($this->Moptions->update("slideshow",$data)){
                $msg = array(
                    "type" => "success",
                    "content" => "Cập nhật thành công!"
                );
            }else{
                $msg = array(
                    "type" => "danger",
                    "content" => "Cập nhật không thành công!"
                );
            }
        }
        $this->_data['current_opt'] = $this->Moptions->get_option("slideshow");
        $this->_data['msg'] = $msg;
        $this->load->view('admin/options/slideshow', $this->_data);
    }

    //Thiết lập feedback
    public function feedback(){
        $current_opt = $this->Moptions->get_option("feedback");
        $this->_data['page_title'] = $current_opt['opt_name'];
        $this->_data['head_title'] = "Thiết lập Feedback";
        $msg = '';
        if($this->input->post("thumbnail")){
            $img = $this->input->post("thumbnail");
            $name = $this->input->post("name");
            $content = $this->input->post("content");
            $list_feedback = [];
            for($i=0;$i<count($img); $i++){
                $list_feedback[] = array(
                    'img' => $img[$i],
                    'name' => $name[$i],
                    'content' => $content[$i]
                );
            }

            $data = array(
                "opt_value" => json_encode($list_feedback)
            );
            if($this->Moptions->update("feedback",$data)){
                $msg = array(
                    "type" => "success",
                    "content" => "Cập nhật thành công!"
                );
            }else{
                $msg = array(
                    "type" => "danger",
                    "content" => "Cập nhật không thành công!"
                );
            }
        }
        $this->_data['current_opt'] = $this->Moptions->get_option("feedback");
        $this->_data['msg'] = $msg;
        $this->load->view('admin/options/feedback', $this->_data);
    }

    // Thiết lập trang chủ

    public function homepagesetting(){
        $current_opt = $this->Moptions->get_option("homepagesetting");
        $this->_data['page_title'] = $current_opt['opt_name'];
        $this->_data['head_title'] = "Thiết lập Trang chủ";
        $msg = '';
        if( $this->input->post() ){
            echo '<pre>';
            print_r("dffff");
            echo '</pre>';
            $content = $this->input->post("area1");
            $content_after = $this->input->post("area2");
            $banner_pricetext = $this->input->post("area3");
            $banner_image = $this->input->post('banner');
            if($this->input->post('link_banner')){
                $banner_link = $this->input->post('link_banner');
            }else{
                $banner_link = "#";
            }
            $list_home_content = [];
            $list_home_content['main-content'] = $content;
            $list_home_content['main_banner_content'] = $content_after;
            $list_home_content['banner_pricetext'] = $banner_pricetext;
            $list_home_content['banner_image'] = $banner_image;
            $list_home_content['banner_link'] = $banner_link;



            // for($i=0;$i<count($img); $i++){
            //     $list_feedback[] = array(
            //         'img' => $img[$i],
            //         'name' => $name[$i],
            //         'content' => $content[$i]
            //     );
        // }

            $data = array(
                "opt_value" => json_encode($list_home_content)
            );
            if($this->Moptions->update("homepagesetting",$data)){
                $msg = array(
                    "type" => "success",
                    "content" => "Cập nhật thành công!"
                );
            }else{
                $msg = array(
                    "type" => "danger",
                    "content" => "Cập nhật không thành công!"
                );
            }


        }
        $this->_data['current_opt'] = $this->Moptions->get_option("homepagesetting");
        $this->_data['msg'] = $msg;
        $this->load->view('admin/options/home', $this->_data);

    }

    public function systems()
    {
        $this->_data['page_title'] = "Thiết lập hệ thống";
        $this->_data['head_title'] = "Thiết lập hệ thống";

        $tables = $this->Moptions->get_all_tables();

        $msg = '';

        if ($this->input->server('REQUEST_METHOD')=='POST')
        {
            $old_link = $this->input->post('old_link');
            $new_link = $this->input->post('new_link');

            if ($tables)
            {
                $msg = array(
                    "type" => "success",
                    "content" => ""
                );
                $content = 'Cập nhật thành công bảng:<br/>';
                foreach ($tables as $table)
                {
                    $field_names = $this->Moptions->get_field_name($table['table_name']);

                    if ($field_names)
                    {
                        foreach ($field_names as $item)
                        {
                            if ($item['DATA_TYPE']=='varchar' || $item['DATA_TYPE']=='text')
                            {
                                $this->Moptions->replace_url($old_link, $new_link, $item['COLUMN_NAME'], $table['table_name']);

                            }
                        }
                    }

                    $content.='- Thay đổi bảng: '.$table['table_name'].'<br/>';

                }
                $msg['content'] = $content;
            }

        }

        $this->_data['msg'] = $msg;

        $this->load->view('admin/options/systems', $this->_data);
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */