<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *  
 */
class Lead extends CI_Controller
{
	protected $_data;
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Mlead');
		$this->load->library('Globals');
		
	}
	public function index()
	{
		$this->_data['page_title'] = "Danh sách người dùng thử";
		$this->_data['head_title'] = "Quản lý người dùng thử";
		$this->_data['users_lead'] = $this->Mlead->get_all_lead();
		$this->load->view('admin/lead/lists.php', $this->_data);
	}
	public function export_user()
	{
		$all_lead = $this->Mlead->get_all_lead();
		$list_export = [];
		$th_array = array(
			'B' => 'Tài khoản',
			'C' => 'Họ và tên',
			'D' => 'Số điện thoại',
			'E' => 'Lĩnh vực',
			'F' => 'Ghi chú',
			'G' => 'Trạng thái',
			'H' => 'Ngày tạo',
			
			
		);
		$tr_array = [];
		foreach ($all_lead as $item) {
			
			$tr_array[] = array(
				'B' => $item['id'],
				'C' => $item['your_name'],
				'D' => $item['your_phone'],
				'E' => $item['your_type'],
				'F' => $item['note'],
				'G' => $item['stautus'],
				'H' => date("d/m/Y",$item['create_at'])
			);
		}
		$this->globals->my_export("list_lead","Danh sách người dùng",$th_array,$tr_array);
	}
	public function check_phone($phone){
		if(preg_match('/[0]{1}[1-9]{9}/', $phone)==1){
			return true;
		}
		return false;
	}
	// thêm người dùng thử
	public function add()
	{
		$msg  = '';
		$data = '';
		$this->_data['page_title'] = "Thêm người dùng thử mới";
		$this->_data['head_title'] = "Quản lí người dùng thử";
		$this->form_validation->set_rules("your_name","Họ và Tên","required|trim");
		$this->form_validation->set_rules("your_phone","Số điện thoại","required|trim|callback_check_phone");
		$this->form_validation->set_message("check_phone", 'Số điện thoại không hợp lệ');
		$data = array(
			'id'         => $this->input->post('id'),
			'your_name'  => $this->input->post('your_name'),
			'your_phone' => $this->input->post('your_phone'),
			'your_type'  => $this->input->post('your_type'),
			'note'       => $this->input->post('note'),
			'status'     => $this->input->post('status'),
			'create_at'  => date('Y-m-d H:i:s')
		);
		if ($this->form_validation->run() == TRUE) {
			$insert_id = $this->Mlead->insertLead($data);
			if ($insert_id > 0) {
				$msg = array(
					'type'    => 'success',
					'content' => 'Thêm người dùng thành công!'
				);
				$this->session->set_flashdata('msg', $msg);
				redirect('admin/lead/');
			}
			else{
				$msg = array(
					'type' => 'danger',
					'content' => 'Thêm người dùng không thành công!'
				);
				redirect('admin/lead/');
			}
		}
		$this->_data['msg'] = $msg;
		$this->_data['data'] = $data;

		$this->load->view('admin/lead/add.php', $this->_data);
	}
	// sửa thông tin người dùng
	public function edit()
	{
		$lead_id = $this->uri->segment(4);
		if ($lead_id) {
			$check = $this->Mlead->check_lead_by_id($lead_id);
			if ($check == 0) {
				redirect('admin/lead','refresh');
			}
			
		}
		else{
			redirect('admin/lead','refresh');
		}
		$this->_data['page_title'] = "Cật nhật người dùng";
		$this->_data['head_title'] = "Quản lý người dùng";

		$msg = '';
		$this->_data['page_title'] = "Sửa thông người dùng thử ";
		$this->_data['head_title'] = "Quản lí người dùng thử";
		$this->form_validation->set_rules("your_name","Họ và Tên","required|trim");
		$this->form_validation->set_rules("your_phone","Số điện thoại","required|trim|callback_check_phone");
		$this->form_validation->set_message("check_phone", 'Số điện thoại không hợp lệ');
		if ($this->form_validation->run() == TRUE) {
			$data = array(
				'your_name'  => $this->input->post('your_name'),
				'your_phone' => $this->input->post('your_phone'),
				'your_type'  => $this->input->post('your_type'),
				'note'       => $this->input->post('note'),
				'status'     => $this->input->post('status'),
				'create_at'  => date('Y-m-d H:i:s') 
			);
			$update_stt = $this->Mlead->update($lead_id,$data);

			if($update_stt){
				$msg = array(
					"type" => "success",
					"content" => "Cập nhật thành công"
				);
				redirect('admin/lead','refresh');
			}else{
				$msg = array(
					"type" => "danger",
					"content" => "Cập nhật không thành công!"
				);
			}
		}
		
		$this->_data['msg'] = $msg;
		$this->_data['current_edit'] = $this->Mlead->get_lead_by("id", $lead_id);
		$this->load->view('admin/lead/edit', $this->_data);



	}
	public function del()
	{
		$msg = '';
		$lead_id = $this->uri->segment(4);
		if ($lead_id) {
			$check = $this->Mlead->check_lead_by_id($lead_id);
			if ($check == 0) {
				$msg = array(
					'type'    => 'danger',
					'content' => 'Người dùng này không tồn tại'
				);
			}
		}
		else{
			redirect('admin/lead');
		}
		if ($this->Mlead->delete($lead_id)) {
			$msg = array(
				'type'    => 'success',
				'content' => 'Xóa thành công!'
			);
		}
		else{
			$msg = array(
				'type'    => 'danger',
				'content' => 'Xóa không thành công!'
			);
		}
		$this->session->set_flashdata('msg', $msg);
		redirect('admin/lead');
		
		

	}
	
	public function switch_status()
    {
        $lead_id = $this->uri->segment(4);

        $row = $this->Mlead->get_lead_by('id', $lead_id);

        if ($row)
        {
            if ($row['status']==1)
            {
                $new_status = 0;
            }
            else
            {
                $new_status = 1;
            }

            $data = array(
                'status' => $new_status,
            );
            $update_stt = $this->Mlead->update($lead_id,$data);

            redirect('admin/lead');
        }
        else
        {
            redirect('admin/lead');
        }
    }

}



?>