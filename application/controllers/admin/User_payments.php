<?php 
/**
 * 
 */
class User_payments extends CI_Controller
{
	protected $_data;
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Muser');
		$this->load->model('Muser_payments');
		$this->load->library('Globals');
		$this->load->library('Get_data');
	}
	public function index()
	{
		$this->_data['page_title']        = "Danh sách tài chính người dùng";
		$this->_data['head_title']        = "Quản lý tài chính người dùng";
		$this->_data['all_user_payments'] = $this->Muser_payments->get_all_user_payments();
		$this->load->view('admin/user_payments/lists.php', $this->_data); 

	}
	public function export_user()
	{
		$all_user_payments = $this->Muser_payments->get_all_user_payments();
		$list_export = [];
		$th_array = array(
			'B' => 'Số thứ tự',
			'C' => 'Id người dùng',
			'D' => 'Số tiền',
			'E' => 'Ngày tạo',
			'F' => 'Nội dung',
			
			
		);
		$tr_array = [];
		foreach ($all_user_payments as $item) {
			$tr_array[] = array(
				'B' => $item['id'],
				'C' => $item['user_id'],
				'D' => $item['amount'],
				'E' => date("d/m/Y",$item['create_at']),
				'F' => $item['content'],
				
			);
		}
		$this->globals->my_export("list_users_payment","Danh sách tài chính người dùng",$th_array,$tr_array);
	}
	public function add()
	{
		$msg   = '';
		$data  = '';
		$this->_data['page_title'] = "Thêm tiền tài khoản";
		$this->_data['head_title'] = "Danh sách tài chính người dùng";
		$this->_data['list_users'] = $this->Muser->get_all_user();
		$this->form_validation->set_rules("content","Nội dung","required|trim");
		$data = array(
			'id'         => $this->input->post('id'),
			'user_id'    => $this->input->post('user_id'),
			'amount'     => $this->input->post('amount'),
			'create_at'  => date('Y-m-d H:i:s'),
			'content'    => $this->input->post('content')

		);
		if ($this->form_validation->run() == TRUE) {
			$insert_id = $this->Muser_payments->insert_user_payment($data);	
			if ($insert_id > 0) {
				$msg = array(
					'type'    => 'success',
					'content' => 'Thêm người dùng thành công!'
				);
				$this->session->set_flashdata('msg', $msg);
				// Lấy số tiền theo id bảng users
				$amount_user = (float)$this->Muser_payments->get_amount_user($data['user_id']);
				// cộng vào số tiền vừa thêm
				$total = ($amount_user + $data['amount']);

				if ($total<0)
                {
                    $total = 0;
                }
				$data_user = array(
					'amount' => $total
				);
                // update số tiền người dùng bảng users
				$this->Muser_payments->update($data['user_id'],$data_user);
				redirect('admin/user_payments/');
			}
			else{
				$msg = array(
					'type' => 'danger',
					'content' => 'Thêm người dùng không thành công!'
				);
				redirect('admin/user_payments/');
			}
		}
		
		$this->_data['msg'] = $msg;
		$this->_data['data'] = $data;
		// $this->_data['user_id'] = $this->get_data->get_user_name($id);
		$this->load->view('admin/user_payments/add.php', $this->_data);

	}
	public function del()
	{
		$msg = '';
		$user = $this->uri->segment(4);
		if ($user) {
			$check = $this->Muser_payments->check_user_by_id($user);
			if ($check == 0) {
				$msg = array(
					'type'    => 'danger',
					'content' => 'Người dùng này không tồn tại'
				);
			}
		}
		else{
			redirect('admin/user_payments');
		}
		if ($this->Muser_payments->delete($user)) {
			$msg = array(
				'type'    => 'success',
				'content' => 'Xóa thành công!'
			);
		}
		else{
			$msg = array(
				'type'    => 'danger',
				'content' => 'Xóa không thành công!'
			);
		}
		$this->session->set_flashdata('msg', $msg);
		redirect('admin/user_payments');
		
		

	}

}

?>