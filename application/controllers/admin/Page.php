<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

    protected $_data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Mpage');
        $this->load->library('Globals');
        $this->load->library('Permalink');
    }

    public function index()
    {
        $this->_data['page_title'] = "Danh sách trang";
        $this->_data['head_title'] = "Quản lý trang";
        $this->_data['all_page'] = $this->Mpage->get_all_page();
        $this->load->view('admin/page/lists.php', $this->_data);
    }

    // Thêm mới
    public function add(){
        $this->_data['page_title'] = "Thêm mới trang";
        $this->_data['head_title'] = "Quản lý trang";

        $msg = '';
        $data_post = '';
        $this->form_validation->set_rules("page_title","Tiêu đề trang","required|trim");
        if($this->form_validation->run() == true){
            $active = ($this->input->post("active"))?1:0;
            $slug = $this->input->post("page_slug");
            if(empty($slug)) $slug = $this->globals->change_to_slug($this->input->post("page_title"));
            $data = array(
                "page_title" => $this->input->post("page_title"),
                "page_slug" => $slug,
                "page_content" => $this->input->post("page_content"),
                "template" => $this->input->post("template"),
                "active" => $active,
            );
            $data_post = $data;
            $insert_id = $this->Mpage->insert($data);
            if($insert_id>0){
                $msg = array(
                    "type" => "success",
                    "content" => "Thêm mới thành công"
                );
                $this->session->set_flashdata('msg', $msg);
                redirect('admin/page/edit/'.$insert_id);
            }else{
                $msg = array(
                    "type" => "danger",
                    "content" => "Thêm mới không thành công!"
                );
            }
            
        }
        $this->_data['msg'] = $msg;
        $this->_data['data_post'] = $data_post;
        $this->load->view('admin/page/add.php', $this->_data);
    }

    // Sửa
    public function edit(){
        $id = $this->uri->segment(4);
        if($id){
            $check = $this->Mpage->check_page_id($id);
            if($check==0){
                redirect('admin/page','refresh');
            }
        }else{
            redirect('admin/page','refresh');
        }
        $this->_data['page_title'] = "Cật nhật thông tin trang";
        $this->_data['head_title'] = "Quản lý thông tin trang";

        $msg = '';
        $this->form_validation->set_rules("page_title","","required|trim");
        if($this->form_validation->run() == true){
            $active = ($this->input->post("active"))?1:0;
            $slug = $this->input->post("page_slug");
            if(empty($slug)) $slug = $this->globals->change_to_slug($this->input->post("page_title"));
            $data = array(
                "page_title" => $this->input->post("page_title"),
                "page_slug" => $slug,
                "page_content" => $this->input->post("page_content"),
                "template" => $this->input->post("template"),
                "active" => $active,
            );
            $update_stt = $this->Mpage->update($id,$data);
            if($update_stt){
                $msg = array(
                    "type" => "success",
                    "content" => "Cập nhật thành công"
                );
            }else{
                $msg = array(
                    "type" => "danger",
                    "content" => "Cập nhật không thành công!"
                );
            }
        }
        $this->_data['msg'] = $msg;
        $this->_data['current_page'] = $this->Mpage->get_page_by('page_id',$id);

        $this->load->view('admin/page/edit.php', $this->_data);
    }

    // Delete
    public function del(){
        $msg = '';
        $id = $this->uri->segment(4);
        if($id){
            $check = $this->Mpage->check_page_id($id);
            if($check==0){
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Trang không tồn tại!'
                );
            }
        }else{
            redirect('admin/page');
        }

        if($this->Mpage->delete($id)){
            $msg = array(
                'type' => 'success',
                'content' => 'Xóa thành công!'
            );
        }else{
            $msg = array(
                    'type' => 'danger',
                    'content' => 'Xóa không thành công!'
                );
        }

        $this->session->set_flashdata('msg', $msg);
        redirect('admin/page');
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */