<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *  
 */

class Broker extends CI_Controller
{
	protected $_data;
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Mbrokers');
		$this->load->library('Globals');
		
	}

	public function index()
	{
		$this->_data['page_title'] = "Danh sách người môi giới";
		$this->_data['head_title'] = "Quản lý người môi giới";
		$page = $this->uri->segment(5);
		if($page == null)
		{
			$page = 0;
		}
		$search = $this->input->get('search');
		if(!isset($search))
		{
			$search = "";
		}
		$this->_data['brokers_broker'] = $this->Mbrokers->get_all_brokers_page($page,$search);
		//phan trang
		$this->load->library('pagination');


		// $config['total_rows']  =  1000;
  //       $config['per_page']  =  50;
		
		$config['total_rows'] = $this->Mbrokers->get_total_num($search);
		$config['per_page'] = 50;
        $config['next_link'] =  'Tiếp ';
        $config['prev_link'] =  '« Trước';
        $config['first_link'] =  '« Đầu';
        $config['last_link'] =  'Cuối »';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['num_tag_open'] =  '<li>';
        $config['num_tag_close'] =  '</li>';
        $config['num_links']    =  5;
        $config['cur_tag_open'] =  '<li class="active"><a>';
        $config['cur_tag_close'] =  '</a></li>';
       	$config['base_url'] = base_url('admin/broker/index/page/');
        $config['uri_segment'] = 5;
        $config['reuse_query_string'] = true;

        

		
		$this->pagination->initialize($config);
		//echo $this->pagination->create_links();
		//die();

		

		$this->load->view('admin/broker/lists.php', $this->_data);
	}



	public function export_broker()
	{
		$all_broker = $this->Mbrokers->get_all_brokers();
		$list_export = [];
		$th_array = array(
			'B' => 'Họ và tên',
			'C' => 'Số điện thoại',
			// 'D' => 'Số điện thoại',
			// 'E' => 'Lĩnh vực',
			// 'F' => 'Ghi chú',
			// 'G' => 'Trạng thái',
			// 'H' => 'Ngày tạo',
			
			
		);
		$tr_array = [];
		foreach ($all_broker as $item) {
			
			$tr_array[] = array(
				'B' => $item['name'],
				'C' => $item['phone'],
				// 'D' => $item['phone'],
				// 'E' => $item['your_type'],
				// 'F' => $item['note'],
				// 'G' => $item['stautus'],
				// 'H' => date("d/m/Y",$item['create_at'])
			);
		}
		$this->globals->my_export("list_broker","Danh sách người môi giới",$th_array,$tr_array);
	}
	
	public function add()
	{
		$msg  = '';
		$data = '';
		$this->_data['page_title'] = "Thêm người môi giới mới";
		$this->_data['head_title'] = "Quản lí người môi giới";
		$this->form_validation->set_rules("your_name","Họ và Tên","required|trim");
		$this->form_validation->set_rules("your_phone","Số điện thoại","required|trim|callback_check_phone");
		$this->form_validation->set_message("check_phone", 'Số điện thoại không hợp lệ');
		$data = array(
			'id'         => $this->input->post('id'),
			'name'  => $this->input->post('your_name'),
			'phone' => $this->input->post('your_phone'),
			// 'your_type'  => $this->input->post('your_type'),
			// 'note'       => $this->input->post('note'),
			// 'status'     => $this->input->post('status'),
			// 'create_at'  => date('Y-m-d H:i:s')
		);
		if ($this->form_validation->run() == TRUE) {
			$insert_id = $this->Mbrokers->insert($data);
			if ($insert_id > 0) {
				$msg = array(
					'type'    => 'success',
					'content' => 'Thêm người môi giới thành công!'
				);
				$this->session->set_flashdata('msg', $msg);
				redirect('admin/broker/');
			}
			else{
				$msg = array(
					'type' => 'danger',
					'content' => 'Thêm người môi giới không thành công!'
				);
				redirect('admin/broker/');
			}
		}
		$this->_data['msg'] = $msg;
		$this->_data['data'] = $data;

		$this->load->view('admin/broker/add.php', $this->_data);
	}
	// sửa thông tin người môi giới
	public function edit()
	{
		$broker_id = $this->uri->segment(4);
		if ($broker_id) {
			$check = $this->Mbrokers->check_broker_id($broker_id);
			if ($check == 0) {
				redirect('admin/broker','refresh');
			}
			
		}
		else{
			redirect('admin/broker','refresh');
		}
		$this->_data['page_title'] = "Cật nhật người môi giới";
		$this->_data['head_title'] = "Quản lý người môi giới";

		$msg = '';
		$this->_data['page_title'] = "Sửa thông người môi giới ";
		$this->_data['head_title'] = "Quản lí người môi giới";
		$this->form_validation->set_rules("your_name","Họ và Tên","required|trim");
		$this->form_validation->set_rules("your_phone","Số điện thoại","required|trim|callback_check_phone");
		$this->form_validation->set_message("check_phone", 'Số điện thoại không hợp lệ (10 số)');
		if ($this->form_validation->run() == TRUE) {
			$data = array(
				'name'  => $this->input->post('your_name'),
				'phone' => $this->input->post('your_phone'),
				// 'your_type'  => $this->input->post('your_type'),
				// 'note'       => $this->input->post('note'),
				// 'status'     => $this->input->post('status'),
				// 'create_at'  => date('Y-m-d H:i:s') 
			);
			$update_stt = $this->Mbrokers->update($broker_id,$data);

			if($update_stt){
				$msg = array(
					"type" => "success",
					"content" => "Cập nhật thành công"
				);
				redirect('admin/broker','refresh');
			}else{
				$msg = array(
					"type" => "danger",
					"content" => "Cập nhật không thành công!"
				);
			}
		}
		
		$this->_data['msg'] = $msg;
		$this->_data['current_edit'] = $this->Mbrokers->get_broker_by("id", $broker_id);
		$this->load->view('admin/broker/edit', $this->_data);



	}
	public function del()
	{
		$msg = '';
		$broker_id = $this->uri->segment(4);
		if (!$broker_id) {
			redirect('admin/broker');
		}
		if ($this->Mbrokers->delete($broker_id)) {
			$msg = array(
				'type'    => 'success',
				'content' => 'Xóa thành công!'
			);
		}
		else{
			$msg = array(
				'type'    => 'danger',
				'content' => 'Xóa không thành công!'
			);
		}
		$this->session->set_flashdata('msg', $msg);
		redirect('admin/broker');
		
		

	}
	
	public function switch_status()
	{
		$broker_id = $this->uri->segment(4);

		$row = $this->Mbrokers->get_broker_by('id', $broker_id);

		if ($row)
		{
			if ($row['status']==1)
			{
				$new_status = 0;
			}
			else
			{
				$new_status = 1;
			}

			$data = array(
				'status' => $new_status,
			);
			$update_stt = $this->Mbrokers->update($broker_id,$data);

			redirect('admin/broker');
		}
		else
		{
			redirect('admin/broker');
		}
	}

	public function check_phone($phone)
	{
		if(preg_match('/[0]{1}[1-9]{9}/', $phone)==1){
			return true;
		}
		return false;
	}

}



?>