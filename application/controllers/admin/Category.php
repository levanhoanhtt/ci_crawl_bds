<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	protected $_data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Mcategory');
        $this->load->library('Globals');
        $this->load->library('Permalink');
    }

	public function index()
	{
		$this->_data['page_title'] = "Danh mục tin đăng";
		$this->_data['head_title'] = "Quản lý danh mục tin đăng";
		$this->_data['all_cate'] = $this->get_cate_recursive();
		$this->load->view('admin/category/lists.php', $this->_data);
	}

	// Đệ quy career
	public function get_cate_recursive($parent_id=0,$result='',$str=''){
		if(!is_array($result)){
			$result = [];
		}
		$get_cate = $this->Mcategory->ad_get_cate_child($parent_id);
		foreach ($get_cate as $item) {
			$item_cate = $item;
			$item_cate['display'] = $str.' '.$item['cate_name'];
			$result[] = $item_cate;
			$result = $this->get_cate_recursive($item['cate_id'],$result,$str.'---');
		}
		return $result;
	}

	// Thêm mới
	public function add(){
		$this->_data['page_title'] = "Thêm mới danh mục tin đăng";
		$this->_data['head_title'] = "Quản lý danh mục tin đăng";
		$this->_data['all_cate'] = $this->get_cate_recursive();

		$msg = '';
		$data_post = '';
		$this->form_validation->set_rules("cate_name","Tên chuyên mục","required|trim|is_unique[categories.cate_name]");
		if($this->form_validation->run() == true){
			$cate_name = $this->input->post("cate_name");
			if($this->Mcategory->check_cate_name_exists($cate_name)){
				$msg = array(
					"type" => "danger",
					"content" => "Tên chuyên mục đã tồn tại"
				);
			}else{
				$active = ($this->input->post("active"))?1:0;
                $slug = $this->input->post("cate_slug");
                if(empty($slug)) $slug = $this->globals->change_to_slug($cate_name);
				$data = array(
					"cate_name" => $cate_name,
					"cate_slug" => $slug,
					"banner" => $this->input->post("banner"),
					"parent_id" => $this->input->post("parent_id"),
					"crawl_link" => $this->input->post("crawl_link"),
					"active" => $active,
				);
				$data_post = $data;
				$insert_id = $this->Mcategory->insert($data);
				if($insert_id>0){
					$msg = array(
						"type" => "success",
						"content" => "Thêm mới thành công"
					);
					$this->session->set_flashdata('msg', $msg);
					redirect('admin/category/edit/'.$insert_id);
				}else{
					$msg = array(
						"type" => "danger",
						"content" => "Thêm mới không thành công!"
					);
				}
			}
		}
		$this->_data['msg'] = $msg;
		$this->_data['data_post'] = $data_post;
		$this->load->view('admin/category/add.php', $this->_data);
	}

	// Sửa
	public function edit(){
		$id = $this->uri->segment(4);
		if($id){
			$check = $this->Mcategory->ad_check_cate_id($id);
			if($check==0){
				redirect('admin/category','refresh');
			}
		}else{
			redirect('admin/category','refresh');
		}
		$this->_data['page_title'] = "Cật nhật danh mục tin đăng";
		$this->_data['head_title'] = "Quản lý danh mục tin đăng";
		$this->_data['all_cate'] = $this->get_cate_recursive();

		$msg = '';
		$this->form_validation->set_rules("cate_name","Tên chuyên mục","required|trim");
		if($this->form_validation->run() == true){
			$cate_name = $this->input->post("cate_name");
			if($this->Mcategory->check_cate_name_exists($cate_name,$id)){
				$msg = array(
					"type" => "danger",
					"content" => "Tên chuyên mục đã tồn tại"
				);
			}else{
				$active = ($this->input->post("active"))?1:0;
                $slug = $this->input->post("cate_slug");
                if(empty($slug)) $slug = $this->globals->change_to_slug($cate_name);
				$data = array(
					"cate_name" => $cate_name,
					"cate_slug" => $slug,
					"banner" => $this->input->post("banner"),
					"parent_id" => $this->input->post("parent_id"),
                    "crawl_link" => $this->input->post("crawl_link"),
					"active" => $active,
				);
				$update_stt = $this->Mcategory->update($id,$data);
				if($update_stt){
					$msg = array(
						"type" => "success",
						"content" => "Cập nhật thành công"
					);
				}else{
					$msg = array(
						"type" => "danger",
						"content" => "Cập nhật không thành công!"
					);
				}
			}
		}
		$this->_data['msg'] = $msg;

		$this->_data['child_cate'] = $this->Mcategory->ad_get_cate_child($id);
		$this->_data['current_cate'] = $this->Mcategory->ad_get_cate_by('cate_id',$id);

		$this->load->view('admin/category/edit.php', $this->_data);
	}

	// Delete
	public function del(){
		$msg = '';
		$id = $this->uri->segment(4);
		if($id){
			$check = $this->Mcategory->ad_check_cate_id($id);
			if($check==0){
				$msg = array(
					'type' => 'danger',
					'content' => 'Danh mục không tồn tại!'
				);
			}
		}else{
			redirect('admin/category');
		}

		if($this->Mcategory->delete($id)){
			$child_cate = $this->get_cate_recursive($id);
			foreach ($child_cate as $item) {
				$this->Mcategory->delete($item['cate_id']);
			}
			$msg = array(
				'type' => 'success',
				'content' => 'Xóa thành công!'
			);
		}else{
			$msg = array(
					'type' => 'danger',
					'content' => 'Xóa không thành công!'
				);
		}

		$this->session->set_flashdata('msg', $msg);
		redirect('admin/category');
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */