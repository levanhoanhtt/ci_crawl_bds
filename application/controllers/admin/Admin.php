<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	protected $_data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Madmin');
        $this->load->library('Globals');
    }

	public function index()
	{
		$this->_data['page_title'] = "Danh sách quản trị viên";
		$this->_data['head_title'] = "Quản lý quản trị viên";
		$this->_data["current_login"] = $this->session->get_userdata();
		$this->_data["all_user"] = $this->Madmin->get_all_user();
		$this->load->view('admin/admin/lists.php', $this->_data);
	}

	// Add
	public function add(){
		$this->_data['page_title'] = "Thêm quản trị mới";
		$this->_data['head_title'] = "Quản lý quản trị viên";

		$msg = '';
		$data_post = '';
        $this->form_validation->set_rules("username", "Tài khoản", "required|trim|is_unique[admins.admin_account]");
        $this->form_validation->set_rules("pw1", "Mật khẩu", "required|trim");
        $this->form_validation->set_rules("fullname", "Họ và tên", "required|trim");
        $this->form_validation->set_rules("email_address", "Email", "required|trim|valid_email|is_unique[admins.admin_email]");

        $username =  $this->input->post('username');
        $data_post = array(
            'thumbnail' => $this->input->post('thumbnail'),
            'admin_account' => $username,
            'admin_password' => md5($this->input->post('pw1')),
            'admin_fullname' => $this->input->post('fullname'),
            'admin_email' => $this->input->post('email_address'),
            'admin_role' => (int)$this->input->post('user_type'),
            'key_forget_password' => ''
        );

        if ($this->form_validation->run() == TRUE)
        {
	        if($this->Madmin->check_username($username)>0){
	        	$msg = array(
            		'type' => 'danger',
            		'content' => 'Tên đăng nhập đã tồn tại!'
            	);
	        }else{
		        $insert_id = $this->Madmin->insertUser($data_post);
	            if($insert_id>0){
	            	$msg = array(
	            		'type' => 'success',
	            		'content' => 'Thêm quản trị viên thành công!'
	            	);
	            	$this->session->set_flashdata('msg', $msg);
					redirect('admin/user/edit/'.$insert_id);
	            }else{
	            	$msg = array(
	            		'type' => 'danger',
	            		'content' => 'Thêm quản trị viên không thành công!'
	            	);
	            }
	        }
        }

		$this->_data['msg'] = $msg;
		$this->_data['data_post'] = $data_post;
		
		$this->load->view('admin/admin/add.php', $this->_data);
	}

	// Edit 
	public function edit(){
		$id = $this->uri->segment(4);
		if($id){
			$check = $this->Madmin->check_user_id($id);
			if($check==0){
				redirect('admin/admin','refresh');
			}
		}else{
			redirect('admin/admin','refresh');
		}

		$this->_data['page_title'] = "Cật nhật thông tin quản trị viên";
		$this->_data['head_title'] = "Quản lý thông tin quản trị viên";

		$msg = '';
        $this->form_validation->set_rules("admin_fullname", "Họ và tên", "required|trim");
        $this->form_validation->set_rules("admin_email", "Email", "required|trim|valid_email");
		if($this->form_validation->run() == true){
		    $user_type = (int)$this->input->post("user_type");
	        $data = array(
	            'thumbnail' => $this->input->post('thumbnail'),
	            'admin_fullname' => $this->input->post('admin_fullname'),
	            'admin_email' => $this->input->post('admin_email'),
	            'admin_role'  => $this->input->post('admin_role')
	        );
			$update_stt = $this->Madmin->update($id,$data);
			if($update_stt){
				$msg = array(
					"type" => "success",
					"content" => "Cập nhật thành công"
				);
			}else{
				$msg = array(
					"type" => "danger",
					"content" => "Cập nhật không thành công!"
				);
			}
		}
		$this->_data['msg'] = $msg;
		$this->_data['current_edit'] = $this->Madmin->get_admin_by("admin_id", $id);
		$this->load->view('admin/admin/edit', $this->_data);
	}

	// Delete
	public function del(){
		$msg = '';
		$id = $this->uri->segment(4);
		if($id){
			$check = $this->Madmin->check_user_id($id);
			if($check==0){
				$msg = array(
					'type' => 'danger',
					'content' => 'Người dùng không tồn tại!'
				);
			}
		}else{
			redirect('admin/admin');
		}

		if($this->Madmin->delete($id)){
			$msg = array(
				'type' => 'success',
				'content' => 'Xóa thành công!'
			);
		}else{
			$msg = array(
				'type' => 'danger',
				'content' => 'Xóa không thành công!'
			);
		}

		$this->session->set_flashdata('msg', $msg);
		redirect('admin/admin');
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */