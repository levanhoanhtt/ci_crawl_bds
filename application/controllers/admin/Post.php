<?php

defined('BASEPATH') or exit('No direct script access allowed');



class post extends CI_Controller

{



    protected $_data;



    public function __construct()

    {

        parent::__construct();

        $this->load->helper('url');

        $this->load->model('Mpost');

        $this->load->model('Muser');

        $this->load->model('Mcategory');

        $this->load->model('Mprovince');

        $this->load->library('Globals');

        $this->load->library('Get_data');

        $this->load->library('pagination');

        $this->load->library('ResizeImage');

        $this->load->model('Mbrokers');

    }





    public function index()

    {

        $this->_data['page_title'] = "Danh sách tin thường";

        $this->_data['head_title'] = "Quản lý tin thường";



        $this->_data['for_vip'] = 0;



        $this->_data['list_province'] = $this->Mprovince->get_all_province();



        $this->_data['category_parent'] = $this->Mcategory->ad_get_cate_child_active(0);



        

        $filter = array('for_vip_user' => 0, 'status_crawler'=>1);



        $mustQueryElastic = [];

        if ($this->input->get('province_id')) {

            $province_id = $this->input->get('province_id');

            $filter['province_id'] = $province_id;



            //ElasticSearch Query Build

            $mustQueryElastic[]= ['term'=>[

                'province_id'=>$province_id,

            ]];

        } else {

            unset($filter['province_id']);



            //$filter['province_id'] = 1;

        }



        if ($this->input->get('district_id')) {

            $district_id = $this->input->get('district_id');

            $filter['district_id'] = $district_id;



            //ElasticSearch Query Build

            $mustQueryElastic[]= ['term'=>[

                'district_id'=>$district_id,

            ]];

        }

        if ($this->input->get('area')) {

            $area = $this->input->get('area');

            $area_arr = explode('-', $area);

            if ($area_arr[0] > 0) {

                $filter['area>='] = intval($area_arr[0]);



                //ElasticSearch Query Build

                $mustQueryElastic[]= ['range'=>[

                    'area'=>[

                            'gte'=>intval($area_arr[0]),

                    ],

                ]];



            }



            if ($area_arr[1] > 0) {

                $filter['area<='] = intval($area_arr[1]);



                //ElasticSearch Query Build

                $mustQueryElastic[]= ['range'=>[

                    'area'=>[

                        'lte'=>intval($area_arr[1]),

                    ],

                ]];



            }

        }



        if ($this->input->get('price')) {//long

            $price = $this->input->get('price');

            

            $price_arr = explode('-', $price);

            if ($price_arr[0] > 0) {

                $filter['price>='] = $price_arr[0] * 1000000;



                //ElasticSearch Query Build

                $mustQueryElastic[]= ['range'=>[

                    'price'=>[

                        'gte'=>$price_arr[0] * 1000000,

                    ],

                ]];





            }



            if ($price_arr[1] > 0) {

                $filter['price<='] = $price_arr[1] * 1000000;





                //ElasticSearch Query Build

                $mustQueryElastic[]= ['range'=>[

                    'price'=>[

                        'lte'=>$price_arr[1] * 1000000,

                    ],

                ]];



            }



            if ($price == -1){

                $filter['price='] = "'Thỏa thuận'";



                //ElasticSearch Query Build

                $mustQueryElastic[]= ['match'=>[

                    'price'=>"'Thỏa thuận'",

                ]];



            }

        }

        // var_dump($this->input->get('unit'));die;

        if ($this->input->get('date_start')) {

            $date_start = $this->input->get('date_start') . ' 00:00:00';

            $date_start = strtotime($date_start);

            $filter['timestamp>='] = $date_start;



            //ElasticSearch Query Build

            $mustQueryElastic[]= ['range'=>[

                'timestamp'=>[

                    'gte'=>$date_start,

                ],

            ]];



        }



        if ($this->input->get('date_end')) {

            $date_end = $this->input->get('date_end') . ' 23:59:59';

            $date_end = strtotime($date_end);

            $filter['timestamp<='] = $date_end;



            //ElasticSearch Query Build

            $mustQueryElastic[]= ['range'=>[

                'timestamp'=>[

                    'lte'=>$date_end,

                ],

            ]];

        }



        if ($this->input->get('crawler_type')) {

            $crawler_type = $this->input->get('crawler_type');



            $filter['crawler_type'] = $crawler_type;





            //ElasticSearch Query Build

            $mustQueryElastic[]= ['term'=>[

                'crawler_type'=>$crawler_type,

            ]];

        }



        $keyword = '';



        if ($this->input->get('keyword')) {

            $keyword = trim($this->input->get('keyword'));



//            $this->db->like('post_title', $keyword);

//            $this->db->or_like('post_content', $keyword);

//            $this->db->or_like('contact', $keyword);



            //ElasticSearch Query Build

            $keywordQuery = [

                    'bool'=>['should'=>[

                        ['match'=>['post_title'=>$keyword]],

                        ['match'=>['post_content'=>$keyword]],

                        ['match'=>['contact'=>$keyword]],



                    ]],

            ];



            $mustQueryElastic[] = $keywordQuery;



        }


        $category = '';

        if ($this->input->get('category_id')) {

            $category_id = $this->input->get('category_id');



            if ($this->Mcategory->ad_get_cate_child($category_id)) {

                $list_cate = array();

                foreach ($this->Mcategory->ad_get_cate_child($category_id) as $cate) {

                    $list_cate[] = json_encode(array($cate['cate_id']));

                }



                $category = $list_cate;

            } else {

                $filter['cate_id'] = json_encode(array($category_id));

            }

        }

        //$total_rows = $this->Mpost->get_count_post($filter, $keyword, $category);



        $total_rows = $this->Mpost->get_count_post_elastic($mustQueryElastic, $keyword, $category);
       

        $config['total_rows']  =  $total_rows;

        $config['per_page']  =  50;

        $config['next_link'] =  'Tiếp ';

        $config['prev_link'] =  '« Trước';

        $config['first_link'] =  '« Đầu';

        $config['last_link'] =  'Cuối »';

        $config['next_tag_open'] = '<li>';

        $config['next_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li>';

        $config['prev_tag_close'] = '</li>';

        $config['last_tag_open'] = '<li>';

        $config['last_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';

        $config['first_tag_close'] = '</li>';

        $config['num_tag_open'] =  '<li>';

        $config['num_tag_close'] =  '</li>';

        $config['num_links']    =  5;

        $config['cur_tag_open'] =  '<li class="active"><a>';

        $config['cur_tag_close'] =  '</a></li>';

        $config['base_url'] =  base_url('admin/post/index/page');

        $config['uri_segment'] = 5;

        $config['reuse_query_string'] = true;





        $this->pagination->initialize($config);



        $pagination =  $this->pagination->create_links();



        $offset  =  ($this->uri->segment(5)=='') ? 0 : $this->uri->segment(5); 

//        $all_post = $this->Mpost->get_all_post($filter, $keyword, $category, $config['per_page'], $offset);



        $all_post = $this->Mpost->get_all_post_elastic($mustQueryElastic, $keyword, $category, $config['per_page'], $offset);
        




       
//         var_dump($all_post);die();

        //$this->_data['post_list'] = $all_post;



        $response = array();



        if ($all_post && !empty($all_post)) {

            $count = $start;

            foreach ($all_post as $item) {

                $count++;



                $broker_id = $item['broker_id'];



                if ($broker_id > 0) {

                    $link_broker = base_url("admin/broker/edit/" . $broker_id);

                } else {

                    $link_broker = "#";

                }



                $crawler_type = $item['crawler_type'];

                if (isset($crawler_type) && !empty($crawler_type)) {

                    if ($crawler_type == 2) {

                        $crawler_lable = '<span class="label label-warning">Môi Giới</span> <a href="' . $link_broker . '" class="label label-primary" ><i class="fa fa-eye" aria-hidden="true"></i> Xem Thông Tin Môi Giới</a> ';

                    }

                    if ($crawler_type == 1) {

                        $crawler_lable = '<span class="label label-primary">Chính Chủ</span>';

                    }

                }

                // var_dump($all_post);die;



                // $price = ($item['price'] && is_numeric($item['price'])) ? number_format($item['price'], 2) : 'Thỏa thuận';

                $price;



                if($item['price']!=0)

                {

                    if($item['unit'] != null && $item['unit'] != '0')

                    {

                        $price =  $this->get_data->get_price_string($item['price']).$item['unit'];

                    }

                    else

                        $price =  $this->get_data->get_price_string($item['price']);



                }

                else{

                    $price = "Thỏa thuận";

                };



                $area = $this->get_data->get_district($item['district_id']) . ' - ' . $this->get_data->get_province($item['province_id']);

                if (trim($area) == '-') {

                    $area = '';

                }

                $handling = ($item['handling'] == 0) ? '<span class="label label-warning">Chưa xử lý</span>' : '<span class="label label-success">Xử lý</span>';

                $active = ($item['active'] == 0) ? '<span class="label label-warning">Chưa kích hoạt</span>' : '<span class="label label-success">Kích hoạt</span>';

                $edit = '<a href="' . base_url("admin/post/edit/" . $item["post_id"]) . '" class="btn btn-primary btn-block"><i class="fa fa-edit"></i></a>';

                $delete = '<a href="' . base_url("admin/post/del/" . $item["post_id"]) . '" style="margin-left: 0px;" class="btn btn-danger btn-block" onclick="return confirm(\'Bạn có chắc chắn muốn xóa?\');"><i class="fa fa-trash-o"></i></a>';

                $date_create = date('d/m/Y', $item['timestamp']);

                $data_list = $crawler_lable;

                $value = array($count, $item['post_title'], $area, $price, $date_create, $active, $handling, $data_list, $edit . $delete);
                $response[] = $value;

            }
        }
        
        


        $this->_data['post_list'] = $response;







        $this->_data['pagination'] = $pagination;





        $this->load->view('admin/post/lists.php', $this->_data);

    }



    public function get_list_post_data($for_vip = 0)

    {





        $filter = array('for_vip_user' => $for_vip);



        if ($this->input->get('province_id')) {

            $province_id = $this->input->get('province_id');

            $filter['province_id'] = $province_id;

        } else {

            $filter['province_id'] = 1;

        }



        if ($this->input->get('district_id')) {

            $district_id = $this->input->get('district_id');

            $filter['district_id'] = $district_id;

        }

        if ($this->input->get('area')) {

            $area = $this->input->get('area');

            $area_arr = explode('-', $area);

            if ($area_arr[0] > 0) {

                $filter['area>='] = intval($area_arr[0]);

            }



            if ($area_arr[1] > 0) {

                $filter['area<='] = intval($area_arr[1]);

            }

        }



        if ($this->input->get('price')) {//long

            $price = $this->input->get('price');

            $price_arr = explode('-', $price);

            if ($price_arr[0] > 0) {

                $filter['price>='] = $price_arr[0] * 1000000;

            }



            if ($price_arr[1] > 0) {

                $filter['price<='] = $price_arr[1] * 1000000;

            }



            if ($price == -1) $filter['price='] = "'Thỏa thuận'";

        }



        if ($this->input->get('unit')) {

            $unit = $this->input->get('unit');

            $filter['unit'] = $unit;

        }



        if ($this->input->get('date_start')) {

            $date_start = $this->input->get('date_start') . ' 00:00:00';

            $date_start = strtotime($date_start);

            $filter['timestamp>='] = $date_start;

        }



        if ($this->input->get('date_end')) {

            $date_end = $this->input->get('date_end') . ' 23:59:59';

            $date_end = strtotime($date_end);

            $filter['timestamp<='] = $date_end;

        }



        if ($this->input->get('crawler_type')) {

            $crawler_type = $this->input->get('crawler_type');



            $filter['crawler_type'] = $crawler_type;

        }



        $keyword = '';



        if ($this->input->get('keyword')) {

            $keyword = $this->input->get('keyword');

        }



        $category = '';

        if ($this->input->get('category_id')) {

            $category_id = $this->input->get('category_id');



            if ($this->Mcategory->ad_get_cate_child($category_id)) {

                $list_cate = array();

                foreach ($this->Mcategory->ad_get_cate_child($category_id) as $cate) {

                    $list_cate[] = json_encode(array($cate['cate_id']));

                }



                $category = $list_cate;

            } else {

                $filter['cate_id'] = json_encode(array($category_id));

            }

        }



        // $start = 0;

        // if ($this->input->get('start')) {

        //     $start = $this->input->get('start');

        // }



        // $arrlist = array(

        //     1 => 'post_title',

        //     2 => 'district_id',

        //     3 => 'price'

        // );



        // $orderBy = 'timestamp';

        // $order = 'desc';

        // if ($this->input->get('order[0][column]')) {

        //     if (!empty($arrlist[$this->input->get('order[0][column]')])) {

        //         $orderBy = $arrlist[$this->input->get('order[0][column]')];

        //     }

        // }



        // if ($this->input->get('order[0][dir]')) {

        //     $order = $this->input->get('order[0][dir]');

        // }



        $all_post = $this->Mpost->get_all_post($filter, $keyword, $category);



        return $all_post;





        /*

        if ($all_post && !empty($all_post)) {

            $count = $start;

            foreach ($all_post as $item) {

                $count++;



                $broker_id = $item['broker_id'];



                if ($broker_id > 0) {

                    $link_broker = base_url("admin/broker/edit/" . $broker_id);

                } else {

                    $link_broker = "#";

                }



                $crawler_type = $item['crawler_type'];

                if (isset($crawler_type) && !empty($crawler_type)) {

                    if ($crawler_type == 2) {

                        $crawler_lable = '<span class="label label-warning">Môi Giới</span> <a href="' . $link_broker . '" class="label label-primary" ><i class="fa fa-eye" aria-hidden="true"></i> Xem Thông Tin Môi Giới</a> ';

                    }

                    if ($crawler_type == 1) {

                        $crawler_lable = '<span class="label label-primary">Chính Chủ</span>';

                    }

                }





                $price = ($item['price'] && is_numeric($item['price'])) ? number_format($item['price']) : 'Thỏa thuận';



                $area = $this->get_data->get_district($item['district_id']) . ' - ' . $this->get_data->get_province($item['province_id']);

                if (trim($area) == '-') {

                    $area = '';

                }

                $handling = ($item['handling'] == 0) ? '<span class="label label-warning">Chưa xử lý</span>' : '<span class="label label-success">Xử lý</span>';

                $active = ($item['active'] == 0) ? '<span class="label label-warning">Chưa kích hoạt</span>' : '<span class="label label-success">Kích hoạt</span>';

                $edit = '<a href="' . base_url("admin/post/edit/" . $item["post_id"]) . '" class="btn btn-primary btn-block"><i class="fa fa-edit"></i></a>';

                $delete = '<a href="' . base_url("admin/post/del/" . $item["post_id"]) . '" style="margin-left: 0px;" class="btn btn-danger btn-block" onclick="return confirm(\'Bạn có chắc chắn muốn xóa?\');"><i class="fa fa-trash-o"></i></a>';

                $date_create = date('d/m/Y', $item['timestamp']);

                $data_list = $crawler_lable;

                $value = array($count, $item['post_title'], $area, $price, $date_create, $active, $handling, $data_list, $edit . $delete);



                $response['data'][] = $value;

            }

            */

            //$co = $this->Mpost->count_all_ajax($filter, $keyword, $category);

            // $response['draw'] = $this->input->get('draw');

            // $response['recordsTotal'] = 100000;

            // $response['recordsFiltered'] = 100000;

        //}





        // if (empty($response)) {

        //     $response['data'] = array();

        // }





        //echo json_encode($response);

        }



        public function post_vip()

        {

            $this->_data['page_title'] = "Danh sách tin VIP";

            $this->_data['head_title'] = "Quản lý tin VIP";



            $this->_data['for_vip'] = 1;



            $this->_data['category_parent'] = $this->Mcategory->ad_get_cate_child_active(0);



            $this->_data['list_province'] = $this->Mprovince->get_all_province();



            $filter = array('for_vip_user' => 1);



            if ($this->input->get('province_id')) {

                $province_id = $this->input->get('province_id');

                $filter['province_id'] = $province_id;

            } else {

                unset($student['province_id']);



            //$filter['province_id'] = 1;

            }



            if ($this->input->get('district_id')) {

                $district_id = $this->input->get('district_id');

                $filter['district_id'] = $district_id;

            }

            if ($this->input->get('area')) {

                $area = $this->input->get('area');

                $area_arr = explode('-', $area);

                if ($area_arr[0] > 0) {

                    $filter['area>='] = intval($area_arr[0]);

                }



                if ($area_arr[1] > 0) {

                    $filter['area<='] = intval($area_arr[1]);

                }

            }



        if ($this->input->get('price')) {//long

            $price = $this->input->get('price');

            $price_arr = explode('-', $price);

            

            if ($price_arr[0] > 0) {

                $filter['price>='] = $price_arr[0] * 1000000;

            }



            if ($price_arr[1] > 0) {

                $filter['price<='] = $price_arr[1] * 1000000;

            }



            if ($price == -1) $filter['price='] = "'Thỏa thuận'";

        }



        if ($this->input->get('date_start')) {

            $date_start = $this->input->get('date_start') . ' 00:00:00';

            $date_start = strtotime($date_start);

            $filter['timestamp>='] = $date_start;

        }



        if ($this->input->get('date_end')) {

            $date_end = $this->input->get('date_end') . ' 23:59:59';

            $date_end = strtotime($date_end);

            $filter['timestamp<='] = $date_end;

        }



        if ($this->input->get('crawler_type')) {

            $crawler_type = $this->input->get('crawler_type');



            $filter['crawler_type'] = $crawler_type;

        }







        $keyword = '';



        if ($this->input->get('keyword')) {

            $keyword = $this->input->get('keyword');

        }



        $category = '';

        if ($this->input->get('category_id')) {

            $category_id = $this->input->get('category_id');



            if ($this->Mcategory->ad_get_cate_child($category_id)) {

                $list_cate = array();

                foreach ($this->Mcategory->ad_get_cate_child($category_id) as $cate) {

                    $list_cate[] = json_encode(array($cate['cate_id']));

                }



                $category = $list_cate;

            } else {

                $filter['cate_id'] = json_encode(array($category_id));

            }

        }



        $total_rows = $this->Mpost->get_count_post($filter, $keyword, $category);



        $config['total_rows']  =  $total_rows;

        $config['per_page']  =  50;

        $config['next_link'] =  'Tiếp ';

        $config['prev_link'] =  '« Trước';

        $config['first_link'] =  '« Đầu';

        $config['last_link'] =  'Cuối »';

        $config['next_tag_open'] = '<li>';

        $config['next_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li>';

        $config['prev_tag_close'] = '</li>';

        $config['last_tag_open'] = '<li>';

        $config['last_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';

        $config['first_tag_close'] = '</li>';

        $config['num_tag_open'] =  '<li>';

        $config['num_tag_close'] =  '</li>';

        $config['num_links']    =  5;

        $config['cur_tag_open'] =  '<li class="active"><a>';

        $config['cur_tag_close'] =  '</a></li>';

        $config['base_url'] =  base_url('admin/post/post_vip/page');

        $config['uri_segment'] = 5;

        $config['reuse_query_string'] = true;



        $this->pagination->initialize($config);



        $pagination =  $this->pagination->create_links();



        $offset  =  ($this->uri->segment(5)=='') ? 0 : $this->uri->segment(5); 



        $all_post = $this->Mpost->get_all_post($filter, $keyword, $category, $config['per_page'], $offset);



        //$this->_data['post_list'] = $all_post;



        $response = array();



        if ($all_post && !empty($all_post)) {

            $count = $start;

            foreach ($all_post as $item) {

                $count++;



                $broker_id = $item['broker_id'];



                if ($broker_id > 0) {

                    $link_broker = base_url("admin/broker/edit/" . $broker_id);

                } else {

                    $link_broker = "#";

                }



                $crawler_type = $item['crawler_type'];

                if (isset($crawler_type) && !empty($crawler_type)) {

                    if ($crawler_type == 2) {

                        $crawler_lable = '<span class="label label-warning">Môi Giới</span> <a href="' . $link_broker . '" class="label label-primary" ><i class="fa fa-eye" aria-hidden="true"></i> Xem Thông Tin Môi Giới</a> ';

                    }

                    if ($crawler_type == 1) {

                        $crawler_lable = '<span class="label label-primary">Chính Chủ</span>';

                    }

                }





               $price;



                if($item['price']!=0)

                {

                    if($item['unit'] != null && $item['unit'] != '0')

                    {

                        $price =  $this->get_data->get_price_string($item['price']).$item['unit'];

                    }

                    else

                        $price =  $this->get_data->get_price_string($item['price']);



                }

                else{

                    $price = "Thỏa thuận";

                };





                $area = $this->get_data->get_district($item['district_id']) . ' - ' . $this->get_data->get_province($item['province_id']);

                if (trim($area) == '-') {

                    $area = '';

                }

                $handling = ($item['handling'] == 0) ? '<span class="label label-warning">Chưa xử lý</span>' : '<span class="label label-success">Xử lý</span>';

                $active = ($item['active'] == 0) ? '<span class="label label-warning">Chưa kích hoạt</span>' : '<span class="label label-success">Kích hoạt</span>';

                $edit = '<a href="' . base_url("admin/post/edit/" . $item["post_id"]) . '" class="btn btn-primary btn-block"><i class="fa fa-edit"></i></a>';

                $delete = '<a href="' . base_url("admin/post/del/" . $item["post_id"]) . '" style="margin-left: 0px;" class="btn btn-danger btn-block" onclick="return confirm(\'Bạn có chắc chắn muốn xóa?\');"><i class="fa fa-trash-o"></i></a>';

                $date_create = date('d/m/Y', $item['timestamp']);

                $data_list = $crawler_lable;

                $value = array($count, $item['post_title'], $area, $price, $date_create, $active, $handling, $data_list, $edit . $delete);



                $response[] = $value;

            }



        }



        $this->_data['post_list'] = $response;



        $this->_data['pagination'] = $pagination;

        

        $this->load->view('admin/post/lists.php', $this->_data);

    }



    public function export_post($for_vip)

    {



        $filter = array('for_vip_user' => $for_vip);



        if ($this->input->get('province_id')) {

            $province_id = $this->input->get('province_id');

            $filter['province_id'] = $province_id;

        } else {

            $filter['province_id'] = 1;

        }



        if ($this->input->get('district_id')) {

            $district_id = $this->input->get('district_id');

            $filter['district_id'] = $district_id;

        }

        if ($this->input->get('area')) {

            $area = $this->input->get('area');

            $area_arr = explode('-', $area);

            if ($area_arr[0] > 0) {

                $filter['area<='] = $area_arr[0];

            }



            if ($area_arr[1] > 0) {

                $filter['area>='] = $area_arr[1];

            }

        }



        if ($this->input->get('date_start')) {

            $date_start = $this->input->get('date_start') . ' 00:00:00';

            $date_start = strtotime($date_start);

            $filter['timestamp>='] = $date_start;

        }



        if ($this->input->get('date_end')) {

            $date_end = $this->input->get('date_end') . ' 23:59:59';

            $date_end = strtotime($date_end);

            $filter['timestamp<='] = $date_end;

        }



        $keyword = '';



        if ($this->input->get('keyword')) {

            $keyword = $this->input->get('keyword');

        }



        if ($this->input->get('category_id')) {

            $category_id = $this->input->get('category_id');



            if ($this->Mcategory->ad_get_cate_child($category_id)) {

                $list_cate = array();

                foreach ($this->Mcategory->ad_get_cate_child($category_id) as $cate) {

                    $list_cate[] = json_encode(array($cate['cate_id']));

                }



                $category = $list_cate;

            } else {

                $filter['cate_id'] = json_encode(array($category_id));

            }

        }



        $all_post = $this->Mpost->get_all_post($filter, $keyword, $category);



        $list_export = [];

        $th_array = array(

            'B' => 'Tiêu đề',

            'C' => 'Điện thoại',

            'D' => 'Nội dung'

        );

        $tr_array = [];

        foreach ($all_post as $item) {

            $tr_array[] = array(

                'B' => $item['post_title'],

                'C' => trim($item['contact']),

                'D' => strip_tags($item['post_content']),

            );

        }

        $this->globals->my_export("list_posts", "Danh sách tin đăng", $th_array, $tr_array);

    }



    // Đệ quy cate

    public function get_cate_recursive($parent_id = 0, $result = '', $str = '')

    {

        if (!is_array($result)) {

            $result = [];

        }

        $get_cate = $this->Mcategory->ad_get_cate_child($parent_id);

        foreach ($get_cate as $item) {

            $item_cate = $item;

            $item_cate['display'] = $str . ' ' . $item['cate_name'];

            $result[] = $item_cate;

            $result = $this->get_cate_recursive($item['cate_id'], $result, $str . '---');

        }

        return $result;

    }



    // Thêm mới

    public function add()

    {

        $this->_data['page_title'] = "Thêm mới tin tức";

        $this->_data['head_title'] = "Quản lý tin tức";

        $this->_data['all_user'] = $this->Muser->get_all_user();

        $this->_data['all_cate'] = $this->get_cate_recursive();

        $login = $this->session->get_userdata();

        $login = $login['admin_info'];

        $this->_data['list_province'] = $this->Mprovince->get_all_province();

        $msg = '';

        $data_post = '';

        $this->form_validation->set_rules("post_title", "Tiêu đề", "required|trim");



        if ($this->form_validation->run() == true) {

            $post_title = $this->input->post("post_title");

            $cate_id = $this->input->post("cate_id");

            // $active = ($this->input->post("active")) ? 1 : 0;

            $active = 1;



            $handling = ($this->input->post("handling")) ? 1 : 0;

            $feadtured_post = ($this->input->post("feadtured_post")) ? 1 : 0;

            // $for_vip_user = ($this->input->post("for_vip_user")) ? 1 : 0;

            $for_vip_user = 1;



            $timestamp = strtotime(date('Y-m-d H:i:s'));

            $slug = $this->input->post("post_slug");



            //xy ly gia theo unit

            $unit = $this->input->post('unit');

            $price = $this->input->post("price");

            // var_dump($price);die;

            // if($unit == "/m2")

            // {



            // }

            

            // elseif($unit == "/Triệu")

            // {

            //     $price = $price * 1000000;

            // }

            // elseif($unit == "/Tỷ")

            // {

            //     $price = $price * 1000000000;

            // }

            



            if (empty($slug)) {

                $slug = $this->globals->change_to_slug($post_title);

            }

            $data = array(

                "thumbnail" => $this->input->post("thumbnail"),

                "post_title" => $post_title,

                "post_slug" => $slug,

                "post_content" => $this->input->post("post_content"),

                "province_id" => $this->input->post("province_id"),

                "district_id" => $this->input->post("district_id"),

                "price" => $price,

                "area" => $this->input->post("area"),

                "contact" => $this->input->post("contact"),

                "address" => $this->input->post("address"),

                "cate_id" => json_encode($cate_id),

                "timestamp" => ($timestamp) ? $timestamp : time(),

                "user_id" => $login["admin_id"],

                "feadtured_post" => $feadtured_post,

                "for_vip_user" => $for_vip_user,

                "active" => $active,

                "handling" => $handling,

                "unit" => $this->input->post("unit"),

                'crawler_type' => 1

            );

            $data_post = $data;



            if ($cate_id) {

                // if ($this->Mpost->check_post_title_exists($post_title)) 

                // {

                    // $msg = array(

                    //     "type" => "danger",

                    //     "content" => "Tiêu đề đã tồn tại"

                    // );

                // } 

                // else 

                // {

                    // var_dump($data);die;



                    $insert_id = $this->Mpost->insert($data);

                    if ($insert_id > 0) {

                        // if ($for_vip_user == 1) {

                        //     $data['for_vip_user'] = $for_vip_user;

                        //     $data['clone'] = 1;

                        //     $data['timestamp'] = ($timestamp) ? $timestamp : time();

                        //     $data['user_id'] = $login["admin_id"];

                        //     $this->Mpost->insert($data);

                        // }



                        if ($this->input->post('images')) {

                            foreach ($this->input->post('images') as $item) {

                                $this->Mpost->insert_post_images(array('image_url' => $item, 'post_id' => $insert_id));

                            }

                        }

                        $msg = array(

                            "type" => "success",

                            "content" => "Thêm mới thành công"

                        );

                        $this->session->set_flashdata('msg', $msg);

                        redirect('admin/post/edit/' . $insert_id);

                    } else {

                        $msg = array(

                            "type" => "danger",

                            "content" => "Thêm mới không thành công!"

                        );

                    }

                // }

            } else {

                $msg = array(

                    "type" => "danger",

                    "content" => "Danh mục tin tức bắt buộc phải chọn!"

                );

            }

        }



        $this->_data['msg'] = $msg;

        $this->_data['data_post'] = $data_post;

        $this->load->view('admin/post/add.php', $this->_data);

    }



    public function upload_images()

    {

        //echo $this->input->get('a');

        foreach ($_FILES['img_file']['name'] as $name => $value) {





            // echo '<pre>';

            // print_r($_FILES);

            // echo '</pre>';



            $name_img = stripslashes($_FILES['img_file']['name'][$name]);

            $ext = end(explode('.', $name_img));

            $new_name = md5(uniqid()) . '.' . $ext;

            $source_img = $_FILES['img_file']['tmp_name'][$name];

            $path_img = FCPATH . "uploads/" . $new_name;

            move_uploaded_file($source_img, $path_img);

            $this->resizeimage->run($path_img);

            ?>

            <input type="hidden" name="images[]" value="<?php echo base_url('uploads/' . $new_name); ?>">

            <?php

        }

    }



    // Sửa

    public function edit()

    {

        $id = $this->uri->segment(4);



        $ref = $_SERVER['HTTP_REFERER'];



        if ($id) {

            $check = $this->Mpost->check_post_id($id);

            if ($check == 0) {

                redirect('admin/post', 'refresh');

            }

        } else {

            redirect('admin/post', 'refresh');

        }

        $this->_data['images'] = $this->db->query('select image_url from post_image where post_id = ' . $id)->result();



        $this->_data['page_title'] = "Cập nhật tin tức";

        $this->_data['head_title'] = "Quản lý tin tức";

        $this->_data['all_user'] = $this->Muser->get_all_user();

        $this->_data['all_cate'] = $this->get_cate_recursive();



        $this->_data['post_images'] = $this->Mpost->get_post_images($id);



        $this->_data['list_province'] = $this->Mprovince->get_all_province();



        $current_post = $this->Mpost->get_post_by('post_id', $id);



        $msg = '';

        $this->form_validation->set_rules("post_title", "Tên doanh nghiệp", "required|trim");



        if ($this->form_validation->run() == true) {

            $post_title = $this->input->post("post_title");

            $active = ($this->input->post("active")) ? 1 : 0;

            $handling = ($this->input->post("handling")) ? 1 : 0;

            $feadtured_post = ($this->input->post("feadtured_post")) ? 1 : 0;

            $for_vip_user = ($this->input->post("for_vip_user")) ? 1 : 0;

            $cate_id = $this->input->post("cate_id");

            $crawler_type = $current_post['crawler_type'];

            $broker_id = $current_post['broker_id'];

            $broker = $this->input->post('broker');

             // var_dump($this->input->post('unit'));die;

            if($broker == "on")

            {

                $crawler_type = 2;

            }

            else

            {

                $crawler_type = 1;

            }

            if ($current_post['crawler_type'] == 1) {//long

                $broker = ($this->input->post("broker")) ? 1 : 0;

                if ($broker) {

                    $crawler_type = 2;

                    $broker_id = $this->Mbrokers->insert_check_phone(array(

                        'name' => $current_post['contact_name'],

                        'phone' => $current_post['contact'],

                        'email' => $current_post['email_contact'],

                    ), $current_post['contact']);

                }

            }



            $slug = $this->input->post("post_slug");

            if (empty($slug)) {

                $slug = $this->globals->change_to_slug($post_title);

            }



            $data = array(

                "thumbnail" => $this->input->post("thumbnail"),

                "post_title" => $post_title,

                "post_slug" => $slug,

                "post_content" => $this->input->post("post_content"),

                "cate_id" => json_encode($cate_id),

                "feadtured_post" => $feadtured_post,

                "active" => $active,

                "province_id" => $this->input->post("province_id"),

                "district_id" => $this->input->post("district_id"),

                "price" => $this->input->post("price"),

                "area" => $this->input->post("area"),

                "contact" => $this->input->post("contact"),

                "address" => $this->input->post("address"),

                "unit" => $this->input->post('unit'),

                "handling" => $handling,

                "crawler_type" => $crawler_type,

                "broker_id" => $broker_id,

            );

            $update_stt = $this->Mpost->update($id, $data);

            if ($update_stt) {

                //Duplicate post if for_vip_user=1

                if ($for_vip_user == 1 && $current_post['clone'] == null && $ref==base_url('admin/post')) {

                    $data['for_vip_user'] = $for_vip_user;

                    $data['clone'] = 1;

                    //$data['timestamp'] = $current_post['timestamp'];

                    $data['user_id'] = $current_post['user_id'];

                    $data['broker_id'] = $current_post['broker_id'];

                    $data['status_crawler'] = $current_post['status_crawler'];

//                    echo '<pre>';

//                    print_r($data);

//                    die();

                    $this->Mpost->insert($data);

                }



                $this->Mpost->delete_post_images($id);



                if ($this->input->post('images')) {

                    foreach ($this->input->post('images') as $item) {

                        $this->Mpost->insert_post_images(array('image_url' => $item, 'post_id' => $id));

                    }

                }



                $msg = array(

                    "type" => "success",

                    "content" => "Cập nhật thành công"

                );

            } else {

                $msg = array(

                    "type" => "danger",

                    "content" => "Cập nhật không thành công!"

                );

            }

        }

        $this->_data['msg'] = $msg;

        $this->_data['current_post'] = $this->Mpost->get_post_by('post_id', $id);



        $this->load->view('admin/post/edit.php', $this->_data);

    }



    // Del

    public function del()

    {

        $msg = '';

        $id = $this->uri->segment(4);

        if ($id) {

            $check = $this->Mpost->check_post_id($id);

            if ($check == 0) {

                $msg = array(

                    'type' => 'danger',

                    'content' => 'Tin tức không tồn tại!'

                );

            }

        } else {

            redirect('admin/post');

        }



        if ($this->Mpost->delete($id)) {

            $msg = array(

                'type' => 'success',

                'content' => 'Xóa tin tức thành công!'

            );

        } else {

            $msg = array(

                'type' => 'danger',

                'content' => 'Xóa không thành công!'

            );

        }



        $this->session->set_flashdata('msg', $msg);

        redirect('admin/post');

    }



    public function load_district()

    {

        $province_id = $this->input->post('province_id');

        $list_district = $this->Mprovince->get_list_district($province_id);

        echo json_encode($list_district);

    }





    public function manager_post_crawl()

    {

        $this->_data['head_title'] = 'Quản lý danh mục tin đăng tự động';

        $this->_data['page_title'] = 'Danh mục tin đăng tự động';



        $this->load->view('admin/post/lists_post_crawl.php', $this->_data);

    }



    public function manager_post_crawl_data()

    {

        header('Content-Type: application/json');



        $filter = array();



        if ($this->input->get('province_id')) {

            $province_id = $this->input->get('province_id');

            $filter['province_id'] = $province_id;

        }

        if ($this->input->get('district_id')) {

            $district_id = $this->input->get('district_id');

            $filter['district_id'] = $district_id;

        }

        if ($this->input->get('area')) {

            $area = $this->input->get('area');

            $area_arr = explode('-', $area);

            if ($area_arr[0] > 0) {

                $filter['area<='] = $area_arr[0];

            }



            if ($area_arr[1] > 0) {

                $filter['area>='] = $area_arr[1];

            }

        }



        $all_post = $this->Mpost->get_all_post($filter, 2);





        $response = array();



        foreach ($all_post as $key => $item) {

            $user_info = $this->Muser->get_user_by('user_id', $item['user_id']);

            $all_post[$key]['user_fullname'] = $user_info['fullname'];



            $cate_id = json_decode($item['cate_id']);

            if ($cate_id) {

                $all_post[$key]['cate_name'] = [];

                foreach ($cate_id as $id) {

                    $cate_info = $this->Mcategory->ad_get_cate_by('cate_id', $id);

                    $all_post[$key]['cate_name'][] = $cate_info['cate_name'];

                }

            }

        }



        if ($all_post) {

            $count = 0;

            foreach ($all_post as $item) {

                $count++;

                $price = ($item['price']) ? number_format($item['price'], 2) : 'Thỏa thuận';

                $type = ($item["crawler_type"] == 1) ? 'Chủ nhà' : 'Môi giới';

                $area = $this->get_data->get_district($item['district_id']) . ' - ' . $this->get_data->get_province($item['province_id']);

                if (trim($area) == '-') {

                    $area = '';

                }

                $edit = '<a href="' . base_url("admin/post/edit_crawl/" . $item["post_id"]) . '" class="btn btn-primary btn-block"><i class="fa fa-edit"></i></a>';

                $delete = '<a href="' . base_url("admin/post/del_crawl/" . $item["post_id"]) . '" style="margin-left: 0px;" class="btn btn-danger btn-block" onclick="return confirm(\'Bạn có chắc chắn muốn xóa?\');"><i class="fa fa-trash-o"></i></a>';

                $date_create = date('d/m/Y', $item['timestamp']);

                $value = array($count, $item['post_title'], $item['cate_name'][0], $price, $type, $area, $date_create, $edit . $delete);



                $response['data'][] = $value;

            }

        }



        echo json_encode($response);

    }



    // edit crawl post

    public function edit_crawl()

    {

        $id = $this->uri->segment(4);

        if ($id) {

            $check = $this->Mpost->check_post_id($id);

            if ($check == 0) {

                redirect('admin/post', 'refresh');

            }

        } else {

            redirect('admin/post', 'refresh');

        }

        $this->_data['page_title'] = "Cật nhật tin tức";

        $this->_data['head_title'] = "Quản lý tin tức";

        $this->_data['all_user'] = $this->Muser->get_all_user();

        $this->_data['all_cate'] = $this->get_cate_recursive();



        $this->_data['post_images'] = $this->Mpost->get_post_images($id);



        $this->_data['list_province'] = $this->Mprovince->get_all_province();



        $msg = '';

        $this->form_validation->set_rules("post_title", "Tên doanh nghiệp", "required|trim");



        if ($this->form_validation->run() == true) {

            $post_title = $this->input->post("post_title");

            if ($this->Mpost->check_post_title_exists($post_title, $id)) {

                $msg = array(

                    "type" => "danger",

                    "content" => "Tiêu đề bài viết đã tồn tại"

                );

            } else {

                $active = ($this->input->post("active")) ? 1 : 0;

                $feadtured_post = ($this->input->post("feadtured_post")) ? 1 : 0;

                $for_vip_user = ($this->input->post("for_vip_user")) ? 1 : 2;



                if ($this->input->post("crawler_type_switch")) {

                    $for_vip_user = 0;

                }



                $cate_id = $this->input->post("cate_id");



                $slug = $this->input->post("post_slug");

                if (empty($slug)) {

                    $slug = $this->globals->change_to_slug($post_title);

                }

                $data = array(

                    "thumbnail" => $this->input->post("thumbnail"),

                    "post_title" => $post_title,

                    "post_slug" => $slug,

                    "post_content" => $this->input->post("post_content"),

                    "cate_id" => json_encode($cate_id),

                    "feadtured_post" => $feadtured_post,

                    "active" => $active,

                    "province_id" => $this->input->post("province_id"),

                    "district_id" => $this->input->post("district_id"),

                    "price" => $this->input->post("price"),

                    "area" => $this->input->post("area"),

                    "contact" => $this->input->post("contact"),

                    "address" => $this->input->post("address"),

                    "unit" => $this->input->post("unit"),

                    "crawler_type" => $for_vip_user

                );

                $update_stt = $this->Mpost->update($id, $data);

                if ($update_stt) {

                    $this->Mpost->delete_post_images($id);



                    if ($this->input->post('images')) {

                        foreach ($this->input->post('images') as $item) {

                            $this->Mpost->insert_post_images(array('image_url' => $item, 'post_id' => $id));

                        }

                    }



                    $msg = array(

                        "type" => "success",

                        "content" => "Cập nhật thành công"

                    );

                } else {

                    $msg = array(

                        "type" => "danger",

                        "content" => "Cập nhật không thành công!"

                    );

                }

            }

        }

        $this->_data['msg'] = $msg;

        $this->_data['current_post'] = $this->Mpost->get_post_by('post_id', $id);



        $this->load->view('admin/post/edit_post_crawl.php', $this->_data);

    }



    // del post crawl

    public function del_crawl()

    {

        $msg = '';

        $id = $this->uri->segment(4);

        if ($id) {

            $check = $this->Mpost->check_post_id($id);

            if ($check == 0) {

                $msg = array(

                    'type' => 'danger',

                    'content' => 'Tin tức không tồn tại!'

                );

            }

        } else {

            redirect('admin/post');

        }



        if ($this->Mpost->delete($id)) {

            $msg = array(

                'type' => 'success',

                'content' => 'Xóa tin tức thành công!'

            );

        } else {

            $msg = array(

                'type' => 'danger',

                'content' => 'Xóa không thành công!'

            );

        }



        $this->session->set_flashdata('msg', $msg);

        redirect('admin/post/manager_post_crawl');

    }



// upload ảnh

    public function upload_img()

    {

        function url()

        {

            if (isset($_SERVER['HTTPS'])) {

                $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";

            } else {

                $protocol = 'http';

            }

            return $protocol . "://" . $_SERVER['HTTP_HOST'];

        }



        // echo '<pre>';

        // print_r($_FILES);

        // echo '</pre>';

        // Sử dụng vòng lặp for để lưu từng file trong mảng

        foreach ($_FILES['images']['name'] as $name => $value) {

            $name_img = stripslashes($_FILES['images']['name'][$name]);

            $source_img = $_FILES['images']['tmp_name'][$name];

            $path_img = "upload/" . $name_img;

            move_uploaded_file($source_img, $path_img);



            // echo '<input type="text" class="bui" type="hidden" value="'.url()."/buido"."/".$path_img.'">' ;



            //echo '<input type="hidden" name="images[]" value="'.url()."/project/nguonnhadat"."/".$path_img.'">';

        }

    }

}





/* End of file Home.php */

/* Location: ./application/controllers/admin/Home.php */

