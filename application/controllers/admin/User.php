<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    protected $_data;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Muser');
        $this->load->model('Muser_payments');
        $this->load->library('Globals');
        $this->load->library('Get_data');
    }

    public function index()
    {
        $this->_data['page_title'] = "Danh sách người dùng";
        $this->_data['head_title'] = "Quản lý người dùng";
        $all_user = $this->Muser->get_all_user();
        $this->_data["all_user"] = $all_user;
        $count_vip = $this->Muser->count_vip();
        $this->_data["count_vip"] = count($count_vip);
        $this->_data["count_normal"] = count($all_user) - count($count_vip);
        $this->load->view('admin/user/lists.php', $this->_data);
    }

    public function export_user()
    {
        $all_user = $this->Muser->get_all_user();
        $list_export = [];
        $th_array = array(
            'B' => 'Tài khoản',
            'C' => 'Họ và tên',
            'D' => 'Email',
            'E' => 'Số điện thoại',
            'F' => 'Địa chỉ',
            'G' => 'Loại tài khoản',
            'H' => 'Ngày đăng ký',

        );
        $tr_array = [];
        foreach ($all_user as $item) {
            $type = ($item['user_type'] == 0) ? "Tài khoản thường" : "Tài khoản VIP";
            $tr_array[] = array(
                'B' => $item['username'],
                'C' => $item['fullname'],
                'D' => $item['email_address'],
                'E' => $item['user_phone'],
                'F' => $item['user_address'],
                'G' => $type,
                'H' => date("d/m/Y", $item['timestamp'])
            );
        }
        $this->globals->my_export("list_users", "Danh sách người dùng", $th_array, $tr_array);
    }

    public function export_renew_log()
    {
        $all_renew_log = $this->Muser->list_renew();
        $list_export = [];
        $th_array = array(
            'B' => 'STT',
            'C' => 'Tên người dùng',
            'D' => 'Từ ngày',
            'E' => 'Đến ngày',
            'F' => 'Thời gian chỉnh sửa',


        );
        $tr_array = [];
        foreach ($all_renew_log as $item) {

            $tr_array[] = array(
                'B' => $item['id'],
                'C' => $item['user_id'],
                'D' => $item['start_time'],
                'E' => $item['end_time'],
                'F' => $item['created_at']

            );
        }
        $this->globals->my_export("list_renew_logs", "Danh sách gia hạn tài khoản", $th_array, $tr_array);

    }

    public function check_phone($phone)
    {
        if (preg_match('/[0]{1}[1-9]{9}/', $phone) == 1) {
            return true;
        }
        return false;
    }

    // Add
    public function add()
    {
        $this->_data['page_title'] = "Thêm người dùng mới";
        $this->_data['head_title'] = "Quản lý người dùng";

        $msg = '';
        $data_post = '';
        $this->form_validation->set_rules("username", "Tài khoản", "required|trim|is_unique[users.username]");
        $this->form_validation->set_rules("pw1", "Mật khẩu", "required|trim");
        $this->form_validation->set_rules("fullname", "Họ và tên", "required|trim");
        $this->form_validation->set_rules("address", "Địa chỉ", "required|trim");
        $this->form_validation->set_rules("user_phone", "Số điện thoại", "required|trim|callback_check_phone");
        $this->form_validation->set_message("check_phone", 'Số điện thoại không hợp lệ');
        $this->form_validation->set_rules("email_address", "Email", "required|trim|valid_email|is_unique[users.email_address]");

        $vip_user = $this->input->post('vip_user') ? 1 : 0;
        $active = $this->input->post('active') ? 1 : 0;
        $username = $this->input->post('username');
        $user_type = (int)$this->input->post("user_type");
        $data_post = array(
            'username' => $username,
            'password' => md5($this->input->post('pw1')),
            'fullname' => $this->input->post('fullname'),
            'email_address' => $this->input->post('email_address'),
            'user_address' => $this->input->post('address'),
            'user_phone' => $this->input->post('user_phone'),
            'vip_user' => $vip_user,
            'active' => $active,
            'user_type' => $user_type,
            'user_type_active' => $user_type,
            'timestamp' => time(),
            'key_forget_password' => ''

        );

        if ($this->form_validation->run() == TRUE) {
            if ($this->Muser->check_username($username) > 0) {
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Tên đăng nhập đã tồn tại!'
                );
            } else {
                $insert_id = $this->Muser->insertUser($data_post);
                if ($insert_id > 0) {
                    $msg = array(
                        'type' => 'success',
                        'content' => 'Thêm người dùng thành công!'
                    );
                    $this->session->set_flashdata('msg', $msg);
                    redirect('admin/user/edit/' . $insert_id);
                } else {
                    $msg = array(
                        'type' => 'danger',
                        'content' => 'Thêm người dùng không thành công!'
                    );
                }
            }
        }

        $this->_data['msg'] = $msg;
        $this->_data['data_post'] = $data_post;

        $this->load->view('admin/user/add.php', $this->_data);
    }

    // Edit
    public function edit()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $check = $this->Muser->check_user_id($id);
            if ($check == 0) {
                redirect('admin/user', 'refresh');
            }
        } else {
            redirect('admin/user', 'refresh');
        }

        $this->_data['page_title'] = "Cật nhật người dùng";
        $this->_data['head_title'] = "Quản lý người dùng";

        $msg = '';
        $this->form_validation->set_rules("username", "Tài khoản", "required|trim");
        $this->form_validation->set_rules("fullname", "Họ và tên", "required|trim");
        $this->form_validation->set_rules("address", "Địa chỉ", "required|trim");
        $this->form_validation->set_rules("user_phone", "Số điện thoại", "required|trim|callback_check_phone");
        $this->form_validation->set_message("check_phone", 'Số điện thoại không hợp lệ');
        $this->form_validation->set_rules("email_address", "Email", "required|trim|valid_email");
        if ($this->form_validation->run() == true) {
            $username = $this->input->post("username");
            if ($this->Muser->check_username($username, $id) > 0) {
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Tên đăng nhập đã tồn tại!'
                );
            } else {
                $vip_user = $this->input->post('vip_user') ? 1 : 0;
                $active = $this->input->post('active') ? 1 : 0;
                $username = $this->input->post('username');
                $user_type = (int)$this->input->post("user_type");
                $data = array(
                    'username' => $username,
                    'fullname' => $this->input->post('fullname'),
                    'email_address' => $this->input->post('email_address'),
                    'user_address' => $this->input->post('address'),
                    'user_phone' => $this->input->post('user_phone'),
                    'vip_user' => $vip_user,
                    'active' => $active,
                    'user_type' => $user_type,
                    'user_type_active' => $user_type,
                );
                if (!empty($this->input->post('pwd'))) {
                    $data['password'] = md5($this->input->post('pwd'));
                }
                $update_stt = $this->Muser->update($id, $data);

                if ($update_stt) {
                    $msg = array(
                        "type" => "success",
                        "content" => "Cập nhật thành công"
                    );
                } else {
                    $msg = array(
                        "type" => "danger",
                        "content" => "Cập nhật không thành công!"
                    );
                }
            }
        }
        $this->_data['msg'] = $msg;
        $this->_data['current_edit'] = $this->Muser->get_user_by("user_id", $id);
        $this->load->view('admin/user/edit', $this->_data);
    }

    // Delete
    public function del()
    {
        $msg = '';
        $id = $this->uri->segment(4);
        if ($id) {
            $check = $this->Muser->check_user_id($id);
            if ($check == 0) {
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Người dùng không tồn tại!'
                );
            }
        } else {
            redirect('admin/user');
        }

        if ($this->Muser->delete($id)) {
            $msg = array(
                'type' => 'success',
                'content' => 'Xóa thành công!'
            );
        } else {
            $msg = array(
                'type' => 'danger',
                'content' => 'Xóa không thành công!'
            );
        }

        $this->session->set_flashdata('msg', $msg);
        redirect('admin/user');
    }

    // Confirm user_type
    public function confirm_type()
    {
        $msg = '';
        $id = $this->uri->segment(4);
        if ($id) {
            $check = $this->Muser->check_user_id($id);
            if ($check == 0) {
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Người dùng không tồn tại!'
                );
            }
        } else {
            redirect('admin/user');
        }
        $data = array(
            "user_type_active" => 1
        );
        if ($this->Muser->update($id, $data)) {
            $msg = array(
                'type' => 'success',
                'content' => 'Xác nhận thành công!'
            );
        } else {
            $msg = array(
                'type' => 'danger',
                'content' => 'Xác nhận không thành công!'
            );
        }

        $this->session->set_flashdata('msg', $msg);
        redirect('admin/user');
    }

    // Auto trừ tiền theo ngay ngày
    public function auto_minus_amount()
    {
        $amount_cost = 100;
        $amount_user = '';
        $result = '';
        $all_user = $this->Muser->get_all_user();
        foreach ($all_user as $items) {
            $id = $items['user_id'];
            $amount_user = $items['amount'];
            $result = $amount_user - $amount_cost;
            $data = array(
                'amount' => $result

            );
            // echo $data['amount'];
            $this->Muser_payments->update($id, $data);

        }
        redirect('admin/user/');


    }

    public function renew_logs()
    {
        $this->_data['head_title'] = "Danh sách tài khoản";
        $this->_data['page_title'] = "Gia hạn tài khoản";
        $this->_data['renew_logs'] = $this->Muser->list_renew();
        $this->load->view('admin/user/lists_renews.php', $this->_data);

    }

    public function add_renews()
    {
        $id = $this->uri->segment(4);
        $this->_data['current_id'] = $id;
        $this->_data['head_title'] = "Danh sách tài khoản";
        $this->_data['page_title'] = "Gia hạn tài khoản";
        $this->_data['list_users'] = $this->Muser->get_all_user();
        $msg = '';
        $data_post = '';
        $user_id = $this->input->post('user_id');

        if (!empty($user_id)) {

            $data_post = array(
                'user_id' => $user_id,
                'start_time' => $this->input->post('start_time'),
                'end_time' => $this->input->post('end_time'),
                'content' => $this->input->post('content'),
                'created_at' => date('Y-m-d H:i:s')
            );
            //số tiền trừ theo ngày
            $cost = $this->get_data->options_info('header');
            $date_cost = $cost->money_fee;
            $date_end = date_create($data_post['end_time']);
            $date_start = date_create($data_post['start_time']);
            // Số ngày gia hạn
            $diff = date_diff($date_start, $date_end)->format("%R%a");
            // Số dư trong tài khoản
            $amount_user = $this->get_data->get_amount_user($data_post['user_id']);
            $condition_renew = $amount_user - ($diff * $date_cost);

            $insert_id = $this->Muser->add_renews($data_post);
            // cập nhật lại tiền user sau khi gia hạn
            $data_update = array('start_date' => $date_start->format('Y-m-d'), 'end_date' => $date_end->format('Y-m-d'));
            $this->Muser->update_amount_user($data_post['user_id'], $data_update);
            if ($insert_id > 0) {
                $msg = array(
                    'type' => 'success',
                    'content' => 'Gia hạn thành công!'
                );
                $this->session->set_flashdata('msg', $msg);
                redirect('admin/user');
            } else {
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Gia hạn không thành công!'
                );
            }

            /*
            if ($condition_renew >= 0) {

            }
            else{
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Tài khoản này không đủ tiền để gia hạn, vui lòng nạp thêm!'
                );
            }
            */

        } else {

        }
        $this->_data['msg'] = $msg;
        $this->_data['data_post'] = $data_post;
        $this->load->view('admin/user/add_renews.php', $this->_data);
    }

    public function edit_renews()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $check = $this->Muser->check_id_renews($id);
            if ($check == 0) {
                redirect('admin/user/renew_logs');
            }
        } else {
            redirect('admin/user/renew_logs');
        }

        $this->_data['page_title'] = "Cật nhật gia hạn tài khoản";
        $this->_data['head_title'] = "Gia hạn tài khoản";
        $msg = '';
        $data_post = '';
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');

        if (!empty($start_time) && !empty($end_time) && !empty($this->input->post('user_id'))) {

            $data_post = array(
                'user_id' => $this->input->post('user_id'),
                'start_time' => $start_time,
                'end_time' => $end_time

            );
            $update_stt = $this->Muser->update_renew($id, $data_post);

            if ($update_stt) {
                $msg = array(
                    "type" => "success",
                    "content" => "Cập nhật thành công"
                );
                redirect('admin/user/renew_logs');
            } else {
                $msg = array(
                    "type" => "danger",
                    "content" => "Cập nhật không thành công!"
                );
            }

        }
        $this->_data['msg'] = $msg;
        $this->_data['list_users'] = $this->Muser->get_all_user();
        $this->_data['current_edit'] = $this->Muser->get_user_renew_by($id);
        $this->load->view('admin/user/edit_renews.php', $this->_data);


    }

    public function del_renews()
    {
        $msg = '';
        $id = $this->uri->segment(4);
        if ($id) {
            $check = $this->Muser->check_id_renews($id);
            if ($check == 0) {
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Người dùng không tồn tại!'
                );
            }
        } else {
            redirect('admin/user/renew_logs');
        }

        if ($this->Muser->del_renews($id)) {
            $msg = array(
                'type' => 'success',
                'content' => 'Xóa thành công!'
            );
        } else {
            $msg = array(
                'type' => 'danger',
                'content' => 'Xóa không thành công!'
            );
        }

        $this->session->set_flashdata('msg', $msg);
        redirect('admin/user/renew_logs');
    }


}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */