<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	protected $_data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Madmin');
        $this->load->model('Mpost');
        $this->load->model('Muser');
        $this->load->model('Mcategory');
        $this->load->library('Globals');
    }

	public function index()
	{
		$this->_data['page_title'] = "Trang quản trị";
		$this->_data['head_title'] = "Trang tổng quan";
        /*
        $this->_data['total'] = array(

            "user" => count($this->Muser->get_all_user()),
            "post" => count($this->Mpost->get_all_post()),
            "category" => count($this->Mcategory->ad_get_all_cate()),
        );
        */
		$this->load->view('admin/home/home.php', $this->_data);
	}

    // Login
	public function login(){
		$this->_data['page_title'] = "Đăng nhập hệ thống";
		$this->form_validation->set_rules("username", "Tài khoản", "required|trim");
        $this->form_validation->set_rules("pwd", "Mật khẩu", "required|trim");

        if ($this->form_validation->run() == TRUE)
        {
            if ($this->Madmin->check_login($this->input->post("username"), $this->input->post("pwd"))==0)
            {
                $this->session->set_flashdata('msg_login_warning', 'Tên đăng nhập hoặc mật khẩu không chính xác. Vui lòng đăng nhập lại.');
            }
            else
            {
                $info_admin = $this->Madmin->get_admin_by("admin_account",$this->input->post("username"));
                $this->session->set_userdata('admin_login', $this->input->post("username"));
                $this->session->set_userdata('admin_login_url', base_url());
                $this->session->set_userdata('admin_info', $info_admin);
            }
            redirect('admin');
        }
		$this->load->view('admin/home/signin.php', $this->_data);
	}

    //Logout
    public function logout()
    {
        $this->session->unset_userdata('admin_login');
        $this->session->unset_userdata('admin_info');
        $this->session->unset_userdata('admin_login_url');
        redirect('admin/home/login');
    }

    //Logout
    public function profile()
    {
        $this->_data['page_title'] = "Thông tin cá nhân";
        $this->_data['head_title'] = "Quản lý thông tin cá nhân";
        $login = $this->session->get_userdata();
        $admin_info = $login['admin_info'];
        $msg = '';
        $this->form_validation->set_rules("admin_fullname", "Họ và tên", "required|trim");
        $this->form_validation->set_rules("admin_email", "Địa chỉ email", "required|trim");
        if ($this->form_validation->run() == TRUE){
            if($this->Madmin->check_email($this->input->post("admin_email"),$admin_info['admin_id'])>0){
                $msg = array(
                    'type' => 'danger',
                    'content' => 'Địa chỉ email đã tồn tại!'
                );
            }else{
                $data = array(
                    "thumbnail" => $this->input->post("thumbnail"),
                    "admin_fullname" => $this->input->post("admin_fullname"),
                    "admin_email" => $this->input->post("admin_email"),
                );
                $pw = $this->input->post("admin_password");
                if(!empty($pw)){
                    $data["admin_password"] = md5($pw);
                }
                if($this->Madmin->update($admin_info['admin_id'],$data)){
                    $msg = array(
                        'type' => 'success',
                        'content' => 'Cập nhật thông tin thành công!'
                    );
                }else{
                    $msg = array(
                        'type' => 'danger',
                        'content' => 'Cập nhật thông tin không thành công!'
                    );
                }
            }
        }
        $this->_data['msg'] = $msg;
        $admin_info_last = $this->Madmin->get_admin_by("admin_id",$admin_info['admin_id']);
        $this->_data['admin_info'] = $admin_info_last;
        $this->session->set_userdata('admin_info', $admin_info_last);
        $this->load->view('admin/home/profile.php', $this->_data);
    }

}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */