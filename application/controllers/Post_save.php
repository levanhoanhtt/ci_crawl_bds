<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_save extends CI_Controller {

    protected $_data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Mpost');
        $this->load->model('Mcategory');
        $this->load->model('Muser');
        $this->load->model('Mpost');
        $this->load->model('Mprovince');
        $this->load->model('Mdistrict');
        $this->load->library('Globals');
        $this->load->library('Get_data');
        $this->load->library('pagination');

    }

    public function index()
    {
        $this->_data['page_title'] = "Tin đã lưu";

        $user_id = $this->get_data->get_current_user();

        $this->_data['list_save_post'] = $this->Mpost->get_all_save_post($user_id);

        $this->load->view('member/post_save.php', $this->_data);
    }


}
