<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_type extends CI_Controller
{

    protected $_data;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Mpost');
        $this->load->model('Mcategory');
        $this->load->model('Muser');
        $this->load->model('Mpost');
        $this->load->model('Mprovince');
        $this->load->model('Mdistrict');
        $this->load->library('Globals');
        $this->load->library('Get_data');
        $this->load->library('pagination');
        $this->load->library('user_agent');

        $login = $this->session->get_userdata();
        if (!empty($login['username'])) {
            $info_user = $this->Muser->get_user_info($login['username']);
            $login['user_info'] = $info_user;
            $this->session->set_userdata('user_info', $info_user);
            $this->_data['login'] = $login['user_info'];
        } else {
            $this->_data['login'] = array();
        }

    }

    public function index()
    {
        $this->_data['page_title'] = "Tin nhà chính chủ";

        $filter = array('crawler_type'=>1);

        $keyword = '';

        $category = array();

        if ($this->input->get('keyword')) {
            $keyword = $this->input->get('keyword');
        }

        if ($this->input->get('category_id')) {
            $category_id = $this->input->get('category_id');

            if ($this->Mcategory->ad_get_cate_child($category_id)) {
                $list_cate = array();
                foreach ($this->Mcategory->ad_get_cate_child($category_id) as $cate) {
                    $list_cate[] = json_encode(array($cate['cate_id']));
                }

                $category = $list_cate;

            } else {
                $filter['cate_id'] = json_encode(array($category_id));
            }
        }

        if ($this->input->get('province_id') && $this->input->get('province_id') != 0 ) {
            $province_id = $this->input->get('province_id');
            $filter['province_id'] = $province_id;
        } else {
            //$filter['province_id'] = 0;
            unset($student['province_id']);
        }

        if ($this->input->get('district_id')) {
            $district_id = $this->input->get('district_id');
            $filter['district_id'] = $district_id;
        }

        if ($this->input->get('area')) {
            $area = $this->input->get('area');
            $area_arr = explode('-', $area);
            if ($area_arr[0] > 0) {
                $filter['area>='] = intval($area_arr[0]);
            }

            if ($area_arr[1] > 0) {
                $filter['area<='] = intval($area_arr[1]);
            }
        }

        if ($this->input->get('price')) {//long
            $price = $this->input->get('price');
            $price_arr = explode('-', $price);
            if ($price_arr[0] > 0) {
                $filter['price>='] = $price_arr[0] * 1000000;
            }

            if ($price_arr[1] > 0) {
                $filter['price<='] = $price_arr[1] * 1000000;
            }

            if ($price == -1) $filter['price='] = "'Thỏa thuận'";
        }

        if ($this->input->get('date_start')) {
            $date_start = $this->input->get('date_start') . ' 00:00:00';

            $date_start = strtotime($date_start);
            $filter['timestamp>='] = $date_start;
        }

        if ($this->input->get('date_end')) {
            $date_end = $this->input->get('date_end') . ' 23:59:59';
            $date_end = strtotime($date_end);
            $filter['timestamp<='] = $date_end;
        }

        // check điều kiện sắp xếp
        $sort = "time";
        if ($this->input->get('sort_by')) {
            $sort = $this->input->get('sort_by');

        }


        //Phân trang
        $per_page = 20;

         $number_rows = $this->Mpost->get_number_post_active(0, $filter, $keyword, $category);
         $currentURL = current_url(); //for simple URL
         $params = $_SERVER['QUERY_STRING']; //for parameters
         
         $fullURL = $currentURL . '?' . $params; //full URL with parameter
        // echo $fullURL;

        
        $this->load->library('pagination');
        // $config['total_rows'] = $number_rows;
        // $config['per_page'] = $per_page;
        // $config['next_link'] = '<span class="number-plus"><i class="fa fa-caret-right" aria-hidden="true"></i></span>';
        // $config['prev_link'] = '<span class="number-minus"><i class="fa fa-caret-left" aria-hidden="true"></i></span>';
        // $config['last_link'] = false;
        // $config['first_link'] = false;
        // $config['num_tag_open'] = '';
        // $config['num_tag_close'] = '';
        // $config['num_links'] = 0;
        // $config['cur_tag_open'] = '<span class="currentpage">';
        // $config['cur_tag_close'] = '</span>';
        // $config['reuse_query_string'] = true;
        // $config['base_url'] = base_url('tin-nha-chinh-chu') . '/page/';
        // //$config['base_url'] = $fullURL . '/page/';
        // $config['uri_segment'] = 3;

        $config['total_rows']  =  $number_rows;
        $config['per_page']  =  $per_page;
        $config['next_link'] =  'Tiếp ';
        $config['prev_link'] =  '« Trước';
        $config['first_link'] =  '« Đầu';
        $config['last_link'] =  'Cuối »';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['num_tag_open'] =  '<li>';
        $config['num_tag_close'] =  '</li>';
        $config['num_links']    =  5;
        $config['cur_tag_open'] =  '<li class="active"><a>';
        $config['cur_tag_close'] =  '</a></li>';
        $config['base_url'] = base_url('tin-nha-chinh-chu') . '/page/';
        $config['uri_segment'] = 3;
        $config['reuse_query_string'] = true;
        # Khởi tạo phân trang
        $this->pagination->initialize($config);

        $agent = $this->agent->is_mobile();
        $this->_data['agent'] = $agent;

        # Tạo link phân trang
        $pagination = $this->pagination->create_links();

        # Lấy offset
        $offset = ($this->uri->segment(3) == '') ? 0 : $this->uri->segment(3);

        $this->_data['pagination'] = $pagination;

        $this->_data['list_post'] = $this->Mpost->get_list_post_by(0, $filter, $keyword, $category, $per_page, $offset, $sort);

        // echo $this->db->last_query();
       

        // die();
        $province_id = $this->input->get('province_id');

        if ($province_id != "") {


            $list_dictrict = $this->Mdistrict->get_district_by_province($province_id);
            $this->_data['list_dictrict'] = $list_dictrict;
        }

        $this->_data['list_province'] = $this->Mprovince->get_all_province();


        $this->_data['category_parent'] = $this->Mcategory->ad_get_cate_child_active(0);


        $this->load->view('member/post_type.php', $this->_data);


    }

    

    public function vip()
    {
        $this->_data['page_title'] = "Tin VIP chính chủ";

        $filter = array(
            //'for_vip_user' => 1,//long
            'crawler_type'=>1,
        );

        $keyword = '';

        $category = array();

        if ($this->input->get('keyword')) {
            $keyword = $this->input->get('keyword');
        }

        if ($this->input->get('category_id')) {
            $category_id = $this->input->get('category_id');

            if ($this->Mcategory->ad_get_cate_child($category_id)) {
                $list_cate = array();
                foreach ($this->Mcategory->ad_get_cate_child($category_id) as $cate) {
                    $list_cate[] = json_encode(array($cate['cate_id']));
                }

                $category = $list_cate;

            } else {
                $filter['cate_id'] = json_encode(array($category_id));
            }
        }

        if ($this->input->get('province_id')) {
            $province_id = $this->input->get('province_id');
            $filter['province_id'] = $province_id;
        } else {
           // $filter['province_id'] = 1;
            unset($student['province_id']);

        }

        if ($this->input->get('district_id')) {
            $district_id = $this->input->get('district_id');
            $filter['district_id'] = $district_id;
        }

        if ($this->input->get('area')) {
            $area = $this->input->get('area');
            $area_arr = explode('-', $area);
            if ($area_arr[0] > 0) {
                $filter['area>='] = intval($area_arr[0]);
            }

            if ($area_arr[1] > 0) {
                $filter['area<='] = intval($area_arr[1]);
            }
        }

        if ($this->input->get('price')) {//long
            $price = $this->input->get('price');
            $price_arr = explode('-', $price);
            if ($price_arr[0] > 0) {
                $filter['price>='] = $price_arr[0] * 1000000;
            }

            if ($price_arr[1] > 0) {
                $filter['price<='] = $price_arr[1] * 1000000;
            }

            if ($price == -1) $filter['price='] = "'Thỏa thuận'";
        }

        if ($this->input->get('date_start')) {
            $date_start = $this->input->get('date_start') . ' 00:00:00';
            $date_start = strtotime($date_start);
            $filter['timestamp>='] = $date_start;
        }

        if ($this->input->get('date_end')) {
            $date_end = $this->input->get('date_end') . ' 23:59:59';
            $date_end = strtotime($date_end);
            $filter['timestamp<='] = $date_end;
        }

        // check điều kiện sắp xếp
        if ($this->input->get('sort_by')) {
            $sort = $this->input->get('sort_by');

        }

        


        //Phân trang
        $per_page = 20;

        $number_rows = $this->Mpost->get_number_post_active(1, $filter, $keyword, $category);

        $this->load->library('pagination');
        // $config['total_rows'] = $number_rows;
        // $config['per_page'] = $per_page;
        // $config['next_link'] = '<span class="number-plus"><i class="fa fa-caret-right" aria-hidden="true"></i></span>';
        // $config['prev_link'] = '<span class="number-minus"><i class="fa fa-caret-left" aria-hidden="true"></i></span>';
        // $config['last_link'] = false;
        // $config['first_link'] = false;
        // $config['num_tag_open'] = '';
        // $config['num_tag_close'] = '';
        // $config['num_links'] = 0;
        // $config['cur_tag_open'] = '<span class="currentpage">';
        // $config['cur_tag_close'] = '</span>';
        // $config['base_url'] = base_url('tin-vip-chinh-chu') . '/page/';
        // $config['uri_segment'] = 3;
 
 $config['total_rows']  =  $number_rows;
        $config['per_page']  =  $per_page;
        $config['next_link'] =  'Tiếp ';
        $config['prev_link'] =  '« Trước';
        $config['first_link'] =  '« Đầu';
        $config['last_link'] =  'Cuối »';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['num_tag_open'] =  '<li>';
        $config['num_tag_close'] =  '</li>';
        $config['num_links']    =  5;
        $config['cur_tag_open'] =  '<li class="active"><a>';
        $config['cur_tag_close'] =  '</a></li>';
        $config['base_url'] = base_url('tin-vip-chinh-chu') . '/page/';
        $config['uri_segment'] = 3;
        $config['reuse_query_string'] = true;
        # Khởi tạo phân trang
        $this->pagination->initialize($config);

        # Tạo link phân trang
        $pagination = $this->pagination->create_links();

        $agent = $this->agent->is_mobile();
        $this->_data['agent'] = $agent;

        # Lấy offset
        $offset = ($this->uri->segment(3) == '') ? 0 : $this->uri->segment(3);

        $this->_data['pagination'] = $pagination;

        $this->_data['list_post'] = $this->Mpost->get_list_post_by(1, $filter, $keyword, $category, $per_page, $offset, $sort);
     
        //  echo $this->db->last_query();
        // die();

        $province_id = $this->input->get('province_id');

        if ($province_id != "") {


            $list_dictrict = $this->Mdistrict->get_district_by_province($province_id);
            $this->_data['list_dictrict'] = $list_dictrict;
        }

        $this->_data['list_province'] = $this->Mprovince->get_all_province();

        $this->_data['category_parent'] = $this->Mcategory->ad_get_cate_child_active(0);

        $this->load->view('member/post_type_vip.php', $this->_data);
    }

    public function save_post()
    {
        $user_id = $this->get_data->get_current_user();
        $post_id = $this->input->post('post_id');
        $response = array();
        if ($user_id && $post_id) {
            if (!$this->Mpost->get_number_save_post($post_id, $user_id)) {
                $insert_id = $this->Mpost->insert_save_post(array('post_id' => $post_id, 'user_id' => $user_id));
                if ($insert_id > 0) {
                    //Trả về kết quả lưu thành công
                    $response['status'] = 2;
                } else {
                    //Trả về kết quả lưu không thành công
                    $response['status'] = 1;
                }
            } else {
                //Trả về kết quả tin bị trùng
                $response['status'] = 0;
            }
        }

        echo json_encode($response);

    }

    public function delete_post()
    {
        $post_id = $this->input->post('post_id');
        $user_id = $this->get_data->get_current_user();
        $response = array();
        if ($post_id && $user_id) {
            $this->Mpost->delete_save_post($post_id, $user_id);
            $response['status'] = 1;
        } else {
            $response['status'] = 0;
        }

        echo json_encode($response);
    }

    public function read_post()
    {
        $post_id = $this->input->post('post_id');
        $user_id = $this->get_data->get_current_user();

        $response = array();

        if ($post_id && $user_id) {
            if ($this->Mpost->get_post_read($user_id, $post_id) == 0) {
                $insert_id = $this->Mpost->insert_post_read(array('user_id' => $user_id, 'post_id' => $post_id, 'read' => 1));
                if ($insert_id > 0) {
                    $response['status'] = 1;
                } else {
                    $response['status'] = 0;
                }
            }

        } else {
            $response['status'] = 0;
        }

        echo json_encode($response);
    }


}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */