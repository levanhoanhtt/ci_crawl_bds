<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	protected $_data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Mcategory');
        $this->load->model('Moptions');
        $this->load->model('Mreview');
        $this->load->model('Mpost');
        $this->load->model('Mpage');
        $this->load->model('Muser');

        $this->load->library('Globals');
        $this->load->library('Get_data');

        $login = $this->session->get_userdata();
        if(!empty($login['username'])){
            $info_user = $this->Muser->get_user_info($login['username']);
            $login['user_info'] = $info_user;
            $this->session->set_userdata('user_info',$info_user);
            $this->_data['login'] = $login['user_info'];
        }else{
            $this->_data['login'] = array();
        }

    }

	public function index()
	{
        $this->load->model('Mleads');
        $this->_data['page_title'] = "Nguồn Nhà Đất";
        $this->_data['home_option'] = $this->Moptions->get_option('homepagesetting');

		if ($this->input->server('REQUEST_METHOD')=='POST')
        {
            $data_insert = array(
                'your_name' => $this->input->post('your_name'),
                'your_phone' => $this->input->post('your_phone'),
                'your_type' => $this->input->post('your_type'),
                'create_at' => date('Y-m-d H:i:s'),
                'status' => 0
            );
            if ($this->Mleads->insert($data_insert)>0)
            {
                ?>
                <script type="text/javascript">
                    alert('Đăng ký dùng thử thành công! Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất!');
                </script>
                <?php
            }
        }
        
		$this->load->view('member/home.php', $this->_data);
	}

    public function page(){
        $id = explode("-",$this->uri->segment(2));
        $id = $id[0];
        if($id){
            $check = $this->Mpage->mem_check_page_id($id);
            if($check==0){
                redirect(base_url(),'refresh');
            }
        }else{
            redirect(base_url(),'refresh');
        }
        $current_page = $this->Mpage->get_page_by("page_id",$id);
        $current_page['page_content'] = $this->globals->change_content_ckeditor($current_page['page_content']);
        $this->_data['page_title'] = $current_page['page_title'];
        $this->_data['current_page'] = $current_page;
        if($current_page['template'] == 0){
            $this->load->view('member/page/default_page.php', $this->_data);
        }else{
            $this->load->view('member/page/contact.php', $this->_data);
        }
    }


}

/* End of file Home.php */
/* Location: ./application/controllers/admin/Home.php */