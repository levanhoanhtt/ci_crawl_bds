<?php
class User_widget extends PVS_Widget
{
    function index()
    {
        $this->load->model('employee/Muser');
        $login = $this->session->get_userdata();
        $user_row = $this->Muser->get_user_info($login['customer_login']);

        $data['user_info'] = $user_row;

        $this->load->view('view', $data);
    }
}
?>