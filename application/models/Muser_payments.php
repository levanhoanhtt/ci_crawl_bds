<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Muser_payments extends CI_Model
{
	protected $_table        = 'user_payments';
	protected $_table_users  = 'users';
	function __construct()
	{
		parent::__construct();

	}
	public function get_all_user_payments()
	{
	    $this->db->order_by('create_at', 'desc');
		return $this->db->get($this->_table)->result_array();
	}
	public function check_user_by_id($user_id)
	{
		$this->db->where("id", $user_id);
		return $this->db->get($this->_table)->num_rows();
	}
	public function insert_user_payment($data)
	{
		$this->db->insert($this->_table,$data);
		return $this->db->insert_id();
	}
	public function delete($user_id)
	{
		$this->db->where('id',$user_id);
		return $this->db->delete($this->_table);
	}
	public function get_amount_user($user_id)
	{
		$this->db->where('user_id',$user_id);
		$query = $this->db->get($this->_table_users);
		foreach ($query->result() as $que ) {
		return 	$que->amount;

		}
	}
	public function get_amount_all_user()
	{
		$query = $this->db->get($this->_table_users);
		foreach ($query->result() as $que ) {
		return $que->amount;
		}
	}
	public function update($user_id,$data)
	{
		$this->db->where('user_id',$user_id);
		return $this->db->update($this->_table_users,$data);
	}

    public function get_user_payment_by($field,$value){
        $this->db->where($field, $value);
        return $this->db->get($this->_table)->result_array();
    }

}


?>