<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muser extends CI_Model
{

    protected $_table = 'users';
    protected $_table_users_log = 'users_log';
    protected $_table_renew_logs = 'renew_logs';

    public function __construct()
    {
        parent::__construct();

    }

    public function get_all_user()
    {
        $this->db->order_by('timestamp', 'desc');
        return $this->db->get($this->_table)->result_array();
    }

    public function user_info($username)
    {
        $this->db->where('username', $username);
        return $this->db->get($this->_table)->row_array();
    }

    public function get_all_business_owner()
    {
        $this->db->where("user_type", 1)->where("user_type_active", 1);
        return $this->db->get($this->_table)->result_array();
    }

    // Insert
    public function insertUser($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    // Check exist
    public function check_exists_signup($field, $value)
    {
        $this->db->where($field, $value);
        return $this->db->get($this->_table)->num_rows();
    }

    //Check user by id
    public function check_user_id($user_id)
    {
        $this->db->where("user_id", $user_id);
        return $this->db->get($this->_table)->num_rows();
    }

    // Check user_name 
    public function check_username($username, $id = 0)
    {
        $this->db->where("user_id <>", $id)->where("username", $username);
        return $this->db->get($this->_table)->num_rows();
    }

    public function check_email($email, $id = 0)
    {
        $this->db->where("user_id <>", $id)->where("email_address", $email);
        return $this->db->get($this->_table)->num_rows();
    }


    //Get user by
    public function get_user_by($field, $value)
    {
        $this->db->where($field, $value);
        return $this->db->get($this->_table)->row_array();
    }

    // Update 
    public function update($user_id, $data)
    {
        $this->db->where('user_id', $user_id);
        return $this->db->update($this->_table, $data);
    }

    // update last login
    public function update_login($user_name, $data)
    {
        $this->db->where('username', $user_name);
        return $this->db->update($this->_table, $data);
    }

    // Delete 
    public function delete($user_id)
    {
        $this->db->where('user_id', $user_id);
        return $this->db->delete($this->_table);
    }

    //Check_login
    public function check_login($username, $password)
    {
        $this->db->where('username', $username)->where('password', $password)->where("active", 1);
        return $this->db->get($this->_table)->num_rows();
    }

    // check login users_log
    public function check_users_log($username)
    {
        $this->db->where('username', $username);
        return $this->db->get($this->_table_users_log)->num_rows();
    }

    // insert users_log
    public function insert_users_log($data)
    {
        $this->db->insert($this->_table_users_log, $data);
        return $this->db->insert_id();
    }

    public function del_user_log($username)
    {
        $this->db->where('username', $username);
        $this->db->delete($this->_table_users_log);
    }

    // get info login
    public function get_user_info($username)
    {
        $this->db->where("username", $username);
        return $this->db->get($this->_table)->row_array();
    }

    /* table renew logs */
    public function list_renew($user_id = 0)
    {
        if ($user_id > 0) {
            $this->db->where('user_id', $user_id);
        }
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->_table_renew_logs)->result_array();
    }

    public function get_user_renew_by($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_table_renew_logs)->row_array();
    }

    public function add_renews($data)
    {
        $this->db->insert($this->_table_renew_logs, $data);
        return $this->db->insert_id();
    }

    public function del_renews($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($this->_table_renew_logs);
    }

    public function check_id_renews($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_table_renew_logs)->num_rows();
    }

    public function update_amount_user($id, $data)
    {
        $this->db->where('user_id', $id);
        return $this->db->update($this->_table, $data);
    }

    public function update_renew($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update($this->_table_renew_logs, $data);
    }

    public function count_vip()
    {
        $this->db->where('user_type', 1);
        return $this->db->get($this->_table)->result_array();
    }


}

/* End of file Muser.php */
/* Location: ./application/models/Muser.php */