<?php 
/**
 * 
 */
class Mdistrict extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function get_district_by_province($idprovince)
	{
		$this->db->where('matp',$idprovince);
		return $this->db->get('district')->result_array();
	}

	public function get_district_by_id($id)
	{
		$this->db->where('maqh',$id);
		return $this->db->get('district')->row_array();
	}
}
 ?>