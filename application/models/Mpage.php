<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpage extends CI_Model {

	protected $_table = 'pages';
    public function __construct()
    {
        parent::__construct();

    }

    /* ==================  Admin ==================== */
    public function get_all_page(){
        $this->db->order_by('page_id', 'desc');
    	return $this->db->get($this->_table)->result_array();
    }

    // Insert
    public function insert($data){
    	$this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

     //Check page by id
    public function check_page_id($page_id){
    	$this->db->where("page_id", $page_id);
    	return $this->db->get($this->_table)->num_rows();
    }

    //Get page by
    public function get_page_by($field,$value){
    	$this->db->where($field, $value);
    	return $this->db->get($this->_table)->row_array();
    }

    // Update 
    public function update($page_id, $data){
        $this->db->where("page_id", $page_id);
        return $this->db->update($this->_table, $data);
    }

    // Delete 
    public function delete($page_id){
        $this->db->where("page_id",$page_id);
        return $this->db->delete($this->_table);
    }

    //Check page by id
    public function mem_check_page_id($page_id){
        $this->db->where("page_id", $page_id)->where("active",1);
        return $this->db->get($this->_table)->num_rows();
    }

}