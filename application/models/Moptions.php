<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Moptions extends CI_Model {

	protected $_table = 'options';
    public function __construct()
    {
        parent::__construct();

    }

    public function get_all_post(){
    	return $this->db->get($this->_table)->result_array();
    }


    //Get option by opt_key
    public function get_option($opt_key){
    	$this->db->where("opt_key", $opt_key);
    	return $this->db->get($this->_table)->row_array();
    }

    // Update 
    public function update($opt_key, $data){
        $this->db->where("opt_key", $opt_key);
        return $this->db->update($this->_table, $data);
    }

    public function get_all_tables()
    {
        $db_name = $this->db->database;
        $query = $this->db->query("SELECT table_name FROM information_schema.tables where table_schema='".$db_name."'");
        return $query->result_array();
    }

    public function get_field_name($table_name)
    {
        $db_name = $this->db->database;

        $query = $this->db->query("SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$db_name."' AND TABLE_NAME = '".$table_name."'");
        return $query->result_array();
    }

    public function replace_url($old_link, $new_link, $field_name, $table_name)
    {
        $query = $this->db->query("UPDATE $table_name SET $field_name = replace($field_name, '".$old_link."', '".$new_link."')");
        if ($query)
        {
            return 1;
        }

        return 0;
    }

    
}

/* End of file Muser.php */
/* Location: ./application/models/Muser.php */