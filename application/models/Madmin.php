<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Madmin extends CI_Model {

	protected $_table = 'admins';
    public function __construct()
    {
        parent::__construct();

    }

    public function get_all_user(){
        $this->db->order_by('admin_id', 'desc');
    	return $this->db->get($this->_table)->result_array();
    }

    // Insert
    public function insertUser($data){
    	$this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

     //Check user by id
    public function check_user_id($admin_id){
    	$this->db->where("admin_id", $admin_id);
    	return $this->db->get($this->_table)->num_rows();
    }

    // Check user_name 
    public function check_username($admin_account, $id=0){
     	$this->db->where("admin_id <>", $id)->where("admin_account",$admin_account);
     	return $this->db->get($this->_table)->num_rows();
    }

    // Check admin_email 
    public function check_email($admin_email, $id=0){
        $this->db->where("admin_id <>", $id)->where("admin_email",$admin_email);
        return $this->db->get($this->_table)->num_rows();
    }

    //Check login
    public function check_login($admin_account, $password)
    {
        $this->db->where(array("admin_account"=>$admin_account, 'admin_password'=>md5($password)));
        return $this->db->get($this->_table)->num_rows();
    }

    //Get admin by
    public function get_admin_by($field,$value){
    	$this->db->where($field, $value);
    	return $this->db->get($this->_table)->row_array();
    }

    // Update 
    public function update($admin_id, $data){
        $this->db->where("admin_id", $admin_id);
        return $this->db->update($this->_table, $data);
    }

    // Delete 
    public function delete($admin_id){
        $this->db->where("admin_id",$admin_id);
        return $this->db->delete($this->_table);
    }

}

/* End of file Muser.php */
/* Location: ./application/models/Muser.php */