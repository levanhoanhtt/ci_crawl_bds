<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcategory extends CI_Model {

	protected $_table = 'categories';
    public function __construct()
    {
        parent::__construct();
    }

    /* ------------ Admin --------------- */

    // Get all cate
    public function ad_get_all_cate(){
    	return $this->db->get($this->_table)->result_array();
    }

    // Get cate children
    public function ad_get_cate_child($parent_id=0){
    	$this->db->where('parent_id', $parent_id);
    	return $this->db->get($this->_table)->result_array();
    }

    // Get cate children
    public function ad_get_cate_child_active($parent_id=0){
        $this->db->where('parent_id', $parent_id)->where("active",1);
        return $this->db->get($this->_table)->result_array();
    }

    //Check cate by id
    public function ad_check_cate_id($cate_id){
    	$this->db->where('cate_id', $cate_id);
    	return $this->db->get($this->_table)->num_rows();
    }

    //Get cate by
    public function ad_get_cate_by($field,$value){
    	$this->db->where($field, $value);
    	return $this->db->get($this->_table)->row_array();
    }

    // Check exists cate_name
    public function check_cate_name_exists($cate_name,$cate_id=0){
        $this->db->where('cate_id <>', $cate_id)->where("cate_name", $cate_name);
        return $this->db->get($this->_table)->num_rows();
    }

    // Update 
    public function update($cate_id, $data){
        $this->db->where('cate_id', $cate_id);
        return $this->db->update($this->_table, $data);
    }

    //Insert
    public function insert($data){
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    // Delete 
    public function delete($cate_id){
        $this->db->where('cate_id',$cate_id);
        return $this->db->delete($this->_table);
    }


    /* ------------------------ Member ------------------ */
    // Get all cate
    public function mem_get_all_cate(){
        $this->db->where("active",1);
        return $this->db->get($this->_table)->result_array();
    }

    // Get cate children
    public function mem_get_cate_child($parent_id=0){
        $this->db->where('parent_id', $parent_id)->where("active",1);
        return $this->db->get($this->_table)->result_array();
    }

    //Check cate by id
    public function mem_check_cate_id($cate_id){
        $this->db->where('cate_id', $cate_id)->where("active",1);
        return $this->db->get($this->_table)->num_rows();
    }

    //Get cate by
    public function mem_get_cate_by($field,$value){
        $this->db->where($field, $value)->where("active",1);
        return $this->db->get($this->_table)->row_array();
    }

}

/* End of file Mcate.php */
/* Location: ./application/models/Mcate.php */