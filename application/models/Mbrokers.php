<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbrokers extends CI_Model
{

    protected $_table = 'broker';

    public function __construct()
    {
        parent::__construct();
    }

    /* ==================  Admin ==================== */
    public function get_all_brokers()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->_table)->result_array();
    }
    public function get_all_brokers_page($page,$search)
    {
        $this->db->like('name',$search);
        $this->db->or_like('phone',$search);
        $this->db->order_by('id', 'desc');
        if ($page > 0) {
            $this->db->limit(50, $page);
        } else {
            $this->db->limit(50);
        }
        return $this->db->get($this->_table)->result_array();
    }

    // Insert
    public function insert($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    // insert check phone 

    public function insert_check_phone($data, $phone)
    {
        if (isset($data['phone']) && !empty($data['phone'])) {
            $this->db->where('phone', $phone);
            $count = $this->db->get($this->_table)->row_array();
            if ($count) {
                return $count['id'];
            }
            else {
                $this->db->insert($this->_table, $data);
                return $this->db->insert_id();
            }
        }
        else {
            return 0;
        }
    }
    public function get_total_num($search)
    {
        if($search != null && $search != "")
        {
            $this->db->like('name',$search);
            $this->db->or_like('phone',$search);
        }
        return $this->db->get($this->_table)->num_rows();
    }


    //Check broker by id
    public function check_broker_id($broker_id)
    {
        $this->db->where("id", $broker_id);
        return $this->db->get($this->_table)->num_rows();
    }

    //Get broker by
    public function get_broker_by($field, $value)
    {
        $this->db->where($field, $value);
        return $this->db->get($this->_table)->row_array();
    }

    // Update
    public function update($id, $data)
    {
        $this->db->where("id", $id);
        return $this->db->update($this->_table, $data);
    }

    // Delete
    public function delete($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete($this->_table);
    }

}