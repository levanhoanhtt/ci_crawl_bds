<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mprovince extends CI_Model {

    protected $_table = 'province';
    public function __construct()
    {
        parent::__construct();

    }

    public function get_all_province()
    {
        $this->db->order_by('name', 'ASC');
        return $this->db->get($this->_table)->result_array();
    }

    public function get_province($id)
    {
        $this->db->where('matp', $id);
        return $this->db->get($this->_table)->row_array();
    }

    public function get_list_district($province_id)
    {
        $this->db->where('matp', $province_id);
        return $this->db->get('district')->result_array();
    }

    public function get_district($id)
    {
        $this->db->where('maqh', $id);
        return $this->db->get('district')->row_array();
    }

}