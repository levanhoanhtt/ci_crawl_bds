<?php
require 'vendor/autoload.php';
use Elasticsearch\ClientBuilder;

defined('BASEPATH') or exit('No direct script access allowed');

class Mpost extends CI_Model
{

    protected $_table = 'posts';

    public function __construct()
    {
        parent::__construct();
    }

    /* ==================  Admin ==================== */
    public function get_all_post($filter = array(), $keyword = '', $category = array(), $per_page, $offset)
    {

        $this->db->order_by('timestamp', 'desc');

        if (!empty($filter)) {
            $this->db->where($filter);
        }

        if (!empty($keyword)) {

            $this->db->like('post_title', $keyword);
            $this->db->or_like('post_content', $keyword);
            $this->db->or_like('contact', $keyword);
        }

        if (!empty($category)) {
            $this->db->where_in('cate_id', $category);
        }

        $this->db->limit($per_page, $offset);

        return $this->db->get($this->_table)->result_array();


    }

    public function get_all_post_elastic($filter = array(), $keyword = '', $category = array(), $per_page, $offset)
    {
        $hosts = [
            '127.0.0.1:9200',         // IP + Port
        ];

        $client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();
        if(!$filter){
            $query = [
                'match_all' => new \stdClass()
            ];
        }else{
            $query = [
                'bool' => [
                    'must'=>$filter
                    ]
                ];
        }
        $params = [
            'from' => $offset,          // how long between scroll requests. should be small!
            'size' => $per_page,             // how many results *per shard* you want back
            'index' => 'bds',
            'body' => [
                'query' => $query,
            ]

        ];

        $posts = [];
        $results = $client->search($params);
        foreach($results['hits']['hits'] as $record){
            $posts[] = $record['_source'];
        }

        return $posts;
    }


    //Get count all post
    public function get_count_post($filter = array(), $keyword = '', $category = array())
    {
        if (!empty($filter)) {
            $this->db->where($filter);
        }

        if (!empty($keyword)) {
            $this->db->like('post_title', $keyword);
            $this->db->or_like('post_content', $keyword);
            $this->db->or_like('contact', $keyword);
        }

        if (!empty($category)) {
            $this->db->where_in('cate_id', $category);
        }
        return $this->db->count_all_results($this->_table);
    }

    public function get_count_post_elastic($filter = array(), $keyword = '', $category = array())
    {
        $hosts = [
            '127.0.0.1:9200',         // IP + Port
        ];

        $client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();

        if(!$filter){
            $query = [
                'match_all' => new \stdClass()
            ];
        }else{
            $query = [
                'bool' => [
                    'must'=>$filter
                ]
            ];
        }

        $params = [
            // how many results *per shard* you want back
            'index'  => 'bds',
            'body'   => [
                'query' => $query,
            ]
        ];


        $results = $client->count($params);
        return $results['count'];
    }



    public function get_all_post_ajax($filter = array(), $keyword = '', $category = array(), $start = 0, $orderBy, $order)
    {

        $this->db->order_by($orderBy, $order);
        if (!empty($filter)) {
            $this->db->where($filter);
        }
        $this->db->where('status_crawler', 1);//long

        if (!empty($keyword)) {
            $this->db->group_start()->like('post_title', $keyword);
            $this->db->or_like('post_content', $keyword)->group_end();//long
        }

        if (!empty($category)) {
            $this->db->where_in('cate_id', $category);
        }

        if ($start > 0) {
            $this->db->limit(50, $start);
        } else {
            $this->db->limit(50);
        }


        return $this->db->get($this->_table)->result_array();
    }

    public function count_all_ajax($filter = array(), $keyword = '', $category = array())
    {
        $this->db->order_by('timestamp', 'desc');
        if (!empty($filter)) {
            $this->db->where($filter);
        }
        $this->db->where('status_crawler', 1);//long

        if (!empty($keyword)) {
            $this->db->like('post_title', $keyword);
            $this->db->or_like('post_content', $keyword);
        }

        if (!empty($category)) {
            $this->db->where_in('cate_id', $category);
        }

        $this->db->get($this->_table)->num_rows();



        return $this->db->get($this->_table)->num_rows();
    }

    public function get_list_post_by($type, $filter = array(), $keyword = '', $category = array(), $per_page, $offset, $sort = null)
    {
        if (!empty($filter)) {
            $this->db->where($filter);
        }

        if (!empty($keyword)) {
            $this->db->group_start()->like('post_title', $keyword);//long
            $this->db->or_like('post_content', $keyword);
            $this->db->or_like('contact', $keyword)->group_end();
        }

        if (!empty($category)) {
            $this->db->where_in('cate_id', $category);
        }

        if (!empty($sort)) {
            if ($sort == "price_desc") {
                $this->db->order_by('price', 'DESC');
            }
            if ($sort == "price_asc") {
                $this->db->order_by('price', 'ASC');
            }
            if ($sort == "time") {
                $this->db->order_by('timestamp', 'DESC');
            }
        }

        if ($type!=1)
        {
             $this->db->where('status_crawler', 1);//long
        }
       

        $this->db->where('for_vip_user', $type);

        $this->db->where('active', 1);

        $this->db->order_by('timestamp', 'DESC');

        $this->db->limit($per_page, $offset);

        return $this->db->get($this->_table)->result_array();


    }

    //Lấy số lượng các post đã kích hoạt
    public function get_number_post_active($type, $filter = array(), $keyword = '', $category = array())
    {
        if (!empty($filter)) {
            $this->db->where($filter);
        }

        if (!empty($keyword)) {
            $this->db->group_start()->like('post_title', $keyword);//long
            $this->db->or_like('post_content', $keyword);
            $this->db->or_like('contact', $keyword)->group_end();
        }

        if (!empty($category)) {
            $this->db->where_in('cate_id', $category);
        }
        
        if ($type!=1)
        {
             $this->db->where('status_crawler', 1);//long
        }

        $this->db->where('for_vip_user', $type);

        $this->db->where('active', 1);

        return $this->db->get($this->_table)->num_rows();

    }

    public function get_list_post_ad($type, $filter = array(), $keyword = '')
    {
        if (!empty($filter)) {
            $this->db->where($filter);
        }

        if (!empty($keyword)) {
            $this->db->like('post_title', $keyword);
            $this->db->or_like('post_content', $keyword);
            $this->db->or_like('contact', $keyword);
        }
        $this->db->where('status_crawler', 1);//long

        $this->db->where('for_vip_user', $type);
        return $this->db->get($this->_table)->result_array();
    }

    // Insert
    public function insert($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    //Check post by id
    public function check_post_id($post_id)
    {
        $this->db->where("post_id", $post_id);
        return $this->db->get($this->_table)->num_rows();
    }

    //Get post by
    public function get_post_by($field, $value)
    {
        $this->db->where($field, $value);
        return $this->db->get($this->_table)->row_array();
    }

    // Update
    public function update($post_id, $data)
    {
        $this->db->where("post_id", $post_id);
        return $this->db->update($this->_table, $data);
    }


    // Delete
    public function delete($post_id)
    {
        $this->db->where("post_id", $post_id);

        $hosts = [
            '127.0.0.1:9200',         // IP + Port
        ];

        //Delete Elasticsearch record by post_id
        $client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();



        $params = [
            'index'  => 'bds',
            'type'  =>'bds_post',
            'id' =>$post_id,
        ];
        try{
            $client->delete($params);
        }catch (Exception $e){
            //Khong tim thay ban ghi can xoa
        }

        return $this->db->delete($this->_table);
    }

    // Check exists post_title
    public function check_post_title_exists($post_title, $post_id = 0)
    {
        $this->db->where('post_id <>', $post_id)->where("post_title", $post_title);
        return $this->db->get($this->_table)->num_rows();
    }

    /* ================== Member =================== */
    //Get all post
    public function mem_get_all_post()
    {
        $this->db->where("active", 1);
        $this->db->order_by("timestamp", "desc");
        return $this->db->get($this->_table)->result_array();
    }

    //Get post by
    public function mem_get_post_by($field, $value)
    {
        $this->db->where($field, $value)->where("active", 1);
        $this->db->order_by("timestamp", "desc");
        return $this->db->get($this->_table)->row_array();
    }

    //Get post feadtured
    public function mem_get_post_featured()
    {
        $this->db->where("feadtured_post", 1)->where("active", 1);
        $this->db->order_by("timestamp", "desc");
        return $this->db->get($this->_table)->result_array();
    }

    //Check post by id
    public function mem_check_post_id($post_id)
    {
        $this->db->where("post_id", $post_id)->where("active", 1);
        return $this->db->get($this->_table)->num_rows();
    }

    public function insert_post_images($data)
    {
        $this->db->insert('post_image', $data);
        return $this->db->insert_id();
    }

    public function delete_post_images($post_id)
    {
        $this->db->where('post_id', $post_id);
        $this->db->delete('post_image');
    }

    public function get_post_images($post_id)
    {
        $this->db->where('post_id', $post_id);
        return $this->db->get('post_image')->result_array();
    }

    public function insert_save_post($data)
    {
        $this->db->insert('post_save', $data);
        return $this->db->insert_id();
    }

    public function delete_save_post($post_id, $user_id)
    {
        $this->db->where(array('post_id' => $post_id, 'user_id' => $user_id));
        $this->db->delete('post_save');
    }

    public function get_number_save_post($post_id, $user_id)
    {
        $this->db->where(array('post_id' => $post_id, 'user_id' => $user_id));
        return $this->db->get('post_save')->num_rows();
    }

    public function get_all_save_post($user_id)
    {
        $this->db->where(array('user_id' => $user_id));
        return $this->db->get('post_save')->result_array();
    }

    public function insert_post_read($data)
    {
        $this->db->insert('post_read', $data);
        return $this->db->insert_id();
    }

    public function get_post_read($user_id, $post_id)
    {
        $this->db->where(array('user_id' => $user_id, 'post_id' => $post_id));
        return $this->db->get('post_read')->num_rows();
    }

    public function get_product_price()
    {
        $this->db->select('price');
        return $this->db->get($this->_table)->result_array();
    }

    public function get_province_id_by_name($province_name)
    {
        $this->db->like('name', $province_name);
        return $this->db->get('province')->row_array();
    }

    public function get_district_id_by_name($district_name)
    {
        $this->db->like('name', $district_name);
        return $this->db->get('district')->row_array();
    }


    public function get_province_id_by_district_id($district_id)
    {
        $this->db->like('maqh', $district_id);
        return $this->db->get('district')->row_array();
    }

    public function check_date($contact, $date)
    {
        if ($contact != null) {
            return $this->db->where('contact', $contact)->where('timestamp >', $date)->count_all_results('posts');
        } else {
            return false;
        }
    }

    // get check post

    public function get_value_check_post()
    {
        /*$this->db->select('post_id, post_title,post_content,contact,contact_name,price,source,email_contact,broker_id, status_crawler');
        $this->db->where('crawler_type', 1);
        return $this->db->get($this->_table)->result_array();*/

        return $this->db->query('SELECT post_id, post_title,post_content,contact,contact_name,price,source,email_contact,broker_id, status_crawler FROM posts WHERE crawler_type != ? AND for_vip_user = 0', array(2))->result_array();
    }

    public function get_all_check_post()
    {
        /*$this->db->select('post_id, post_title,post_content,contact,price,source,email_contact,broker_id');
        $this->db->where('crawler_type', 2);
        return $this->db->get($this->_table)->result_array();*/

        return $this->db->query('SELECT post_id, post_title,post_content,contact,price,source,email_contact,broker_id FROM posts WHERE crawler_type != ? AND for_vip_user = 0', array(1))->result_array();
    }

    public function get_post_by_contact_phone($phone)
    {
        $this->db->select('post_id,contact_name,contact,email_contact, broker_id, crawler_type');
        $this->db->where('contact', $phone);
        return $this->db->get($this->_table)->result_array();
    }

    // check phone duplicate

    public function check_phone()
    {
        $query = 'SELECT `post_id`,`contact`,`contact_name`,`email_contact`, `status_crawler`, COUNT(1) FROM `posts` WHERE crawler_type = 1 AND for_vip_user = 0 AND source IS NOT NULL GROUP BY `contact` HAVING COUNT(contact) > 2';
        $result = $this->db->query($query)->result_array();
        return $result;
    }

    public function check_area($contact)
    {
        $query = 'SELECT `post_id`, `contact`,`contact_name`,`email_contact`, `area` FROM `posts` WHERE `contact` = ? AND for_vip_user = 0 AND source IS NOT NULL';
        $result = $this->db->query($query, array($contact))->result_array();
        return $result;
    }

    public function updateTypeBrokerByPhone()
    {
        $this->db->query("UPDATE posts SET crawler_type = 2, status_crawler = 1 WHERE crawler_type = 1 AND contact != '' AND for_vip_user = 0 AND source IS NOT NULL AND contact IN(SELECT phone FROM broker)");
    }


    public function get_posts_by_districtid()
    {

        // $this->db->where("district_id", NULL)->or_where('district_id =', "")->where('district_name_source !=', "")->where('district_name_source !=', NULL);
        
        // return $this->db->get($this->_table)->result_array();

        $query = "SELECT * FROM `posts` WHERE (`district_id` IS NULL OR `district_id` = '') AND `district_name_source` != '' AND `district_name_source` IS NOT NULL";
        $result = $this->db->query($query)->result_array();
        return $result;

    }
}

/* End of file Muser.php */
/* Location: ./application/models/Muser.php */
