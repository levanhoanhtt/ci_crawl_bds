<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mreview extends CI_Model {

	protected $_table = 'review_business';
    public function __construct()
    {
        parent::__construct();

    }

    public function get_all_review(){
    	return $this->db->get($this->_table)->result_array();
    }

    // Insert
    public function insert($data){
    	$this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    //Check review by id
    public function check_review_id($review_id){
    	$this->db->where("review_id", $review_id);
    	return $this->db->get($this->_table)->num_rows();
    }

    //Get review by
    public function get_review_by($field,$value){
    	$this->db->where($field, $value);
    	return $this->db->get($this->_table)->row_array();
    }

    //Get review by
    public function get_list_review_by($field,$value){
        $this->db->where($field, $value);
        return $this->db->get($this->_table)->result_array();
    }

    //Get review of user for business
    public function get_review_user_for_business($user_id,$business_id){
        $this->db->where("user_id", $user_id)->where("business_id",$business_id);
        return $this->db->get($this->_table)->num_rows();
    }

    // Update 
    public function update($review_id, $data){
        $this->db->where("review_id", $review_id);
        $this->db->update($this->_table, $data);
        return $this->db->affected_rows();
    }

    // Delete 
    public function delete($review_id){
        $this->db->where("review_id",$review_id);
        return $this->db->delete($this->_table);
    }

    /* ================== Member ==================== */
    //Get review by
    public function mem_get_list_review_by($field,$value){
        $this->db->where($field, $value)->where("active",1);
        return $this->db->get($this->_table)->result_array();
    }

}

/* End of file Mreview.php */
/* Location: ./application/models/Mreview.php */