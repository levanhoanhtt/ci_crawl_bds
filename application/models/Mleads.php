<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mleads extends CI_Model {

    protected $_table = 'leads';
    public function __construct()
    {
        parent::__construct();

    }

    /* ==================  Admin ==================== */
    public function get_all_leads(){
        $this->db->order_by('id', 'desc');
        return $this->db->get($this->_table)->result_array();
    }

    // Insert
    public function insert($data){
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    //Check lead by id
    public function check_lead_id($lead_id){
        $this->db->where("id", $lead_id);
        return $this->db->get($this->_table)->num_rows();
    }

    //Get lead by
    public function get_lead_by($field,$value){
        $this->db->where($field, $value);
        return $this->db->get($this->_table)->row_array();
    }

    // Update
    public function update($page_id, $data){
        $this->db->where("id", $page_id);
        return $this->db->update($this->_table, $data);
    }

    // Delete
    public function delete($page_id){
        $this->db->where("page_id",$page_id);
        return $this->db->delete($this->_table);
    }

}