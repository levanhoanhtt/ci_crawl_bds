<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Mlead extends CI_Model
{
	protected $_table = 'leads';
	function __construct()
	{
		parent::__construct();


	}
	public function get_all_lead()
	{
	    $this->db->order_by('create_at', 'desc');
		return $this->db->get($this->_table)->result_array();
	}
	public function insertLead($data)
	{
		$this->db->insert($this->_table,$data);
		return $this->db->insert_id();

	}
	public function check_lead_by_id($lead_id)
	{
		$this->db->where("id", $lead_id);
		return $this->db->get($this->_table)->num_rows();
	}
	public function get_lead_by($field, $value)
	{
		$this->db->where($field, $value);
		return $this->db->get($this->_table)->row_array();

	}
	public function update($lead_id, $data)
	{
		$this->db->where('id', $lead_id);
		return $this->db->update($this->_table,$data);

	}
	public function delete($lead_id)
	{
		$this->db->where('id',$lead_id);
		return $this->db->delete($this->_table);
	}


}


?>