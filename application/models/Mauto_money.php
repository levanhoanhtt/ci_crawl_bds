<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Mauto_money extends CI_Model
{
	protected $_table = 'money_logs';
	function __construct()
	{
		parent::__construct();
	}
	public function insert_money_logs($data)
	{
		$this->db->insert($this->_table,$data);
		return $this->db->insert_id();

	}

	public function check_logs($user_id, $now)
    {
        $this->db->where('timestamp >=', $now);
        $this->db->where('user_id', $user_id);
        if ($this->db->get($this->_table)->num_rows()>0)
        {
            return false;
        }

        return true;

    }

}

 ?>