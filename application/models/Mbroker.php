<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbroker extends CI_Model {

	protected $_table = 'broker';
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }
    public function check($phone){
        $this->db->where("phone", $phone);
        return $this->db->get($this->_table)->num_rows();
    }

   
    

}

/* End of file Muser.php */
/* Location: ./application/models/Muser.php */