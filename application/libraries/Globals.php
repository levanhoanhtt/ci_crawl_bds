<?php



class Globals

{

    public function get_message($type, $title, $content)

    {

        ?>

        <script>

            jQuery.msgBox({

                title: "<?php echo $title; ?>",

                content: "<?php echo $content; ?>",

                type: "<?php echo $type; ?>"

            });

        </script>

        <?php

    }



    public function get_dateformat($strDate, $format)

    {

        $date = date_create($strDate);

        return date_format($date, $format);

    }



    public function get_the_content($content)

    {

        return nl2br($content);

    }



    public function sendMail($to, $subject, $content)

    {

        require_once(APPPATH . '/libraries/phpmailer/class.phpmailer.php');

        require_once(APPPATH . '/libraries/phpmailer/class.smtp.php');

        $mail = new PHPMailer();



        $mail->IsSMTP(); // set mailer to use SMTP



        $mail->Host = 'smtp.mailgun.org';

        $mail->Port = '587'; // set the port to use

        $mail->SMTPAuth = true; // turn on SMTP authentication



        $mail->SMTPSecure = 'tls';

        $mail->Username = 'postmaster@sender.sendervn.com'; // your SMTP username or your gmail username

        $mail->Password = 'eb6e9caaf327c676c9e4d06e1ee38746-9ce9335e-e816d3b1'; // your SMTP password or your gmail password

        $mail->Timeout = 3600;



        $mail->From = 'no-reply@nguonnhadat.com.vn';

        $mail->FromName = 'Nguồn Nhà Đất';

        // Name to indicate where the email came from when the recepient received

        $mail->AddAddress($to);

        $mail->CharSet = 'UTF-8';

        $mail->WordWrap = 50; // set word wrap

        $mail->IsHTML(true); // send as HTML

        $mail->Subject = $subject;

        $mail->Body = $content; //HTML Body



        $mail->SMTPDebug = 0;

        if (!$mail->Send()) {

            return false;

        } else {

            return true;

        }

    }



    function custom_cut($str, $number)

    {

        $excerpt = $str;

        $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);

        $excerpt = explode("&nbsp;", $excerpt);

        $new_str = "";

        $d = 0;

        if (count($excerpt) < $number) $number = count($excerpt);

        for ($i = 0; $i < $number; $i++) {

            $excerpt[$i] = strip_tags($excerpt[$i]);

            $d++;

            if ($d > 1) $new_str .= " ";

            $new_str .= $excerpt[$i];

        }

        if ($d < count($excerpt) - 1) {

            $new_str .= "...";

        }

        return $new_str;

    }



    function change_content_ckeditor($str)

    {

        $excerpt = $str;

        $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);

        $excerpt = explode("&nbsp;", $excerpt);

        $new_str = "";

        $d = 0;

        foreach ($excerpt as $item) {

            $d++;

            if ($d > 1) $new_str .= " ";

            $new_str .= $item;

        }

        return $new_str;

    }



    function current_url()

    {

        $CI =& get_instance();

        $url = $CI->config->site_url($CI->uri->uri_string());

        return $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;

    }



    function my_export($name, $title, $th_array, $tr_array)

    {

        // Bước 1:

        // Lấy dữ liệu từ database



        // Bước 2: Import thư viện phpexcel

        require_once(APPPATH . 'libraries/PHPExcel.php');



        // Bước 3: Khởi tạo đối tượng mới và xử lý

        $PHPExcel = new PHPExcel();



        // Bước 4: Chọn sheet - sheet bắt đầu từ 0

        $PHPExcel->setActiveSheetIndex(0);



        // Bước 5: Tạo tiêu đề cho sheet hiện tại

        $PHPExcel->getActiveSheet()->setTitle($title);



        // Bước 6: Tạo tiêu đề cho từng cell excel,

        // Các cell của từng row bắt đầu từ A1 B1 C1 ...

        $PHPExcel->getActiveSheet()->setCellValue('A1', 'STT');

        foreach ($th_array as $key => $item) {

            $PHPExcel->getActiveSheet()->setCellValue($key . '1', $item);

        }



        // Bước 7: Lặp data và gán vào file

        // Vì row đầu tiên là tiêu đề rồi nên những row tiếp theo bắt đầu từ 2

        $rowNumber = 2;

        $d = 1;

        if ($tr_array) {

            foreach ($tr_array as $tr_item) {

                $PHPExcel->getActiveSheet()->setCellValue('A' . $rowNumber, $d);

                foreach ($tr_item as $key => $td) {

                    $PHPExcel->getActiveSheet()->setCellValue($key . $rowNumber, $td);

                }



                // Tăng row lên để khỏi bị lưu đè

                $rowNumber++;

                $d++;

            }

        }

        // Bước 8: Khởi tạo đối tượng Writer

        $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');



        // Bước 9: Trả file về cho client download

        header('Content-Type: application/vnd.ms-excel');

        $file_name = $name . "_" . time();

        header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');

        header('Cache-Control: max-age=0');

        if (isset($objWriter)) {

            $objWriter->save('php://output');

        }

    }



    function white_file_employee($user_info, $action, $name = "history.txt")

    {

        $path = FCPATH . 'history';

        if (!file_exists($path)) {

            mkdir($path, 0777, true);

        }

        $path = $path . '/' . $name;

        $content = json_decode(file_get_contents($path));

        $content[] = array(

            'id' => time(),

            'fullname' => $user_info['fullname'],

            'username' => $user_info['username'],

            'email_address' => $user_info['email_address'],

            'user_phone' => $user_info['user_phone'],

            'user_type' => $user_info['user_type'],

            'time' => time(),

            'action' => $action

        );

        $new_content = json_encode($content);

        $fp = fopen($path, "wb");

        fwrite($fp, $new_content);

        fclose($fp);

    }



    function re_white_file($content, $name = "history.txt")

    {

        $path = FCPATH . 'history';

        if (!file_exists($path)) {

            mkdir($path, 0777, true);

        }

        $path = $path . '/' . $name;

        $new_content = json_encode($content);

        $fp = fopen($path, "wb");

        fwrite($fp, $new_content);

        fclose($fp);

    }



    function get_history($name = "history.txt")

    {

        $path = FCPATH . 'history/' . $name;

        $content = json_decode(file_get_contents($path));

        return $content;

    }



    function change_to_slug($str)

    {

        $str = trim(mb_strtolower($str));

        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);

        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);

        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);

        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);

        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);

        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);

        $str = preg_replace('/(đ)/', 'd', $str);

        $str = preg_replace('/\&/', 'va', $str);

        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);

        $str = preg_replace('/([\s]+)/', '-', $str);

        return $str;

    }



    public function change_to_words($price)

    {



        if ($ity = strpos($price, 'tỷ') !== false) {

            preg_match_all("/(.*?) tỷ/", $price, $matchesTy);



            $ty = $matchesTy[1][0];



            if ($idot = strpos($ty, ',') !== false) {

                $trieu = substr($ty, $ity + 1);



                if ($trieu / 100 < 1) {



                    if ($trieu / 10 < 1) {

                        $trieu = $trieu * 100;

                    } else

                        $trieu = $trieu * 10;

                }



                $ty = substr($ty, 0, $ity);



                // echo $ty."-".$trieu;

                $price = $ty * (1000000000) + $trieu * (1000000);



            } else {

                $price = $ty * (1000000000);

            }

            return $price;



        } else if ($itrieu = strpos($price, 'triệu') !== false) {



            // $ty = substr($price, 0,$ity+3);

            preg_match_all("/(.*?) triệu/", $price, $matchesTy);



            $trieu = $matchesTy[1][0];



            if ($idot = strpos($trieu, ',') !== false) {

                $nghin = substr($trieu, $itrieu + 1);

                // echo $nghin."<br>";

                if ($nghin / 100 < 1) {



                    if ($nghin / 10 < 1) {

                        $nghin = $nghin * 100;

                    } else

                        $nghin = $nghin * 10;

                }

                // echo $nghin."<br>";

                $trieu = substr($trieu, 0, $itrieu);



                // echo $ty."-".$trieu;

                $price = $trieu * (1000000) + $nghin * (1000);



            } else {

                $price = $trieu * (1000000);

            }

            return $price;

        }

    }



    public function check_key($title, $name, $phone)

    {

        $resultN = 0;

        $arr_key = array('miễn phí xem nhà', ' thổ cư', ' chuyên thổ cư', ' chuyên chung cư', ' hỗ trợ vay vốn', ' hỗ trợ vay ngân hàng', ' chuyên nghiệp', ' chiết khấu', ' tham quan thực tế', ' ngân hàng hỗ trợ lãi suất', ' liên hệ ngay để nhận ưu đãi/xem nhà', ' tư vấn', ' hỗ trợ', ' nhiệt tình', ' trung thực', ' tư vấn và xem nhà miễn phí', '  thương lượng trực tiếp với chủ nhà', ' hỗ trợ pháp lý từ a –> z hoàn toàn miễn phí', '  không mất bất kỳ khoản chi phí nào', ' miễn phí tư vẫn mua nhà 24/7', ' hỗ trợ', ' miễn phí hoàn toàn', ' thủ tục pháp lý', ' làm việc trực tiếp với chủ nhà', '  tư vấn hỗ trợ tốt nhất', ' miễn phí 100% khi xem nhà', ' chủ và khách tự thương lượng giá cả',

            'để xem nhà', 'hoàn toàn miễn phí', 'trân trọng cảm ơn', 'miễn phí', 'phí dịch vụ', 'liên hệ xem nhà', 'thương lượng trực tiếp với chủ nhà');



        $title = mb_strtolower($title);



        foreach ($arr_key as $key => $value) {

            //kiem tra tieu de co ton tai key ko // CÓ -> tin moi gioi

            if (strpos($title, mb_strtolower($value)) !== false && $phone != "") {

                echo $title . "<br>";

                $resultN = 1;

                $data_in = array('name' => $name, 'phone' => $phone);

                echo $value . "<br>";

                $CI =& get_instance();

                $CI->load->model('Mbroker');

                if ($CI->Mbroker->check($phone) == 0) {



                    if ($CI->Mbroker->insert($data_in) != 0) {

                        echo "thanh cong!!!";





                    } else {

                        echo $this->db->last_query($CI->Mbroker->insert($data_in));

                        echo "sai cau lenh";

                        echo $this->db->last_query($CI->Mbroker->insert($data_in));

                    }

                }





            }

            // else

            // {

            //     echo "string<br>";

            // }

        }

        return $resultN;

    }



    //lọc ra 1 mảng có contact + area bị lặp ít nhất 3 lần

    public function check_area($all_post)

    {

        $result = array();

        for ($i = 0; $i < count($all_post); $i++) {

            $count = 1;

            for ($k = 0; $k < count($all_post); $k++) {

                if ($all_post[$i] != $all_post[$k]) {

                    $count++;

                }

                if ($count > 2) {

                    array_push($result, $all_post[$i]);

                    break;

                }

            }

        }

        return array_unique($result);

    }



    public function check_phone_in_broker($phone)

    {

        if (empty($phone)) return false;

        $CI =& get_instance();

        $CI->load->model('Mbroker');



        if ($CI->Mbroker->check($phone) > 0) {

            return true;

        }

        return false;

    }



    public function check_phonedate($name, $phone)

    {

        $resultN = 0;

        $today = date('Y-m-d H:i:s');

        $date = strtotime(date("Y-m-d H:i:s", strtotime($today)) . " -3 days");

        $CI =& get_instance();



        $CI->load->model('Mbroker');



        $CI->load->model('Mpost');

        //kiem tra so dien thoai nay xuât hien n lan // CÓ -> tin moi gioi

        if ($CI->Mpost->check_date($phone, $date) >= 1) {

            $resultN = 1;

            echo "có";

            if ($CI->Mbroker->check($phone) == 0) {



                $data_in = array('name' => $name, 'phone' => $phone);

                if ($CI->Mbroker->insert($data_in) != 0) {

                    echo "thanh cong";



                } else {



                    echo "sai cau lenh";

                    echo $this->db->last_query($CI->Mbroker->insert($data_in));

                }

            }



        }

        return $resultN;

    }



    public function check_black_key($content, $listPhones){

        $arr_key = array('miễn phí xem nhà', 'thổ cư', 'chuyên thổ cư', 'chuyên chung cư', 'hỗ trợ vay vốn', 'hỗ trợ vay ngân hàng', 'chuyên nghiệp', 'chiết khấu', 'tham quan thực tế', 'ngân hàng hỗ trợ lãi suất',

            'liên hệ ngay để nhận ưu đãi/xem nhà', 'tư vấn', 'hỗ trợ', 'nhiệt tình', 'trung thực', 'tư vấn và xem nhà miễn phí', 'thương lượng trực tiếp với chủ nhà', 'hỗ trợ pháp lý từ a –> z hoàn toàn miễn phí',

            'không mất bất kỳ khoản chi phí nào', 'miễn phí tư vẫn mua nhà 24/7', 'hỗ trợ', 'miễn phí hoàn toàn', 'thủ tục pháp lý', 'làm việc trực tiếp với chủ nhà', 'tư vấn hỗ trợ tốt nhất', 'miễn phí 100% khi xem nhà',

            'chủ và khách tự thương lượng giá cả', 'để xem nhà', 'hoàn toàn miễn phí', 'trân trọng cảm ơn', 'miễn phí', 'phí dịch vụ', 'liên hệ xem nhà', 'thương lượng trực tiếp với chủ nhà');

        $content = mb_strtolower($content);

        foreach ($arr_key as $key) {

            if (mb_strpos($content, $key) !== false) {

                return true;

            }

        }

        $content = str_replace('.', '', $content);

        foreach($listPhones as $phone){

            if(strpos($content, $phone) !== false) {

                return true;

            }

        }

        return false;

    }



    // check lặp số 3 lần

    public function check_phone_area(){

        $CI =& get_instance();

        $all_post_check_phone = $CI->Mpost->check_phone();

        //Hoan code

        foreach ($all_post_check_phone as $post) {

            if (!empty($post['contact'])) {

                $post['contact'] = trim($post['contact']);

                $all_post_check_area = $CI->Mpost->check_area($post['contact']);

                if(!empty($all_post_check_area)){

                    //xet area khac  nhau

                    $areas = array();

                    foreach ($all_post_check_area as $p2) {

                        if(!in_array($p2['area'], $areas)) $areas[] = $p2['area'];

                    }

                    if(count($areas) > 2){

                        $broker_id = $CI->Mbrokers->insert_check_phone(array(

                            'name' => $post['contact_name'],

                            'phone' => $post['contact'],

                            'email' => $post['email_contact'],

                        ), $post['contact']);

                        foreach ($all_post_check_area as $p2) {

                            $CI->Mpost->update($p2['post_id'], array(

                                'crawler_type' => 2,

                                'broker_id' => $broker_id,

                                'status_crawler' => 1,//long

                            ));

                            echo $post['contact'] . '-' . $p2['post_id'] . '-' . $broker_id . PHP_EOL;

                        }

                    }

                }

            }

        }

        //end Hoan code



        /*$arr_mg = array(

            'crawler_type' => 2,

        );

        $arr_cc = array(

            'crawler_type' => 1,

        );

        $arr_stt = array(

            'status_crawler' => 1,

        );



        $results = array();

        foreach ($all_post_check_phone as $post) {

            $this->update_type_crawler($arr_stt, $post['post_id']);

            if (isset($post['contact']) && !empty($post['contact'])) {

                $all_post_check_area = $CI->Mpost->check_area($post['contact']);

                for ($i = 0; $i < count($all_post_check_area); $i++) {

                    $count = 1;

                    for ($k = 0; $k < count($all_post_check_area); $k++) {

                        if ($all_post_check_area[$i]['area'] != $all_post_check_area[$k]['area']) {

                            $count++;

                        }

                        if ($count > 2) {

                            $arr_checked = $all_post_check_area[$i]['contact'];

                            array_push($results, $arr_checked);

                            break;

                        }

                    }

                }

            }

        }



        $results = array_unique($results);

        foreach ($results as $result) {

            $all_post_by_phone = $CI->Mpost->get_post_by_contact_phone($result);

            foreach ($all_post_by_phone as $post_by_phone) {

                $name = "";

                $phone = "";

                $email = "";



                if (isset($post_by_phone['contact_name']) && !empty($post_by_phone['contact_name'])) {

                    $name = $post_by_phone['contact_name'];

                }

                if (isset($post_by_phone['contact']) && !empty($post_by_phone['contact'])) {

                    $phone = $post_by_phone['contact'];

                }

                if (isset($post_by_phone['email_contact']) && !empty($post_by_phone['email_contact'])) {

                    $email = $post_by_phone['email_contact'];

                }

                // insert bảng broker

                $data_insert = array(

                    'name' => $name,

                    'phone' => $phone,

                    'email' => $email,

                );



                echo "<pre>";

                print_r($data_insert);

                echo "</pre>";



                $insert = $CI->Mbrokers->insert_check_phone($data_insert, $phone);



                if ($insert > 0) {

                    $update_data = array(

                        'broker_id' => $insert,

                        'crawler_type' => 2,

                    );

                    $this->update_type_crawler($update_data, $post_by_phone['post_id']);

                }

            }

        }*/

    }



    // update trạng thái tin

    public function update_type_crawler($array, $post_id)

    {

        $CI =& get_instance();

        $CI->load->model('Mpost');

        $update = $CI->Mpost->update($post_id, $array);

        if ($update) {

            return true;

        } else {

            return false;

        }



    }



    // kiểm tra trong chuỗi cùng giá



    public function check_price_duplicate($string1, $string2)

    {

        $string1 = strip_tags($string1);

        $string2 = strip_tags($string2);

        $explode_string_1 = explode(" ", $string1);

        $explode_string_2 = explode(" ", $string2);

        $uni = array('triệu', 'Triệu', 'tỉ', 'Tỉ', 'Tỷ', 'tỷ', 'triệu.', 'Triệu.', 'tỉ.', 'Tỉ.', 'Tỷ.', 'tỷ.', 'triệu,', 'Triệu,', 'tỉ,', 'Tỉ,', 'Tỷ,', 'tỷ,');

        if (!empty($uni)) {

            foreach ($uni as $key => $item) {

                if (in_array($item, $explode_string_1) && in_array($item, $explode_string_2)) {

                    // nếu tồn tại các đơn vị tiền tệ trong chuỗi

                    $key_1 = array_search($item, $explode_string_1);

                    $key_2 = array_search($item, $explode_string_2);

                    if ($explode_string_1[$key_1 - 1] == $explode_string_2[$key_2 - 1]) {

                        return true;

                    } else {

                        return false;

                    }

                }

            }

        }

    }



    // kiểm tra độ giống nhau của 2 đoạn string

    public function check_string_duplicate($string1, $string2)

    {

        $string1 = strip_tags($string1);

        $string2 = strip_tags($string2);

        $explode_string_1 = explode(" ", $string1);

        $explode_string_2 = explode(" ", $string2);

        $max_count = count($explode_string_1);

        if (count($explode_string_2) > count($explode_string_1)) {

            $max_count = count($explode_string_2);

        }

        $count = 0;

        for ($i = 0; $i < count($explode_string_1); $i++) {

            for ($j = 0; $j < count($explode_string_2); $j++) {



                if (trim($explode_string_1[$i]) == trim($explode_string_2[$j])) {

                    $count++;

                }

            }

        }

        return $count / $max_count;

    }

    // update trạng thái tin

    // public function update_type_crawler($post_id){

    //     $CI     =& get_instance();

    //     $CI->load->model('Mpost');

    //     $arr = array(

    //         'crawler_type' => 2,

    //     );

    //     $update = $CI->Mpost->update($post_id,$arr);

    //     if($update){

    //         return true;

    //     }else{

    //         return false;

    //     }



    // }



    // reset

    public function resset()

    {



        $CI =& get_instance();

        $CI->load->model('Mpost');

        $all_posts = $CI->Mpost->get_all_check_post();

        $arr = array(

            'crawler_type' => 1,

            'broker_id' => 0,

        );

        foreach ($all_posts as $value) {

            $this->update_type_crawler($arr, $value['post_id']);

        }



    }



    public function statistical()

    {

        $CI =& get_instance();

        $CI->load->model('Mpost');



        $mg_post = $CI->Mpost->get_value_check_post();



        $all_posts = $CI->Mpost->get_all_check_post();



        echo '<pre>';

        print_r(count($mg_post));

        echo '</pre>';



        echo '<pre>';

        print_r(count($all_posts));

        echo '</pre>';





    }



    // check cronjob in table posts

    public function master_check_posts()

    {

        $CI =& get_instance();

        $CI->load->model('Mpost');

        $CI->load->model('Mbrokers');

        //check phone xuat hien 3 lan cung area

        echo '###Check phone 3 lan --------------------------------' . PHP_EOL;

        $this->check_phone_area();

        
        // die();

        //update tat ca ve moi gioi khi có sdt là moi gioi

        echo '###Update trong moi gioi------------------------------' . PHP_EOL;

        $CI->Mpost->updateTypeBrokerByPhone();

        //check keyword in content

        echo '###Check keyword------------------------------' . PHP_EOL;

        $all_posts = $CI->Mpost->get_value_check_post();

        // echo '<pre>';
        //     print_r("aaaâ");
        //     echo '</pre>';
        
        

        if (isset($all_posts) && !empty($all_posts)) {
            $listPhones = array();

            $listBrokers = $CI->Mbrokers->get_all_brokers();

            foreach($listBrokers as $b){

                $phone = trim(str_replace('.', '', $b['phone']));

                if(!in_array($phone, $listPhones)) $listPhones[] = $phone;

            }

            // echo '<pre>';
            // print_r("jhelô");
            // echo '</pre>';
            // die();

            foreach ($all_posts as $key => $post) {

                

                // if($key < 2){
                //$this->update_type_crawler($arr_stt, $post['post_id']);

                $check_content = $this->check_black_key($post['post_content'], $listPhones);

                $check_title = $check_content ? true : $this->check_black_key($post['post_title'], $listPhones);
               
                //$check_phone_broker = $check_title ? true : $this->check_phone_in_broker($post['contact']);

                if ($check_content || $check_title) {

                    $broker_id = $CI->Mbrokers->insert_check_phone(array(

                        'name' => $post['contact_name'],

                        'phone' => $post['contact'],

                        'email' => $post['email_contact']

                    ), $post['contact']);

                    $this->update_type_crawler(array(

                        'broker_id' => $broker_id,

                        'crawler_type' => 2,

                        'status_crawler' => 1,//long

                    ), $post['post_id']);

                    echo $post['contact'] . '-' . $post['post_id'] . '-' . $broker_id . PHP_EOL;

                }

                else {//long

                    $arr = array(

                        'status_crawler' => 1,

                    );

                    $this->update_type_crawler($arr, $post['post_id']);

                    echo '<pre>';
                    print_r($post['post_id']);
                    echo '</pre>';

                }

            // }

            }

        }

        /*$arr_mg = array(

            'crawler_type' => 2,

        );

        $arr_cc = array(

            'crawler_type' => 1,

        );

        $arr_stt = array(

            'status_crawler' => 1,

        );*/



        ///-------------



        /*$all_posts = $CI->Mpost->get_value_check_post();

        for ($i = 0; $i < count($all_posts); $i++) {

            $count_duplicate = 0;

            for ($k = $i + 1; $k < count($all_posts); $k++) {

                // echo "trùng ở vị trí".$i."và".$k."<br>";

                if ($this->check_string_duplicate($all_posts[$i]['post_title'], $all_posts[$k]['post_title']) > 0.6 && $this->check_price_duplicate($all_posts[$i]['post_title'], $all_posts[$k]['post_title'])) {



                    $count_duplicate++;



                    // if($count_duplicate > 2){



                    // }



                    $update_1 = $this->update_type_crawler($arr_mg, $all_posts[$i]['post_id']);

                    $update_2 = $this->update_type_crawler($arr_mg, $all_posts[$k]['post_id']);



                    $name_1 = "";

                    $phone_1 = "";

                    $email_1 = "";



                    if (isset($all_posts[$i]['contact_name']) && !empty($all_posts[$i]['contact_name'])) {

                        $name_1 = $all_posts[$i]['contact_name'];

                    }

                    if (isset($all_posts[$i]['contact']) && !empty($all_posts[$i]['contact'])) {

                        $phone_1 = $all_posts[$i]['contact'];

                    }

                    if (isset($all_posts[$i]['email_contact']) && !empty($all_posts[$i]['email_contact'])) {

                        $email_1 = $all_posts[$i]['email_contact'];

                    }





                    $name_2 = "";

                    $phone_2 = "";

                    $email_2 = "";



                    if (isset($all_posts[$k]['contact_name']) && !empty($all_posts[$k]['contact_name'])) {

                        $name_2 = $all_posts[$k]['contact_name'];

                    }

                    if (isset($all_posts[$k]['contact']) && !empty($all_posts[$k]['contact'])) {

                        $phone_2 = $all_posts[$k]['contact'];

                    }

                    if (isset($all_posts[$k]['email_contact']) && !empty($all_posts[$k]['email_contact'])) {

                        $email_2 = $all_posts[$k]['email_contact'];

                    }

                    // insert bảng broker

                    $data_insert = array(

                        'name' => $name_1,

                        'phone' => $phone_1,

                        'email' => $email_1,

                    );

                    $insert = $CI->Mbrokers->insert_check_phone($data_insert, $phone_1);

                    if ($insert > 0) {



                        $update_data = array(

                            'broker_id' => $insert,

                        );

                        $this->update_type_crawler($update_data, $all_posts[$i]['post_id']);

                    }



                    $data_insert = array(

                        'name' => $name_2,

                        'phone' => $phone_2,

                        'email' => $email_2,

                    );

                    $insert = $CI->Mbrokers->insert_check_phone($data_insert, $phone_2);

                    if ($insert > 0) {



                        $update_data = array(

                            'broker_id' => $insert,

                        );

                        $this->update_type_crawler($update_data, $all_posts[$k]['post_id']);

                    }

                    // end  /



                    echo '<pre>';

                    print_r($all_posts[$i]['post_title']);

                    echo '</pre>';



                    if ($update_1) {

                        echo '<pre>';

                        print_r("done..");

                        echo '</pre>';

                    }



                    echo '<pre>';

                    print_r($all_posts[$k]['post_title']);

                    echo '</pre>';



                    if ($update_2) {

                        echo '<pre>';

                        print_r("done..");

                        echo '</pre>';

                    }



                    echo '<pre>';

                    print_r("----------------------");

                    echo '</pre>';



                }



                // if(isset($all_posts[$i]['contact'])&&!empty($all_posts[$i]['contact']) && isset($all_posts[$k]['contact']) && !empty($all_posts[$k]['contact']) ){



                //     if(trim($all_posts[$i]['contact']) == trim($all_posts[$k]['contact'])&& $all_posts[$i]['contact']!=" "&&$all_posts[$k]['contact'] != " "  ){



                //         $count_duplicate ++;

                //         break;

                //         // if($count_duplicate > 3){

                //         //     echo "số đt : " . $all_posts[$i]['contact'];

                //         //     $post_by_phones = $CI->Mpost->get_post_by_contact_phone($all_posts[$i]['contact']);



                //         //     if(isset($post_by_phones) && !empty($post_by_phones)){

                //         //         foreach ($post_by_phones as $key => $post) {

                //         //             $update_2 = $this->update_type_crawler($arr_mg,$post['post_id']);

                //         //             // insert bảng broker

                //         //             $data_insert = array(

                //         //                 'name' => $post['contact_name'],

                //         //                 'phone' => $post['contact'],

                //         //                 'email' => $post['email_contact'],

                //         //             );

                //         //             $insert = $CI->Mbrokers->insert($data_insert);



                //         //             if($insert > 0){

                //         //                 $update_data = array(

                //         //                     'broker_id' => $insert,

                //         //                 );

                //         //                 $this->update_type_crawler($update_data,$post['post_id']);

                //         //             }

                //         //         }

                //         //     }

                //         // }





                //     }

                // }





            }

        }*/





    }



//        if (isset($all_post_check_phone) && !empty($all_post_check_phone)) {

//            // has 3

//            foreach ($all_post_check_phone as $key => $post_phone) {

//

//                $update = $this->update_type_crawler($arr_mg, $post_phone['post_id']);

//                // echo '<pre>';

//                // print_r($post_phone);

//                // echo '</pre>';

//                $name_1 = "";

//                $phone_1 = "";

//                $email_1 = "";

//

//                if (isset($post_phone['contact_name']) && !empty($post_phone['contact_name'])) {

//                    $name_1 = $post_phone['contact_name'];

//                }

//                if (isset($post_phone['contact']) && !empty($post_phone['contact'])) {

//                    $phone_1 = $post_phone['contact'];

//                }

//                if (isset($post_phone['email_contact']) && !empty($post_phone['email_contact'])) {

//                    $email_1 = $post_phone['email_contact'];

//                }

//                // insert bảng broker

//                $data_insert = array(

//                    'name' => $name_1,

//                    'phone' => $phone_1,

//                    'email' => $email_1,

//                );

//                $insert = $CI->Mbrokers->insert_check_phone($data_insert, $phone_1);

//                echo '<pre>';

//                print_r($insert);

//                echo '</pre>';

//                if ($insert > 0) {

//

//                    $update_data = array(

//                        'broker_id' => $insert,

//                    );

//                    $this->update_type_crawler($update_data, $post_phone['post_id']);

//                }

//

//            }

//        }



}



?>