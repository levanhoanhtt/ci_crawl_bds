<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class ResizeImage
    {
        // protected $CI;
    
        public function __construct()
        {
            // this->CI =& get_instance();
            
            // $this->load->library('Imageinter');
            
        }

        public function compress( $source , $quantity ){
            //        get info image
            $path_parts = pathinfo($source);
            $name_image = $path_parts['filename'];
            $base_file_name = $path_parts['basename'];
            $name_image = $name_image . ".jpg";
            //      replace name
            $new_source = str_replace($base_file_name , $name_image , $source );
            try {
                // $source = $this->change_format_to_jpg( $source , $new_source);
                $img_info = getimagesize($source);
                $image = null;
                $ext = $img_info['mime'];
                switch ($ext) {
                    case 'image/jpg':
                    case 'image/jpeg':
                        // create a jpeg extension
                        $image = imagecreatefromjpeg($source);
                        if (imagetypes() & IMG_JPG) {
                            imagejpeg($image, $source, $quantity);
                        }
                        break;
                    // Image is a GIF
                    case 'image/gif':
                        $image = @imagecreatefromgif($source);
                        if (imagetypes() & IMG_GIF) {
                            imagegif($image, $source);
                        }
                        break;
                    // Image is a PNG
                    case 'image/png':
                        $image = @imagecreatefrompng($source);
                        $invertScaleQuality = 9 - round(($quantity/100) * 9);
                            // Check PHP supports this file type
                        if (imagetypes() & IMG_PNG) {
                            imagepng($image, $source, $invertScaleQuality);
                        }
                        break;
                    // Mime type not found
                    default:
                        throw new Exception("File is not an image, please use another file type.", 1);
                }
    
            } catch ( Exception $ex ) {
                echo "ERR: " . $ex->getMessage();
            }
        }
    
        public function change_format_to_jpg($source, $path){
            if( file_exists( $source ) ){
                // Khởi tạo ảnh blank 
                $path_parts = pathinfo($source);
                // $path_parts_624_476 = $path_parts['dirname']."/624x476"."/".$path_parts['basename'];
                imagejpeg(imagecreatefromstring(file_get_contents($source)) ,$path);
                // imagejpeg(imagecreatefromstring(file_get_contents($source)) ,$path_parts_624_476);
                // delete old file
                // $path_parts_170_115 = $path_parts['dirname']."170x115" ;
                

                if($path_parts['extension'] != "jpg" && $path_parts['extension'] != "jpeg" ){
                    unlink($source);
                }
                return $path;
            }else{
                throw new Exception("File not exits");
            }
        }

        public function HeightByWidth( $old_w , $old_h , $new_w ){
            $new_h = ( $old_h / $old_w ) * $new_w ;
            return $new_h;
        }

        public function save_image( $type, $new_image , $save_path , $quantity ){
    
            switch ($type) {
                case 'image/jpg':
                case 'image/jpeg':
                    // Check PHP supports this file type
                    if (imagetypes() & IMG_JPG) {
                        imagejpeg($new_image, $save_path, $quantity);
                    }
                    break;
                case 'image/gif':
                    // Check PHP supports this file type
                    if (imagetypes() & IMG_GIF) {
                        imagegif($new_image, $save_path);
                    }
                    break;
                case 'image/png':
                    $invertScaleQuality = 9 - round(($quantity/100) * 9);
                    // Check PHP supports this file type
                    if (imagetypes() & IMG_PNG) {
                        imagepng($new_image, $save_path, $invertScaleQuality);
                    }
                    break;
            }

            
        }

        public function resize($max_width , $source){
            // get info image 
    
            $ext = null;
            $image = null;
            $newImage = null;
            $origWidth = null;
            $origHeight = null;
            $resizeWidth = null;
            $resizeHeight = null;
            $file_create = null;
            $image_info = getimagesize($source);

            $width = $image_info[0];
            $height = $image_info[1];
            $type = $image_info['mime'];
    
            // $this->HeightByWidth( $old_w , $old_h , $new_w ){
            //     $new_h = ( $old_h / $old_w ) * $new_w ;
            //     return $new_h;
            // }
    
            // function save_image( $type, $new_image , $save_path , $quantity ){
    
            //     switch ($type) {
            //         case 'image/jpg':
            //         case 'image/jpeg':
            //             // Check PHP supports this file type
            //             if (imagetypes() & IMG_JPG) {
            //                 imagejpeg($new_image, $save_path, $quantity);
            //             }
            //             break;
            //         case 'image/gif':
            //             // Check PHP supports this file type
            //             if (imagetypes() & IMG_GIF) {
            //                 imagegif($new_image, $save_path);
            //             }
            //             break;
            //         case 'image/png':
            //             $invertScaleQuality = 9 - round(($quantity/100) * 9);
            //             // Check PHP supports this file type
            //             if (imagetypes() & IMG_PNG) {
            //                 imagepng($new_image, $save_path, $invertScaleQuality);
            //             }
            //             break;
            //     }
    
                
            // }
    
            //  nén ảnh 
    
            
            if($width > $max_width){
                // prosesss...
                // check type image
                switch ($type) {
                    case 'image/jpg':
                    case 'image/jpeg':
                        // create a jpeg extension
                        $image = imagecreatefromjpeg($source);
                        break;
                    // Image is a GIF
                    case 'image/gif':
                        $image = @imagecreatefromjpeg($source);
                        break;
                    // Image is a PNG
                    case 'image/png':
                        $image = @imagecreatefromjpeg($source);
                        break;
                    // Mime type not found
                    default:
                        throw new Exception("File is not an image, please use another file type.", 1);
                }
                $origHeight = imagesy($image);
                $origWidth = imagesx($image);
    
                $resizeWidth = $max_width;
    
                $resizeHeight = $this->HeightByWidth( $origWidth , $origHeight , $resizeWidth );
    
                $newImage = imagecreatetruecolor($resizeWidth , $resizeHeight);
    
                imagecopyresampled($newImage, $image, 0, 0, 0, 0, $resizeWidth, $resizeHeight, $origWidth, $origHeight);
    
                $this->save_image($type, $newImage , $source , 100);
    

            }
    
        }

        public function resample($jpgFile, $thumbFile, $width) {

            if (function_exists('exif_read_data')) {
                // $exif = exif_read_data($filename);


                // if($exif && isset($exif['Orientation'])) {

                    $orientation = 0;
                    $exifData = exif_read_data($jpgFile);
                    foreach($exifData as $key => $val)
                    {
                        if(strtolower($key) == "orientation" )
                        {
                            $orientation = $val;
                            break;
                        }
                    }
                  
                //   $orientation = $exif['Orientation'];
                  if($orientation != 1){
                    $img = imagecreatefromjpeg($jpgFile);
                    $deg = 0;
                    switch ($orientation) {
                      case 3:
                        $deg = 180;
                        break;
                      case 6:
                        $deg = 270;
                        break;
                      case 8:
                        $deg = 90;
                        break;
                    }

                    if ($deg) {
                      $img = imagerotate($img, $deg, 0);        
                    }
                    // then rewrite the rotated image back to the disk as $filename 
                    imagejpeg($img, $jpgFile, 95);
                  } // if there is some rotation necessary

              } // if function exists  
        }

        public function run($source){
            list($width_orig, $height_orig) = getimagesize($source);

            // if($height_orig < $width_orig){
                $this->resample($source , $source , 1024);
            // }
            
            

            $this->resize(1024, $source);
            $this->compress($source , 70);
            
            // $this->correctImageOrientation($source);
        }


    }
    
    /* End of file LibraryName.php */
    
?>
