<?php
class Permalink
{
    public function get_page_link($id)
    {
        $CI =& get_instance();
        $CI->load->model('Mpage');
        $page_row = $CI->Mpage->get_page_by('page_id', $id);
        $slug = '';
        $page_link = base_url('thong-tin');
        if ($page_row)
        {
            $slug = '-'.$page_row['page_slug'];
        }

        $page_link = $page_link.'/'.$id.$slug;

        return $page_link;
    }

    public function get_category_link($id)
    {
        $CI =& get_instance();
        $CI->load->model('Mcategory');
        $category_row = $CI->Mcategory->ad_get_cate_by('cate_id', $id);
        $slug = '';
        $category_link = base_url('danh-muc');
        if ($category_row)
        {
            $slug = '-'.$category_row['cate_slug'];
        }

        $category_link = $category_link.'/'.$id.$slug;

        return $category_link;

    }
}
?>