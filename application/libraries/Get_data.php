<?php 
class Get_data{

    // Get menu
    public function main_menu(){
        $CI =& get_instance();
        $CI->load->model('Moptions');
        $menu = $CI->Moptions->get_option("menu");
        return $this->show_menu(json_decode($menu['opt_value']));
    }
    public function main_menu_logout()
    {
       $CI =& get_instance();
       $CI->load->model('Moptions');
       $menu = $CI->Moptions->get_option("menu");
       return $this->show_menu(json_decode($menu['opt_value_logout']));

   }

    // Get menu
   public function options_info($key){
    $CI =& get_instance();
    $CI->load->model('Moptions');
    $menu = $CI->Moptions->get_option($key);
    return json_decode($menu['opt_value']);
}
    
public function curl_post($url,$data)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
    // print_r($server_output);
    return $server_output;
}

    // Đệ quy menu
public function show_menu($arr_menu,$result='',$ul_class='main-menu'){
    if($arr_menu){
        $result.='<ul class="'.$ul_class.'">';
        foreach ($arr_menu as $key => $item) {
            $item = get_object_vars($item);
            $name = $item['name'];
            $link = $item['link'];
            $class_m = "";
            if(array_key_exists("children",$item)){
                $class_m = "menu-item-has-children";
            }
            $result.='<li class="'.$class_m.'">';
            $result.='<a href="'.$link.'">'.$name.'</a>';
            if(array_key_exists("children",$item)){
                $result=$this->show_menu($item["children"],$result,"sub-menu");
            } 
            $result.='</li>';
        }
        $result.='</ul>';
    }
    return $result;
}

public function get_province($province_id)
{
    $CI =& get_instance();
    $CI->load->model('Mprovince');
    $row = $CI->Mprovince->get_province($province_id);
    return $row['name'];
}

public function get_district($district_id)
{
    $CI =& get_instance();
    $CI->load->model('Mprovince');
    $row = $CI->Mprovince->get_district($district_id);
    return $row['name'];
}

public function get_category($id)
{
    $CI =& get_instance();
    $CI->load->model('Mcategory');
    $row = $CI->Mcategory->ad_get_cate_by('cate_id', $id);
    return $row['cate_name'];
}
public function get_user_name($id)
{
    $CI =& get_instance();
    $CI->load->model('Muser');
    $row = $CI->Muser->get_user_by('user_id',$id);
    return $row['fullname'];
}
public function get_amount_user($user_id)
{
    $CI =& get_instance();
    $CI->load->model('Muser');
    $row = $CI->Muser->get_user_by('user_id',$user_id);
    return $row['amount'];
}

public function get_category_child($parent_id)
{
    $CI =& get_instance();
    $CI->load->model('Mcategory');
    $row = $CI->Mcategory->ad_get_cate_child_active($parent_id);
    return $row;
}

    public function get_post_images($post_id)
    {
        $CI =& get_instance();
        $CI->load->model('Mpost');
        return $CI->Mpost->get_post_images($post_id);
    }

    public function get_post_detail($post_id)
    {
        $CI =& get_instance();
        $CI->load->model('Mpost');
        return $CI->Mpost->get_post_by('post_id', $post_id);
    }

    public function is_post_read($post_id)
    {
        $CI =& get_instance();
        $CI->load->model('Mpost');
        $user_id = $this->get_current_user();
        if ($CI->Mpost->get_post_read($user_id, $post_id)>0)
        {
            return true;
        }
        return false;
    }

    public function is_user_login()
    {
        $CI =& get_instance();
        $CI->load->model('Muser');

        $login = $CI->session->get_userdata();
        if(!empty($login['username']))
        {
            $info_user = $CI->Muser->get_user_info($login['username']);
            if ($info_user)
            {
                return true;
            }
        }

        return false;
    }

    public function get_current_user()
    {
        $CI =& get_instance();
        $CI->load->model('Muser');

        $login = $CI->session->get_userdata();
        if(!empty($login['username']))
        {
            $info_user = $CI->Muser->get_user_info($login['username']);
            if ($info_user)
            {
                return $info_user['user_id'];
            }
        }

        return 0;

    }
    public function get_province_id_by_name($province_name)
    {
        $CI  =& get_instance();
        $CI->load->model('Mpost');
        $row = $CI->Mpost->get_province_id_by_name(trim($province_name));
        return $row['matp'];
    }
    public function get_district_id_by_name($district_name)
    {
        $CI     =& get_instance();
        $CI->load->model('Mpost');
        $row    = $CI->Mpost->get_district_id_by_name(trim($district_name));
        return $row['maqh'];
    }

    public function set_activity_user()
    {
        $CI =& get_instance();
        $CI->load->model('Muser');
        if ($this->is_user_login())
        {
            $user_id = $this->get_current_user();
            $data_update = array('activity'=>date('Y-m-d H:i:s'));
            $CI->Muser->update($user_id, $data_update);
        }

        if ($CI->Muser->get_all_user())
        {
            foreach ($CI->Muser->get_all_user() as $item)
            {
                if (strtotime(date('Y-m-d H:i:s'))-strtotime($item['activity'])>600)
                {
                    $CI->Muser->del_user_log($item['username']);
                }
            }
        }
    }

    public function get_cate_by_crawl($name)
    {
        $CI =& get_instance();
        $CI->load->model('Mcategory');
        $all_categories = $CI->Mcategory->ad_get_all_cate();
        $cate_bds = array();
        $cate_muaban = array();
        $cate_alonhadat = array();
        $cate_dothi = array();
        $cate_123nhadatviet = array();
        $cate_tinbatdongsan = array();
        $cate_homedy = array();
        $cate_kenhbds = array();
        $cate_nhadat24h = array();
        $cate_langbatdongsan = array();
        $cate_mogi = array();
        $cate_nhadatinfo = array();
        $cate_sanbatdongsanviet = array();
        $cate_sotaynhadat = array();
        $cate_i_batdongsan = array();
        $cate_nhadathanoi24h = array();
        $cate_inhadat = array();
        $cate_bannhahn = array();
        $cate_muabannhadat = array();
        $cate_muabanchinhchu = array();
        $cate_sanbatdongsanhanoi = array();
        $cate_nhadatvideo = array();
        $cate_nhadathay = array();
        $cate_phonhadat = array();
        $cate_timmuanhadat = array();
        $cate_biggee = array();
        $cate_chotot = array();
        $cate_nhadat24hnet = array();

        $cate_timkiemnhadat = array();
        $cate_nhabanhanoi = array();

        $cate_nhadathanoiban = array();


        if ($all_categories)
        {
            foreach ($all_categories as $item)
            {
                $list_link_str = $item['crawl_link'];
                if (!empty($list_link_str))
                {
                    $list_link_arr = explode(PHP_EOL, $list_link_str);
                    if ($list_link_arr)
                    {
                        // echo "<pre>";
                        // print_r($list_link_arr);
                        foreach ($list_link_arr as $link)
                        {
                            if (preg_match('/batdongsan.com.vn/', $link))
                            {
                                $cate_bds[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/muaban.net/', $link))
                            {
                                $cate_muaban[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/alonhadat.com.vn/', $link))
                            {
                                $cate_alonhadat[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/dothi.net/', $link))
                            {
                                $cate_dothi[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/123nhadatviet.com/', $link))
                            {
                                $cate_123nhadatviet[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/tinbatdongsan.com/', $link))
                            {
                                $cate_tinbatdongsan[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/homedy.com/', $link))
                            {
                                $cate_homedy[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            // if (preg_match('/kenhbds.vn/', $link))
                            // {
                            //     $cate_kenhbds[(string)trim($link)] = (string)$item['cate_id'];
                            // }
                            if (preg_match('/langbatdongsan.vn/', $link))
                            {
                                $cate_langbatdongsan[(string)trim($link)] = (string)$item['cate_id'];
                            }

                           // phúc nhadat24h.com
                            if (preg_match('/nhadat24h.com/', $link))
                            {
                                $cate_nhadat24h[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            // phúc kenhbatdongsan
                            if (preg_match('/kenhbds.vn/', $link))
                            {
                                $cate_kenhbds[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            //ninh
                            if (preg_match('/mogi.vn/', $link))
                            {
                                $cate_mogi[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/nhadatinfo.com/', $link))
                            {
                                $cate_nhadatinfo[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/sanbatdongsanviet.com.vn/', $link))
                            {
                                $cate_sanbatdongsanviet[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/sotaynhadat.vn/', $link))
                            {
                                $cate_sotaynhadat[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            // push 2

                            // phúc i_batdongsan
                            if (preg_match('/i-batdongsan.com/', $link))
                            {
                                $cate_i_batdongsan[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            // phúc nhadathanoi24h

                            if (preg_match('/nhadathanoi24h.vn/', $link))
                            {
                                $cate_nhadathanoi24h[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/i-nhadat.com/', $link))
                            {
                                $cate_inhadat[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/www.muabannhadat.vn/', $link))
                            {
                                $cate_muabannhadat[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/bannhahn.com/', $link))
                            {
                                $cate_bannhahn[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/muabanchinhchu.net/', $link))
                            {
                                $cate_muabanchinhchu[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/sanbatdongsanhanoi.info.vn/', $link))
                            {
                                $cate_sanbatdongsanhanoi[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/nhadatvideo.vn/', $link))
                            {
                                $cate_nhadatvideo[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/nhadathay.com/', $link))
                            {
                                $cate_nhadathay[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/phonhadat.net/', $link))
                            {
                                $cate_phonhadat[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/timmuanhadat.com.vn/', $link))
                            {
                                $cate_timmuanhadat[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/biggee.vn/', $link))
                            {
                                $cate_biggee[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/nha.chotot.com/', $link))
                            {
                                $cate_chotot[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/nhadat24h.net/', $link))
                            {
                                $cate_nhadat24hnet[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/nhabanhanoi.net/', $link))
                            {
                                $cate_nhabanhanoi[(string)trim($link)] = (string)$item['cate_id'];
                            }
                            if (preg_match('/timkiemnhadat.vn/', $link))
                            {
                                $cate_timkiemnhadat[(string)trim($link)] = (string)$item['cate_id'];
                            }

                            if (preg_match('/nhadathanoiban.com/', $link))
                            {
                                $cate_nhadathanoiban[(string)trim($link)] = (string)$item['cate_id'];
                            }                           

                        }
                        // echo "<pre>";
                        // print_r($cate_homedy);
                    }
                }
            }
        }

        if ($name=='batdongsan')
        {
            return $cate_bds;
        }

        if ($name=='muaban')
        {
            return $cate_muaban;
        }

        if ($name=='dothi')
        {
            return $cate_dothi;
        }

        if ($name=='alonhadat')
        {
            return $cate_alonhadat;
        }

        if ($name=='123nhadatviet')
        {
            return $cate_123nhadatviet;
        }

        if ($name=='tinbatdongsan')
        {
            return $cate_tinbatdongsan;
        }
        
        
        if ($name=='langbatdongsan')
        {
            return $cate_langbatdongsan;
        }
        if ($name=='nhadat24h')
        {
            return $cate_nhadat24h;
        }

        if($name=='kenhbds')
        {
            return $cate_kenhbds;
        }

        if($name=='mogi')
        {
            return $cate_mogi;
        }

        if($name=='nhadatinfo')
        {
            return $cate_nhadatinfo;
        }

        if($name=='sanbatdongsanviet')
        {
            return $cate_sanbatdongsanviet;
        }

        if($name=='sotaynhadat')
        {
            return $cate_sotaynhadat;
        }

        //push2
        if($name=='i_batdongsan'){

            return $cate_i_batdongsan;
        }
        if($name=='nhadathanoi24h'){
            return $cate_nhadathanoi24h;
        }
        //
        if($name=='i-nhadat'){
            return $cate_inhadat;
        }
        if($name=='muabannhadat'){
            return $cate_muabannhadat;
        }
        if($name=='bannhahn'){
            return $cate_bannhahn;
        }
        if($name=='sanbatdongsanhanoi'){
            return $cate_sanbatdongsanhanoi;
        }
        if($name=='homedy'){
            return $cate_homedy;
        }
        if($name=='nhadatvideo'){
            return $cate_nhadatvideo;
        }
        if($name=='nhadathay'){
            return $cate_nhadathay;
        }
        if($name=='phonhadat'){
            return $cate_phonhadat;
        }
        if($name=='timmuanhadat'){
            return $cate_timmuanhadat;
        }
        if($name=='biggee'){
            return $cate_biggee;
        }
        if($name=='chotot'){
            return $cate_chotot;
        }
        if($name=='nhadat24hnet'){
            return $cate_nhadat24hnet;
        }

        if($name=='timkiemnhadat'){
            return $cate_timkiemnhadat;
        }

        if($name=='nhabanhanoi'){
            return $cate_nhabanhanoi;
        }

        if($name=='muabanchinhchu'){
            return $cate_muabanchinhchu;
        }

        if ($name=='nhadathanoiban')
        {
            return $cate_nhadathanoiban;
        }


        $cate_i_batdongsan = array();
        $cate_nhadathanoi24h = array();
        

    }

    public function get_price_string($number)
    {
        $str = '';

        if ($number>=1000000 && $number<1000000000)
        {
            $str = ($number/1000000). ' triệu';
        }
        elseif ($number>=1000000000)
        {
            $str = ($number/1000000000). ' tỷ';
        }
        else
        {
            //$str = number_format($number, 2);
            $str = $number;
        }

        return $str;

    }

    public function get_province_id_by_district_id($district_id)
    {
        $CI     =& get_instance();
        $CI->load->model('Mpost');
        $row    = $CI->Mpost->get_province_id_by_district_id($district_id);
        return $row['matp'];
    }

    
    

}