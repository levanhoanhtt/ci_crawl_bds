<?php
Class Crawler
{
	public function __construct() {
		require_once APPPATH.'/libraries/simple_html_dom.php';
	}

	public function get_term_nhadathanoi24h($link_cate)
	{
		$html = $this->get_dom_single_page($link_cate);

		preg_match_all('/view-dom-id-(.*?)">/', $html, $matches);
		preg_match_all('/term\/(.*?)"/', $html, $matches1);
		


		return ['term_id'=>$matches1[1][0],'dom_id'=>$matches[1][1]];
	}

	public function get_dom_single_page($url)
	{


		$user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

		$options = array(

			CURLOPT_CUSTOMREQUEST => "GET",        //set request type post or get
			CURLOPT_POST => false,        //set to GET
			CURLOPT_USERAGENT => $user_agent, //set user agent
			CURLOPT_COOKIEFILE => "cookie.txt", //set cookie file
			CURLOPT_COOKIEJAR => "cookie.txt", //set cookie jar
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING => "",       // handle all encodings
			CURLOPT_AUTOREFERER => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT => 120,      // timeout on response
			CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => false
		);

		$ch = curl_init($url);
		curl_setopt_array($ch, $options);
		$content = curl_exec($ch);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);
		curl_close($ch);
		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['content'] = $content;
		return $header['content'];
	}

	public function get_list_url($url, $elemment, $domain = NUll,$count = NULL)
	{


		$html = $this->get_dom_single_page($url);

		$html = str_get_html($html);

		$get = $elemment . ' a';

		$url_arr = array();
		if($count == NUll)
		{

			foreach ($html->find($get) as $key => $dom) {

				$url = $domain . $dom->href;

				$url_arr[] = $url;

			}
		}

		else
		{
			$i = 0;
			foreach ($html->find($get) as $key => $dom) {
				$i++;
				if($i % 3 != 1)
				{
					continue;

				}
				$url = $domain . $dom->href;

				$url_arr[] = $url;

			}
		}

		

		return $url_arr;
	}

	public function get_data($html, array $data_array, $domain = NUll)
	{

		$str_html = str_get_html($html);

		$result = array();

		foreach ($data_array as $index => $elemment) {

			$data = json_decode($elemment);
			
			$attr = $data->element;

			$count = $data->count;

			if (!isset($data->convert) || empty($data->convert)) {
				if (!isset($data->loop) || empty($data->loop)) {
					//$main = $str_html->find('div.pm-title',0);
					// echo $main;
					$value = $str_html->find($attr, $count);
					// $value = str;
					$res = (isset($value->plaintext) || $value->plaintext != '' ) ? $value->plaintext : '';
					$result[$index] = trim($res);

					// if($index == "desc")
					// 	$result[$index] = trim($value->outertext);
					
				} else {
					$gallery_arr = array();

					foreach ($str_html->find($attr) as $key => $value) {
						$gallery_arr[$key] = $domain . $value->src;
					}

					$result[$index] = $gallery_arr;

					$result[$index] = array_unique($result[$index]);

				}

				// isset loop

			} else {

				if (!isset($data->loop) || empty($data->loop)) {
					// unset loop data ..
					$value = $str_html->find($attr, $count);
					// // echo  $str_html;
					$value = html_entity_decode($value);
					$result[$index] = $value;
				} else {
					//isset loop data and data-covert

					$loop_arr = array();
					$str_loop = $str_html->find($attr, $count);


					foreach ($str_loop->find($data->loop) as $key => $value) {

						$value = html_entity_decode($value);
						$loop_arr[$key] = $value;


					}

					$result[$index] = $loop_arr;


				}


			}

			// custom alo nha dat
			if (isset($data->loop_alonhadat) && !empty($data->loop_alonhadat)) {

				$loop_arr = array();
				$str_loop = $str_html->find($data->loop_alonhadat, $count);
				$main_content = $str_loop->find($attr);

				for ($i = 0; $i < count($main_content); $i++) {

					if ($i % 2 == 0) {
						$key = $main_content[$i]->plaintext;
						$value = html_entity_decode($main_content[$i + 1]);
						$loop_arr[$key] = trim($value);
					}
				}

				$result[$index] = $loop_arr;
			}

			// custom gallery muaban.net

			if (isset($data->loop_muaban) && !empty($data->loop_muaban)) {

				// $loop_arr = array();
				// $str_loop = $str_html->find($data->loop_muaban,$count);
				// $main_content = $str_loop->find($attr);

				$gallery_arr = array();

				foreach ($str_html->find($attr) as $key => $value) {
					if ($key == 0) {
						$gallery_arr[$key] = $domain . $value->src;
					} else {
						// $gallery_arr[$key] = $domain.$value->'data-src';
						$gallery_arr[$key] = $domain . $value->attr['data-src'];
					}
				}

				$result[$index] = $gallery_arr;
			}

			if (isset($data->loop_muabancc) && !empty($data->loop_muabancc)) {

				

				$desc1 = "";

				foreach ($str_html->find($attr) as $key => $value) {
					$desc1 .= $value->outertext;
					// if($value == "" || $value == null)
					// {
					// 	str_replace($value[$key-1], '', $desc1);
					// 	$desc1 .= "<br>Ninh</br>";
					// }
					


					// if ($key == 0) {
					// 	$gallery_arr[$key] = $domain . $value->src;
					// } else {
					// 	// $gallery_arr[$key] = $domain.$value->'data-src';
					// 	$gallery_arr[$key] = $domain . $value->attr['data-src'];
					// }
				}

				$result[$index] = $desc1;
			}

			// custom gallery nhadat24h

			if (isset($data->loop_nhadat24h_gallery) && !empty($data->loop_nhadat24h_gallery)) {

				$gallery_arr = array();

				foreach ($str_html->find($attr) as $key => $value) {

					$gallery_arr[$key] = $domain . $value->href;


				}

				$result[$index] = $gallery_arr;
			}

			//custom table info nhadat24h.com
			if (isset($data->loop_nhadat24h_tbs) && !empty($data->loop_nhadat24h_tbs)) {

				$tbs_info = array();

				foreach ($str_html->find($attr) as $key => $value) {

					// $gallery_arr[$key] = $domain.$value->href;
					$node = $value->find('span', 0);
					if(!empty($node))
					{
						$node = $node->plaintext;

						$node_index = $value->find('text', 1)->plaintext;

						// echo $node;

						$tbs_info[$node] = $node_index;
					}


				}
				$result[$index] = $tbs_info;
			}




			if (isset($data->loop_bannhahn_tbs) && !empty($data->loop_bannhahn_tbs)) {

				$tbs_info = array();

				foreach ($str_html->find($attr) as $key => $value) {

					// $gallery_arr[$key] = $domain.$value->href;
					$node = $value->find('span', 0);
					if(!empty($node))
					{
						$node = $node->plaintext;

						$node_index = $value->find('text', 1)->plaintext;



						// echo $node;

						$tbs_info[$node] = $node_index;
					}

				}
				$result[$index] = $tbs_info;
			}

			//map lat

			if (isset($data->map_) && !empty($data->map_)) {

				$lat = $data->map_;

				if (isset($lat) && !empty($lat)) {

					$map_lat = $str_html->find($attr, 0);


					$map_lat = $map_lat->attr['value'];
					$map_lat = html_entity_decode($map_lat);

					$result[$index] = $map_lat;

					// echo $map_lat;
				}


			}

			//custom i - nhadat

			if (isset($data->loop_i_nha_dat) && !empty($data->loop_i_nha_dat)) {

				$table_arr = array();
				$main_arr = $str_html->find($attr);
				foreach ($str_html->find($attr) as $key => $value) {

					if ($key % 2 == 0) {
						$arr_index = strip_tags($main_arr[$key]);
						if($key + 1 < count($str_html->find($attr)))
						{
							$table_arr[$arr_index] = strip_tags($main_arr[$key + 1]);
						}
					}
				}

				$result[$index] = $table_arr;
			}




			// findtext


			if (isset($data->findtext) && !empty($data->findtext)) {

				$value = $str_html->find($attr, $count);

				$find_text_value = $value->find('text', $data->findtext);

				$find_text_value = $find_text_value->plaintext;

				$find_text_value = trim($find_text_value);

				// echo $value;
				// $value = str;
				// echo $data->findtext;
				// $value = $value->find('text',$data->findtext);
				// $result[$index] = trim($value->plaintext);

				$result[$index] = $find_text_value;
			}

			//custom table info timmuanhadat
			if (isset($data->loop_timmuanhadat_tbs) && !empty($data->loop_timmuanhadat_tbs)) {

				$tbs_info = array();

				foreach ($str_html->find($attr) as $key => $value) {

					// $gallery_arr[$key] = $domain.$value->href;
					$node = $value->find('td', 0);
					$node = $node->plaintext;

					$node_index = $value->find('text', 1)->plaintext;

					// echo $node;

					$tbs_info[$node] = $node_index;


				}
				$result[$index] = $tbs_info;
			}

			// custom title mogi
			if(isset($data->title_mogi)&&!empty($data->title_mogi)&&!isset($data->element)) {
				$title_mogi = $data->title_mogi;
				$count = $data->count;
				// unset loop data ..
				$value = $str_html->find($title_mogi, $count);
//            $value = html_entity_decode($value);
				$result[$index] = $value->attr['content'];
			}
		}
		// custom homedy
		if(isset($data->loop_homedy)&&!empty($data->loop_homedy)) {
			$gallery_arr = array();



			foreach ($str_html->find($attr) as $key => $value) {
				//$gallery_arr[$key] = $value->attr['data-original'];
				echo $value;
			}

			$result[$index] = $gallery_arr;

//        $result[$index] = array_unique($result[$index]);
		}

		// custom timkiemnhadat
		if(isset($data->loop_timkiemnhadat_tbs)&&!empty($data->loop_timkiemnhadat_tbs)) {
			$tbs_info = array();
			foreach ($str_html->find($attr,0) as $key => $value) {
				// $gallery_arr[$key] = $domain.$value->href;
				$node = $value->find('.para', 0);
//            $node = $node->plaintext;
				$node = html_entity_decode($node);
				$node_index = $value->find('text', 0)->plaintext;
				// echo $node;
				$tbs_info[trim($node_index)] = trim($node);
			}
			$result[$index] = $tbs_info;
		}



		// custom contact
		if(isset($data->loop_timkiemnhadat_contact)&&!empty($data->loop_timkiemnhadat_contact)) {
			$tbs_info = array();
			foreach ($str_html->find($attr) as $key => $value) {
//            echo $value;
				// $gallery_arr[$key] = $domain.$value->href;
				$node = $value->find('span', 0);
//            $node = $node->plaintext;
				$node = html_entity_decode($node);
				$node_index = $value->find('text', 0)->plaintext;
				// echo $node;
				$tbs_info[trim($node_index)] = trim($node);
			}
			$result[$index] = $tbs_info;
		}

		if(isset($data->loop_kenhbatdongsan)&&!empty($data->loop_kenhbatdongsan)) {
			$tbs_info = array();
			foreach ($str_html->find($attr) as $key => $value) {
//            echo $value;
				// $gallery_arr[$key] = $domain.$value->href;
				$node = $value->find('text', 1);
//            $node = $node->plaintext;
				$node = html_entity_decode($node);
				$node_index = $value->find('span', 0)->plaintext;
				// echo $node;
				$tbs_info[trim($node_index)] = trim($node);
			}
			$result[$index] = $tbs_info;
		}

		if(isset($data->get_data_nhadatvideo)){

			$str_html = $this->get_dom_single_page("https://nhadatvideo.vn/");
			$str_html = str_get_html($str_html);
			$content = $str_html->find('script[type=text/javascript]',0);
			$content = $content->find('text',0);
			$re_math = preg_match('/my_first_page =(.*)/',$content,$matchs);
			$main_content = $matchs[1];
			$main_content = json_decode($main_content,true);
			$main_content = $main_content['first_page'];
			$result[$index] = $main_content;

		}


		// echo "<pre>";
		// print_r($result);
		// echo "</pre>";

		return $result;


	}

	public function get_data_biggee($url, $domain = null)
    {

        $html = $this->get_dom_single_page($url);

        $html = str_get_html($html);

        $result_arr = array();


        foreach ($html->find('.col-md-9') as $key => $value) {
            foreach ($value->find('.row') as $num => $result) {
            	if($num == 0) continue;
            	if($num == 9) break;

                foreach ($result->find('.col-md-3 a') as $cou => $dom) {
                    $url_sg = $domain . $dom->href;
                    //$url_arr[] = $url;
                }
                $attr = array();
                foreach ($result->find('.list-inline') as $loop => $list) {
                    if ($loop == 0) {
                        foreach ($list->find('li') as $kl => $val) {
                            if ($kl == 0) {
                                $attr['price'] = preg_replace('/[,| đ]/', '', trim($val->plaintext));
                            }
                            if ($kl == 2) {
                                $attr['square'] = str_replace(' m2', '', trim($val->plaintext));
                            }
                        }
                    }

                    if ($loop == 1) {
                        foreach ($list->find('li') as $kl => $val) {
                        	if($kl == 0)
                        	{
                        		$attr['name'] = trim($val->plaintext);
                        	}
                            if ($kl == 1) {
                                $attr['contact'] = trim($val->plaintext);
                            }
                        }
                    }
                }
                //$ti = $result->find('.col-md-8 h4.text-info', 0);

                $cont = $result->find('p', 0);

                // echo $cont->outertext;
                // die();

                $content_sg = trim($cont->outertext);

                $single_sg = $this->get_dom_single_page($url_sg);

                $single_sg = str_get_html($single_sg);


                $mco = $single_sg->find('.panel-body', 1);


                //ninh
                $contentN = $single_sg->find('.panel-body', 3);


                $ti = $single_sg->find('.lead', 0);

                if (!empty($mco)) {
                    $content_sg = trim($mco->plaintext);
                }

                preg_match_all('#<p class="text-info">(.*)</p>#msiU', $single_sg, $matches);

                if (isset($matches[0]) && !empty($matches[0])) {
                    $listc = trim($matches[0][2]);

                    preg_match_all('#<a (.*)>(.*)</a>#msiU', $listc, $mkj);

                    if (isset($mkj[2]) && !empty($mkj[2])) {
                        $area = $mkj[2][2];
                    }
                }
                //preg_match_all('/<span class="fa fa-address-card-o">(.*?)<\/span>/', $html, $matN);
                

                $result_arr[] = array(
                    'title' => trim($ti->plaintext),
                    'content' => $content_sg,
                    'area' => $area,
                    'attr' => $attr,
                    'content_ninh' => $contentN,
                    
                );
            }
        }

        array_pop($result_arr);

        return $result_arr;
    }

    




    public function get_data_nhadatvideo($url, $domain = null)
    {

       

			$str_html = $this->get_dom_single_page("https://nhadatvideo.vn/");
			$str_html = str_get_html($str_html);
			$content = $str_html->find('script[type=text/javascript]',0);
			$content = $content->find('text',0);
			$re_math = preg_match('/my_first_page =(.*)/',$content,$matchs);
			$main_content = $matchs[1];
			$main_content = json_decode($main_content,true);
			$main_content = $main_content['first_page'];
			$result['res'] = $main_content;

		

        //array_pop($result_arr);

        	return $result;
    }

    


}
?>